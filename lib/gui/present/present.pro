#-------------------------------------------------
#
# Project created by QtCreator 2019-04-30T21:35:37
#
#-------------------------------------------------

TARGET = present
TEMPLATE = lib

QT += widgets
QT += core
QT += gui
QT += printsupport

CONFIG -= app_bundle
CONFIG += staticlib
CONFIG += c++11

DEFINES += PRESENT_LIBRARY
DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
	gexportdialog.cpp \
	gimagesexporter.cpp \
	aexportable.cpp \
	reporteditor.cpp

HEADERS += \
	aexportable.h \
	gimagesexporter.h \
	gexportdialog.h \
	reporteditor.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    present.pri

RESOURCES += \
    icons.qrc

TRANSLATIONS += \
    present_cs.ts
