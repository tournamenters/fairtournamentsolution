#include "cresult.h"
#include <QString>
#include <QStringList>

const QString overTimeMark = "*";
const QString separatorMark = ":";

CResult::CResult()
{
    reset();
}

CResult::CResult(int hostScore, int guestScore)
{
    update(hostScore, guestScore);
}

CResult::CResult(const QString &string)
{
    update(string);
}

bool CResult::update(int hostScore, int guestScore)
{
    QString old = toString();
    _hostScore = hostScore;
    _guestScore = guestScore;
    _isSet = true;
    return old != toString();
}

bool CResult::update(const QString &string)
{
    QString old = toString();
    if(isResult(string)){
        QStringList list = string.split(separatorMark);
        list = overTimeCheck(list);
        return update(list.at(0).toInt(), list.at(1).toInt());
    }
    if(isOverTimeMark(string) && _isSet){
        _overTime = true;
        return old != toString();
    }
    reset();
    return old != toString();
}

void CResult::updateReversed(const QString &string)
{
    reset();
    if(isResult(string)){
        QStringList list = string.split(separatorMark);
        list = overTimeCheck(list);
        update(list.at(1).toInt(), list.at(0).toInt());
    }
}

int CResult::getGuest() const
{
    return _guestScore;
}

int CResult::getHost() const
{
    return _hostScore;
}

bool CResult::wasPlayed() const
{
    return _isSet;
}

bool CResult::isOverTime() const
{
    return _overTime;
}

void CResult::reset()
{
    update(0,0);
    _isSet = false;
    _overTime = false;
}

QString CResult::getOverTimeMark() const
{
    if(_overTime){
        return overTimeMark;
    }
    return "";
}

QStringList CResult::overTimeCheck(const QStringList& resultList)
{
    QStringList list = resultList;
    if(list.last().contains(overTimeMark)){
        _overTime = true;
        list.last() = list.last().remove(overTimeMark);
    }
    return list;
}

bool CResult::isOverTimeMark(const QString &text)
{
    return text == "* ot." || text == "* pp.";
}

QString CResult::toString() const
{
    if(_isSet){
        return QString::number(_hostScore) + " " + separatorMark + " " + QString::number(_guestScore) + getOverTimeMark();
    }
    return "";
}

QString CResult::toStringReversed() const
{
    if(_isSet){
        return QString::number(_guestScore) + " " + separatorMark + " " + QString::number(_hostScore) + getOverTimeMark();
    }
    return "";
}

QList<int> CResult::toIntList() const
{
    QList<int> list;
    if(_isSet){
        list.push_back(_hostScore);
        list.push_back(_guestScore);
        if(_overTime){
            list.push_back(_overTime);
        }
    }
    return list;
}

bool CResult::equals(const CResult &result) const
{
    return (_isSet && result._isSet && _hostScore == result._hostScore && _guestScore == result._guestScore) || (!_isSet && !result._isSet);
}

bool CResult::equalsReverse(const CResult &result) const
{
    return (_isSet && result._isSet && _hostScore == result._guestScore && _guestScore == result._hostScore) || (!_isSet && !result._isSet);
}

bool CResult::wonHost() const
{
    return _hostScore>_guestScore;
}

bool CResult::wonGuest() const
{
    return _guestScore>_hostScore;
}

bool CResult::wasDraw() const
{
    return _isSet && _guestScore == _hostScore;
}

bool CResult::isResult(const QString &string)
{
    QStringList list;
    bool firstOk = false;
    bool secondOk = false;
    if(string.contains(separatorMark)){
        list = string.split(separatorMark);
        if(list.last().contains(overTimeMark)){
            list.last() = list.last().remove(overTimeMark);
        }
        if(list.size() == 2){
            list.at(0).toInt(&firstOk);
            list.at(1).toInt(&secondOk);
        }
    }
    return firstOk && secondOk;
}
