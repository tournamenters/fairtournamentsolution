#include "grobinroundview.h"
#include "astagemodel.h"

#include "ggrouptable.h"
#include "gtournamenttable.h"

#include <QPushButton>
#include <iostream>
#include <QMap>
#include <QVariant>
#include <QSplitter>
#include <QDialog>
#include <QLabel>
#include <QMessageBox>

GRobinRoundView::GRobinRoundView(AStageModel *model, QWidget *parent)
    : AStageView(parent)
{
    _model = model;
    _identificator = model->getIdentificator();
    _title = _model->getName();

    createMainTable();
    createMatchesTable();

    update();
    connect(model, SIGNAL(dataChanged()),
            this, SLOT(update()));
}

GRobinRoundView::~GRobinRoundView()
{

}

QWidget *GRobinRoundView::getViewWidget()
{
    if(!splitter){
        splitter = new QSplitter();
        addEmptySpace(splitter, 2);
        addWidget(splitter,mainTable,1);
        addEmptySpace(splitter,1);
        addWidget(splitter,matchesTable,1);
        addEmptySpace(splitter,8);
    }
    return splitter;
}

AExportable *GRobinRoundView::getExportableWidget()
{
    return mainTable;
}

void GRobinRoundView::findText(const QString &text)
{
    mainTable->findText(text);
    matchesTable->findText(text);
}

void GRobinRoundView::update()
{
    blockSignals(true);
    QVariant data = _model->getData();
    mainTable->setData(data.toMap()["table"]);

    matchesTable->setData(data.toMap()["matches_list"]);

    matchesTable->setMinimumWidth(matchesTable->getRealWidth());
    requestData = data.toMap()["mini_table"];
    blockSignals(false);
}

void GRobinRoundView::uniqueEditationRequest(const QString &header, const QString &value)
{
    GGroupEditationDialog dialog(requestData, header);
    if(dialog.exec() == QDialog::Accepted){
        QVariantMap data;
        data["entered_values"] = dialog.getEnteredValues();
        data["header"] = header;
        data["value"] = value;
        emit stateEdited(ExecType::Order, _identificator, data);
    }
}

void GRobinRoundView::createMainTable()
{
    mainTable = new GGroupTable(_model->teamsCount(), _identificator);
    mainTable->setTitle(_title);
    connectTeamsTable(mainTable);
    connectMatchesTable(mainTable);
    connect(mainTable, SIGNAL(uniqueEditationRequested(QString, QString)),
            this, SLOT(uniqueEditationRequest(QString, QString)));
}

void GRobinRoundView::createMatchesTable()
{
    matchesTable = new GMatchesTable(nullptr,2,0);
    matchesTable->setMaximumHeight(mainTable->height());
    matchesTable->setHorizontalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
    matchesTable->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
}

void GRobinRoundView::setHighlightColor(const QColor& color)
{
    mainTable->setHighlightColor(color);
}

AStageView *GRobinRoundView::create(AStageModel *model)
{
    return new GRobinRoundView(model);
}
