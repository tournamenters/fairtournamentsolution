#ifndef GFIELDSPAGE_H
#define GFIELDSPAGE_H

#include <QWizardPage>
#include <QVariant>

class QLabel;
class QGroupBox;
class QComboBox;
class QSpinBox;
class QTableWidget;

class GFieldsSettingPage : public QWizardPage
{
    Q_OBJECT
    Q_PROPERTY(bool _doFields READ doFields WRITE setDoFields NOTIFY fieldsCheckChanged)

public:
    GFieldsSettingPage(QWidget *parent = nullptr);

    bool doFields() const;
    void setDoFields(bool fields);

signals:
    void fieldsCheckChanged();

protected:
    void initializePage() override;
    int nextId() const override;

private slots:
    void fieldsCheckChange();

private:
    QLabel *createTitle();
    QGroupBox *createMainBox();
    QLayout * createMainBoxLayout();

    QGroupBox * boxFields;
    QSpinBox * sbNumFields;
    QComboBox * cbAlgorithmCreation;

    bool _doFields;

};
#endif // GFIELDSPAGE_H
