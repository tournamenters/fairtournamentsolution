include($$PWD/../../controller/controller.pri)
include($$PWD/../view/view.pri)
include($$PWD/../tournamentwidgets/tournamentwidgets.pri)
include($$PWD/../present/present.pri)
include($$PWD/../creation/creation.pri)

win32:CONFIG(release, debug|release): LIBS += -L$$TOP_BUILDDIR/lib/gui/mainwindow/release/ -lmainwindow
else:win32:CONFIG(debug, debug|release): LIBS += -L$$TOP_BUILDDIR/lib/gui/mainwindow/debug/ -lmainwindow
else:unix: LIBS += -L$$TOP_BUILDDIR/lib/gui/mainwindow/ -lmainwindow

INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/gui/mainwindow/release/libmainwindow.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/gui/mainwindow/debug/libmainwindow.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/gui/mainwindow/release/mainwindow.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/gui/mainwindow/debug/mainwindow.lib
else:unix: PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/gui/mainwindow/libmainwindow.a
