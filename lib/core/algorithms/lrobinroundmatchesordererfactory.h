#ifndef LGROUPMATCHESDRAWERFACTORY_H
#define LGROUPMATCHESDRAWERFACTORY_H
#include <QObject>
#include <QMap>

#include "arobinroundmatchesorderer.h"

typedef AMatchesOrderer* (__stdcall * FcnCreateMatchesDrawer)(void);

class LMatchesOrdererFactory: public QObject
{
    Q_OBJECT

public:
    ~LMatchesOrdererFactory();

    static LMatchesOrdererFactory *Instance()
    {
        static LMatchesOrdererFactory instance;
        return &instance;
    }

    LMatchesOrdererFactory(LMatchesOrdererFactory const& other) = delete;
    void operator=(LMatchesOrdererFactory const& other) = delete;

    QMap<QString, QString> getPossibleTypesKeyLabelMap();
    void registerOrdererType(const QString& type, const QString& label, FcnCreateMatchesDrawer create);
    AMatchesOrderer * createMatchesOrderer(const QString& type);

private:
    LMatchesOrdererFactory();
    QMap<QString, FcnCreateMatchesDrawer> _registeredTypes;
    QMap<QString, QString> _typeLabelMap;
};
#endif // LGROUPMATCHESDRAWERFACTORY_H
