#ifndef GPALETTEMANAGER_H
#define GPALETTEMANAGER_H

#include <QString>
#include <QStringList>
#include <QPalette>

class QPalette;

class GPaletteFactory
{
    GPaletteFactory(){}

public:
    enum Theme { Dark, Standard, Green };
    static QPalette create(const QString &themeString);
    static QPalette create(GPaletteFactory::Theme theme);
    static QStringList keys();

private:
    static QPalette getDarkPalette();
    static QPalette getGreenPalette();

};



#endif // GPALETTEMANAGER_H
