#include "lrobinroundmatchesordererfactory.h"
#include "arobinroundmatchesorderer.h"

LMatchesOrdererFactory::LMatchesOrdererFactory()
{
    registerOrdererType("berger", tr("Bergers tables"), LBergerTables::create);
    registerOrdererType("kirkman", tr("Kirkmans algorithm"), LKirkman::create);
    registerOrdererType("rotation", tr("Simple rotation"), LRotation::create);
}

LMatchesOrdererFactory::~LMatchesOrdererFactory()
{
    _registeredTypes.clear();
    _typeLabelMap.clear();
}

QMap<QString, QString> LMatchesOrdererFactory::getPossibleTypesKeyLabelMap()
{
    return _typeLabelMap;
}

void LMatchesOrdererFactory::registerOrdererType(const QString& type, const QString& label, FcnCreateMatchesDrawer create)
{
    _registeredTypes[type] = create;
    _typeLabelMap[type] = label;
}

AMatchesOrderer *LMatchesOrdererFactory::createMatchesOrderer(const QString& type)
{
    if(_registeredTypes.contains(type)){
        return _registeredTypes[type]();
    }
    return nullptr;
}
