#-------------------------------------------------
#
# Project created by QtCreator 2019-04-30T20:49:10
#
#-------------------------------------------------

TARGET = FairTournament
TEMPLATE = app

QT += widgets
QT += core
QT += xml
QT += gui
QT += printsupport

CONFIG += c++11 windows

include($$PWD/../lib/lib.pri)

SOURCES += \
	main.cpp \
	appsetter.cpp

HEADERS += \
	appsetter.h

DEFINES += QT_DEPRECATED_WARNINGS



unix {
    target.path = /usr/lib
    INSTALLS += target
}


TRANSLATIONS += \
    app_cs.ts

RC_FILE = myapp.rc

DISTFILES += \
    myapp.rc

RESOURCES += \
	splash.qrc

