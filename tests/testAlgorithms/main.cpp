#include "tst_groupscreation.h"
#include "tst_robinroundmatchesorder.h"
#include "tst_playoffcreation.h"
#include "tst_flieldsplanner.h"
#include <gtest/gtest.h>

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
