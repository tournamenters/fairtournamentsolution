#ifndef GOPENPAGE_H
#define GOPENPAGE_H

#include <QWidget>
#include <QList>

class QAction;
class QToolButton;
class QScrollArea;
class QGridLayout;
class QVBoxLayout;
class QHBoxLayout;

class GOpenPage: public QWidget
{
    Q_OBJECT

public:
    GOpenPage(const QString& title, const QString& subTitle, QWidget* parent = nullptr);
    void setFilesTypeLabel(const QString& filesTypeAlias, const QString& filesTypeIconPath = "");
    void showTitlePart(const QString& title, const QString& subTitle);
    void showHelpPart(QAction* actHelp, const QString& text);
    void showButtonsPart(const QList<QAction*>& actions);
    void showOpenRecentFilesPart(const QList<QAction*>& openRecentActions);

private:
    void createMainLayout();
    QWidget* createOpenRecentButtonsWidget(const QList<QAction*>& openRecentActions);
    QToolButton* createActionButton(QAction* action, float fontRatio = 1, const QString& appendText = "");
    QToolButton* createRecentButton(QAction* action);
    QString createRecentFileButtonText(const QString& actionText);
    QVBoxLayout * createRowLayout(const QList<QLayout*>& layouts, const QList<int>& stretches);
    void setWidgetsFontSize(QWidget* widget, float ratio, bool bold = false);

    QHBoxLayout* buttonsLayout;
    QHBoxLayout* recentLayout;
    QVBoxLayout* titleLayout;
    QVBoxLayout* helpLayout;

    const float _titleFontSizeRatio;
    const float _buttonsFontSizeRatio;
    const float _recentFileFontRatio;

    QString _filesTypeAlias;
    QString _filesTypeIconPath;
};

#endif // GOPENPAGE_H
