#ifndef GNEWDIALOGPAGES_H
#define GNEWDIALOGPAGES_H














class GAutoGenerationSettingPage : public QWizardPage
{
    Q_OBJECT

public:
    GAutoGenerationSettingPage(QWidget *parent = 0);

protected:
    void initializePage() override;
    int nextId() const override;

private:
    QGroupBox * boxParams;
    QSpinBox * sbNumFields;
    QComboBox * cbAlgorithmCreation;

};




#endif // GNEWDIALOGPAGES_H
