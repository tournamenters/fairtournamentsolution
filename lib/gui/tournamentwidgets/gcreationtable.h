#ifndef GCREATIONTABLE_H
#define GCREATIONTABLE_H

#include "gtournamenttable.h"

class GCreationTable : public GTournamentTable
{
    Q_OBJECT

public:
    GCreationTable(QWidget *parent = nullptr);

    void setData(const QVariant& data) override;
    void setMinRows(int minRows);
    void setUnremovableColumns(const QStringList &unremovableColumns);
    void setNumericColumns(const QStringList &numericColumns);
    int findColumnIndex(const QString& header) const;
    int nonEmptyCellsCount(const QString& header) const;

signals:
    void columnsChanged();
    void tableChanged();

public slots:
    void setVisibleColumn(bool visible, const QString& header);

    void addTableColumn();
    void removeTableRow();
    void removeTableColumn();
    void addTableRowAbove();
    void addTableRowBelow();

    void cellEditationCare(int row, int col);

private slots:
    void showMenu() override;

protected:
    QMenu* createFormContextMenu();

private:
    bool isColumnEmpty(int col);
    void insertRowNearSelected(bool under);
    bool isOneRowSelected();
    bool isOneColumnSelected();
    void numericControl(int row, int col);

    QStringList _unremovableColumns;
    QStringList _numericColumns;
    int _minRows;
};


#endif // GCREATIONTABLE_H
