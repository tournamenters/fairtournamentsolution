#include "lgroupsdrawerfactory.h"

LGroupsDrawerFactory::LGroupsDrawerFactory()
{
    registerOrdererType("advantageous", tr("Advantegeous"), LGroupsDrawerAdvantageous::create);
    registerOrdererType("equal", tr("Equal"), LGroupsDrawerEqual::create);
    registerOrdererType("modified", tr("Modified Equal"), LGroupsDrawerModified::create);
    registerOrdererType("random", tr("Random"), LGroupsDrawerRandom::create);
    registerOrdererType("baskets", tr("Baskets"), LGroupsDrawerBaskets::create);

}

LGroupsDrawerFactory::~LGroupsDrawerFactory()
{
    _registeredTypes.clear();
    _typeLabelMap.clear();
}

QMap<QString, QString> LGroupsDrawerFactory::getPossibleTypesKeyLabelMap()
{
    return _typeLabelMap;
}

void LGroupsDrawerFactory::registerOrdererType(const QString& type, const QString& label, FcnCreateGroupsDrawer create)
{
    _registeredTypes[type] = create;
    _typeLabelMap[type] = label;
}

AGroupsDrawer *LGroupsDrawerFactory::createDrawer(const QString& type)
{
    if(_registeredTypes.contains(type)){
        return _registeredTypes[type]();
    }
    return nullptr;
}
