#ifndef ASSISTANT_H
#define ASSISTANT_H
//#-------------------------------------------------
//#
//# Based on an example from the documentation available on https://doc.qt.io/qt-5/qtassistant-remotecontrol-example.html
//#
//#-------------------------------------------------
#include <QCoreApplication>
#include <QString>

class QProcess;

class Assistant
{
    Q_DECLARE_TR_FUNCTIONS(Assistant)

public:
    Assistant();
    ~Assistant();
    void showDocumentation(const QString &file);

private:
    bool startAssistant();
    QProcess *process;
};

#endif // ASSISTANT_H
