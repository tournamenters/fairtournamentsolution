#ifndef GPLAYOFF_H
#define GPLAYOFF_H

#include "aview.h"
#include "gteamedit.h"
#include "gresultedit.h"

class GPlayOffWidget;


class GPlayOffView : public AStageView
{
    Q_OBJECT

public:
    GPlayOffView(AStageModel* model, QWidget *parent = nullptr);
    ~GPlayOffView() override;

    static AStageView * __stdcall create(AStageModel* model);

    QWidget * getViewWidget() override;
    AExportable * getExportableWidget() override;

public slots:
    void findText(const QString& text) override;
    void update() override;

protected:
    void connectPlayOffWidget(GPlayOffWidget * widget);

    GPlayOffWidget * _playOffWidget;

};

#endif // GPLAYOFF_H
