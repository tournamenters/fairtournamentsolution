#include "serializationcontroller.h"
#include "serialization.h"

#include <QTimer>
#include <QStatusBar>
#include <QSettings>
#include <QFileInfo>
#include <QApplication>

const QString examplePathPlayOff = "/Examples/playoff.turn";
const QString examplePathRobinRound = "/Examples/robinround.turn";
const QString examplePathGroups = "/Examples/groups-and-playoff.turn";
const QString examplePathComplex = "/Examples/complex.turn";

SerializationController::SerializationController(ASerializer *serializer, ASerializable *model, QObject *parent)
   :QObject(parent)
{
    readSetting();

    _model = model;
    _serializer = serializer;

    setFileName("");
    setModification(false);
    updateRecentList();

    _suffix = "turn";
}

SerializationController::~SerializationController()
{
    writeSetting();
    _recentList.clear();
}

void SerializationController::save()
{
    if(_fileName.isEmpty()){
        emit saveAsRequested();
        return;
    }
    _serializer->save(_model, _fileName, "tournament");
    emit statusBarChangeRequested(tr("Tournament: ") + _fileName + tr(" has been saved!"));
    setModification(false);
    updateRecentList();
}

void SerializationController::saveAs(const QString& fileName)
{
    if(!fileName.isEmpty()){
        setFileName(fileName);
        save();
    }
}

void SerializationController::open(const QString &fileName)
{
    _serializer->load(_model, fileName);
    emit statusBarChangeRequested(tr("Tournament: ") + fileName + tr(" has been opened!"));
    setFileName(fileName);
    setModification(false);
}

void SerializationController::close()
{
    emit statusBarChangeRequested(tr("Tournament: ") + _fileName + tr(" has been closed!"));
    setModification(false);
    setFileName("");
}

void SerializationController::clearRecentList()
{
    _recentList.clear();
    emit recentListChanged(_recentList);
    writeSetting();
}

void SerializationController::autoSave()
{
    if(_modified){
        _serializer->save(_model, generateAutoSaveName(), "tournament");
    }
}

void SerializationController::updateRecentList()
{
    if(QFile::exists(_fileName)){
        _recentList.removeAll(_fileName);
        _recentList.push_front(_fileName);
    }
    emit recentListChanged(_recentList);
    writeSetting();
}

void SerializationController::setFileName(const QString &fileName)
{
    _fileName = fileName;
    QString shownName;
    if (fileName.isEmpty()){
        shownName = tr("untitled") + "." + _suffix ;
    }
    else{
        shownName = QFileInfo(fileName).fileName();
    }
    emit currentFileChanged(shownName);
    updateRecentList();
}

QList<QString> SerializationController::getRecentList() const
{
    return _recentList;
}

void SerializationController::readSetting()
{
    _recentList.clear();

    QSettings settings;
    settings.beginGroup("serialization");   
    if(settings.contains("recent_files")){
        QStringList loadedFiles = settings.value("recent_files").toStringList();
        for(const auto& fileName : loadedFiles){
            if(QFile::exists(fileName)){
                _recentList.push_back(fileName);
            }
        }
    }
    else{
        if(QFile::exists(QApplication::applicationDirPath() + examplePathPlayOff)){
            _recentList.push_back(QApplication::applicationDirPath() + examplePathPlayOff);
        }
        if(QFile::exists(QApplication::applicationDirPath() + examplePathRobinRound)){
            _recentList.push_back(QApplication::applicationDirPath() + examplePathRobinRound);
        }
        if(QFile::exists(QApplication::applicationDirPath() + examplePathGroups)){
            _recentList.push_back(QApplication::applicationDirPath() + examplePathGroups);
        }
        if(QFile::exists(QApplication::applicationDirPath() + examplePathComplex)){
            _recentList.push_back(QApplication::applicationDirPath() + examplePathComplex);
        }
    }
    settings.endGroup();
}

void SerializationController::writeSetting()
{
    QSettings settings;
    settings.beginGroup("serialization");
    settings.setValue("recent_files",_recentList);
    settings.endGroup();
}

QString SerializationController::generateAutoSaveName()
{
    QString part = _fileName;
    if(part.isEmpty()){
        part = tr("untitled") + "." + _suffix;
    }
    _autoSaveName = QApplication::applicationDirPath() + "/auto-saves/auto-save-" + QFileInfo(part).fileName();
    return _autoSaveName;
}

void SerializationController::setModification(bool modified)
{
    _modified = modified;
    emit modificationChanged(modified);
}
