#ifndef GTEXTEDIT_H
#define GTEXTEDIT_H

#include <QMainWindow>
#include <QTextEdit>
#include <QPrintPreviewDialog>
#include <QPointer>

class QAction;
class QActionGroup;
class QMenu;

class QTextCharFormat;
class QTextEditWithMargins;

class AExportable;
class GExportDialog;

class QPrinter;
class QPrintPreview;
class QPrintPreviewWidget;

class ReportEditor : public QMainWindow
{
    Q_OBJECT

public:
    ReportEditor(QWidget *parent = nullptr);
    ~ReportEditor() override;

    void addExportable(AExportable* exportable);
    void setExportables(QList<AExportable*> exportables);
    void clearExportables();
    void newReport(const QString& title = "");

public slots:
    void previewChanged();
    bool exportReport();
    bool exportReportAs();
    void printReport();

protected:
    void closeEvent(QCloseEvent *event) override;

private slots:
    void newReportSlot();

    void changeTextAlign(QAction *action);
    void changeFont();
    void changeOrientation(QAction * action);

    void documentChanged();
    void cursorPositionChanged();
    void alignmentChanged(Qt::Alignment alignment);
    void clipboardDataChanged();

    void renderReportForPreview(QPrinter * printer);

    void insertNewTable();
    void insertTournamentData();
    void insertChosenExportables(GExportDialog &dialog, bool doPageBreaks = true);

private:
    bool maybeExport();

    void createCentralWidget();
    void createPrinter();
    void createPreview();

    void makeFileMenu();
    void makeEditMenu();
    void makeFormatMenu();
    void makePreviewMenu();
    void makeAlignActons(QMenu *menu);
    void makeClipboardActions(QMenu *menu);
    void makeUndoActions(QMenu* menu);
    void makeInsertActions(QMenu* menu);
    void makeOrientationActions(QMenu* menu);
    void makeMainToolBar();
    void setupAlignAction(QAction* action, const QKeySequence &sequence);

    void insertPageBreak();
    void insertTitle(const QString &title);
    void insertSmallTitle(const QString &title);
    void insertExportable(AExportable *widget);
    void insetImage(const QImage &image, const QString &fileName);
    void insertHtml(const QString& html);
    QIcon rotateIcon(const QString &iconPath);
    QString generatePictureName();

    QString _reportName;
    int _picturesCount;
    QList<AExportable*> _exportables;

    QPrinter * printer;
    QDockWidget * dwPreview;
    QPrintPreview * printPreviewWindow;
    QPrintPreviewWidget *printPreviewWidget;

    QAction *actNew;
    QAction *actExport;
    QAction *actExportAs;
    QAction *actPrint;
    QAction *actExit;

    QAction *actFont;

    QActionGroup *alignGroup;
    QAction *actAlignLeft;
    QAction *actAlignCenter;
    QAction *actAlignRight;
    QAction *actAlignJustify;

    QActionGroup *orientationGroup;
    QAction *actPortrait;
    QAction *actLandscape;

    QAction *actUndo;
    QAction *actRedo;

    QAction *actCut;
    QAction *actCopy;
    QAction *actPaste;

    QAction* actInsertTournamentData;
    QAction *actInsertTable;

    QPointer<QAction> actRefreshPreview;

    QToolBar *tbMain;
    QTextEditWithMargins *teReport;

};

class QTextEditWithMargins : public QTextEdit
{

    qreal _leftMargin = 0;
    qreal _topMargin = 0;

public:

    virtual void resizeEvent(QResizeEvent *event) override
    {
        setViewportMargins(qRound(leftMargin()), qRound(topMargin()), qRound(leftMargin()), qRound(topMargin()));
        QTextEdit::resizeEvent(event);
    }
    void setMargins(qreal left, qreal top)
    {
        _leftMargin = left;
        _topMargin = top;
    }
    qreal leftMargin() const
    {
        return _leftMargin;
    }
    qreal topMargin() const
    {
        return _topMargin;
    }
};

class QPrintPreview : public QPrintPreviewDialog
{
    Q_OBJECT

public:
    QPrintPreview(QPrinter * printer):QPrintPreviewDialog (printer){}
    virtual void done(int) override{}
};

#endif // GTEXTEDIT_H
