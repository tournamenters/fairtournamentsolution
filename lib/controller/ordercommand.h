#ifndef GTABLEORDERCOMMAND_H
#define GTABLEORDERCOMMAND_H
#include <QUndoCommand>
#include <QVariant>

class AStageModel;
class QVariant;

class OrderCommand: public QUndoCommand
{

public:
    OrderCommand(AStageModel * model, const QVariant& identificator, const QVariant& data, QUndoCommand *parent = nullptr);

    void undo() override;
    void redo() override;

private:
    AStageModel * _model;
    QVariant _identificator;
    QVariant _data;
};



#endif // GTABLEORDERCOMMAND_H
