#ifndef CGROUP_H
#define CGROUP_H
#include "astagemodel.h"

class LTeamsSorter;
class AMatchesOrderer;


class CRobinRound: public AStageModel
{
    Q_OBJECT

public:
    CRobinRound(int id, const QString& name, LTeamsSorter* sorter, AMatchesOrderer * drawer, QObject* parent = nullptr);
    ~CRobinRound() override;

    void setTeamsCount(int numTeams) override;
    void setTeams(const QList<CTeam*>& teams) override;
    void setTeams(CTeamsList* teams) override;
    void setTeamsFromSources(const QList<CTeamsList*>& teams) override;
    void setTeamsFromVariant(const QVariant& variant, const QMap<int,CTeam*>& teamsById) override;

    void setNextStage(AStageModel* nextStage) override;

    CTeamsList * countStandings() override;

    CTeam* getTeam(const QVariant& identificator) const override;
    QVariant getData() const override;

    void resultsFromVariant(const QVariant& variant) override;
    QVariant toVariant() const override;

    void changeTeam(const QVariant& identificator, CTeam* team) override;
    void nextStageCare() override;

public slots:
    bool setResult(const QVariant& identificator, const QString& result) override;
    bool execEdit(ExecType type, const QVariant& identificator, const QVariant& data) override;

private slots:
    //void teamChange();
    void recountOrder();

protected:
    bool swapTeams(const QVariantMap &data) override;
    bool setOrder(const QVariantMap &data);
    void setResult(int row, int col, const QString& result);

private:
    void createMatches(int numTeams);
    void updateMatches(const QList<QList<int>>& matches);

    void placeTeamsToMatches(CTeamsList* teams);
    int sameRank() const;
    void setOrder(int oldRank, const QList<int> &newOrder);
    void resetOrder(int numTeams);
    void resetMatchesMatrix();

    CTeam* findTeam(const QVariantMap& identificator) const;
    QPair<int,int> findNextMatch() const;
    bool isMatch(int row, int col) const;

    QVariant createTableVariant() const;
    QVariant getMiniTable(int order) const;

    QStringList createTableHeaders() const;
    QVariantList createTableRows() const;
    QVariantList createTableGreys() const;
    QList<int> getTeamsIndexes() const;
    QList<CTeam*> getTeamsAtOrder(int order) const;
    int getRealTeamIndex(int rankInGroup);

    QStringList toStringList(const QList<int>& list) const;
    QVariantList toVariantList(const QList<int>& list) const;
    QVariantList toVariantList(const QList<QPair<int,int>>& list) const;

    LTeamsSorter* _sorter;
    AMatchesOrderer * _planner;
    QList<int> _order;
    QList<QList<CMatch*>> _matchesMatrix;

};

#endif // CGROUP_H
