#ifndef GSTARTLISTPAGE_H
#define GSTARTLISTPAGE_H

#include <QWizardPage>
#include <QMap>
#include <QVariant>

class QLabel;
class QLineEdit;
class QCheckBox;
class QGroupBox;
class QRadioButton;
class QTableWidget;
class QComboBox;
class QTabWidget;
class QToolBar;
class QAction;
class GTabStagesEditor;
class QSpinBox;
class QFormLayout;
class QVBoxLayout;

class GCreationTable;


class GStartListInsertingPage : public QWizardPage
{
    Q_OBJECT
    Q_PROPERTY(bool _seedTeams READ isSeed WRITE setSeed NOTIFY seedTeamChanged)
    Q_PROPERTY(int _numTeams READ getNumTeams WRITE setNumTeams NOTIFY numTeamsChanged)
    Q_PROPERTY(QVariant _startListData READ getData WRITE setData NOTIFY teamsDataChanged)

public:
    GStartListInsertingPage(QWidget *parent = nullptr);
    ~GStartListInsertingPage() override;

    int nextId() const override;
    bool isComplete() const override;

    int getNumTeams() const;
    void setNumTeams(int numTeams);
    bool isSeed() const;
    void setSeed(bool seedTeams);
    QVariant getData();
    void setData(const QVariant &data);

signals:
    void teamsDataChanged();
    void seedTeamChanged();
    void numTeamsChanged();

private slots:
    void importStartList();
    void generateStartList();

    void tableDataChanged();
    void tableColumnsChanged();

    void checkClubs(int check);
    void checkSeed(bool check);

private:
    bool isInt(const QString& text) const;
    QVariant modifieData(const QVariant& data);

    QLayout* createMainLayout();
    QLabel * createTitle();
    QLayout * createButtonsLayout();
    QTableWidget * createStartListTable();
    QPushButton * createImportButton();
    QPushButton * createRandomButton();
    QGroupBox * createSeedBox();
    QLayout * createSeedBoxLayout();
    QCheckBox *createClubsCheckBox();
    QPushButton *createClearButton();
    QPushButton *createNewColumnButton();

    QVariant parseTextFileLine(QString &line);
    QStringList readLineFromFile(QString &line);

    void initializeHeaders();
    void showRowsInTable(const QVariant& rows);

    GCreationTable* tableStartList;

    QGroupBox* boxSeed;
    QCheckBox* boxClubs;
    QRadioButton* radCoefficient;
    QRadioButton* radRankings;

    QString _nameColumnHeader;
    QString _seedColumnHeader;
    QString _clubColumnHeader;

    int _numTeams;
    int _minRows;
    bool _clubs;
    bool _seedTeams;
    QVariant _startListData;

};



#endif // GSTARTLISTPAGE_H
