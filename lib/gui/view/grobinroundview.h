#ifndef GGROUPVIEW_H
#define GGROUPVIEW_H

#include "aview.h"
#include <QPointer>

class QVariant;
class QSplitter;
class AStageModel;
class GGroupTable;
class GTournamentTable;

class GRobinRoundView : public AStageView
{
    Q_OBJECT

public:
    GRobinRoundView(AStageModel* model, QWidget *parent = nullptr);
    ~GRobinRoundView() override;

    static AStageView * __stdcall create(AStageModel* model);

    QWidget * getViewWidget() override;
    AExportable * getExportableWidget() override;

    void setHighlightColor(const QColor &color);

public slots:
    void findText(const QString& text) override;
    void update() override;

protected slots:
    void uniqueEditationRequest(const QString& header, const QString& value);

private:
    void createMainTable();
    void createMatchesTable();

    GGroupTable* mainTable;
    GTournamentTable* matchesTable;
    QVariant requestData;
    QPointer<QSplitter> splitter;

};

#endif // GGROUPVIEW_H
