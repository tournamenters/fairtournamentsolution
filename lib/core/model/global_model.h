#ifndef CMODELSGLOBALCONSTANTS_H
#define CMODELSGLOBALCONSTANTS_H

#include <QObject>
#include <QString>
#include <QMap>

#include "lrobinroundmatchesordererfactory.h"
#include "lgroupsdrawerfactory.h"
#include "lteamssorterfactory.h"
#include "lplayoffdrawerfactory.h"
#include "lfieldsassignerfactory.h"

const QString key_GROUPS_STAGE = "groups_stage";
const QString key_ROBIN_ROUND = "robin_round";
const QString key_PLAY_OFF = "play_off";

const QString key_PARAMETERS = "params";
const QString key_SETTING = "setting";

const QString key_NUM_TEAMS = "num_teams";
const QString key_NUM_GROUPS = "num_groups";
const QString key_SIZE_GROUPS = "size_groups";
const QString key_NAME = "name";
const QString key_STAGE_TYPE = "stage_type";

enum class ExecType {Rename, Swap, Rewrite, Editation, Order};
enum class StageType {Groups, RobinRound, PlayOff};

class CModelConstants: QObject
{
    Q_OBJECT

public:
    static CModelConstants& Instance()
    {
        static CModelConstants instance;
        return instance;
    }
    CModelConstants(CModelConstants const&) = delete;
    void operator=(CModelConstants const&) = delete;

    QMap<QString, QString> getKeyTextStagesMap() const
    {
        return keyTextStagesMap;
    }

    QMap<QString, QString> getKeyTextGroupsCreationMap() const
    {
        return keyTextGroupsCreationMap;
    }

    QMap<QString, QString> getKeyTextMatchesOrderMap() const
    {
        return keyTextMatchesOrderMap;
    }

    QMap<QString, QString> getKeyLabelOrdererCriteriaMap() const
    {
        return keyLabelOrdererCriteria;
    }

    QList<QString> getKeyImplicitCriteriaOrder() const
    {
        return keyImplicitCriteriaOrder;
    }

    QMap<QString, QString> getKeyTextPlayOffMap() const
    {
        return keyTextPlayOffMap;
    }

    QMap<QString, QString> getKeyTextFieldsMap() const
    {
        return keyTextFieldsMap;
    }

private:
    QMap<QString, QString> keyTextStagesMap;
    QMap<QString, QString> keyTextGroupsCreationMap;
    QMap<QString, QString> keyTextMatchesOrderMap;
    QMap<QString, QString> keyLabelOrdererCriteria;
    QMap<QString, QString> keyTextPlayOffMap;
    QMap<QString, QString> keyTextFieldsMap;
    QList<QString> keyImplicitCriteriaOrder;

    CModelConstants(){
        keyTextStagesMap[key_PLAY_OFF] = tr("Playoff");
        keyTextStagesMap[key_ROBIN_ROUND] = tr("Robin round");
        keyTextStagesMap[key_GROUPS_STAGE] = tr("Groups");
        keyTextGroupsCreationMap = LGroupsDrawerFactory::Instance()->getPossibleTypesKeyLabelMap();
        keyTextMatchesOrderMap = LMatchesOrdererFactory::Instance()->getPossibleTypesKeyLabelMap();
        keyTextPlayOffMap = LPlayOffDrawerFactory::Instance()->getPossibleTypesKeyLabelMap();
        keyTextFieldsMap = LFieldsAssignerFactory::Instance()->getPossibleTypesKeyLabelMap();
        keyLabelOrdererCriteria = LTeamsSorterFactory::Instance()->getPossibleCriteriumsKeyLabel();
        keyImplicitCriteriaOrder = LTeamsSorterFactory::Instance()->getImplicitCriteriumsOrder();
    }
    ~CModelConstants(){
        keyTextStagesMap.clear();
        keyTextMatchesOrderMap.clear();
        keyLabelOrdererCriteria.clear();
        keyTextGroupsCreationMap.clear();
        keyImplicitCriteriaOrder.clear();
    }

};

#endif // CMODELSGLOBALCONSTANTS_H
