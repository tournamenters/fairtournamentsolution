#ifndef AGROUPSDRAWER_H
#define AGROUPSDRAWER_H
#include <QList>

class AGroupsDrawer
{

public:
    AGroupsDrawer();
    virtual ~AGroupsDrawer();
    virtual QList<QList<int>>createGroups(int numGroups, int numTeams, int proceedPerGroup=2) = 0;

protected:
    QList<QList<int>> drawFirstRound(int numGroups);
    QList<QList<int>> drawNormalRound(const QList<QList<int> > &groups, int &nextToBePlaced, const int numTeams);
    QList<QList<int>> drawReverseRound(const QList<QList<int> > &groups, int &nextToBePlaced, const int numTeams);
    QList<QList<int>> drawRandomRound(const QList<QList<int> > &groups, int &nextToBePlaced, const int numTeams);
};

class LGroupsDrawerBaskets: public AGroupsDrawer
{

public:
    ~LGroupsDrawerBaskets() override;
    QList<QList<int>> createGroups(int numGroups, int numTeams, int proceedPerGroup=2) override;

    static AGroupsDrawer * __stdcall create();

private:
    LGroupsDrawerBaskets();
};

class LGroupsDrawerRandom: public AGroupsDrawer
{

public:
    ~LGroupsDrawerRandom() override;
    QList<QList<int>> createGroups(int numGroups, int numTeams, int proceedPerGroup=2) override;

    static AGroupsDrawer * __stdcall create();

private:
    LGroupsDrawerRandom();
};

class LGroupsDrawerAdvantageous: public AGroupsDrawer
{

public:
    ~LGroupsDrawerAdvantageous() override;
    QList<QList<int>> createGroups(int numGroups, int numTeams, int proceedPerGroup=2) override;

    static AGroupsDrawer * __stdcall create();

private:
    LGroupsDrawerAdvantageous();
};

class LGroupsDrawerEqual: public AGroupsDrawer
{

public:
    ~LGroupsDrawerEqual() override;
    QList<QList<int>> createGroups(int numGroups, int numTeams, int proceedPerGroup=2) override;

    static AGroupsDrawer * __stdcall create();

private:
    LGroupsDrawerEqual();
};

class LGroupsDrawerModified: public AGroupsDrawer
{

public:
    ~LGroupsDrawerModified() override;
    QList<QList<int>> createGroups(int numGroups, int numTeams, int proceedPerGroup=2) override;

    static AGroupsDrawer * __stdcall create();

private:
    LGroupsDrawerModified();
};

#endif // AGROUPSDRAWER_H
