#ifndef ASTAGEMODEL_H
#define ASTAGEMODEL_H
#include "amodel.h"
#include "cteamstable.h"
#include "global_model.h"

class QVariant;
class CMatch;
class CTeam;
class CTeamsList;

class AStageModel: public AMatchesModel
{
    Q_OBJECT

public:
    AStageModel(int id, StageType type, QObject *parent = nullptr);
    ~AStageModel() override;

    virtual void setTeamsCount(int numTeams);
    virtual void setTeams(const QList<CTeam*>& teams) = 0;
    virtual void setTeams(CTeamsList* teams) = 0;
    virtual void setTeamsFromSources(const QList<CTeamsList*>& teams) = 0;
    virtual void setTeamsFromVariant(const QVariant& variant, const QMap<int, CTeam*>& teamsById) = 0;

    virtual void setNextStage(AStageModel* nextStage) = 0;

    virtual CTeamsList* countStandings() = 0;

    int getId() const;
    QString getType() const;
    virtual CTeam* getTeam(const QVariant& identificator) const = 0;
    virtual QList<AStageModel*> getSubStages() const;
    virtual bool hasSubStages() const;

    virtual int teamsCount() const;
    virtual bool finished() const;
    bool isFinal() const;

    virtual void resultsFromVariant(const QVariant& variant) = 0;
    virtual QVariant toVariant() const = 0;

    virtual void changeTeam(const QVariant& identificator, CTeam* team) = 0;
    virtual void nextStageCare() = 0;

protected:
    virtual bool swapTeams(const QVariantMap& data) = 0;

    bool isChange(int numTeams);
    bool isChange(const QList<int>& numBySources);

    void prepareTeamsTable();

    AStageModel* _nextStage;
    CTeamsList* _teams;

private:
    void createIdentificator();

    int _uniqueId;
    StageType _type;
    QList<int> _lastParams;

};



#endif // ASTAGEMODEL_H
