#include "gtournamenttable.h"

#include <QAction>
#include <QMenu>
#include <QFileDialog>
#include <QMessageBox>
#include <QPainter>
#include <QImage>
#include <QTableWidgetItem>
#include <QApplication>
#include <QLineEdit>
#include <QApplication>
#include <QClipboard>
#include <QTextStream>
#include <QMimeData>
#include <QHeaderView>
#include <QScrollBar>

#include "gdragableedit.h"

GTournamentTable::GTournamentTable(QWidget *parent)
    :QTableWidget(parent)
{
    _editable = false;
    setRecomendedFormat(Format::HTML);
    setSelectionBehavior(QAbstractItemView::SelectItems);
    setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, SIGNAL(customContextMenuRequested(const QPoint&)),
            this, SLOT(showMenu()));
}

GTournamentTable::~GTournamentTable()
{

}

void GTournamentTable::setData(const QVariant &data)
{
    QVariantMap map = data.toMap();
    QStringList keys = map["keys"].toStringList();
    QVariantList rows = map["rows"].toList();

    adjustSize(rows.size(), keys.size());
    setColumnHeaders(keys);

    for(int row = 0; row < rows.size(); row++){
        QVariantMap rowData = rows[row].toMap();
        for(int column = 0; column<keys.size(); column++){
            if(rowData.contains(keys[column])){
                setCellText(row,column,rowData[keys[column]].toString());
            }
        }
    }
}

QVariant GTournamentTable::getData()
{
    QVariantList rows;
    QStringList keys = getColumnHeaders();
    for(int row = 0; row<rowCount(); row++){
        QVariantMap rowData;
        for(int col = 0; col<columnCount(); col++){
            rowData[keys[col]] = getCellText(row, col);
        }
        rows.push_back(rowData);
    }
    QVariantMap data;
    data["keys"] = keys;
    data["rows"] = rows;
    return data;
}

QList<QStringList> GTournamentTable::getDataLists()
{
    QList<QStringList> rows;
    rows.push_back(getColumnHeaders());
    for(int row = 0; row<rowCount(); row++){
        QStringList rowData;
        for(int col = 0; col<columnCount(); col++){
            rowData.push_back(getCellText(row, col));
        }
        rows.push_back(rowData);
    }
    return rows;
}

void GTournamentTable::setRowTextColor(int row, const QColor &color)
{
    for(int col = 0;col<columnCount();col++){
        if(item(row,col)){
            item(row,col)->setTextColor(color);
        }
        QLineEdit* teamEdit = qobject_cast<QLineEdit*>(cellWidget(row,col));
        if(teamEdit){
            QPalette palette = teamEdit->palette();
            palette.setColor(QPalette::Text, color);
            teamEdit->setPalette(palette);
        }

    }
    if(verticalHeaderItem(row)){
        verticalHeaderItem(row)->setTextColor(color);
    }
}

void GTournamentTable::findText(const QString &text)
{
    blockSignals(true);
    for (int col=0;col<columnCount();col++){
        for (int row=0;row<rowCount();row++){
            if (!text.isEmpty() && getCellText(row,col).toLower().contains(text.toLower())){
                select(row,col);
            }
            else {
                deselect(row,col);
            }
        }
    }
    blockSignals(false);
}

QStringList GTournamentTable::getColumnHeaders()
{
    QStringList headers;
    for(int col = 0; col<columnCount(); col++){
        if(horizontalHeaderItem(col)){
            headers.push_back(horizontalHeaderItem(col)->text());
        }
        else{
            headers.push_back(QString::number(col+1));
        }
    }
    return headers;
}

QStringList GTournamentTable::getColumnData(int col)
{
    QStringList list;
    for(int row = 0;row<rowCount(); row++){
        list.push_back(getCellText(row, col));
    }
    return list;
}

void GTournamentTable::setColumnHeaders(const QStringList &headers)
{
    for(int col = 0; col<columnCount()&&col<headers.size();col++){
        setHorizontalHeaderItem(col, new QTableWidgetItem(headers[col]));
    }
}

int GTournamentTable::getRealWidth()
{
    int width = 0;
    if(verticalHeader()){
        width += verticalHeader()->width();
    }
    for(int col = 0; col< columnCount();col++){
        width += columnWidth(col);
    }
    if(verticalScrollBar()){
        width+=verticalScrollBar()->width()/2;
    }
    return width;
}

void GTournamentTable::clearTable()
{
    clearContents();
}

QString GTournamentTable::getCellText(int row, int col) const
{
    QLineEdit* teamEdit = qobject_cast<QLineEdit*>(cellWidget(row,col));
    if(teamEdit){
        if(!teamEdit->text().isEmpty()){
            return teamEdit->text();
        }
        return teamEdit->placeholderText();
    }
    if(item(row,col)){
        return item(row,col)->text();
    }
    return "";
}

int GTournamentTable::getCellAlign(int row, int col)
{
    QLineEdit* teamEdit = qobject_cast<QLineEdit*>(cellWidget(row,col));
    if(teamEdit){
        return teamEdit->alignment();
    }
    if(item(row,col)){
        return item(row,col)->textAlignment();
    }
    return Qt::AlignCenter;
}

QString GTournamentTable::getCellAlignKey(int row, int col)
{
    return qtAlignToHtml(getCellAlign(row, col));
}

void GTournamentTable::setCellText(int row, int col, const QString& text)
{
    if(qobject_cast<GDragableEdit*>(cellWidget(row,col))){
        qobject_cast<GDragableEdit*>(cellWidget(row,col))->setText(text);
        return;
    }
    QLineEdit* teamEdit = qobject_cast<QLineEdit*>(cellWidget(row,col));
    if(teamEdit){
        teamEdit->blockSignals(true);
        teamEdit->setText(text);
        teamEdit->blockSignals(false);
    }
    blockSignals(true);
    if(item(row,col)){
        item(row,col)->setText(text);
    }
    else if(!teamEdit){
        setItem(row, col, createItem(text));
    }
    blockSignals(false);
}

void GTournamentTable::setAllEditable(bool editable)
{
    for (int col=0;col<columnCount();col++){
        setColumnEditable(col, editable);
    }
}

void GTournamentTable::setColumnEditable(int col, bool editable)
{
    for (int row=0;row<rowCount();row++){
        setCellEditable(row, col, editable);
    }
}

void GTournamentTable::setCellEditable(int row, int col, bool editable)
{
    if(item(row,col)){
        if(editable){
            item(row,col)->setFlags((item(row,col)->flags() | Qt::ItemIsEditable));
            item(row,col)->setFlags((item(row,col)->flags() | Qt::ItemIsSelectable));
        }
        else{
            item(row,col)->setFlags((item(row,col)->flags() | Qt::ItemIsEditable)^ Qt::ItemIsEditable);
            item(row,col)->setFlags((item(row,col)->flags() | Qt::ItemIsSelectable)^ Qt::ItemIsSelectable);
        }
    }
    QLineEdit* teamEdit = qobject_cast<QLineEdit*>(cellWidget(row,col));
    if(teamEdit){
        teamEdit->setReadOnly(editable);
    }
}

void GTournamentTable::adjustSize(int rows, int cols)
{
    setRowCount(rows);
    setColumnCount(cols);
    for(int row = 0; row<rows;row++){
        for(int col = 0; col<cols; col++){
            if(!item(row,col)){
                setItem(row,col, createItem());
            }
        }
    }

}

QMenu *GTournamentTable::createContextMenu()
{
    QMenu * menu = new QMenu(tr("Export table"));
    QMenu * copyMenu = menu->addMenu(QIcon(":/icons/copy.ico"),
                                          tr("Copy table as..."));
    copyMenu->addAction(tr("Image"), this,
                        &GTournamentTable::copyPicture);
    copyMenu->addAction(tr("HTML table"), this,
                        &GTournamentTable::copyHtml);
    copyMenu->addAction(tr("CSV table"), this,
                        &GTournamentTable::copyExcel);
    copyMenu->addAction(tr("Plain Text"), this,
                        &GTournamentTable::copyText);
    QMenu * saveMenu = menu->addMenu(QIcon(":/icons/save.png"),
                                          tr("Save table as..."));
    saveMenu->addAction(tr("Image"), this,
                        &GTournamentTable::savePicture);
    saveMenu->addAction(tr("HTML table"), this,
                        &GTournamentTable::saveHtml);
    saveMenu->addAction(tr("CSV table"), this,
                        &GTournamentTable::saveExcel);
    saveMenu->addAction(tr("Plain Text"), this,
                        &GTournamentTable::saveText);
    return menu;
}

QTableWidgetItem *GTournamentTable::createItem(const QString& text)
{
    QTableWidgetItem* item = new QTableWidgetItem();
    if(!_editable){
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        item->setFlags(item->flags() ^ Qt::ItemIsSelectable);
    }
    item->setTextAlignment(Qt::AlignCenter);
    item->setText(text);
    return item;
}

QString GTournamentTable::toPlainText()
{
    QString data;
    QList<QStringList> table = getDataLists();
    for(QStringList& row : table){
        row.replaceInStrings("", " \t ");
        data += row.join(" \t ") + " \n ";
    }
    return data;
}

QString GTournamentTable::toCsv()
{
    QString data;
    QList<QStringList> table = getDataLists();
    for(QStringList& row : table){
        data += "\"" + row.join("\" ; \"") + "\" \n ";
    }
    return data;
}

QString GTournamentTable::toHtml()
{
    QString data;
    data += "<table align=\"center\" border=\"1\" "
            "style=\"page-break-before: auto;margin-top: 10;"
            "margin-bottom: 10;\" cellspacing=\"0\" "
            "cellpadding=\"10\">\n";

    data += getHtmlHeader();
    QStringList rowData;
    for (int row = 0; row < rowCount(); row++) {
        rowData.push_back("<td valign=\"middle\">" + QString::number(row+1) + "</td>");
        for (int j = 0; j < columnCount(); j++) {
            rowData.push_back("<td valign=\"middle\" align=\"" + getCellAlignKey(row,j) + "\">" + getCellText(row,j) + "</td>");
        }
        data += "<tr>"+ rowData.join(" ") + "</tr>\n";
        rowData.clear();
    }
    data += "</table>";
    return data;
}

QPixmap GTournamentTable::toPixmap()
{
    QTableWidget table(rowCount(),columnCount());
    QStringList headers = getColumnHeaders();
    int width = table.rowHeight(0);
    for(int col = 0; col<columnCount();col++){
        table.setHorizontalHeaderItem(col, new QTableWidgetItem(headers[col]));
        for(int row = 0; row<rowCount();row++){
            QTableWidgetItem * newItem = new QTableWidgetItem(getCellText(row,col));
            table.setItem(row,col,newItem);
            newItem->setTextAlignment(getCellAlign(row,col));
        }
        table.setColumnWidth(col,columnWidth(col));
        width += columnWidth(col);
    }
    int height = table.rowHeight(0);
    for(int row = 0; row<table.rowCount();row++){
        table.setRowHeight(row,rowHeight(row));
        height += table.rowHeight(row);
    }
    table.setVerticalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
    table.setHorizontalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
    table.setFixedWidth(width);
    table.setFixedHeight(height);
    return table.grab();
}

QString GTournamentTable::getHtmlHeader()
{
    QStringList strList;
    strList.push_back("<th> </th>");
    QStringList headers = getColumnHeaders();
    for(int j = 0; j<columnCount();j++){
        strList.push_back("<th width=\""+QString::number(columnWidth(j))+"\">" + headers[j] + "</th>");
    }
    return "<thead><tr>"+ strList.join(" ") + "</tr></thead>\n";
}

void GTournamentTable::select(int row, int col)
{
    if (item(row,col)){
        item(row,col)->setFlags(item(row,col)->flags() | Qt::ItemIsSelectable);
        item(row,col)->setSelected(true);
    }
    QLineEdit* teamEdit = qobject_cast<QLineEdit*>(cellWidget(row,col));
    if(teamEdit){
        teamEdit->selectAll();
    }
}

void GTournamentTable::deselect(int row, int col)
{
    if (item(row, col)){
        if (item(row, col)->isSelected()){
            item(row, col)->setSelected(false);
            item(row, col)->setFlags((item(row, col)->flags() | Qt::ItemIsSelectable )^ Qt::ItemIsSelectable);
        }
    }
    QLineEdit* teamEdit = qobject_cast<QLineEdit*>(cellWidget(row,col));
    if(teamEdit && teamEdit->selectionLength()){
        teamEdit->deselect();
    }
}

void GTournamentTable::setEditable(bool editable)
{
    _editable = editable;
    setAllEditable(editable);
}

void GTournamentTable::copyExcel()
{
    copyToFormat(AExportable::CSV);
}

void GTournamentTable::copyText()
{
    copyToFormat(AExportable::TXT);
}

void GTournamentTable::copyHtml()
{
    copyToFormat(AExportable::HTML);
}

void GTournamentTable::copyPicture()
{
    copyToFormat(AExportable::PNG);
}

void GTournamentTable::savePicture()
{
    saveToFormat(AExportable::PNG);
}

void GTournamentTable::saveExcel()
{
    saveToFormat(AExportable::CSV);
}

void GTournamentTable::saveText()
{
    saveToFormat(AExportable::TXT);
}

void GTournamentTable::saveHtml()
{
    saveToFormat(AExportable::HTML);
}

void GTournamentTable::showMenu()
{
    if(!mnuContext){
        mnuContext = createContextMenu();
    }
    mnuContext->exec(QCursor::pos());
}
