#ifndef LPLAYOFFDRAWER_H
#define LPLAYOFFDRAWER_H
#include <QQueue>
#include <QList>
#include <QPair>

struct MatchNode;
struct Entry;

class APlayOffDrawer
{
public:
    APlayOffDrawer();
    virtual ~APlayOffDrawer();
    virtual QList<QList<QList<int>>> drawPlayOff(int numTeams) = 0;
    virtual QList<QList<QList<Entry >>> drawPlayOffWithDifferentSources(const QList<int>& numTeamsInSources) = 0;

    QList<QList<QList<int>>> getActualPlayOff();
    QList<QList<QList<Entry >>> getActualPlayOffWithSources();
    QList<QList<int>> getMatchesInOrder();
    QList<QList<Entry> > getMatchesInOrderWithSources();

protected:
    enum Principle { Advantageous, Equal };

    MatchNode * createTree(int numTeams, APlayOffDrawer::Principle principle);
    MatchNode * createTree(const QList<int>& numTeamsInSources, APlayOffDrawer::Principle principle);
    QList<MatchNode*> sortMatches(MatchNode * root);

    QList<QList<MatchNode*>> treeToMatrix(MatchNode * root, int numTeams);
    QList<QList<QList<int>>> treeToListMatrix(MatchNode * root, int numTeams);
    QList<QList<QList<Entry>>> treeToEntryMatrix(MatchNode * root, int numTeams);

    QList<QList<int>> roundToListList(const QList<MatchNode*>& round);
    QList<QList<Entry>> roundToEtryList(const QList<MatchNode*>& round);

    QList<Entry*> createEntryList(const QList<int> &numTeamsInSources);
    QList<Entry*> createEntryListRandom(const QList<int> &numTeamsInSources, int iteration);

    int getAdvantageousWorstIndex(int betterIndex, int roundSize);
    int getEqualWorstIndex(int betterIndex, int numTeams, int roundSize);

    int countSourcesInTree(MatchNode* root, int sourceIndex);
    QList<MatchNode*> matchesOfMinimalRound(const QList<MatchNode*>& matches);
    MatchNode* findBestPossibility(const QList<MatchNode*>& posibilities, Entry* entry, Principle principle);
    bool canBePlaced(MatchNode* match, int numTeams);

    void placeEntry(Entry* entry, MatchNode* match);

    bool predecessorNeeded(int betterIndex, int numTeams, int roundSize);

    int sumList(const QList<int>& list);

    void removeNotFullMatches(MatchNode* root);
    void deleteTree(MatchNode*& root);

private:
    MatchNode* _root;
    int _numTeams;
};

class LPlayOffDrawerAdvantageous: public APlayOffDrawer
{

public:
    LPlayOffDrawerAdvantageous();
    ~LPlayOffDrawerAdvantageous() override;

    QList<QList<QList<int>>> drawPlayOff(int numTeams) override;
    QList<QList<QList<Entry >>> drawPlayOffWithDifferentSources(const QList<int>& numTeamsInSources) override;

    static APlayOffDrawer * __stdcall create();
};

class LPlayOffDrawerEqual: public APlayOffDrawer
{

public:
    LPlayOffDrawerEqual();
    ~LPlayOffDrawerEqual() override;

    QList<QList<QList<int>>> drawPlayOff(int numTeams) override;
    QList<QList<QList<Entry >>> drawPlayOffWithDifferentSources(const QList<int>& numTeamsInSources) override;

    static APlayOffDrawer * __stdcall create();
};



struct Entry {

    int _sourceIndex;
    int _rankInSource;
    int _rankOverall;

    Entry();
    Entry(int rankOverall);
    Entry(int rankOverall, int rankInSource, int sourceIndex);

    bool isEmpty();
};

struct MatchNode {

    int _myRoundSize;
    Entry* _upEntry;
    Entry* _downEntry;
    MatchNode* _upPredecessor;
    MatchNode* _downPredecessor;

    MatchNode();
    explicit MatchNode(int roundSize);
    ~MatchNode();

    MatchNode* createUpPredecessor();
    MatchNode* createDownPredecessor();
    void nullSelf();
    int getBetterIndex();
    bool hasPlace();
};

#endif // LPLAYOFFDRAWER_H
