#ifndef GTOURNAMENTTABLE_H
#define GTOURNAMENTTABLE_H
#include <QTableWidget>
#include "aexportable.h"

#include <QPointer>

class QVariant;

class GTournamentTable: public QTableWidget, public AExportable
{
    Q_OBJECT

public:
    explicit GTournamentTable(QWidget*parent=nullptr);
    virtual ~GTournamentTable() override;

    virtual void setData(const QVariant& data);
    void setRowTextColor(int row, const QColor& color);
    virtual void setAllEditable(bool editable);
    virtual void setColumnEditable(int col, bool editable);
    virtual void setCellEditable(int row, int col, bool editable);
    void setEditable(bool editable);
    void setColumnHeaders(const QStringList& headers);

    virtual QVariant getData();
    virtual QList<QStringList> getDataLists();
    QStringList getColumnHeaders();
    QStringList getColumnData(int col);
    int getRealWidth();

    void clearTable();

public slots:
    virtual void findText(const QString& text);

private slots:
    virtual void showMenu();

    void copyPicture();
    void savePicture();
    void copyExcel();
    void saveExcel();
    void copyText();
    void saveText();
    void copyHtml();
    void saveHtml();


protected:
    void adjustSize(int rows, int cols);
    QTableWidgetItem* createItem(const QString &text = "");
    void setCellText(int row, int col, const QString &text);
    QString getCellText(int row, int col) const;
    virtual QMenu* createContextMenu();

    QString toHtml() override;
    QPixmap toPixmap() override;
    QString toPlainText() override;
    QString toCsv() override;

    QPointer<QMenu> mnuContext;

private:
    int getCellAlign(int row, int col);
    QString getCellAlignKey(int row, int col);
    QString getHtmlHeader();

    void select(int row, int col);
    void deselect(int row, int col);

    bool _editable;
};



#endif // GTOURNAMENTTABLE_H
