#pragma once

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "lfieldsassignerFactory.h"
#include "tst_robinroundmatchesorder.h"
#include "tst_playoffcreation.h"
#include "tst_groupscreation.h"

#include <QList>
#include <QtMath>

using namespace testing;

int maxListSize(const QList<QList<QList<int>>>& matchesByFields)
{
    int maxSize = 0;
    for(const auto& field : matchesByFields){
        if(field.size()>maxSize){
            maxSize = field.size();
        }
    }
    return maxSize;
}


void nobodyPlaysMoreThenOnesInEachRound(const QList<QList<QList<int>>>& matchesByFields, int numTeams)
{
    int maxSize = maxListSize(matchesByFields);
    for (int round = 0; round < maxSize; round++) {
        for(int team = 0; team <= numTeams; team++){
            int count = 0;
            for(const auto& field : matchesByFields){
                if(field.size()>round){
                    count += field[round].count(team);
                }
            }
            ASSERT_TRUE(count <= 1);
        }
    }
}


void isPlanValid(const QList<QList<QList<int>>>& matchesByFields, const QList<QList<int>>& matchesInOrder)
{
    rightNumberOfMatches(matchesByFields, matchesInOrder.size()+1);
    nobodyPlaysMoreThenOnesInEachRound(matchesByFields, maxRank(matchesInOrder));
}

void testAssignerByCombination(AFieldsAssigner* assigner)
{
    APlayOffDrawer* playOff = LPlayOffDrawerFactory::Instance()->createDrawer("advantageous");
    AMatchesOrderer* robinRound = LMatchesOrdererFactory::Instance()->createMatchesOrderer("kirkman");
    for (int numTeams = 8; numTeams < 30; numTeams++) {
        auto matches = robinRound->planMatches(numTeams);
        playOff->drawPlayOff(numTeams/2);
        matches.append(playOff->getMatchesInOrder());
        for(int numFields = 1; numFields < 20; numFields++){
            auto matchesByFields = assigner->planFields(matches,numFields);
            isPlanValid(matchesByFields, matches);
        }
    }
    delete playOff;
    delete robinRound;
}

void testAssignerByPlayOff(AFieldsAssigner* assigner)
{
    APlayOffDrawer* playOff = LPlayOffDrawerFactory::Instance()->createDrawer("advantageous");
    for (int numTeams = 3; numTeams < 30; numTeams++) {
        playOff->drawPlayOff(numTeams);
        auto matches = playOff->getMatchesInOrder();
        for(int numFields = 1; numFields < 20; numFields++){
            auto matchesByFields = assigner->planFields(matches,numFields);
            isPlanValid(matchesByFields, matches);
        }
    }
    delete playOff;
}

void testAssignerByRobinRound(AFieldsAssigner* assigner)
{
    AMatchesOrderer* robinRound = LMatchesOrdererFactory::Instance()->createMatchesOrderer("kirkman");
    for (int numTeams = 3; numTeams < 30; numTeams++) {
        auto matches = robinRound->planMatches(numTeams);
        for(int numFields = 1; numFields < 10; numFields++){
            auto matchesByFields = assigner->planFields(matches,numFields);
            isPlanValid(matchesByFields, matches);
        }
    }
    delete robinRound;
}

void testAssigner(AFieldsAssigner* assigner)
{
    testAssignerByPlayOff(assigner);
    testAssignerByRobinRound(assigner);
    testAssignerByCombination(assigner);
}

TEST(fieldsplanning, simple)
{
    AFieldsAssigner* simple = LFieldsAssignerFactory::Instance()->createAssigner("simple");
    testAssigner(simple);
    delete simple;
}

TEST(fieldsplanning, min_transition)
{
    AFieldsAssigner* transition = LFieldsAssignerFactory::Instance()->createAssigner("transition");
    testAssigner(transition);
    delete transition;
}

TEST(fieldsplanning, max_changeovers)
{
    AFieldsAssigner* changeovers = LFieldsAssignerFactory::Instance()->createAssigner("changeover");
    testAssigner(changeovers);
    delete changeovers;
}


