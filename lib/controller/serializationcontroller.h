#ifndef TOURNAMENTSERIALIZER_H
#define TOURNAMENTSERIALIZER_H
#include <QObject>

class QTimer;

class ASerializable;
class ASerializer;

class SerializationController : public QObject
{
    Q_OBJECT

public:
    SerializationController(ASerializer* serializer, ASerializable* model, QObject *parent = nullptr);
    ~SerializationController();

    void setModification(bool modified);
    void setFileName(const QString& fileName);

    QList<QString> getRecentList() const;

public slots:
    void save();
    void saveAs(const QString& fileName);
    void open(const QString& fileName);
    void close();
    void clearRecentList();

signals:
    void saveAsRequested();
    void statusBarChangeRequested(const QString& text);
    void modificationChanged(bool modified);
    void currentFileChanged(const QString& fileName);
    void recentListChanged(const QList<QString>& recentList);

private slots:
    void autoSave();

private:
    void connectView();
    void updateRecentList();
    void readSetting();
    void writeSetting();
    QString generateAutoSaveName();

    QString _fileName;
    QString _autoSaveName;
    bool _modified;
    ASerializable* _model;
    ASerializer* _serializer;
    QStringList _recentList;
    QString _suffix;
    QTimer* _timer;

};


#endif // TOURNAMENTSERIALIZER_H
