#include "geditationdialog.h"
#include "geditationtable.h"
#include <QAction>
#include <QMenu>
#include <QCheckBox>
#include <QLineEdit>
#include <QLabel>
#include <QVBoxLayout>
#include <QDialogButtonBox>

GEditDialog::GEditDialog(const QVariant &data, QWidget *parent)
    : QDialog (parent)
{
    QVBoxLayout* layout = new QVBoxLayout();
    twEditation = new GEditationTable(data, this);
    layout->addWidget(twEditation);
    QDialogButtonBox * buttonBox = new QDialogButtonBox( {QDialogButtonBox::Ok, QDialogButtonBox::Cancel});
    connect(buttonBox, SIGNAL(accepted()),
            this, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()),
            this, SLOT(reject()));
    layout->addWidget(buttonBox);
    setLayout(layout);
}

QVariant GEditDialog::getNewData()
{
    return twEditation->getNewData();
}

QVariant GEditDialog::getOriginalData()
{
    return twEditation->getOriginalData();
}
