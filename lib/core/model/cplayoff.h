#ifndef CPLAYOFF_H
#define CPLAYOFF_H
#include "astagemodel.h"
#include "cmatch.h"

class CPlayOffMatch;
class APlayOffDrawer;

class CPlayOff: public AStageModel
{
    Q_OBJECT

public:
    CPlayOff(int id, const QString& name, APlayOffDrawer* drawer, QObject *parent = nullptr);
    ~CPlayOff() override;

    void setTeamsCount(int numTeams) override;
    void setTeams(const QList<CTeam*>& teams) override;
    void setTeams(CTeamsList* teams) override;
    void setTeamsFromSources(const QList<CTeamsList*>& teams) override;
    void setTeamsFromVariant(const QVariant& variant, const QMap<int,CTeam*>& teamsById) override;

    void setNextStage(AStageModel* nextStage) override;

    CTeamsList* countStandings() override;

    CTeam* getTeam(const QVariant& identificator)  const override;    
    QVariant getData() const override;

    void resultsFromVariant(const QVariant& variant) override;
    QVariant toVariant() const override;

    void changeTeam(const QVariant& identificator, CTeam* team) override;
    void nextStageCare() override;

public slots:
    bool setResult(const QVariant& identificator, const QString& result) override;
    bool execEdit(ExecType type, const QVariant& identificator, const QVariant& data) override;

private slots:
    void teamChange();
    void matchChanged();

protected:
    bool swapTeams(const QVariantMap &data) override;
    bool renameTeam(const QVariant &identificator, const QVariant &data);
    bool sameRound(CPlayOffMatch *first, CPlayOffMatch *second);
    void setResult(int roundIndex, int matchIndex, const QString &result);

private:
    void clear();

    void createMatchesMatrix(const QList<QList<QList<int>>>& intMatches);
    void createMatches(int numTeams);
    void placeTeamsToMatches(CTeamsList* teams);

    void setNextMatches();
    void sortMatches();

    CTeam* getWinner() const;
    CPlayOffMatch *findMatch(const QVariantMap& identificator) const;
    CTeam* findTeam(const QVariantMap& identificator) const;
    CPlayOffMatch* findMatch(int roundIndex, int matchIndex) const;

    QString createResultPlaceHolder(int place) const;
    QString createMatchPlaceHolder(int round) const;

    APlayOffDrawer* _drawer;
    QList<QList<CPlayOffMatch*>> _matchesByRounds;
};

class CPlayOffMatch: public CMatch
{
    Q_OBJECT

public:
    CPlayOffMatch(int sourceId, QObject* parent = nullptr);

    void setResult(const QString &result) override;

    void setMatchForWinner(CPlayOffMatch *matchForWinner);
    CPlayOffMatch* getMatchForWinner() const;
    void setMatchForLosser(CPlayOffMatch *matchForLosser);
    void setIsTopPredecessor(bool isTopPredecessor);
    void setNextMatchTeam();

    bool isTopPredecessor() const;

private:
    bool _isTopPredecessor = true;
    CPlayOffMatch* _matchForWinner = nullptr;
    CPlayOffMatch* _matchForLoser = nullptr;
};

#endif // CPLAYOFF_H
