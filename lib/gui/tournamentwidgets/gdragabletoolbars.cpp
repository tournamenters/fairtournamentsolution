#include "gdragabletoolbars.h"
#include <QLayout>
#include "gdragabletoolbutton.h"


GDragableToolBar::GDragableToolBar(const QString& title, QWidget* parent)
    : QToolBar(title, parent)
{
    _align = Qt::AlignCenter;
}

QToolButton *GDragableToolBar::addToolButton(const QString &text, const QString &tips)
{
    GDragableToolButton* toolButton = new GDragableToolButton(text, this);
    toolButton->setToolTip(tips);
    addWidget(toolButton);
    setAlign(_align);
    return toolButton;
}

QToolButton* GDragableToolBar::addToolButton(const QIcon &icon, const QString &text, const QString &tips)
{
    QToolButton* toolButton = addToolButton(text, tips);
    toolButton->setIcon(icon);
    return toolButton;
}

void GDragableToolBar::setAlign(Qt::Alignment align)
{
    layout()->setAlignment(align);
    for(int i = 0; i < layout()->count(); i++){
        layout()->itemAt(i)->setAlignment(align);
    }
    _align = align;
}


GDragableResultsToolBar::GDragableResultsToolBar(const QString& title, QWidget *parent, int maxScore)
    :GDragableToolBar (title, parent)
{
    createResultButtons(maxScore);
}

void GDragableResultsToolBar::createResultButtons(int maxScore)
{
    addToolButton(tr("Remove"));
    addToolButton(tr("* ot."));
    for (int first = 0; first<maxScore;first++){
        for (int second = 0; second<=first;second++){
            addToolButton(QString::number(first) + " : " + QString::number(second));
            if (first!=second){
                addToolButton(QString::number(second) + " : " + QString::number(first));
            }
        }
    }
}
