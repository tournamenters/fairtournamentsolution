#include "gstagetabsfactory.h"
#include "global_creation.h"
#include "gstageeditortabs.h"

GStageTabsFactory::GStageTabsFactory()
{
    registerType(keyRobinRound, GRobinRoundEditorTab::create);
    registerType(keyGroups, GGroupsEditorTab::create);
    registerType(keyPlayOff, GPlayOffEditorTab::create);
}

GStageTabsFactory *GStageTabsFactory::getInstance()
{
    static GStageTabsFactory instance;
    return &instance;
}

GStageTabsFactory::~GStageTabsFactory()
{
    registeredTypes.clear();
}

QStringList GStageTabsFactory::keys()
{
    return registeredTypes.keys();
}

void GStageTabsFactory::registerType(const QString &key, FcnCreateStageTab create)
{
    registeredTypes[key]=create;
}

AStageEditorTab* GStageTabsFactory::create(const QString& key)
{
    return registeredTypes[key]();
}
