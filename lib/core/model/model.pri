include($$PWD/../algorithms/algorithms.pri)

win32:CONFIG(release, debug|release): LIBS += -L$$TOP_BUILDDIR/lib/core/model/release/ -lmodel
else:win32:CONFIG(debug, debug|release): LIBS += -L$$TOP_BUILDDIR/lib/core/model/debug/ -lmodel
else:unix: LIBS += -L$$TOP_BUILDDIR/lib/core/model/ -lmodel

INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/core/model/release/libmodel.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/core/model/debug/libmodel.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/core/model/release/model.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/core/model/debug/model.lib
else:unix: PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/core/model/libmodel.a


