#include "gstartlistpage.h"
#include "global_creation.h"
#include "global_model.h"
#include "gcreationtable.h"
#include "serialization.h"
#include "cstartlist.h"
#include "tournament.h"

#include <QFile>
#include <QLabel>
#include <QBoxLayout>
#include <QPushButton>
#include <QGroupBox>
#include <QRadioButton>
#include <QCheckBox>
#include <QMessageBox>
#include <QFileDialog>
#include <QInputDialog>
#include <QApplication>
#include <QFileInfo>

GStartListInsertingPage::GStartListInsertingPage(QWidget *parent)
    : QWizardPage(parent),
      _numTeams(0),
      _minRows(8),
      _clubs(true),
      _seedTeams(true),
      _startListData()
{
    initializeHeaders();

    setTitle(tr("Start List"));
    setSubTitle(tr("Please enter your tournament participants' data"
                   " in the table below or set the number of them at least."));

    setLayout(createMainLayout());

    registerField("start_list.num_teams", this, "_numTeams", SIGNAL(numTeamsChanged()));
    registerField("start_list.data", this, "_startListData", SIGNAL(teamsDataChanged()));
}

GStartListInsertingPage::~GStartListInsertingPage()
{
    _startListData.clear();
}

int GStartListInsertingPage::nextId() const
{
    return PageIndex::Page_Format;
}

bool GStartListInsertingPage::isComplete() const
{
    return tableStartList->nonEmptyCellsCount(_nameColumnHeader) > 2;
}

QLabel *GStartListInsertingPage::createTitle()
{
    QLabel * label = new QLabel(tr("You can edit the names and data later. However the "
                                  "total number of participants cannot be increased"
                                  " after the creation of the tournament."), this);
    label->setWordWrap(true);
    return label;
}

QLayout *GStartListInsertingPage::createButtonsLayout()
{
    QVBoxLayout* layout = new QVBoxLayout();
    layout->addWidget(createImportButton());
    layout->addWidget(createRandomButton());
    layout->addStretch(1);
    layout->addWidget(createSeedBox());
    layout->addWidget(createClubsCheckBox());
    layout->addStretch(1);
    layout->addWidget(createNewColumnButton());
    layout->addWidget(createClearButton());
    layout->addStretch(4);
    layout->setAlignment(Qt::AlignTop);
    return layout;
}

QTableWidget *GStartListInsertingPage::createStartListTable()
{
    tableStartList = new GCreationTable(this);
    tableStartList->setEditable(true);
    tableStartList->setMinRows(_minRows);
    tableStartList->setColumnCount(3);
    tableStartList->setColumnHeaders({_nameColumnHeader, _clubColumnHeader, _seedColumnHeader});
    tableStartList->setUnremovableColumns({_nameColumnHeader});
    tableStartList->setNumericColumns({_seedColumnHeader});
    connect(tableStartList, SIGNAL(tableChanged()),
            this, SLOT(tableDataChanged()));
    connect(tableStartList, SIGNAL(columnsChanged()),
            this, SLOT(tableColumnsChanged()));
    return tableStartList;
}

QPushButton *GStartListInsertingPage::createImportButton()
{
    QPushButton* btnImport = new QPushButton(tr("Import start list from a file"),this);
    connect(btnImport, SIGNAL(clicked()),
            this, SLOT(importStartList()));
    return btnImport;
}

QPushButton *GStartListInsertingPage::createRandomButton()
{
    QPushButton* btnRandom = new QPushButton(tr("Enter only size of the list"),this);
    connect(btnRandom, SIGNAL(clicked()),
            this, SLOT(generateStartList()));
    return btnRandom;
}

QGroupBox *GStartListInsertingPage::createSeedBox()
{
    boxSeed = new QGroupBox(tr("Seed participants"), this);
    boxSeed->setCheckable(true);
    boxSeed->setChecked(_seedTeams);
    boxSeed->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
    boxSeed->setLayout(createSeedBoxLayout());
    connect(boxSeed, SIGNAL(toggled(bool)),
            this, SLOT(checkSeed(bool)));
    registerField("start_list.ranked", this, "_seedTeams", SIGNAL(seedTeamChanged()));
    return boxSeed;
}

QLayout *GStartListInsertingPage::createSeedBoxLayout()
{
    QVBoxLayout* layout = new QVBoxLayout;
    radRankings = new QRadioButton(tr("Rankings [1,2,...]"), this);
    radRankings->setChecked(true);
    registerField("start_list.descendinly", radRankings);
    layout->addWidget(radRankings);
    radCoefficient = new QRadioButton(tr("Coefficient [ELO,...]"), this);
    registerField("start_list.ascendinly", radCoefficient);
    layout->addWidget(radCoefficient);
    return layout;
}

QCheckBox *GStartListInsertingPage::createClubsCheckBox()
{
    boxClubs = new QCheckBox(tr("Classify participants by clubs"), this);
    boxClubs->setChecked(_clubs);
    registerField("start_list.clubs", boxClubs);
    connect(boxClubs, SIGNAL(stateChanged(int)),
            this, SLOT(checkClubs(int)));
    return boxClubs;
}

QPushButton *GStartListInsertingPage::createClearButton()
{
    QPushButton* btnClear = new QPushButton(tr("Clear start list"), this);
    connect(btnClear, SIGNAL(clicked()),
            tableStartList, SLOT(clearContents()));
    connect(btnClear, SIGNAL(clicked()),
            this, SIGNAL(completeChanged()));
    return btnClear;
}

QPushButton *GStartListInsertingPage::createNewColumnButton()
{
    QPushButton* btnAddColumn = new QPushButton(tr("Add a custom column"), this);
    connect(btnAddColumn, SIGNAL(clicked()),
            tableStartList, SLOT(addTableColumn()));
    return btnAddColumn;
}

QVariant GStartListInsertingPage::parseTextFileLine(QString &line)
{
    QMap<QString, QVariant> map;
    QStringList lineList = readLineFromFile(line);
    if(!lineList.isEmpty()){
        map[_nameColumnHeader] = lineList[0].trimmed();
        if(lineList.size()>1 && !lineList[1].isEmpty()){
            if(!isInt(lineList[1])){
                map[_clubColumnHeader]=lineList[1];
                if(line.size()>2 && isInt(lineList[2])){
                    map[_seedColumnHeader]=lineList[2];
                }
            }
            else{
                map[_seedColumnHeader] = lineList[1];
            }
        }
    }
    return map;
}

QStringList GStartListInsertingPage::readLineFromFile(QString &line)
{
    line.remove("\n");
    if(line.contains(";")){
        return line.split(";");
    }
    if(line.contains(",")){
        return line.split(",");
    }
    return QStringList(line);
}

void GStartListInsertingPage::initializeHeaders()
{
    _nameColumnHeader = tr("Name");
    _seedColumnHeader = tr("Rank");
    _clubColumnHeader = tr("Club");
}

void GStartListInsertingPage::showRowsInTable(const QVariant &rows)
{
    QVariantMap data;
    data["rows"] = rows;
    data["keys"] = QStringList({_nameColumnHeader, _clubColumnHeader, _seedColumnHeader});
    tableStartList->blockSignals(true);
    tableStartList->setData(data);
    tableStartList->blockSignals(false);
    tableDataChanged();
    tableColumnsChanged();
}

bool GStartListInsertingPage::isInt(const QString &text) const
{
    bool isNumber = false;
    text.toInt(&isNumber);
    return isNumber;
}

QVariant GStartListInsertingPage::modifieData(const QVariant &data)
{
    QVariantMap dataOld = data.toMap();
    QStringList newKeys;
    for(const auto& row : dataOld["keys"].toStringList()){
        if(row==_nameColumnHeader){
            newKeys.push_back("name");
        }
        else if(row==_clubColumnHeader){
            newKeys.push_back("club");
        }
        else if(row==_seedColumnHeader){
            newKeys.push_back("rank");
        }
        else{
            newKeys.push_back(row);
        }
    }
    QVariantList newRows;
    for(const auto& row : dataOld["rows"].toList()){
        QVariantMap rowMap = row.toMap();
        QVariantMap newRow;
        for (const auto& key : rowMap.keys()) {
            if(key ==_nameColumnHeader){
                newRow["name"] = rowMap[key];
            }
            else if(key ==_clubColumnHeader){
                newRow["club"] = rowMap[key];
            }
            else if(key ==_seedColumnHeader){
                newRow["rank"] = rowMap[key];
            }
            else{
                newRow[key] = rowMap[key];
            }
        }
        newRows.push_back(newRow);
    }

    QVariantMap newData;
    newData["keys"] = newKeys;
    newData["rows"] = newRows;
    return newData;
}

QLayout *GStartListInsertingPage::createMainLayout()
{
    QVBoxLayout* vLayout = new QVBoxLayout;
    vLayout->addWidget(createTitle());
    QHBoxLayout* hLayout = new QHBoxLayout;
    hLayout->addWidget(createStartListTable());
    hLayout->insertLayout(0, createButtonsLayout());
    hLayout->setAlignment(Qt::AlignLeft);
    vLayout->addLayout(hLayout);
    return vLayout;
}

void GStartListInsertingPage::generateStartList()
{
    bool input = false;
    int participants = QInputDialog::getInt(this,tr("Number of participants"), tr("Enter the number of participants:"),16,3,100,1,&input);
    if(input){
        QVariantList list;
        for(int row = 0; row < participants; row++){
            QVariantMap map;
            map[_nameColumnHeader] = "[" + tr("Team ") + QString::number(row+1) + "]";
            map[_clubColumnHeader] = "[" + tr("Club ") + QString::number(row+1) + "]";
            map[_seedColumnHeader] = row+1;
            list.push_back(map);
        }
        showRowsInTable(list);
    }
}

void GStartListInsertingPage::tableDataChanged()
{
    setData(tableStartList->getData());
    setNumTeams(tableStartList->nonEmptyCellsCount(_nameColumnHeader));
    emit completeChanged();
}

void GStartListInsertingPage::tableColumnsChanged()
{
    bool seed = tableStartList->getColumnHeaders().contains(_seedColumnHeader);
    setSeed(seed);
    boxSeed->setChecked(seed);
    bool clubs = tableStartList->getColumnHeaders().contains(_clubColumnHeader);
    boxClubs->setChecked(clubs);
}

void GStartListInsertingPage::checkClubs(int check)
{
    tableStartList->setVisibleColumn(check, _clubColumnHeader);
}

void GStartListInsertingPage::checkSeed(bool check)
{
    setSeed(check);
    tableStartList->setVisibleColumn(check, _seedColumnHeader);
}

void GStartListInsertingPage::importStartList()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Select file"), QApplication::applicationDirPath() +  "/Examples", tr("Lists (*.txt);Tournaments (*.turn)"));
    if(!fileName.isEmpty()){
        QFile file(fileName);
        if (!file.open(QFile::ReadOnly | QFile::Text)){
            QMessageBox::warning(this,"Error",tr("The file could not be opened: %1\nERROR:%22").arg(QDir::toNativeSeparators(fileName),file.errorString()));
            return;
        }        
        if(QFileInfo(fileName).suffix() != "turn"){
            QVariantList data;
            while(!file.atEnd()){
                QString string = file.readLine();
                data.push_back(parseTextFileLine(string));
            }
            file.close();
            showRowsInTable(data);
        }
        else{
            CStartList list;
            CXMLSerializer serializer;
            serializer.load(&list, fileName);
            tableStartList->blockSignals(true);
            tableStartList->setData(list.getData());
            tableStartList->blockSignals(false);
            tableDataChanged();
            tableColumnsChanged();
        }
    }
}

int GStartListInsertingPage::getNumTeams() const
{
    return _numTeams;
}

void GStartListInsertingPage::setNumTeams(int numTeams)
{
    if(_numTeams == numTeams){
        return;
    }
    _numTeams = numTeams;
    emit numTeamsChanged();
}

bool GStartListInsertingPage::isSeed() const
{
    return _seedTeams;
}

void GStartListInsertingPage::setSeed(bool seedTeams)
{
    if(seedTeams==_seedTeams){
        return;
    }
    _seedTeams = seedTeams;
    emit seedTeamChanged();
}

void GStartListInsertingPage::setData(const QVariant &data)
{
    QVariant modData = modifieData(data);
    if(_startListData==modData){
        return;
    }
    _startListData = modData;
    emit teamsDataChanged();
}

QVariant GStartListInsertingPage::getData()
{
    return _startListData;
}


