#ifndef GTHEMESETTINGDIALOG_H
#define GTHEMESETTINGDIALOG_H

#include <QDialog>
#include <QList>

class QGroupBox;
class QRadioButton;

class GThemeSettingDialog : public QDialog
{
    Q_OBJECT

    QList<QRadioButton*> paletteButtons;
    QList<QRadioButton*> styleButtons;
    QGroupBox* palette;
    QGroupBox* style;

public:
    GThemeSettingDialog(QStringList possiblePalettes, QStringList possibleStyles, QWidget *parent = nullptr);
    void setCurrentPalette(QString paletteKey);
    void setCurrentStyle(QString styleKey);

    QString getCurrentPalette();
    QString getCurrentStyle();

private slots:
    void setEnablePossibilities(bool checked);

private:
    QGroupBox* createGroupBox(QString title, QStringList buttonsTexts, QList<QRadioButton*>& buttonsContainer);
    void checkButtonWithText(QList<QRadioButton*> buttons, QString key);
    QRadioButton* findCheckedButton(QList<QRadioButton*> buttons);
    QString getCheckedButtonsText(QList<QRadioButton*> buttons);
};

#endif // GTHEMESETTINGDIALOG_H
