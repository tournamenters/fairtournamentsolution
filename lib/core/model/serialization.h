#ifndef SERIALIZATION_H
#define SERIALIZATION_H
#include <QVariant>

class QXmlStreamWriter;
class QXmlStreamReader;

//#-------------------------------------------------
//#
//# Based on an example from the Mastering Qt 5, Guillaume Lazar, Robin Penea, 2016, ISBN13: 9781786467126
//#
//#-------------------------------------------------

class ASerializable
{
public:
    virtual ~ASerializable(){}
    virtual QVariant toVariant() const = 0;
    virtual void fromVariant(const QVariant& variant) = 0;
};


class ASerializer
{
public:
    virtual ~ASerializer(){}
    virtual void save(const ASerializable* serializable, const QString& filepath, const QString& rootname = "serialization") = 0;
    virtual void load(ASerializable* serializable, const QString& filepath) = 0;

};

class CXMLSerializer: public ASerializer
{
public:
    CXMLSerializer(){}
    void save(const ASerializable* serializable, const QString& filepath, const QString& rootName) override;
    void load(ASerializable* serializable, const QString& filepath) override;

private:
    void writeVariantToStream(const QString& nodeName, const QVariant& variant, QXmlStreamWriter& stream);
    void writeVariantMapToStream(const QVariant& variant, QXmlStreamWriter& stream);
    void writeVariantListToStream(const QVariant& variant, QXmlStreamWriter& stream);
    void writeVariantValueToStream(const QVariant& variant, QXmlStreamWriter& stream);

    QVariant readVariantFromStream(QXmlStreamReader& stream);
    QVariant readVariantValueFromStream(QXmlStreamReader& stream);
    QVariant readVariantListFromStream(QXmlStreamReader& stream);
    QVariant readVariantMapFromStream(QXmlStreamReader& stream);
};

#endif // SERIALIZATION_H
