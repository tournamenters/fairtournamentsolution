#include "gplayoffwidget.h"
#include "gteamedit.h"
#include "gresultedit.h"
#include <QMenu>
#include <QPainter>
#include <QRegion>

GPlayOffWidget::GPlayOffWidget(const QVariant &data, const QVariant &identificator, QWidget *parent)
    : QWidget(parent)
{
    _identificator = identificator;
    _treeLineWidth = 3;
    _lineEditMarginCoef = 0.9;
    _numRounds = -1;
    createWinner();
    setData(data);
    setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, SIGNAL(customContextMenuRequested(const QPoint&)),
            this, SLOT(showMenu()));
}

void GPlayOffWidget::setData(const QVariant &data)
{
    QVariantList rounds = data.toMap()["rounds"].toList();
    bool change = false;
    adjustMatchesSize(rounds.size());
    for(int round = 0; round< rounds.size(); round++){
        QVariantList matches = rounds[round].toList();
        for(int match = 0; match < matches.size(); match++){           
            if(!matchExists(round, match)){
                change = true;
            }
            addMatch(round, match, matches[match]);
        }
    }
    if(data.toMap().contains("winner")){
        _winner->setText(data.toMap()["winner"].toString());
    }
    else{
        _winner->setText("");
    }
    if(change){
        repaintPlayOff();
    }
}

void GPlayOffWidget::findText(const QString &text)
{
    blockSignals(true);
    for(int round = 0; round<_matches.size(); round++){
        for(int match = 0; match<_matches[round].size(); match++){
            if(matchExists(round, match)){
                _matches[round][match]->findText(text);
            }
        }
    }
    blockSignals(false);
}

QString GPlayOffWidget::toHtml()
{
    return QString();
}

QPixmap GPlayOffWidget::toPixmap()
{
    QPixmap pixmap(size());
    pixmap.fill(palette().color(QPalette::Base));
    render(&pixmap);
    return pixmap;
}

QString GPlayOffWidget::toPlainText()
{
    return QString();
}

QString GPlayOffWidget::toCsv()
{
    return QString();
}

QMenu *GPlayOffWidget::createContextMenu()
{
    QMenu * menu = new QMenu();
    menu->addAction(QIcon(":/icons/save.png"), tr("Save as image..."), this,
                        &GPlayOffWidget::savePicture);
    menu->addAction(QIcon(":/icons/copy.ico"), tr("Copy as image"), this,
                        &GPlayOffWidget::copyPicture);
    return menu;
}

void GPlayOffWidget::showMenu()
{
    if(!mnuContext){
        mnuContext = createContextMenu();
    }
    mnuContext->exec(QCursor::pos());
}

void GPlayOffWidget::copyPicture()
{
    copyToFormat(AExportable::PNG);
}

void GPlayOffWidget::savePicture()
{
    saveToFormat(AExportable::PNG);
}

void GPlayOffWidget::repaintPlayOff()
{
    setSize(_numRounds);
    placeTreeWidgets();
    paintTreeBackground();
}

void GPlayOffWidget::adjustMatchesSize(int numRounds)
{
    if(numRounds != _numRounds){
        _matches.clear();
        while(_matches.size() < numRounds){
            _matches.push_back({});
        }
        _numRounds = numRounds;
        adjustRoundsSize();
        setSize(numRounds);
    }
}

void GPlayOffWidget::adjustRoundsSize()
{
    int firstRoundSize = static_cast<int>(std::pow(2, _matches.size()-1));
    for(auto& round : _matches){
        while(round.size()<firstRoundSize){
            round.push_back(nullptr);
        }
        firstRoundSize*=0.5;
    }
}

void GPlayOffWidget::placeTreeWidgets()
{
    int matchCenterW = _columnWidth/10;
    for(int round = 0; round<_matches.size(); round++){
        int matchHeight = height()/(_matches[round].size()*4);
        int matchCenterH = height()/(_matches[round].size()*2);
        for(int match = 0; match<_matches[round].size(); match++){
            placeMatch(round, match, matchCenterH, matchCenterW, matchHeight);
            matchCenterH += height()/_matches[round].size();
        }
        matchCenterW += _columnWidth;
    }
    if(_winner){
        int widgetHeightAdjust = _rowHeight + _treeLineWidth + 1;
        _winner->move(matchCenterW, height()/2 - widgetHeightAdjust);
    }
}

void GPlayOffWidget::placeWinner()
{
    if(_winner){
        int finalCenterW = static_cast<int>(width() - _columnWidth * _lineEditMarginCoef);
        int finalCenterH = height()/2;
        int widgetHeightAdjust = _rowHeight + _treeLineWidth/2 + 1;
        _winner->move(finalCenterW, finalCenterH - widgetHeightAdjust);
    }
}

void GPlayOffWidget::placeMatch(int round, int match, int centerH, int centerW, int matchHeight)
{
    if(matchExists(round, match)){
        _matches[round][match]->moveTeamsLabels(centerW, centerH - _treeLineWidth - 1, matchHeight);
        _matches[round][match]->moveResulEdit(centerW + _columnWidth, centerH + _treeLineWidth + 1);
    }
}

void GPlayOffWidget::paintTreeBackground()
{
    QPixmap backgroundPixmap(width(), height());
    backgroundPixmap.fill(QColor(Qt::transparent));

    QPen treeLinePen;
    treeLinePen.setColor(palette().color(QPalette::WindowText));
    treeLinePen.setWidth(_treeLineWidth);

    QPainter painter(&backgroundPixmap);
    painter.setPen(treeLinePen);
    paintTreeLines(&painter);

    QPalette palette = this->palette();
    palette.setBrush(QPalette::Background, backgroundPixmap);
    setPalette(palette);
}

void GPlayOffWidget::paintTreeLines(QPainter *painter)
{
    int matchCenterW = 0;
    for(int round = 0; round<_matches.size(); round++){
        int matchHeight = height()/(_matches[round].size()*4);
        int matchCenterH = height()/(_matches[round].size()*2);
        for(int match = 0; match<_matches[round].size(); match++){
            if(matchExists(round, match)){
                paintMatchLines(painter, matchCenterH, matchCenterW, matchHeight);
            }
            matchCenterH += height()/_matches[round].size();
        }
        matchCenterW += _columnWidth;
    }

    painter->drawLine(matchCenterW, height()/2, width(), height()/2);
}

bool GPlayOffWidget::matchExists(int roundIndex, int matchIndex)
{
    if(roundIndex<0 || matchIndex<0){
        return false;
    }
    if(_matches.size()>roundIndex && _matches[roundIndex].size()>matchIndex){
        return _matches[roundIndex][matchIndex];
    }
    return false;
}

void GPlayOffWidget::paintMatchLines(QPainter *painter, int centerH, int centerW, int height)
{
    int hostHeight = centerH + height;
    int guestHeight = centerH - height;
    int nextCenterW = centerW + _columnWidth;
    painter->drawLine(nextCenterW, guestHeight, nextCenterW, hostHeight);
    painter->drawLine(centerW, guestHeight, nextCenterW, guestHeight);
    painter->drawLine(centerW, hostHeight, nextCenterW, hostHeight);
}

int GPlayOffWidget::countMatchHeight(int roundIndex)
{
    int matchHeight = static_cast<int>(std::pow(2, _numRounds - roundIndex));
    matchHeight = matchHeight * _rowHeight;
    return matchHeight;
}


GPlayOffMatch *GPlayOffWidget::createMatch(int roundIdnex, int matchIndex)
{
    GPlayOffMatch * matchWidget = new GPlayOffMatch(roundIdnex, matchIndex, _identificator, this);
    connect(matchWidget->result(), SIGNAL(resultChanged(int, int, QString, QString)),
            this, SIGNAL(resultChanged(int, int, QString, QString)));
    connectTeamEdit(matchWidget->host());
    connectTeamEdit(matchWidget->guest());
    if(roundIdnex<_matches.size() && matchIndex<_matches[roundIdnex].size()){
        _matches[roundIdnex][matchIndex] = matchWidget;
    }
    return  matchWidget;
}

void GPlayOffWidget::addMatch(int round, int match, const QVariant &data)
{
    if(matchExists(round, match)){
        _matches[round][match]->setData(data,!matchExists(round-1,match*2),!matchExists(round-1,match*2+1));
        return;
    }
    if(!data.isNull()){
        GPlayOffMatch * matchWidget = createMatch(round, match);
        matchWidget->setData(data,!matchExists(round-1,match*2),!matchExists(round-1,match*2+1));
    }
}

void GPlayOffWidget::connectTeamEdit(GTeamEdit *team)
{
    connect(team, SIGNAL(teamChangeRequested(QVariant,QString,QString)),
            this, SIGNAL(teamChangeRequested(QVariant,QString,QString)));
    connect(team, SIGNAL(teamsReplacementRequested(QVariant,QVariant)),
            this, SIGNAL(teamsReplacementRequested(QVariant,QVariant)));
}

void GPlayOffWidget::createWinner()
{
    _winner = new GTeamEdit(0,0,_identificator,this);
    _winner->setReadOnly(true);
    _winner->setDragAndDrop(false);
    _winner->setFrame(QFrame::NoFrame);
}

void GPlayOffWidget::setSize(int numRounds)
{
    QLineEdit* widthKeeper = new QLineEdit(this);
    _columnWidth = widthKeeper->sizeHint().width() * 5/4;
    _rowHeight = widthKeeper->sizeHint().height();
    delete widthKeeper;
    int firstRoundSize = static_cast<int>(std::pow(2, numRounds-1));
    int width = (numRounds + 1) * _columnWidth;
    int height = firstRoundSize * _rowHeight * 4;
    setFixedWidth(width);
    setFixedHeight(height);
}




GPlayOffMatch::GPlayOffMatch(int roundIndex, int matchIndex, const QVariant &identificator, QWidget *parent)
    : QWidget(parent)
{
    _host = new GTeamEdit(roundIndex, matchIndex, addKey("host", identificator), parent);
    _guest = new GTeamEdit(roundIndex, matchIndex, addKey("guest", identificator), parent);

    _result = new GResultEdit(roundIndex, matchIndex, parent);
    _result->setFrame(true);
    _result->resize(_result->sizeHint().width()/2, _result->sizeHint().height());
}

GPlayOffMatch::~GPlayOffMatch()
{
    delete _host;
    delete _guest;
    delete _result;
}

void GPlayOffMatch::setData(const QVariant& data, bool hostPlaceholder, bool guestPlaceholder)
{
    QVariantMap map = data.toMap();
    _result->setVisible(true);
    setTeamsData(_host, map["host"].toMap(), hostPlaceholder);
    setTeamsData(_guest, map["guest"].toMap(), guestPlaceholder);
    _result->blockSignals(true);
    _result->setText(map["result"].toString());
    _result->blockSignals(false);
}

void GPlayOffMatch::findText(const QString &text)
{
    _host->selectText(text);
    _guest->selectText(text);
}

QVariant GPlayOffMatch::addKey(const QString &key, const QVariant &variant)
{
    QVariantMap map = variant.toMap();
    map[key] = 1;
    return map;
}

void GPlayOffMatch::setTeamsData(GTeamEdit *edit, const QVariantMap &data, bool placeholder)
{
    QPalette palette = this->palette();
    if(data["active"].toBool()){
        palette.setColor(QPalette::Text, this->palette().color(QPalette::Text));
    }
    else{
        palette.setColor(QPalette::Text, this->palette().color(QPalette::PlaceholderText));
    }
    edit->setPalette(palette);

    if(!data["active"].toBool()){
        if(placeholder){
            edit->setText(data["name"].toString());
        }
        else{
            edit->setText("");
        }
        _result->setVisible(false);
        return;
    }  
    edit->setText(data["name"].toString());

}

GResultEdit *GPlayOffMatch::result() const
{
    return _result;
}

GTeamEdit *GPlayOffMatch::guest() const
{
    return _guest;
}

GTeamEdit *GPlayOffMatch::host() const
{
    return _host;
}

void GPlayOffMatch::moveTeamsLabels(int w, int h, int matchHeight)
{
    _host->move(w, h - matchHeight - _host->sizeHint().height());
    _guest->move(w, h + matchHeight - _guest->sizeHint().height());

}

void GPlayOffMatch::moveResulEdit(int w, int h)
{
    _result->move(w,h);

}


