#include "gplayoffview.h"
#include <QScrollArea>
#include <cmath>
#include <QWindow>
#include <QVariant>
#include "gexportdialog.h"
#include "cplayoff.h"
#include "astagemodel.h"
#include "gplayoffwidget.h"

GPlayOffView::GPlayOffView(AStageModel* model, QWidget *parent)
    : AStageView(parent)
{
    _model = model;
    _title = model->getName();
    _identificator = model->getIdentificator();
    _playOffWidget = new GPlayOffWidget(model->getData(), _identificator, parent);
    _playOffWidget->setTitle(_title);
    connectPlayOffWidget(_playOffWidget );
    connect(model, SIGNAL(dataChanged()),
            this, SLOT(update()));
}

GPlayOffView::~GPlayOffView()
{

}

AStageView *GPlayOffView::create(AStageModel *model)
{
    return new GPlayOffView(model);
}

QWidget *GPlayOffView::getViewWidget()
{
    QScrollArea* area = new QScrollArea();
    area->setWidget(_playOffWidget);
    area->setFixedSize(_playOffWidget->size());
    area->setAlignment(Qt::AlignCenter);
    area->setFrameShape(QFrame::NoFrame);
    return area;
}

AExportable *GPlayOffView::getExportableWidget()
{
    return _playOffWidget;
}

void GPlayOffView::findText(const QString &text)
{
    _playOffWidget->findText(text);
}

void GPlayOffView::update()
{
    _playOffWidget->setData(_model->getData());

}

void GPlayOffView::connectPlayOffWidget(GPlayOffWidget * widget)
{
    connect(widget, SIGNAL(teamChangeRequested(QVariant,QString,QString)),
            this, SLOT(teamEditChange(QVariant,QString,QString)));
    connect(widget, SIGNAL(teamsReplacementRequested(QVariant,QVariant)),
            this, SLOT(teamsReplacement(QVariant,QVariant)));
    connect(widget, SIGNAL(resultChanged(int,int,QString,QString)),
            this, SLOT(resultEditChange(int,int,QString,QString)));
}






