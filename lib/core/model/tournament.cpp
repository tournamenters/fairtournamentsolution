#include "tournament.h"

#include "cstagesfactory.h"
#include "global_model.h"

#include "amodel.h"
#include "cstats.h"
#include "cnotes.h"
#include "cfieldsmanager.h"
#include "cstartlist.h"
#include "cmatcheslist.h"
#include "cteamstable.h"

Tournament::Tournament(CStagesFactory *creator, QObject *parent)
    :QObject (parent)
{
    _creator = creator;
}

Tournament::~Tournament()
{
    clearTournament();
}

void Tournament::newTournament(const QString &name)
{
    clearTournament();
    _notes = new CNotes(this);
    _models.push_back(_notes);
    _stats = new CStats(this);
    _stats->setTournamentName(name);
    _models.push_back(_stats);
    _matches = new CMatchesList(this);
    _models.push_back(_matches);
}

void Tournament::clearTournament()
{
    _creator->reset();
    qDeleteAll(_stages);
    _stages.clear();
    qDeleteAll(_models);
    _models.clear();
    if(_fields){
        delete _fields;
    }
}

void Tournament::createStartList(const QVariant& teamsData, const QVariant& parameters)
{
    QVariantMap paramsMap = parameters.toMap();
    bool ranked = paramsMap["ranked"].toBool();
    bool descendinly = paramsMap["descendinly"].toBool();
    bool clubs = paramsMap["clubs"].toBool();

    _startList = new CStartList(this);
    _startList->setRanked(ranked);
    _startList->setRankedType(descendinly);
    _startList->setHaveClubs(clubs);
    _startList->createTeams(teamsData);
    _models.push_back(_startList);
    _stats->setTeams(_startList);
}

bool Tournament::addStage(const QString& type, const QString& name, const QVariant& parameters, const QVariant& setting)
{
    if(!setting.toMap().isEmpty()){
        _creator->changeSetting(type, setting);
    }
    if(_startList){
        AStageModel* stage = _creator->createStage(type, name, parameters);
        stage->setParent(this);
        if(stage){
            if(!_stages.empty()){
                _stages.last()->setNextStage(stage);
                _stages.last()->nextStageCare();
            }
            else{
                 stage->setTeams(_startList->getTeamsInValidOrder());
            }
            _stages.push_back(stage);
            if(_matches){
                _matches->addMatches(stage->getMatches());
            }
            if(_fields){
                _fields->addMatches(stage->getMatches());
                _stats->setMatches(_fields->getFields());
            }
            else {
                _stats->setMatches({_matches});
            }
            return true;
        }
    }
    return false;   
}

void Tournament::sheduleFields(const QVariant &parameters)
{
    _fields = _creator->createFieldsManager(parameters);;
    if(_fields){
        for(auto stage : _stages){
            _fields->addMatches(stage->getMatches());
        }
        if(_stats){
            _stats->setMatches(_fields->getFields());
        }
        _models.push_back(_fields);
    }
}

AModel *Tournament::getStatsModel() const
{
    return _stats;
}

AModel *Tournament::getNotesModel() const
{
    return _notes;
}

AModel *Tournament::getStartListModel() const
{
    return _startList;
}

AMatchesModel *Tournament::getMatchesModel() const
{
    return  _matches;
}

QList<AMatchesModel *> Tournament::getFieldsModels() const
{
    if(_fields){
        return _fields->getFields();
    }
     return {};
}

QList<AStageModel *> Tournament::getStagesModels() const
{
    return _stages;
}

void Tournament::fromVariant(const QVariant &variant)
{
    QVariantMap map = variant.toMap();
    newTournament(map["name"].toString());

    _startList = new CStartList(this);
    _startList->fromVariant(variant);

    _stats->setTeams(_startList);
    _creator->fromVariant(map["setting"]);

    for(auto& stageData : map["stages"].toList()){
        QVariantMap stageDataMap = stageData.toMap();
        AStageModel* stage = _creator->createStage(stageDataMap["type"].toString(), stageDataMap["name"].toString(), stageDataMap["parameters"]);
        if(stage){
            if(!_stages.empty()){
                _stages.last()->setNextStage(stage);
            }
            stage->setTeamsFromVariant(stageData, _startList->getTeamsById());
            stage->resultsFromVariant(stageData);
            _stages.push_back(stage);
            _matches->addMatches(stage->getMatches());
        }
    }

    if(map.contains("fields")){
        _fields = _creator->createFieldsManager(map["fields"]);
        for(auto stage : _stages){
            _fields->addMatches(stage->getMatches());
        }
        _stats->setMatches(_fields->getFields());
    }
    else{
        _stats->setMatches({_matches});
    }
    _notes->fromVariant(map["notes"]);
}

QVariant Tournament::toVariant() const
{
    QVariantMap map;
    map["name"] = _stats->getTournamentName();
    map["teams"] = _startList->toVariant();
    map["matches"] = _matches->toVariant();
    if(_fields){
        map["fields"] = _fields->toVariant();
    }
    map["stats"] = _stats->toVariant();
    map["notes"] = _notes->toVariant();

    QVariantList stages;
    for(const auto stage : _stages){
        stages.push_back(stage->toVariant());
    }
    map["stages"] = stages;
    map["setting"] = _creator->toVariant();
    return map;
}
