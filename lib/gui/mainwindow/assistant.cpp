#include "assistant.h"

//#-------------------------------------------------
//#
//# Based on an example from the documentation available on https://doc.qt.io/qt-5/qtassistant-remotecontrol-example.html
//#
//#-------------------------------------------------

#include <QByteArray>
#include <QDir>
#include <QLibraryInfo>
#include <QMessageBox>
#include <QProcess>
#include <QApplication>

Assistant::Assistant()
    : process(nullptr)
{
}

Assistant::~Assistant()
{
    if (process && process->state() == QProcess::Running) {
        process->terminate();
        process->waitForFinished(3000);
    }
    delete process;
}

void Assistant::showDocumentation(const QString &page)
{
    if (!startAssistant()){
        return;
    }
    QByteArray ba("SetSource ");
    ba.append("qthelp://suchomel.ales.tournament.fair/doc/");
    process->write(ba + page.toLocal8Bit() + '\n');
}

bool Assistant::startAssistant()
{
    if (!process){
        process = new QProcess();
    }
    if (process->state() != QProcess::Running) {
        QString app = QApplication::applicationDirPath() + QDir::separator();
        app += QLatin1String("assistant");
        QStringList args;
        args << QLatin1String("-collectionFile")
             << QLatin1String(QApplication::applicationDirPath().toLatin1() + "/documentation/fairtournament.qhc")
             << QLatin1String("-enableRemoteControl");
        process->start(app, args);

        if (!process->waitForStarted()) {
            QMessageBox::critical(nullptr,
                                  QApplication::applicationName(),
                                  tr("Unable to launch Qt Assistant (%1)").arg(app));
            return false;
        }
    }
    return true;
}
