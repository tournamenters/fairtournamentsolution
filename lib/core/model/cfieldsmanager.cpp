#include "cfieldsmanager.h"

#include <QVariant>

#include "cmatcheslist.h"
#include "cmatch.h"
#include "global_model.h"
#include "afieldsassigner.h"

CFieldsManager::CFieldsManager(int numFields, AFieldsAssigner *assigner, QObject *parent)
    : AModel (parent)
{
    _assigner = assigner;
    for (int i =0; i < numFields; i++){
        CMatchesList * field = new CMatchesList(this);
        field->setName(tr("Field") + " " + QString::number(i+1));
        field->addToIdentificator("field_index", i);
        _fields.push_back(field);
    }
}

CFieldsManager::~CFieldsManager()
{
    delete _assigner;
    qDeleteAll(_fields);
    _fields.clear();
}

void CFieldsManager::addMatches(const QList<CMatch *>& matches)
{
    QList<QList<int>> intMatches = getIntList(matches);
    if(maxFieldSize()){
        _assigner->addMatchesToPlan(intMatches);
    }
    else{
        _assigner->planFields(intMatches, _fields.size());
    }
    placeMatches(_assigner->getFieldsPlan(), matches);
}

int CFieldsManager::maxFieldSize() const
{
    int max = 0;
    for (const auto field : _fields) {
        if(field->matchesCount()>max){
            max = field->getMatches().size();
        }
    }
    return max;
}

QList<AMatchesModel *> CFieldsManager::getFields() const
{
    QList<AMatchesModel *> list;
    for(const auto field : _fields){
        list.push_back(field);
    }
    return list;
}

QList<QList<CMatch*>> CFieldsManager::getMatches() const
{
    QList<QList<CMatch*>> list;
    for(const auto field : _fields){
        list.push_back(field->getMatches());
    }
    return list;
}

bool CFieldsManager::execEdit(ExecType type, const QVariant &identificator, const QVariant &data)
{
    if(isItForMe(identificator) && identificator.toMap().contains("field_index")){
        int fieldIndex = identificator.toMap()["field_index"].toInt();
        if(fieldIndex<_fields.size()){
            return _fields[fieldIndex]->execEdit(type, identificator, data);
        }
    }
    return false;
}

QVariant CFieldsManager::getData() const
{
    QVariantList list;
    for(const auto field: _fields){
        list.push_back(field->getData());
    }
    return list;
}

QVariant CFieldsManager::toVariant() const
{
    QVariantMap map;
    map["num_fields"] = _fields.size();
    map["fields"] = getData();
    return map;
}

QList<QList<int>> CFieldsManager::getIntList(const QList<CMatch *> &matches)
{
    QList<QList<int>> intLists;
    for(const auto match : matches){
        intLists.push_back(match->getTeamsIndexes());
    }
    return intLists;
}

void CFieldsManager::placeMatches(const QList<QList<QList<int> > >& intMatches, const QList<CMatch *> &realMatches)
{
    QList<CMatch *> matchesToSort = realMatches;
    int round = maxFieldSize();
    QList<int> intMatch;
    while(!matchesToSort.isEmpty() || round > realMatches.size() + maxFieldSize()){
        for (int field = 0; field < intMatches.size(); field++) {
            intMatch = intMatches[field][round];
            if(intMatch.isEmpty()){
                _fields[field]->addMatch(nullptr);
            }
            else{
                for (const auto match : matchesToSort) {
                    if(match->isThisMatch(intMatch[0], intMatch[1])){
                        _fields[field]->addMatch(match);
                        matchesToSort.removeOne(match);
                        break;
                    }
                }
            }
        }
        round++;
    }
}




