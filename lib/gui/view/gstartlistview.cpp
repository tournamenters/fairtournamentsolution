#include "gstartlistview.h"

#include "amodel.h"
#include "gteameditstable.h"
#include "geditationdialog.h"
#include "geditationtable.h"

GStartListView::GStartListView(AModel *model, QObject *parent)
    : ATeamsView(parent)
{
    _title = model->getName();
    _model = model;
    _identificator = model->getIdentificator();
    createStartListTable();
    connect(_model, SIGNAL(dataChanged()),
            this, SLOT(update()));
}

GStartListView::~GStartListView()
{

}

QWidget *GStartListView::getViewWidget()
{
    return gteStartList;
}

AExportable *GStartListView::getExportableWidget()
{
    return gteStartList;
}

void GStartListView::update()
{
    gteStartList->setData(_model->getData());
}

void GStartListView::editStartListRequest()
{
    GEditDialog editationDialog(gteStartList->getData());
    editationDialog.setWindowTitle(tr("Start List Editation"));
    if(editationDialog.exec() == QDialog::Accepted){
        QVariantMap data;
        data["new"] = editationDialog.getNewData();
        data["old"] = gteStartList->getData();
        emit stateEdited(ExecType::Editation, _identificator, data);
    }
}

void GStartListView::findText(const QString &text)
{
    gteStartList->findText(text);
}

void GStartListView::createStartListTable()
{
    gteStartList = new GTeamEditsTable(_identificator);
    gteStartList->setData(_model->getData());
    gteStartList->setTeamEditRemovable(true);
    gteStartList->setTeamEditDragAndDrop(true);
    gteStartList->setTitle(_title);
    connectTeamsTable(gteStartList);
}
