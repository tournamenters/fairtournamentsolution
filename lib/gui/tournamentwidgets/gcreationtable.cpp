#include "gcreationtable.h"

#include <QAction>
#include <QMenu>
#include <QMessageBox>
#include <QInputDialog>
#include <QHeaderView>

GCreationTable::GCreationTable(QWidget *parent)
    :GTournamentTable (parent),
      _minRows(8)
{
    setRowCount(_minRows);
    setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    connect(this,SIGNAL(cellChanged(int,int)),
            this,SLOT(cellEditationCare(int, int)));
}

void GCreationTable::setData(const QVariant &data)
{
    GTournamentTable::setData(data);
}

void GCreationTable::setVisibleColumn(bool visible, const QString& header)
{
    if(_unremovableColumns.contains(header)){
        QMessageBox::warning(this,tr("Something went wrong while removing a column."),tr("The \"")+ header + tr("\" column cannot be removed."));
        return;
    }
    int col = findColumnIndex(header);
    if(visible && col == -1){
        int numColumns = columnCount();
        insertColumn(numColumns);
        setHorizontalHeaderItem(numColumns, new QTableWidgetItem(header));
        emit columnsChanged();
    }
    if(!visible && col != -1){
        removeColumn(col);
        emit columnsChanged();
    }
}

void GCreationTable::addTableColumn()
{
    bool ok = false;
    QString columnHeader = QInputDialog::getText(this, tr("New column"), tr("Please enter the column name:"),QLineEdit::Normal,QString(),&ok);
    if(ok){
        if (getColumnHeaders().contains(columnHeader)){
            QMessageBox::warning(this,tr("Something went wrong while inserting a column."),tr("Each column header must have a unique name."));
            return;
        }
        int numColumns = columnCount();
        insertColumn(numColumns);
        if(columnHeader.trimmed().isEmpty()){
            columnHeader = QString::number(numColumns+1);
        }
        setHorizontalHeaderItem(numColumns, new QTableWidgetItem(columnHeader));
        emit columnsChanged();
    }
}

void GCreationTable::removeTableRow()
{
    if(isOneRowSelected()){
        removeRow(selectedRanges().front().topRow());
        if(rowCount()< 2){
            setRowCount(_minRows);
        }
    }
    else{
        QMessageBox::warning(this,tr("Something went wrong while removing a row."),tr("Single row could not be removed, too many rows selected."));
    }
}

void GCreationTable::removeTableColumn()
{
    if(isOneColumnSelected()){
        int column = selectedRanges().front().leftColumn();
        if(isColumnEmpty(column) || QMessageBox::Yes == QMessageBox::question(this,tr("Warning!"),tr("Do you really want to remove the column, all its data will by lost."))){
            setVisibleColumn(false, getColumnHeaders()[column]);
        }
    }
    else{
        QMessageBox::warning(this,tr("Something went wrong while removing a column."),tr("Single column could not be removed, too many columns selected."));
    }
}

void GCreationTable::addTableRowAbove()
{
    insertRowNearSelected(false);
}

void GCreationTable::addTableRowBelow()
{
    insertRowNearSelected(true);
}

void GCreationTable::cellEditationCare(int row, int col)
{
    if(_numericColumns.contains(getColumnHeaders()[col])){
        numericControl(row, col);
    }
    if(row >= rowCount()-2){
        setRowCount(rowCount() + 3 - rowCount() + row);
    }
    emit tableChanged();
}

void GCreationTable::showMenu()
{
    if(!mnuContext){
        mnuContext = createFormContextMenu();
    }
    mnuContext->exec(QCursor::pos());
}

QMenu *GCreationTable::createFormContextMenu()
{
    QMenu * menu = new QMenu(this);
    menu->addAction(tr("Add row above"),
                          this, &GCreationTable::addTableRowAbove);
    menu->addAction(tr("Add row below"),
                          this, &GCreationTable::addTableRowBelow);
    menu->addAction(tr("Remove the selected row"),
                          this, &GCreationTable::removeTableRow);
    menu->addAction(tr("&Add new column"),
                          this, &GCreationTable::addTableColumn);
    menu->addAction(tr("&Remove the selected column"),
                          this, &GCreationTable::removeTableColumn);
    menu->addAction(tr("Clear &table"),
                          this, &GCreationTable::clearTable);
    menu->addMenu(createContextMenu());
    return menu;
}

bool GCreationTable::isColumnEmpty(int col)
{
    for(int row=0;rowCount();row++){
        if(getCellText(row,col).isEmpty()){
            return false;
        }
    }
    return true;
}

void GCreationTable::insertRowNearSelected(bool under)
{
    if(isOneRowSelected()){
        int row = selectedRanges().front().topRow();
        insertRow(row+under);
    }
    else{
        QMessageBox::warning(this,tr("Something went wrong while adding a row."),tr("The row could not be added, too many rows selected."));
    }
}

bool GCreationTable::isOneRowSelected()
{
    return  selectedRanges().size() == 1 && selectedRanges().first().rowCount() == 1;
}

bool GCreationTable::isOneColumnSelected()
{
    return selectedRanges().size() == 1 && selectedRanges().first().columnCount() == 1;
}

void GCreationTable::numericControl(int row, int col)
{
    QString text = getCellText(row,col);
    bool isNumber = false;
    text.toInt(&isNumber);
    if(!isNumber && ! text.isEmpty()){
        QMessageBox::warning(this,tr("Something went wrong while editing a column."),tr("Only numbers can be entered to the column."));
        setCellText(row,col,"");
    }
}

int GCreationTable::findColumnIndex(const QString &header) const
{
    for (int col = 0; col< columnCount(); col++){
        if(horizontalHeaderItem(col) && horizontalHeaderItem(col)->text() == header){
            return col;
        }
    }
    return -1;
}

int GCreationTable::nonEmptyCellsCount(const QString &header) const
{
    int nonEmptyCells = 0;
    int col = findColumnIndex(header);
    for (int row = 0; row < rowCount(); row++){
        nonEmptyCells += !getCellText(row, col).isEmpty();
    }
    return nonEmptyCells;
}

void GCreationTable::setMinRows(int minRows)
{
    _minRows = minRows;
    if(rowCount()<_minRows){
        setRowCount(_minRows);
    }
}

void GCreationTable::setUnremovableColumns(const QStringList &unremovableColumns)
{
    _unremovableColumns = unremovableColumns;
}

void GCreationTable::setNumericColumns(const QStringList &numericColumns)
{
    _numericColumns = numericColumns;
}
