#include "gexportdialog.h"
#include <QVBoxLayout>
#include <QCheckBox>
#include <QLabel>
#include <QGroupBox>
#include <QRadioButton>
#include <QApplication>
#include <QDialogButtonBox>
#include "aexportable.h"

GExportDialog::GExportDialog(const QList<AExportable *> &exportables, QWidget *parent, GExportDialog::DefaultSelection selection)
    : QDialog (parent)
{
    setWindowTitle(QApplication::applicationName() + tr("| Export Dialog"));
    QVBoxLayout * layout = new QVBoxLayout();
    layout->addWidget(createTitle());
    layout->addStretch(1);
    layout->addWidget(createMainGroupBox(exportables));
    layout->addStretch(4);
    layout->addWidget(createButtonBox());
    setLayout(layout);
    setCheckStates(selection);
    setMinimumWidth(layout->sizeHint().height()/2);
}

GExportDialog::~GExportDialog()
{
    _complexExportables.clear();
    _simpleExportables.clear();
}

bool GExportDialog::isExportableChosen(AExportable *exportable)
{
    if(_simpleExportables.contains(exportable)){
        return _simpleExportables.value(exportable)->isChecked();
    }
    if(_complexExportables.contains(exportable)){
        return _complexExportables.value(exportable)->isChecked();
    }
    return false;
}

QLabel *GExportDialog::createTitle()
{
    QLabel* label = new QLabel(tr("Please, select elements that you wish to export.        "));
    return label;
}

QGroupBox *GExportDialog::createMainGroupBox(QList<AExportable *> exportables)
{
    QGroupBox* groupBox = new QGroupBox(tr("Objects to export"));
    QVBoxLayout * boxLayout = new QVBoxLayout();
    for(const auto exportable : exportables){
        if(exportable){
            if(exportable->hasMoreParts()){
                boxLayout->addWidget(createCheckableGroupBox(exportable));
            }
            else{
                boxLayout->addWidget(createCheckBox(exportable));
            }
        }
    }
    groupBox->setLayout(boxLayout);
    return groupBox;
}

QGroupBox *GExportDialog::createCheckableGroupBox(AExportable* exportable)
{
    QGroupBox * groupBox = new QGroupBox(exportable->getTitle());
    groupBox->setCheckable(true);
    QVBoxLayout * boxLayout = new QVBoxLayout();
    for(const auto part : exportable->getIndividualParts()){
        boxLayout->addWidget(createCheckBox(part));
    }
    groupBox->setLayout(boxLayout);
    _complexExportables.insert(exportable, groupBox);
    return groupBox;
}

QCheckBox *GExportDialog::createCheckBox(AExportable *exportable)
{
    QCheckBox * stage = new QCheckBox(exportable->getTitle());
    stage->setChecked(true);
    _simpleExportables.insert(exportable, stage);
    return stage;
}

QDialogButtonBox *GExportDialog::createButtonBox()
{
    QDialogButtonBox * buttonBox = new QDialogButtonBox( {QDialogButtonBox::Ok, QDialogButtonBox::Cancel});
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
    return buttonBox;
}

void GExportDialog::setCheckStates(GExportDialog::DefaultSelection selection)
{
    bool check = selection==DefaultSelection::All;
    for(const auto groupBox : _complexExportables.values()){
        groupBox->setChecked(check);
    }
    for(const auto checkBox : _simpleExportables.values()){
        checkBox->setChecked(check);
    }
}

