#include "gdraganddropmanager.h"
#include <QMouseEvent>
#include <QApplication>

GDragAndDropManager::GDragAndDropManager(QObject *parent)
    : QObject(parent)
{
    _mousePressed = false;
}

void GDragAndDropManager::mousePressEventCare(QMouseEvent *event)
{
    if(!_mousePressed){
        if (event->button() == Qt::LeftButton){
            _dragStartPosition = event->pos();
            _mousePressed = true;
        }
    }
    event->accept();
}

bool GDragAndDropManager::mouseMoveEventCare(QMouseEvent *event)
{
    if(_mousePressed){
        if (!(event->buttons() & Qt::LeftButton)){
            return false;
        }
        if ((event->pos() - _dragStartPosition).manhattanLength() < QApplication::startDragDistance()){
            return false;
        }
        _mousePressed = false;
        return true;
    }
    event->accept();
    return false;
}

