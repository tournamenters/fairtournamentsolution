#include "arobinroundmatchesorderer.h"

AMatchesOrderer::AMatchesOrderer()
{

}

AMatchesOrderer::~AMatchesOrderer()
{

}

QList<QList<int> > AMatchesOrderer::prepareStandardFirstRound(int numTeams)
{
    numTeams += numTeams % 2;
    QList<QList<int>> matches;
    int host = 0;
    int guest = numTeams - 1;
    while (host < guest) {
        matches.push_back({host, guest});
        host++;
        guest--;
    }
    return matches;
}

QPair<QList<int>, QList<int> > AMatchesOrderer::getHostAndGuestsLists(const QList<QList<int> > &matches)
{
    QPair<QList<int>, QList<int> > hostsGuests;
    for (const auto& match : matches) {
        hostsGuests.first.push_back(match[0]);
        hostsGuests.second.push_back(match[1]);
    }
    return hostsGuests;
}

QList<QList<int> > AMatchesOrderer::getMatches(const QPair<QList<int>, QList<int> >& hostsAndGuests)
{
    QList<QList<int> > matches;
    for(int i = 0; i < hostsAndGuests.first.size(); i++){
        matches.push_back({hostsAndGuests.first[i], hostsAndGuests.second[i]});
    }
    return matches;
}

QList<QList<int> > AMatchesOrderer::rotateMatrix(const QList<QList<int> > &matches, const QPair<int, int> &fixedPosition)
{
    QPair<QList<int>, QList<int> > hostsGuests = getHostAndGuestsLists(matches);
    int fixedTeam;
    if(fixedPosition.second == 0){
        fixedTeam = hostsGuests.first[fixedPosition.first];
        hostsGuests.first.removeAt(fixedPosition.first);
    }
    else{
        fixedTeam = hostsGuests.second[fixedPosition.first];
        hostsGuests.second.removeAt(fixedPosition.first);
    }

    hostsGuests.first.push_front(hostsGuests.second.first());
    hostsGuests.second.push_back(hostsGuests.first.last());
    hostsGuests.first.removeLast();
    hostsGuests.second.removeFirst();

    if(fixedPosition.second == 0){
        hostsGuests.first.insert(fixedPosition.first, fixedTeam);
    }
    else{
        hostsGuests.second.insert(fixedPosition.first, fixedTeam);
    }
    return getMatches(hostsGuests);
}

QList<QList<int> > AMatchesOrderer::removeMatchesWithTeam(const QList<QList<int> >& matches, int teamIndex)
{
    QList<QList<int> > newMatches;
    for(const auto& match : matches){
        if(!match.contains(teamIndex)){
            newMatches.push_back(match);
        }
    }
    return newMatches;
}

QList<int> AMatchesOrderer::switchHostAndGuest(QList<int> match)
{
    QList<int> matchNew = match;
    matchNew.push_front(match.last());
    matchNew.removeLast();
    return matchNew;
}

LBergerTables::LBergerTables()
{

}

LBergerTables::~LBergerTables()
{

}

QList<QList<int>> LBergerTables::planMatches(int numTeams)
{
    int numTeamsEven = numTeams + numTeams % 2;
    QPair<int,int> fixedTeam(0,1);
    QList<QList<int> > matches = prepareStandardFirstRound(numTeamsEven);
    QList<QList<int> > actualRound = matches;
    QList<QList<int> > modifiedRound;
    for (int round = 1;round < numTeamsEven-1;round++) {
        for(int i=0; i<(numTeamsEven-2)/2; i++){
            actualRound = rotateMatrix(actualRound,fixedTeam);
        }
        modifiedRound = actualRound;
        if(round%2){
            modifiedRound[0] = switchHostAndGuest(modifiedRound.first());
        }
        matches.append(modifiedRound);
    }
    if(numTeams % 2){
        matches = removeMatchesWithTeam(matches, numTeams);
        matches = optimalizeOddTeamsPlan(matches, numTeams);
    }
    return matches;
}

AMatchesOrderer *LBergerTables::copy()
{
    return new LBergerTables();
}

AMatchesOrderer *LBergerTables::create()
{
    return new LBergerTables();
}

QList<QList<int> > LBergerTables::optimalizeOddTeamsPlan(const QList<QList<int> >& matches, int numTeams)
{
    int roundSize = 0;
    int firstRoundFirstMatchFirstTeam = 0;
    while(roundSize<matches.size()){
        if(matches[roundSize].contains(0)){
            firstRoundFirstMatchFirstTeam = roundSize;
            break;
        }
        roundSize += floor(numTeams/2);
    }
    QList<QList<int> > newMatches;
    for(int i = firstRoundFirstMatchFirstTeam; i<matches.size();i++){
        newMatches.push_back(matches[i]);
    }
    for(int i = 0; i<firstRoundFirstMatchFirstTeam;i++){
        newMatches.push_back(matches[i]);
    }
    return newMatches;
}

LRotation::LRotation()
{

}

LRotation::~LRotation()
{

}

QList<QList<int> > LRotation::planMatches(int numTeams)
{
    int numTeamsEven = numTeams + numTeams % 2;
    QPair<int,int> fixedTeam(0,0);
    QList<QList<int> > matches = prepareStandardFirstRound(numTeamsEven);
    QList<QList<int>> actualRound = matches;
    for (int round = 1;round < numTeamsEven-1;round++) {
        actualRound = rotateMatrix(actualRound,fixedTeam);
        matches.append(actualRound);
    }
    if(numTeams % 2){
        matches = removeMatchesWithTeam(matches, numTeams);
    }
    return matches;
}

AMatchesOrderer *LRotation::copy()
{
    return new LRotation();
}

AMatchesOrderer *LRotation::create()
{
    return new LRotation();
}


LKirkman::LKirkman()
{

}

LKirkman::~LKirkman()
{

}

QList<QList<int> > LKirkman::planMatches(int numTeams)
{
    int numTeamsEven = numTeams + numTeams % 2;
    QPair<int,int> fixedTeam(0,0);
    QList<QList<int> > matches = prepareStandardFirstRound(numTeamsEven);
    for (int i = 0; i< matches.size();i++) {
        matches[i] = switchHostAndGuest(matches[i]);
    }
    QList<QList<int> > actualRound = matches;
    matches = switchHostAndGuestInEvenMatches(matches);
    QList<QList<int> > modifiedRound;
    for (int round = 1;round < numTeamsEven-1;round++) {
        actualRound = rotateMatrix(actualRound,fixedTeam);
        modifiedRound = switchHostAndGuestInEvenMatches(actualRound);
        if(round % 2){
            modifiedRound[0] = switchHostAndGuest(modifiedRound.first());
        }
        modifiedRound = moveMatchWithFirstUp(modifiedRound);
        matches.append(modifiedRound);
    }
    if(numTeams % 2){
        matches = removeMatchesWithTeam(matches, numTeams);
    }
    return matches;
}

AMatchesOrderer *LKirkman::copy()
{
    return new LKirkman();
}

AMatchesOrderer *LKirkman::create()
{
    return new LKirkman();
}

QList<QList<int> > LKirkman::switchHostAndGuestInEvenMatches(const QList<QList<int> > &matches)
{
    QList<QList<int> > newMatches;
    for(int i = 1; i<=matches.size(); i++){
        if(i % 2 == 0){
            newMatches.push_back(switchHostAndGuest(matches[i-1]));
        }
        else{
            newMatches.push_back(matches[i-1]);
        }
    }
    return newMatches;
}

QList<QList<int> > LKirkman::moveMatchWithFirstUp(const QList<QList<int> >& matches)
{
    QList<QList<int> > newMatches;
    for(const auto& match : matches){
        if(match.contains(0)){
            newMatches.push_front(match);
        }
        else{
            newMatches.push_back(match);
        }
    }
    return newMatches;
}
