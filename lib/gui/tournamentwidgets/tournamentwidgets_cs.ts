<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ">
<context>
    <name>GCreationTable</name>
    <message>
        <location filename="gcreationtable.cpp" line="27"/>
        <location filename="gcreationtable.cpp" line="84"/>
        <source>Something went wrong while removing a column.</source>
        <translation>Něco se pokazilo při odstraňování sloupce.</translation>
    </message>
    <message>
        <location filename="gcreationtable.cpp" line="27"/>
        <source>The &quot;</source>
        <translation>Sloupec &quot;</translation>
    </message>
    <message>
        <location filename="gcreationtable.cpp" line="27"/>
        <source>&quot; column cannot be removed.</source>
        <translation>&quot; nemohl být odstraněn.</translation>
    </message>
    <message>
        <location filename="gcreationtable.cpp" line="46"/>
        <source>New column</source>
        <translation>Nový sloupec</translation>
    </message>
    <message>
        <location filename="gcreationtable.cpp" line="46"/>
        <source>Please enter the column name:</source>
        <translation>Prosím zvolte jmého sloupce:</translation>
    </message>
    <message>
        <location filename="gcreationtable.cpp" line="49"/>
        <source>Something went wrong while inserting a column.</source>
        <translation>Něco se pokazilo při vkládání sloupce.</translation>
    </message>
    <message>
        <location filename="gcreationtable.cpp" line="49"/>
        <source>Each column header must have a unique name.</source>
        <translation>Každý sloupec musí mít jedinečné jméno.</translation>
    </message>
    <message>
        <location filename="gcreationtable.cpp" line="71"/>
        <source>Something went wrong while removing a row.</source>
        <translation>Něco se pokazilo při odstraňování řádku.</translation>
    </message>
    <message>
        <location filename="gcreationtable.cpp" line="71"/>
        <source>Single row could not be removed, too many rows selected.</source>
        <translation>Řádek nemohl být odstraněn. Bylo jich vybráno více.</translation>
    </message>
    <message>
        <location filename="gcreationtable.cpp" line="79"/>
        <source>Warning!</source>
        <translation>Pozor!</translation>
    </message>
    <message>
        <location filename="gcreationtable.cpp" line="79"/>
        <source>Do you really want to remove the column, all its data will by lost.</source>
        <translation>Opravdu chcete odstranit sloupec? Jeho data budou stracena.</translation>
    </message>
    <message>
        <location filename="gcreationtable.cpp" line="84"/>
        <source>Single column could not be removed, too many columns selected.</source>
        <translation>Sloupec nemohl být odstraněn. Bylo jich vybráno více.</translation>
    </message>
    <message>
        <location filename="gcreationtable.cpp" line="120"/>
        <source>Add row above</source>
        <translation>Přidat řádek výše</translation>
    </message>
    <message>
        <location filename="gcreationtable.cpp" line="122"/>
        <source>Add row below</source>
        <translation>Přidat řádek níže</translation>
    </message>
    <message>
        <location filename="gcreationtable.cpp" line="124"/>
        <source>Remove the selected row</source>
        <translation>Odstraň vybraný řádek</translation>
    </message>
    <message>
        <location filename="gcreationtable.cpp" line="126"/>
        <source>&amp;Add new column</source>
        <translation>&amp;Přidej sloupec</translation>
    </message>
    <message>
        <location filename="gcreationtable.cpp" line="128"/>
        <source>&amp;Remove the selected column</source>
        <translation>&amp;Odstraň vybraný sloupec</translation>
    </message>
    <message>
        <location filename="gcreationtable.cpp" line="130"/>
        <source>Clear &amp;table</source>
        <translation>Vyčisti tabulku</translation>
    </message>
    <message>
        <location filename="gcreationtable.cpp" line="153"/>
        <source>Something went wrong while adding a row.</source>
        <translation>Něco se pokazilo při přidávání řádku.</translation>
    </message>
    <message>
        <location filename="gcreationtable.cpp" line="153"/>
        <source>The row could not be added, too many rows selected.</source>
        <translation>Řádek nemohl být odstraňen. Bylo jich vybráno příliš mnoho.</translation>
    </message>
    <message>
        <location filename="gcreationtable.cpp" line="173"/>
        <source>Something went wrong while editing a column.</source>
        <translation>Něco se pokazilo při editováníí sloupce.</translation>
    </message>
    <message>
        <location filename="gcreationtable.cpp" line="173"/>
        <source>Only numbers can be entered to the column.</source>
        <translation>Pouze čísla mohou být vložena do tohoto sloupce.</translation>
    </message>
</context>
<context>
    <name>GDragableResultsToolBar</name>
    <message>
        <location filename="gdragabletoolbars.cpp" line="46"/>
        <source>Remove</source>
        <translation>Odstraň</translation>
    </message>
    <message>
        <location filename="gdragabletoolbars.cpp" line="47"/>
        <source>* ot.</source>
        <translation>* pp.</translation>
    </message>
</context>
<context>
    <name>GEditationTable</name>
    <message>
        <location filename="geditationtable.cpp" line="60"/>
        <source>New </source>
        <translation>Nový </translation>
    </message>
</context>
<context>
    <name>GGroupEditationDialog</name>
    <message>
        <location filename="ggrouptable.cpp" line="213"/>
        <source>Please decide on the order of the following participants or let the program do that by chance.</source>
        <translation>Prosím rozhodněte o pořadí účastníků v tabulce níže, nebo nechte program určit pořadí náhodně.</translation>
    </message>
    <message>
        <location filename="ggrouptable.cpp" line="252"/>
        <source>Draw Values</source>
        <translation>Losuj pořadí</translation>
    </message>
    <message>
        <location filename="ggrouptable.cpp" line="257"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="ggrouptable.cpp" line="261"/>
        <source>Save Changes</source>
        <translation>Ulož změny</translation>
    </message>
</context>
<context>
    <name>GGroupTable</name>
    <message>
        <location filename="ggrouptable.cpp" line="126"/>
        <source>Click to edit value</source>
        <translation>Stiskněte pro editování pořadí</translation>
    </message>
</context>
<context>
    <name>GPlayOffWidget</name>
    <message>
        <location filename="gplayoffwidget.cpp" line="86"/>
        <source>Save as image...</source>
        <translation>Ulož jako obrázek...</translation>
    </message>
    <message>
        <location filename="gplayoffwidget.cpp" line="88"/>
        <source>Copy as image</source>
        <translation>Kopíruj jako obrázek</translation>
    </message>
</context>
<context>
    <name>GResultEdit</name>
    <message>
        <location filename="gresultedit.cpp" line="16"/>
        <location filename="gresultedit.cpp" line="17"/>
        <source>Result format: 2 : 0</source>
        <translation>Formát výsledku: 2 : 0</translation>
    </message>
    <message>
        <location filename="gresultedit.cpp" line="68"/>
        <source>Clear</source>
        <translation>Vymazat</translation>
    </message>
    <message>
        <location filename="gresultedit.cpp" line="70"/>
        <source>Standard Menu</source>
        <translation>Klasické menu</translation>
    </message>
    <message>
        <location filename="gresultedit.cpp" line="78"/>
        <source>Draw</source>
        <translation>Remíza</translation>
    </message>
    <message>
        <location filename="gresultedit.cpp" line="87"/>
        <source>Host win</source>
        <translation>Vyhra domácí</translation>
    </message>
    <message>
        <location filename="gresultedit.cpp" line="88"/>
        <source>Guest win</source>
        <translation>Vyhra hosté</translation>
    </message>
</context>
<context>
    <name>GTeamEdit</name>
    <message>
        <location filename="gteamedit.cpp" line="19"/>
        <source>Participant name</source>
        <translation>Jméno účastníka</translation>
    </message>
    <message>
        <location filename="gteamedit.cpp" line="66"/>
        <source>Rename</source>
        <translation>Přejmenuj</translation>
    </message>
    <message>
        <location filename="gteamedit.cpp" line="69"/>
        <source>Remove</source>
        <translation>Odstraň</translation>
    </message>
    <message>
        <location filename="gteamedit.cpp" line="71"/>
        <source>Restore</source>
        <translation>Obnov</translation>
    </message>
</context>
<context>
    <name>GTournamentTable</name>
    <message>
        <location filename="gtournamenttable.cpp" line="274"/>
        <source>Export table</source>
        <translation>Exportuj tabulku</translation>
    </message>
    <message>
        <location filename="gtournamenttable.cpp" line="276"/>
        <source>Copy table as...</source>
        <translation>Kopíruj tabulku jako...</translation>
    </message>
    <message>
        <location filename="gtournamenttable.cpp" line="277"/>
        <location filename="gtournamenttable.cpp" line="287"/>
        <source>Image</source>
        <translation>Obrázek</translation>
    </message>
    <message>
        <location filename="gtournamenttable.cpp" line="279"/>
        <location filename="gtournamenttable.cpp" line="289"/>
        <source>HTML table</source>
        <translation>HTML tabulka</translation>
    </message>
    <message>
        <location filename="gtournamenttable.cpp" line="281"/>
        <location filename="gtournamenttable.cpp" line="291"/>
        <source>CSV table</source>
        <translation>CSV tabulka</translation>
    </message>
    <message>
        <location filename="gtournamenttable.cpp" line="283"/>
        <location filename="gtournamenttable.cpp" line="293"/>
        <source>Plain Text</source>
        <translation>Obyčejný text</translation>
    </message>
    <message>
        <location filename="gtournamenttable.cpp" line="286"/>
        <source>Save table as...</source>
        <translation>Ulož tabulku jako...</translation>
    </message>
</context>
</TS>
