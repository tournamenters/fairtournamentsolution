#ifndef GSTAGEEDITORTABS_H
#define GSTAGEEDITORTABS_H

#include <QScrollArea>
#include <QList>

class QLineEdit;
class QLabel;
class QSpinBox;
class QGroupBox;
class QComboBox;
class QPushButton;
class GPointsSettingBox;

class AStageEditorTab: public QScrollArea
{
    Q_OBJECT

public:
    AStageEditorTab(QWidget * parent = nullptr);
    ~AStageEditorTab() override;

    virtual void setNumTeams(int numTeams);
    virtual void setNumTeamsRanges(int min, int max);

    virtual int numTeams();
    int getIndex() const;
    void setIndex(int index);
    void setName(const QString& name);

    virtual QVariant getData() const;

signals:
    void numTeamsChanged();
    void nameChanged(int index, const QString& name);
    void dataChanged(int index, const QVariant& getData);

protected:
    void dataChange();
    void addItemsToComboBox(QComboBox* box, const QMap<QString,QString>& keyLabelPossibilities);

    QLineEdit * leName;
    QSpinBox * sbNumTeams;
    int _index;
    QVariant _data;

private slots:
    void nameChange();

private:
    QGroupBox *createNameGroupBox();
    QSpinBox *createNumTeamsSpinBox();
    QLineEdit *createNameLineEdit();

};


class GGroupsEditorTab: public AStageEditorTab
{
    Q_OBJECT

public:
    GGroupsEditorTab(QWidget * parent = nullptr);
    ~GGroupsEditorTab() override;
    void setNumTeams(int numTeams) override;
    void setNumTeamsRanges(int minTeams, int maxTeams) override;

    static AStageEditorTab* __stdcall create();

private slots:
    void numTeamsChange();
    void numGroupsChange();
    void sizeGroupsChange();
    void createDataVariant();
    void showAdvancedSetting();

private:
    void createParametersSpinBoxes();
    void createAdvancedSettingDialog();
    QGroupBox * createParametersBox();
    QGroupBox * createAlgorithmsBox();
    QComboBox * createCreationComboBox();
    QComboBox * createMatchesComboBox();
    QGroupBox * createAdvancedSettingBox();
    QPushButton * createAdvancedSettingButton();
    QVariantMap getParametersData();
    QVariantMap getSettingData();

    QSpinBox * sbNumGroups;
    QSpinBox * sbSizeGroups;
    QComboBox * cbAlgorithmCreation;
    QComboBox * cbAlgorithmMatchesOrder;
    GPointsSettingBox * ordererBox;
    QDialog * otherSettingDialog;
};

class GPlayOffEditorTab: public AStageEditorTab
{
    Q_OBJECT

public:
    GPlayOffEditorTab(QWidget * parent = nullptr);
    ~GPlayOffEditorTab();

    static AStageEditorTab* __stdcall create();

private slots:
    void createDataVariant();

private:
    QGroupBox * createParametersBox();
    QGroupBox * createAlgorithmsBox();
    QComboBox * createCreationComboBox();
    QComboBox * cbAlgorithmCreation;

};

class GRobinRoundEditorTab: public AStageEditorTab
{
    Q_OBJECT

public:
    GRobinRoundEditorTab(QWidget * parent = nullptr);
    ~GRobinRoundEditorTab() override;

    static AStageEditorTab* __stdcall create();

private slots:
    void createDataVariant();
    void showAdvancedSetting();

private:
    QGroupBox * createParametersBox();
    QGroupBox * createAlgorithmsBox();
    QComboBox * createMatchesComboBox();
    QGroupBox * createAdvancedSettingBox();
    void createAdvancedSettingDialog();
    QPushButton * createAdvancedSettingButton();

    QComboBox * cbAlgorithmMatchesOrder;
    GPointsSettingBox * ordererBox;
    QDialog * otherSettingDialog;

};
#endif // GSTAGEEDITORTABS_H
