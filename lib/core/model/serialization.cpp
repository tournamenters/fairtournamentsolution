#include "serialization.h"
#include <QFile>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QVariant>


void CXMLSerializer::save(const ASerializable *serializable, const QString &filepath, const QString &rootName)
{
    QFile file(filepath);
    if (!file.open(QFile::WriteOnly))
        return;
    QXmlStreamWriter stream(&file);
    stream.setAutoFormatting(true);
    stream.writeStartDocument();
    writeVariantToStream(rootName, serializable->toVariant(), stream);
    stream.writeEndDocument();
    file.close();
}

void CXMLSerializer::load(ASerializable *serializable, const QString &filepath)
{
    QFile file(filepath);
    if (!file.open(QFile::ReadOnly))
        return;
    QXmlStreamReader stream(&file);
    stream.readNextStartElement();
    serializable->fromVariant(readVariantFromStream(stream));
    file.close();
}

void CXMLSerializer::writeVariantToStream(const QString &nodeName, const QVariant &variant, QXmlStreamWriter &stream)
{
    stream.writeStartElement(nodeName);
    stream.writeAttribute("type", variant.typeName());
    switch(variant.type()){
    case QVariant::Type::List:
        writeVariantListToStream(variant,stream);
        break;
    case QVariant::Type::Map:
        writeVariantMapToStream(variant, stream);
        break;
    default:
        writeVariantValueToStream(variant,stream);
        break;
    }
    stream.writeEndElement();
}

void CXMLSerializer::writeVariantMapToStream(const QVariant &variant, QXmlStreamWriter &stream)
{
    QVariantMap map = variant.toMap();
    for(const auto & key : map.keys()){
        writeVariantToStream(key,map.value(key),stream);
    }
}

void CXMLSerializer::writeVariantListToStream(const QVariant &variant, QXmlStreamWriter &stream)
{
    for (const auto &node : variant.toList()){
        writeVariantToStream("item", node, stream);
    }
}

void CXMLSerializer::writeVariantValueToStream(const QVariant &variant, QXmlStreamWriter &stream)
{
    stream.writeCharacters(variant.toString());
}

QVariant CXMLSerializer::readVariantFromStream(QXmlStreamReader &stream)
{
    QXmlStreamAttributes attributes = stream.attributes();
    QString typeString = attributes.value("type").toString();
    QVariant variant;
    switch(QVariant::nameToType(typeString.toStdString().c_str())){
    case QMetaType::QVariantList:
        variant = readVariantListFromStream(stream);
        break;
    case QMetaType::QVariantMap:
        variant = readVariantMapFromStream(stream);
        break;
    default:
        variant = readVariantValueFromStream(stream);
        break;
    }
    return variant;
}

QVariant CXMLSerializer::readVariantValueFromStream(QXmlStreamReader &stream)
{
    QXmlStreamAttributes attributes = stream.attributes();
    QString typeString = attributes.value("type").toString();
    QString dataString = stream.readElementText();
    QVariant variant(dataString);
    variant.convert(QVariant::nameToType(typeString.toStdString().c_str()));
    return variant;
}

QVariant CXMLSerializer::readVariantListFromStream(QXmlStreamReader &stream)
{
    QVariantList list;
    while(stream.readNextStartElement()){
        list.push_back(readVariantFromStream(stream));
    }
    return list;
}

QVariant CXMLSerializer::readVariantMapFromStream(QXmlStreamReader &stream)
{
    QVariantMap map;
    while(stream.readNextStartElement()){
        map.insert(stream.name().toString(), readVariantFromStream(stream));
    }
    return map;
}
