#ifndef APPSETTER_H
#define APPSETTER_H

#include <QObject>
#include <QMap>
#include <QFont>

class MainWindow;
class QTranslator;

class AppSetter : public QObject
{

    Q_OBJECT

public:
    ~AppSetter();
    static AppSetter* getInstance()
    {
        static AppSetter instance;
        return &instance;
    }

    AppSetter(AppSetter const&) = delete;
    void operator=(AppSetter const&)  = delete;

    void setUI(MainWindow* view);
    QStringList getPossibleLanguages();
    QStringList getPossiblePalettes();
    QStringList getPossibleStyles();

    void readSettings();
    void writeSetting();

    void registerCustomStyle(const QString& key, const QString& path);

public slots:
    void changeLanguageRequest(const QString& languageKey);
    void changeFontRequest(const QFont& font);
    void changeThemeRequest();

private:
    explicit AppSetter(QObject* parent = nullptr);
    void setLanguage(QString languageKey);
    void setStyle(QString styleKey);
    void setPalette(const QString& paletteKey);
    void setFont(const QFont& font);
    void sendResetSuggestion();
    void addTranslator(const QString& name, const QString& path);

    QString _languageKey;
    QString _styleKey;
    QString _paletteKey;
    MainWindow *_view;
    QMap<QString,QString> _customStylesPaths;
    QList<QString> _possibleStylesKeys;
    QList<QString> _possiblePalettesKeys;
    QList<QString> _possibleLanguages;
    QList<QTranslator*> _installedTranslators;
};


#endif // APPSETTER_H
