#ifndef EDITATIONCOMMAND_H
#define EDITATIONCOMMAND_H
#include <QUndoCommand>
#include <QVariant>
class AModel;

class EditationCommand: public QUndoCommand
{

public:
    EditationCommand(AModel *model, const QVariant& identificator, const QVariant& data, QUndoCommand *parent = nullptr);
    ~EditationCommand() override;

    void undo() override;
    void redo() override;

private:
    AModel * _model;
    QVariant _identificator;
    QVariant _dataNew;
    QVariant _dataOld;
};

#endif // EDITATIONCOMMAND_H
