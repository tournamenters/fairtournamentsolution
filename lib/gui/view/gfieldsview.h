#ifndef GFIELDSVIEW_H
#define GFIELDSVIEW_H

#include "aview.h"
#include <QPointer>

class QLayout;
class QLabel;
class QSplitter;
class QScrollArea;
class AMatchesModel;
class GMatchesTableView;

class GFieldsView: public AMatchesView
{
    Q_OBJECT

public:
    GFieldsView(const QList<AMatchesModel *> &models, QWidget *parent = nullptr);
    ~GFieldsView() override;

    QWidget* getViewWidget() override;
    AExportable * getExportableWidget() override;

    QList<AMatchesView* > getFieldViews() const;    

public slots:
    void update() override;
    void findText(const QString& text) override;

private:
    QLabel * createTitleLabel(const QString& title);
    QWidget * createViewWidget();
    AExportable * createExportable();
    QLayout * createFieldLayout(GMatchesTableView* field);
    QScrollArea * createFieldScrollArea(GMatchesTableView* field);

    QList<AMatchesModel *> _models;
    QList<GMatchesTableView *> _fieldViews;
    QPointer<QSplitter> _viewWidget;
    AExportable* _exportable;
};

#endif // GFIELDSVIEW_H
