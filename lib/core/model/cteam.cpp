#include "cteam.h"
#include <QVariant>

CTeam::CTeam(int id, QObject *parent)
    : QObject(parent)
{
    _id = id;
    _rank = -1;
    _active = true;
}

CTeam::CTeam(int id, const QString &name, QObject *parent)
    : CTeam(id, parent)
{
    _name = name;
}

CTeam::~CTeam()
{

}

void CTeam::setClub(const QString &value)
{
    _club = value;
}

void CTeam::setRank(int value)
{
    _rank = value;
}

void CTeam::setActive(bool active)
{
    if(_active!=active){
        _active = active;
        emit updated();
        emit activeChanged();
    }
}

void CTeam::rename(const QString& newName)
{
    if(newName != _name){
        _name = newName;
        emit updated();
    }
}

int CTeam::getRank() const
{
    return _rank;
}

int CTeam::getID() const
{
    return _id;
}

QString CTeam::getClub() const
{
    return _club;
}

bool CTeam::isActive() const
{
    return _active;
}

QString CTeam::toString() const
{
    return _name;
}

QStringList CTeam::toStringList() const
{
    QStringList list;
    list.push_back(_name);
    list.push_back(_club);
    return list;
}

QVariant CTeam::toVariant() const
{
    QVariantMap map;
    map["id"] = _id;
    map["club"] = _club;
    map["name"] = _name;
    map["rank"] = _rank;
    map["active"] = _active;
    return map;
}

void CTeam::swapData(CTeam *first, CTeam *second)
{
    if (first && second && first != second){
        int rank = first->_rank;
        int id = first->_id;
        QString name = first->_name;
        QString club = first->_club;

        first->_rank = second->_rank ;
        first->_id = second->_id;
        first->_name = second->_name;
        first->_club = second->_club;

        second->_rank = rank ;
        second->_id = id;
        second->_name = name;
        second->_club = club;

        emit first->updated();
        emit second->updated();
    }
}

