#include "gnotesview.h"

#include <QTextEdit>
#include <QVariant>
#include <QVBoxLayout>
#include <QMap>

#include "amodel.h"

GNotesView::GNotesView(AModel *model, QWidget *parent) : AView(parent)
{
    _model = model;
    _title = tr("Notes");
    _identificator = model->getIdentificator();
    teNotes = createTextEdit();
    update();
    connect(model, SIGNAL(dataChanged()),
            this, SLOT(update()));
}

GNotesView::~GNotesView()
{

}

QWidget *GNotesView::getViewWidget()
{
    return teNotes;
}

void GNotesView::update()
{
    teNotes->blockSignals(true);
    teNotes->setText(_model->getData().toString());
    teNotes->blockSignals(false);
}

void GNotesView::dataChange()
{
    emit stateEdited(ExecType::Rewrite, _identificator, QVariant(teNotes->toPlainText()));
}

QTextEdit *GNotesView::createTextEdit()
{
    QTextEdit* textEdit = new QTextEdit();
    textEdit->setPlaceholderText(tr("Write your notes here..."));
    connect(textEdit, SIGNAL(textChanged()),
            this, SLOT(dataChange()));
    return textEdit;
}
