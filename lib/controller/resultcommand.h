#ifndef RESULTCOMMAND_H
#define RESULTCOMMAND_H

#include <QUndoCommand>
#include <QVariant>

class AMatchesModel;

class ResultCommand : public QUndoCommand
{

public:
    ResultCommand(AMatchesModel *model, const QVariant& identificator, const QString& newResult, const QString& oldResult, QUndoCommand *parent = nullptr);
    ~ResultCommand() override;

    void undo() override;
    void redo() override;

private:
    AMatchesModel * _model;
    QVariant _identificator;
    QString _newResult;
    QString _oldResult;
};
#endif // RESULTCOMMAND_H
