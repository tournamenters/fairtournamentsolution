#ifndef LGROUPSDRAWERFACTORY_H
#define LGROUPSDRAWERFACTORY_H
#include "agroupsdrawer.h"

#include <QMap>
#include <QObject>

typedef AGroupsDrawer* (__stdcall * FcnCreateGroupsDrawer)(void);

class LGroupsDrawerFactory: public QObject
{
    Q_OBJECT

public:
    ~LGroupsDrawerFactory();

    static LGroupsDrawerFactory *Instance()
    {
        static LGroupsDrawerFactory instance;
        return &instance;
    }

    LGroupsDrawerFactory(LGroupsDrawerFactory const& other) = delete;
    void operator=(LGroupsDrawerFactory const& other) = delete;

    QMap<QString, QString> getPossibleTypesKeyLabelMap();
    void registerOrdererType(const QString &type, const QString &label, FcnCreateGroupsDrawer create);
    AGroupsDrawer * createDrawer(const QString &type);

private:
    LGroupsDrawerFactory();
    QMap<QString, FcnCreateGroupsDrawer> _registeredTypes;
    QMap<QString, QString> _typeLabelMap;
};

#endif // LGROUPSDRAWERFACTORY_H
