#ifndef CSTATS_H
#define CSTATS_H
#include "amodel.h"

class CTeam;
class CFieldsManager;
class CStartList;

class CStats: public AModel
{
    Q_OBJECT

public:
    explicit CStats(QObject *parent = nullptr);
    ~CStats() override;

    void setTournamentName(const QString& name);
    void setMatches(const QList<AMatchesModel*>& fields);
    void setTeams(CStartList* startList);

    QVariant getData() const override;
    QString getTournamentName() const;
    QVariant toVariant();

public slots:
    bool execEdit(ExecType type, const QVariant& identificator, const QVariant& data) override;

private slots:    
    void matchChange();
    void teamChange();

private:
    void updateNumbers();
    void resetMatches();

    int _numTeams;
    int _numMatches;
    int _numMatchesPlayed;
    int _maxSizeField;
    int _maxUnplayedMatches;

    QString _tournamentName;

    QList<AMatchesModel*> _fields;
    CStartList* _startList;
};

#endif // CSTATS_H
