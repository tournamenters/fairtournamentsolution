<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ">
<context>
    <name>LFieldsAssignerFactory</name>
    <message>
        <location filename="lfieldsassignerfactory.cpp" line="5"/>
        <source>No Added Requirements</source>
        <translation>Žádné zvláštní požadavky</translation>
    </message>
    <message>
        <location filename="lfieldsassignerfactory.cpp" line="6"/>
        <source>Max Changeovers</source>
        <translation>Co nejvíce různých hřišť pro každého účastníka</translation>
    </message>
    <message>
        <location filename="lfieldsassignerfactory.cpp" line="7"/>
        <source>Min Transitions</source>
        <translation>Co nejmenší počet přesunů účastníků</translation>
    </message>
</context>
<context>
    <name>LGroupsDrawerFactory</name>
    <message>
        <location filename="lgroupsdrawerfactory.cpp" line="5"/>
        <source>Advantegeous</source>
        <translation>Zvýhodňovací</translation>
    </message>
    <message>
        <location filename="lgroupsdrawerfactory.cpp" line="6"/>
        <source>Equal</source>
        <translation>Srovnávací</translation>
    </message>
    <message>
        <location filename="lgroupsdrawerfactory.cpp" line="7"/>
        <source>Modified Equal</source>
        <translation>Upravený srovnávací</translation>
    </message>
    <message>
        <location filename="lgroupsdrawerfactory.cpp" line="8"/>
        <source>Random</source>
        <translation>Náhodně</translation>
    </message>
    <message>
        <location filename="lgroupsdrawerfactory.cpp" line="9"/>
        <source>Baskets</source>
        <translation>Koše</translation>
    </message>
</context>
<context>
    <name>LMatchesOrdererFactory</name>
    <message>
        <location filename="lrobinroundmatchesordererfactory.cpp" line="6"/>
        <source>Bergers tables</source>
        <translation>Bergerovy tabulky</translation>
    </message>
    <message>
        <location filename="lrobinroundmatchesordererfactory.cpp" line="7"/>
        <source>Kirkmans algorithm</source>
        <translation>Kirkmanův algoritmus</translation>
    </message>
    <message>
        <location filename="lrobinroundmatchesordererfactory.cpp" line="8"/>
        <source>Simple rotation</source>
        <translation>Prostá rotace</translation>
    </message>
</context>
<context>
    <name>LPlayOffDrawerFactory</name>
    <message>
        <location filename="lplayoffdrawerfactory.cpp" line="5"/>
        <source>Advantegeous</source>
        <translation>Zvýhodňovací</translation>
    </message>
    <message>
        <location filename="lplayoffdrawerfactory.cpp" line="6"/>
        <source>Equal</source>
        <translation>Srovnávací</translation>
    </message>
</context>
<context>
    <name>LTeamsSorterFactory</name>
    <message>
        <location filename="lteamssorterfactory.cpp" line="13"/>
        <source>Points</source>
        <translation>Body</translation>
    </message>
    <message>
        <location filename="lteamssorterfactory.cpp" line="14"/>
        <source>Mutual match</source>
        <translation>Vzájemný zápas</translation>
    </message>
    <message>
        <location filename="lteamssorterfactory.cpp" line="15"/>
        <source>Mini table</source>
        <translation>Minitabulka</translation>
    </message>
    <message>
        <location filename="lteamssorterfactory.cpp" line="16"/>
        <source>Score diff</source>
        <translation>Rozdíl skóre</translation>
    </message>
    <message>
        <location filename="lteamssorterfactory.cpp" line="17"/>
        <source>Score ratio</source>
        <translation>Podíl skóre</translation>
    </message>
    <message>
        <location filename="lteamssorterfactory.cpp" line="18"/>
        <source>Goals</source>
        <translation>Počet gólů</translation>
    </message>
    <message>
        <location filename="lteamssorterfactory.cpp" line="19"/>
        <source>Wins</source>
        <translation>Počet výher</translation>
    </message>
</context>
</TS>
