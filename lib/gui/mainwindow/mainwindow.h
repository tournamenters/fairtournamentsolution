#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStringList>
#include <QAction>
#include <QFont>

class Assistant;
class QUndoStack;
class QComboBox;
class QWidgetAction;
class QLineEdit;
class QUndoStack;

class Controller;
class SerializationController;

class AView;
class AMatchesView;
class AStageView;
class GFieldsView;
class AModel;
class AMatchesModel;
class AStageModel;

class AExportable;
class GOpenPage;
class GTeamEditsTable;
class GTournamentTable;
class GFieldsView;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    enum State {Empty, Welcome, Tournament};

    MainWindow(Controller* controller, SerializationController* serializer, QWidget *parent = nullptr);
    ~MainWindow() override;

    void showTournament();
    AView * showStartList(AModel* model);
    AMatchesView * showMatches(AMatchesModel *model);
    AStageView * showStage(AStageModel* stage);
    QList<AMatchesView *> showFields(const QList<AMatchesModel *> &models);
    AView *showNotes(AModel* model);
    AView *showInformation(AModel* model);
    void showMessage(const QString& title, const QString& message);

    void setState(State newState);
    void setPossibleLanguages(const QStringList& languageKeys, const QString& currentLanguage = nullptr);


    QUndoStack * startUndoStack();

signals:
    void setFontRequest(const QFont& newFont);
    void setLanguageRequest(const QString& languageKey);
    void setAppearanceRequest();
    void clearRecentListRequest();

public slots:
    void newTournament();
    void openTournament();
    void saveTournament();
    void saveAsTournament();
    void closeTournament();

    void openUserGuidePage(const QString& page = "index.html");
    void showStatusBarMessage(const QString& message);
    void setCurrentFileName(const QString &fileName);
    void setRecentTournamentList(const QList<QString> &fileNames);

private slots:
    void setFont();
    void setLanguage(const QString& language);
    void setAppearance();

    void openRecentTournament(QAction *sender);
    void clearRecentList();

    void openReportEditor();
    void exportIndividualFiles();

    void about();
    void requestNewFeature();
    void reportBug();
    void openUserGuide();

    void findText();
    void dragToolBarOrientation(Qt::Orientation orientation);

    void documentNameChange(const QString &newName);

private:
    void makeFileActions();
    void makeEditActions();
    void makeViewActions();
    void makeSettingActions();
    void makeExportActions();
    void makeAboutActions();

    void makeMainToolBar();
    void makeFindToolBar();
    void makeResultsToolBar();

    void writeSettings();
    void readSettings();

    void showTournamentPage();
    void showWelcomePage();
    void showEmptyPage();

    void enableWelcomeActions(bool enable);
    void enableTournamnetActions(bool enable);
    void enableAllActions(bool enable);

    void createDockWidgets();
    void setImplicitDockWidgetsState();
    void restoreDockWidget(QDockWidget* dock, QWidget* widget);
    void setUseful(QDockWidget* dock, bool useful = false);
    void setUseful(QToolBar* toolBar, bool useful = false);
    void suspendDockWidgets();

    void createFindEdit();
    void connectSerializer();

    void removeCentralWidget();
    bool wishToSaveAndClose();
    void closeEvent(QCloseEvent *event) override;

    void setupAction(QAction *action, const QString &iconPath,
                     const QString &tips,
                     const QKeySequence shortCut = 0,
                     const QString &iconThemeName = "",
                     QAction::Priority priority = QAction::NormalPriority);
    void setupActionAsCheckable(QAction *action, bool enabled = false, bool state = false);
    QWidgetAction * createSetLanguageAction();
    QString modifiedLanguageKey(const QString& key);
    QIcon getStageIcon(const QString& key);

    QAction *actNewTournament;
    QAction *actOpenTournament;
    QActionGroup* actgrOpenRecent;
    QMenu * mnuRecentTournaments;
    QAction *actSaveTournament;
    QAction *actSaveAsTournament;
    QAction *actClearRecentList;
    QAction *actCloseTournament;
    QAction *actExit;

    QAction *actUndo;
    QAction *actRedo;
    QAction *actFind;
    QAction *actEditTeams;

    QAction *actSetFont;
    QComboBox *cbSetLanguage;
    QAction *actSetAppearance;

    QAction *actExportIndividualFiles;
    QAction *actOpenReportEditor;

    QAction *actAbout;
    QAction *actNewFeature;
    QAction *actReportBug;
    QAction *actGetHelp;

    QToolBar *tbarMain;
    QToolBar *tbarFind;
    QToolBar *tbarResults;

    QDockWidget* dwTeams;
    QDockWidget* dwMatches;
    QDockWidget* dwFields;
    QDockWidget* dwNotes;
    QDockWidget* dwInformation;

    QLineEdit* leFind;

    GOpenPage* gopWelcomePage;
    QTabWidget* tabStages;

    QUndoStack* _undoStack;
    QList<AExportable*> _exportableWidgets;
    QList<AView*> _views;

    Assistant * _assistant;
    Controller* _controller;
    SerializationController* _serializer;
    State _state;
    QString _tournamentName;
    QString _saveDialogPath;
    QString _openDialogPath;
};



#endif // MAINWINDOW_H
