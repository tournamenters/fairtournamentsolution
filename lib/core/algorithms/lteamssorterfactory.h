#ifndef LORDERERFACTORY_H
#define LORDERERFACTORY_H

#include <QMap>
#include <QObject>

class LTeamsSorter;

class LTeamsSorterFactory: public QObject
{
    Q_OBJECT

public:
    ~LTeamsSorterFactory();

    QList<QString> getImplicitCriteriumsOrder() const;
    QMap<QString, QString> getPossibleCriteriumsKeyLabel() const;
    LTeamsSorter * createOrderer();
    LTeamsSorter * createOrderer(QList<QString> criteriumsOrder);

    static LTeamsSorterFactory *Instance()
    {
        static LTeamsSorterFactory instance;
        return &instance;
    }

    LTeamsSorterFactory(LTeamsSorterFactory const& other) = delete;
    void operator=(LTeamsSorterFactory const& other) = delete;

private:
    LTeamsSorterFactory();
    QList<QString> _implicitCriteriumsOrder;
    QMap<QString, QString> _possibleCriteriums;


};

#endif // LORDERERFACTORY_H
