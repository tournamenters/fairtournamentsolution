win32:CONFIG(release, debug|release): LIBS += -L$$TOP_BUILDDIR/lib/core/algorithms/release/ -lalgorithms
else:win32:CONFIG(debug, debug|release): LIBS += -L$$TOP_BUILDDIR/lib/core/algorithms/debug/ -lalgorithms
else:unix: LIBS += -L$$TOP_BUILDDIR/lib/core/algorithms/ -lalgorithms

INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/core/algorithms/release/libalgorithms.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/core/algorithms/debug/libalgorithms.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/core/algorithms/release/algorithms.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/core/algorithms/debug/algorithms.lib
else:unix: PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/core/algorithms/libalgorithms.a
