include($$PWD/../present/present.pri)

win32:CONFIG(release, debug|release): LIBS += -L$$TOP_BUILDDIR/lib/gui/tournamentwidgets/release/ -ltournamentwidgets
else:win32:CONFIG(debug, debug|release): LIBS += -L$$TOP_BUILDDIR/lib/gui/tournamentwidgets/debug/ -ltournamentwidgets
else:unix: LIBS += -L$$TOP_BUILDDIR/lib/gui/tournamentwidgets/ -ltournamentwidgets

INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/gui/tournamentwidgets/release/libtournamentwidgets.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/gui/tournamentwidgets/debug/libtournamentwidgets.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/gui/tournamentwidgets/release/tournamentwidgets.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/gui/tournamentwidgets/debug/tournamentwidgets.lib
else:unix: PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/gui/tournamentwidgets/libtournamentwidgets.a
