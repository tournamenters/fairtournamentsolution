#ifndef CMATCH_H
#define CMATCH_H
#include <QObject>

#include "cresult.h"
#include <QList>

class CTeam;
struct Match;

class CMatch: public QObject
{
    Q_OBJECT

public:
    CMatch(int sourceId, QObject* parent = nullptr);
    CMatch(int sourceId, CTeam *host, CTeam *guest, QObject* parent = nullptr);
    CMatch(int sourceId, CTeam *host, CTeam *guest, const QString &result, QObject* parent = nullptr);
    ~CMatch() override;

    virtual void setResult(const QString &result);
    virtual void setResultReversed(const QString &result);

    void setHost(CTeam *team);
    void setGuest(CTeam *team);
    void setTeam(CTeam *team, bool host);

    void setSourceName(const QString &sourceName);

    void setIndexes(int hostIndex, int guestIndex);
    void setPlaceHolders(const QString &placeHolderHost, const QString &placeHolderGuest);
    void setPlaceHolderHost(const QString &placeHolderHost);
    void setPlaceHolderGuest(const QString &placeHolderGuest);

    CTeam* getWinner() const;
    CTeam* getLoser() const;
    CTeam* getHost() const;
    CTeam* getGuest() const;
    CTeam* getTeam(bool host) const;

    int getHostIndex() const;
    int getFalseHostIndex() const;

    int getGuestIndex() const;
    int getFalseGuestIndex() const;

    QString getHostString() const;
    QString getGuestString() const;
    QString getSourceString() const;

    QList<int> getTeamsIndexes() const;
    QString getResult() const;
    QString getResultReversed() const;


    QVariant toVariant() const;
    Match toMatchPackage() const;

    bool isIn(CTeam*team) const;
    bool isHost(CTeam*team) const;
    bool isGuest(CTeam*team) const;
    bool isThisMatch(int hostIndex, int guestIndex);

    bool hasResult() const;
    bool needsResult() const;

    bool equals(CMatch* other) const;

signals:
    void resultUpdated();
    void teamsUpdated();

private:
    void change(CTeam*& originTeam, CTeam* newTeam);
    QVariant teamToVariant(CTeam* team, const QString& placeholder) const;

    CTeam* _host;
    CTeam* _guest;
    CResult _result;

    QString _sourceName;
    int _sourceId;

    QString _hostPlaceholder;
    QString _guestPlaceholder;

    int _hostIndex;
    int _guestIndex;
};


#endif // CMATCH_H
