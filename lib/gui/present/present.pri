
win32:CONFIG(release, debug|release): LIBS += -L$$TOP_BUILDDIR/lib/gui/present/release/ -lpresent
else:win32:CONFIG(debug, debug|release): LIBS += -L$$TOP_BUILDDIR/lib/gui/present/debug/ -lpresent
else:unix: LIBS += -L$$TOP_BUILDDIR/lib/gui/present/ -lpresent

INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/gui/present/release/libpresent.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/gui/present/debug/libpresent.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/gui/present/release/present.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/gui/present/debug/present.lib
else:unix: PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/gui/present/libpresent.a
