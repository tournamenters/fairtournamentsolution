#ifndef GSTAGEVIEWFACTORY_H
#define GSTAGEVIEWFACTORY_H

class AStageView;
class AStageModel;

#include <QStringList>
#include <QMap>

typedef AStageView* (__stdcall * FcnCreateStageView)(AStageModel*);

class GStageViewFactory
{

public:
    ~GStageViewFactory();
    static GStageViewFactory *getInstance();

    AStageView * create(AStageModel * model);
    QStringList keys();
    void registerType(const QString& key, FcnCreateStageView create);

    GStageViewFactory(GStageViewFactory const& other) = delete;
    void operator=(GStageViewFactory const& other) = delete;

private:
    GStageViewFactory();

    QMap<QString, FcnCreateStageView> registeredTypes;
};

#endif // GSTAGEVIEWFACTORY_H
