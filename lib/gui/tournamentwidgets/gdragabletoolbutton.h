#ifndef GDRAGABLETOOLBUTTON_H
#define GDRAGABLETOOLBUTTON_H

#include <QToolButton>
#include <QPoint>

class QDragEnterEvent;
class QDropEvent;
class GDragAndDropManager;

class GDragableToolButton: public QToolButton
{

public:
    GDragableToolButton(const QString& text, QWidget *parent = nullptr);

protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent * event) override;
    void provideDragAction();

private:
    GDragAndDropManager * _dragableManager;
};




#endif // GDRAGABLETOOLBUTTON_H
