#ifndef AVIEW_H
#define AVIEW_H
#include <QObject>

#include "global_model.h"

#include <QVariant>
#include <QMap>

class AExportable;
class AStageModel;
class GTeamEditsTable;
class GMatchesTable;
class QSplitter;

class AView : public QObject
{
    Q_OBJECT

public:
    explicit AView(QObject *parent = nullptr);
    ~AView();

    QString getTitle();
    void setTitle(const QString &title);

    virtual QWidget * getViewWidget() = 0;
    virtual AExportable * getExportableWidget();

signals:
    void stateEdited(ExecType type, const QVariant& identificator, const QVariant& data);

public slots:
    virtual void update() = 0;

protected:
    QVariant createIdentificator(const QVariant& viewIdentificator);
    QVariant createIdentificator(const QVariant& mainIdentificator, const QVariant& appendix);

    QString _title;
    QVariant _identificator;

};

class ATeamsView : public AView
{
    Q_OBJECT

public:
    explicit ATeamsView(QObject *parent = nullptr);

public slots:
    virtual void findText(const QString& text) = 0;

protected:
    virtual void connectTeamsTable(GTeamEditsTable* table);

protected slots:
    void teamEditChange(const QVariant& editIdentificator, const QString& newName, const QString& oldName);
    void teamsReplacement(const QVariant &thisEditIdentificator, const QVariant &otherEditIdentificator);
};

class AMatchesView : public ATeamsView
{
    Q_OBJECT

public:
    explicit AMatchesView(QObject *parent = nullptr);

signals:
    void resultEntered(const QVariant& identificator, const QString& newResult, const QString& oldResult);

protected:
    virtual void connectMatchesTable(GMatchesTable* table);

protected slots:
    void resultEditChange(int row, int col, const QString& newResult, const QString& oldResult);
};

class AStageView: public AMatchesView
{
    Q_OBJECT

public:
    explicit AStageView(QObject *parent = nullptr);
    virtual ~AStageView(){}

protected:
    void addEmptySpace(QSplitter* splitter, int stretchFactor = 1);
    void addWidget(QSplitter* splitter, QWidget* widget, int stretchFactor = 1);

    AStageModel * _model;
};


#endif // AVIEW_H
