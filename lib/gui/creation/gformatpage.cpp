#include "gformatpage.h"
#include "global_creation.h"

#include <QLabel>
#include <QGroupBox>
#include <QCheckBox>
#include <QRadioButton>
#include <QBoxLayout>

GFormatPage::GFormatPage(QWidget *parent)
    : QWizardPage(parent),
      _preparedType(),
      _formatPrepared(true),
      _firstVisit(true)
{    
    setTitle(tr("Tournament Format"));
    QVBoxLayout *layout = new QVBoxLayout();
    layout->addWidget(createTitle());
    layout->addStretch(1);
    layout->addWidget(createPreparedGroupBox());
    layout->addWidget(createCustomCheckBox());
    layout->addWidget(createGenerateCheckBox());
    layout->addWidget(generateFormat);
    layout->addStretch(2);
    setLayout(layout);
}

GFormatPage::~GFormatPage()
{
    _preparedFormats.clear();
}

void GFormatPage::initializePage()
{
    setDefaultPreparedFormat();
    changePreparedType();
    if(_firstVisit){
        setFormatPrepared(true);
        _firstVisit = false;
    }
}

QString GFormatPage::preparedType() const
{
    return _preparedType;
}

bool GFormatPage::formatPrepared() const
{
    return _formatPrepared;
}

void GFormatPage::setFormatPrepared(bool formatPrepared)
{
    if(_formatPrepared == formatPrepared){
        return;
    }
    _formatPrepared = formatPrepared;
    preparedFormatsBox->setChecked(formatPrepared);
    customFormat->setChecked(!formatPrepared);
    generateFormat->setChecked(false);
    emit formatCreationChanged();
}

void GFormatPage::setPreparedType(const QString &preparedType)
{
    if(_preparedType == preparedType){
        return;
    }
    _preparedType = preparedType;
    emit preparedTypeChanged();
}

int GFormatPage::nextId() const
{
    if (generateFormat->isChecked()) {
        return PageIndex::Page_Auto;
    }
    return PageIndex::Page_Stages;
}

void GFormatPage::checkChange()
{
    QCheckBox* senderObject = qobject_cast<QCheckBox*>(sender());
    setFormatPrepared(!senderObject);
    if(senderObject == generateFormat){
        customFormat->setChecked(false);
        generateFormat->setChecked(true);
    }
    if(senderObject == customFormat){
        customFormat->setChecked(true);
        generateFormat->setChecked(false);

    }
}

void GFormatPage::changePreparedType()
{
    for(const auto& key : _preparedFormats.keys()){
        if(_preparedFormats[key]->isChecked()){
            setPreparedType(key);
        }
    }
}

QLabel *GFormatPage::createTitle()
{
    QLabel * label = new QLabel(tr("Please choose your tournament"
                                  " format. More detailed settings and editation"
                                  " can be both made on the following page."),this);
    label->setWordWrap(true);
    return label;
}

QGroupBox *GFormatPage::createPreparedGroupBox()
{
    preparedFormatsBox = new QGroupBox(tr("Prepared formats"),this);
    preparedFormatsBox->setCheckable(true);
    connect(preparedFormatsBox, SIGNAL(clicked()),
            this, SLOT(checkChange()));
    registerField("format.prepared", this, "_formatPrepared",
                  SIGNAL(formatCreationChanged()));
    preparedFormatsBox->setLayout(createPreparedLayout());
    return preparedFormatsBox;
}

QCheckBox *GFormatPage::createCustomCheckBox()
{
    customFormat = new QCheckBox(tr("Custom format"),this);
    registerField("format.custom", customFormat);
    connect(customFormat, SIGNAL(clicked()),
            this, SLOT(checkChange()));
    return customFormat;
}

QCheckBox *GFormatPage::createGenerateCheckBox()
{
    generateFormat = new QCheckBox(tr("Generate format [Experimental]"),this);
    generateFormat->setEnabled(false);
    registerField("format.generate", generateFormat);
    connect(generateFormat, SIGNAL(clicked()),
            this, SLOT(checkChange()));
    return generateFormat;
}

QLayout *GFormatPage::createPreparedLayout()
{
    registerField("format.prepared_type", this, "_preparedType",
                  SIGNAL(preparedTypeChanged()));
    QVBoxLayout* vLayout = new QVBoxLayout();
    for(const auto& name : CreationConstants::getInstance().getPreparedFormats()){
        QRadioButton * radFormat = createPreparedRadButton(name._label);
        _preparedFormats[name._key] = radFormat;
        vLayout->addWidget(radFormat);
    }
    return vLayout;
}

QRadioButton *GFormatPage::createPreparedRadButton(const QString &title)
{
    QRadioButton* radFormat = new QRadioButton(title, this);
    connect(radFormat, SIGNAL(clicked()),
            this, SLOT(changePreparedType()));
    return radFormat;
}

void GFormatPage::setDefaultPreparedFormat()
{
    int numTeams = field("start_list.num_teams").toInt();
    if(numTeams<8){
        _preparedFormats[keyRobinRound]->setChecked(true);
    }
    else if(numTeams<33){
        _preparedFormats[keyGroupsPlayOff]->setChecked(true);
    }
    else {
        _preparedFormats[keyPlayOff]->setChecked(true);
    }
}
