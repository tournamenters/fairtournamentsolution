#ifndef GSTAGESPAGE_H
#define GSTAGESPAGE_H

#include <QWizardPage>
#include <QMap>
#include <QVariant>

class QLabel;
class QTabWidget;
class QToolBar;
class GTabStagesEditor;
class GDragableToolBar;

class GStagesCreationPage : public QWizardPage
{
    Q_OBJECT
    Q_PROPERTY(QList<QVariant> _stagesData READ stagesData WRITE setStagesData NOTIFY stagesDataChanged)

public:
    GStagesCreationPage(QWidget *parent = nullptr);
    ~GStagesCreationPage() override;

    int nextId() const override;    
    bool isComplete() const override;

    void setStagesData(const QList<QVariant> &stagesData);
    QList<QVariant> stagesData() const;

signals:
    void stagesDataChanged();

protected:
    void initializePage() override;

private slots:
    void stagesDataChange();
    void currentStageChange(int index);

private:
    QToolBar * createStagesToolBar();
    QTabWidget * createStagesTabWidget();
    QLabel * createTitle();

    QList<QVariant> _stagesData;
    GTabStagesEditor * stagesSettingTabWidget;
    GDragableToolBar* tbarStages;

};
#endif // GSTAGESPAGE_H
