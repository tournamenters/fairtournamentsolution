#include "ggrouptable.h"
#include "gresultedit.h"

#include <QTableWidgetItem>
#include <QVariant>
#include <QToolButton>
#include <QAction>
#include <QBoxLayout>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QLabel>
#include <QLineEdit>

#include <qglobal.h>
#include <ctime>

GGroupTable::GGroupTable(int rows, const QVariant& identificator, QWidget *parent)
    : GMatchesTable(identificator, parent, 1, rows)
{
    _resultEditColumnsCount = rows;
    _highlightCellColor = palette().color(QPalette::Highlight);
    _highlightCellCol = -1;
    _highlightCellRow = -1;
    _editatedCol = -1;
    setTeamEditRemovable(true);
    setTeamEditDragAndDrop(true);
}

GGroupTable::GGroupTable(QWidget *parent)
    : GMatchesTable (parent)
{
    _highlightCellColor = palette().color(QPalette::Highlight);
    _highlightCellCol = -1;
    _highlightCellRow = -1;
    _editatedCol = -1;
    setTeamEditColumnsCount(1);
    setTeamEditRemovable(false);
    setTeamEditDragAndDrop(false);
}

GGroupTable::GGroupTable(int rows, QWidget *parent)
    : GGroupTable(rows, QVariant(), parent)
{

}

GGroupTable::GGroupTable(const QVariant& identificator, QWidget *parent)
    : GGroupTable(parent)
{
    _identificator = identificator;
}

GGroupTable::~GGroupTable()
{

}

void GGroupTable::setData(const QVariant &data)
{
    resetTable();
    int columns = columnCount();
    int rows = data.toMap()["rows"].toList().size();
    bool change = rows > rowCount();
    GMatchesTable::setData(data);
    if(change){
        removeDiagonaleResults();
    }   
    if(columns != columnCount()){
        setDefaultSize();
    }
    for (int row=0;row<rowCount();row++){
        setCellText(row, row+1,"x");
        item(row, row+1)->setTextColor(palette().color(QPalette::Shadow));
    }
    cleanEmptyRows();
    if(data.toMap().contains("unique_needed")){
        setupUniqueButtons(data.toMap()["unique_needed"]);
    }
    if(data.toMap().contains("next_match")){
        highliteNextMatch(data.toMap()["next_match"]);
    }
}

void GGroupTable::removeDiagonaleResults()
{
    for (int row=0;row<rowCount();row++){
        removeResultEdit(row,row+1);
        setItem(row,row+1,createDiagonalItem());
    }
}

void GGroupTable::setupUniqueButtons(const QVariant &data)
{
    QVariantMap uniqueMap = data.toMap();
    _editatedCol = getColumnHeaders().indexOf(uniqueMap["header"].toString());
    QString value = uniqueMap["value"].toString();
    for(int row = 0; row<rowCount();row++){
        if(getCellText(row,_editatedCol)==value){
            setCellWidget(row, _editatedCol, createUniqueButton(value));
        }
    }
}

bool GGroupTable::isRowEmpty(int row)
{
    for (int col=1;col<rowCount()+1;col++){
        QLineEdit* edit =  qobject_cast<QLineEdit*>(cellWidget(row,col));
        if(edit && !edit->text().isEmpty()){
            return false;
        }
    }
    return true;
}

QTableWidgetItem *GGroupTable::createDiagonalItem()
{
    QTableWidgetItem * cellItem = createItem("x");
    cellItem->setBackgroundColor(palette().color(QPalette::Shadow));
    cellItem->setTextColor(palette().color(QPalette::Shadow));
    return cellItem;
}

QToolButton *GGroupTable::createUniqueButton(const QString &value)
{
    QAction * actEdit = new QAction(value + "*", this);
    actEdit->setToolTip(tr("Click to edit value"));
    QToolButton * button = new QToolButton(this);
    button->setDefaultAction(actEdit);
    connect(button, SIGNAL(triggered(QAction*)),
            this, SLOT(uniqueEditationRequest(QAction*)));
    return button;
}

void GGroupTable::setHighlightColor(const QColor& color)
{
    _highlightCellColor = color;
    setMatchColor(_highlightCellRow, _highlightCellCol, color);
}

void GGroupTable::setDefaultSize()
{
    int colWidth = columnWidth(0)/7*4;
    for(int col = _teamEditColumnsCount + _resultEditColumnsCount; col<columnCount();col++){
        setColumnWidth(col,colWidth);
    }
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setMinimumWidth(getRealWidth());
    setMinimumHeight((rowCount()+1)*rowHeight(0));
    setMaximumHeight((rowCount()+3)*rowHeight(0));
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
}

void GGroupTable::resetTable()
{
    setMatchColor(_highlightCellRow,_highlightCellCol, palette().color(QPalette::Base));
    for(int row = 0; row<rowCount();row++){
        if(cellWidget(row, _editatedCol)){
            removeCellWidget(row, _editatedCol);
        }
    }
}

void GGroupTable::uniqueEditationRequest(QAction * action)
{
    emit uniqueEditationRequested(getColumnHeaders()[_editatedCol],action->text().remove("*"));
}

void GGroupTable::cleanEmptyRows()
{
    for (int row=0;row<rowCount();row++){
        if(isRowEmpty(row)){
            for(int col = rowCount() + 1; col<columnCount(); col++){
                setCellText(row, col, "");
            }
        }
    }
}

void GGroupTable::highliteNextMatch(const QVariant &data)
{
    QVariantMap map = data.toMap();
    _highlightCellCol = map["col"].toInt();
    _highlightCellRow = map["row"].toInt();
    setMatchColor(_highlightCellRow, _highlightCellCol, _highlightCellColor);
}

void GGroupTable::setMatchColor(int row, int col, const QColor &color)
{
    if(0 <= row && row < rowCount() && 0 < col && col <= rowCount()){
        QPalette palette = this->palette();
        palette.setColor(QPalette::Base, color);
        cellWidget(row, col)->setPalette(palette);
        cellWidget(col-1,row+1)->setPalette(palette);
    }
}

void GGroupTable::setResultsEditable(bool editable)
{
    for (auto result : _resultEditCells){
        result->setReadOnly(!editable);
    }
}

GGroupEditationDialog::GGroupEditationDialog(const QVariant& data, const QString& header, QWidget *parent)
    : QDialog (parent)
{
    QVariantMap map = data.toMap();
    if(map.contains("unique_needed")){
        map.remove("unique_needed");
    }
    QVBoxLayout* layout = new QVBoxLayout();
    QLabel* label = new QLabel(tr("Please decide on the order of the following"
                                  " participants or let the program do that by chance."));
    label->setWordWrap(true);
    layout->addWidget(label);
    layout->addWidget(createTable(map));
    _editatedCol = _table->getColumnHeaders().indexOf(header);
    _table->setColumnEditable(_editatedCol, true);
    layout->addLayout(createButtonsLayout());
    setLayout(layout);

    connect(_table, SIGNAL(cellChanged(int,int)),
            this, SLOT(tableCellChange()));

    std::srand(unsigned(std::time(nullptr)));
}

QStringList GGroupEditationDialog::getEnteredValues()
{
    return _table->getColumnData(_editatedCol);
}

void GGroupEditationDialog::tableCellChange()
{
    saveChanges->setEnabled(isEditationCorrect());
}

QTableWidget *GGroupEditationDialog::createTable(const QVariant& data)
{
    _table = new GGroupTable(data.toMap()["rows"].toList().size(), this);
    _table->setData(data);
    _table->setResultsEditable(false);
    _table->setMinimumWidth(_table->getRealWidth());
    return _table;
}

QLayout *GGroupEditationDialog::createButtonsLayout()
{
    QHBoxLayout* hLayout = new QHBoxLayout;
    hLayout->addStretch(0);
    QPushButton* btnAutoDraw = new QPushButton(tr("Draw Values"), this);
    connect(btnAutoDraw, SIGNAL(clicked()),
            this, SLOT(drawValues()));
    btnAutoDraw->setDefault(true);
    hLayout->addWidget(btnAutoDraw);
    QPushButton* btnCancel = new QPushButton(tr("Cancel"), this);
    connect(btnCancel, SIGNAL(clicked()),
            this, SLOT(reject()));
    hLayout->addWidget(btnCancel);
    saveChanges = new QPushButton(tr("Save Changes"), this);
    saveChanges->setEnabled(false);
    connect(saveChanges, SIGNAL(clicked()),
            this, SLOT(accept()));
    hLayout->addWidget(saveChanges);
    return hLayout;
}

bool GGroupEditationDialog::isEditationCorrect()
{
    bool isInteger = true;
    auto column = getEnteredValues();
    for(const auto& cell : column){
        if(cell.isEmpty()){
            return false;
        }
        if(column.count(cell) > 1){
            return false;
        }
        cell.toInt(&isInteger);
        if(!isInteger){
            _table->item(getEnteredValues().indexOf(cell), _editatedCol)->setText("");
            return false;
        }
    }
    return true;
}

void GGroupEditationDialog::drawValues()
{
    QList<int> list;
    for(int position = 1; position < _table->rowCount()+1; position++){
        list.push_back(position);
    }
    std::random_shuffle(list.begin(), list.end());
    setTableValues(list);
}

void GGroupEditationDialog::setTableValues(QList<int> values)
{
    _table->blockSignals(true);
    int row = 0;
    for(int value : values){        
        _table->item(row, _editatedCol)->setText(QString::number(value));
        row++;
    }
    _table->blockSignals(false);
    tableCellChange();
}

int GGroupEditationDialog::getEditatedColumnIndex() const
{
    return _editatedCol;
}

void GGroupEditationDialog::setEditatedColumnIndex(int value)
{
    _editatedCol = value;
}
