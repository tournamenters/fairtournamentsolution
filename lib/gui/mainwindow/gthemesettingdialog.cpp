#include "gthemesettingdialog.h"
#include <QBoxLayout>
#include <QLabel>
#include <QRadioButton>
#include <QGroupBox>
#include <QDialogButtonBox>

GThemeSettingDialog::GThemeSettingDialog(QStringList possiblePalettes, QStringList possibleStyles, QWidget *parent) : QDialog(parent)
{
    setWindowTitle(tr("Theme Setting"));

    QVBoxLayout * layout = new QVBoxLayout();
    QLabel* label = new QLabel(tr("Please select desing of this application."));
    layout->addWidget(label);
    layout->addStretch(1);
    QHBoxLayout * hLayout = new QHBoxLayout();
    style = createGroupBox(tr("Style"), possibleStyles, styleButtons);
    hLayout->addWidget(style);

    palette = createGroupBox(tr("Palette"), possiblePalettes, paletteButtons);
    hLayout->addWidget(palette);

    layout->addLayout(hLayout);
    layout->addStretch(4);

    QDialogButtonBox * buttonBox = new QDialogButtonBox( {QDialogButtonBox::Ok, QDialogButtonBox::Cancel});
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
    layout->addWidget(buttonBox);
    setLayout(layout);
}

void GThemeSettingDialog::setCurrentPalette(QString paletteKey)
{
    checkButtonWithText(paletteButtons, paletteKey);
}

void GThemeSettingDialog::setCurrentStyle(QString styleKey)
{
    checkButtonWithText(styleButtons, styleKey);
}

QString GThemeSettingDialog::getCurrentPalette()
{
    return getCheckedButtonsText(paletteButtons);
}

QString GThemeSettingDialog::getCurrentStyle()
{
    return getCheckedButtonsText(styleButtons);
}

void GThemeSettingDialog::setEnablePossibilities(bool checked)
{
    if(checked){
        paletteButtons.first()->setChecked(checked);
    }
    palette->setEnabled(!checked);
}

QGroupBox *GThemeSettingDialog::createGroupBox(QString title, QStringList buttonsTexts, QList<QRadioButton *> &buttonsContainer)
{
    QGroupBox* box = new QGroupBox(title);
    QVBoxLayout * layout = new QVBoxLayout();
    for(const auto & text : buttonsTexts){
        QRadioButton * radButton = new QRadioButton(text, this);
        layout->addWidget(radButton);
        buttonsContainer.push_back(radButton);
        if(text=="Default"){//nic moc reseni
            connect(radButton, SIGNAL(toggled(bool)),
                    this, SLOT(setEnablePossibilities(bool)));
        }
    }
    box->setLayout(layout);
    return  box;
}

void GThemeSettingDialog::checkButtonWithText(QList<QRadioButton *> buttons, QString key)
{
    for(const auto button : buttons){
        if(button->text()==key){
            button->setChecked(true);
        }
    }
}

QRadioButton *GThemeSettingDialog::findCheckedButton(QList<QRadioButton *> buttons)
{
    for(const auto button : buttons){
        if(button->isChecked()){
            return button;
        }
    }
    return nullptr;
}

QString GThemeSettingDialog::getCheckedButtonsText(QList<QRadioButton *> buttons)
{
    QRadioButton* button = findCheckedButton(buttons);
    if(button){
        return button->text();
    }
    return "";
}

