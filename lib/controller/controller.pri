include($$PWD/../core/core.pri)

win32:CONFIG(release, debug|release): LIBS += -L$$TOP_BUILDDIR/lib/controller/release/ -lcontroller
else:win32:CONFIG(debug, debug|release): LIBS += -L$$TOP_BUILDDIR/lib/controller/debug/ -lcontroller
else:unix: LIBS += -L$$TOP_BUILDDIR/lib/controller/ -lcontroller

INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/controller/release/libcontroller.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/controller/debug/libcontroller.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/controller/release/controller.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/controller/debug/controller.lib
else:unix: PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/controller/libcontroller.a
