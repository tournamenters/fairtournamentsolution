#ifndef GORDERERSETTINGBOX_H
#define GORDERERSETTINGBOX_H

#include <QGroupBox>
#include <QVariant>
#include <QMap>

class QSpinBox;
class QTabBar;
class QComboBox;
struct OrdererProfile;

class GPointsSettingBox: public QGroupBox
{
    Q_OBJECT

public:
    GPointsSettingBox(QWidget * parent = nullptr);
    ~GPointsSettingBox();

    void setData(const QVariant& data);
    QVariant getData();
    void setProfileValues(QString profileKey);

signals:
    void dataChanged();

private slots:
    void sbWinChange();
    void sbDrawChange();
    void sbLossChange();
    void sbLossOTChange();
    void sbWinOTChange();
    void isOTChange();

    void orderCriteriaChange();
    void criteriaCloseRequest(int index);
    void criteriaContextMenuRequest();
    void profileChange();
    void addCriteriaTab();

private:
    void initializateFields();
    void setCriteriaOrder(const QStringList& criteriaOrder);
    QComboBox * createProfileComboBox();
    QGroupBox * createPointsBox();
    QTabBar *createCriteriaOrderTabBar();
    QLayout *createMainPointsLayout();
    QGroupBox *createOverTimeBox();
    QLayout *createOverTimeBoxLayout();
    bool isChoosen(const QString& criteria);
    void setCustomProfile();

    QComboBox * cbOrdererProfile;

    QSpinBox * sbWin;
    QSpinBox * sbDraw;
    QSpinBox * sbLoss;
    QSpinBox * sbWinOT;
    QSpinBox * sbLossOT;

    QGroupBox * boxOverTime;
    QTabBar* tabarCriteriaOrder;

    QStringList _possibleProfiles;
    QMap<QString, OrdererProfile *> _profilesSetting;

    QList<QString> _implicitCriteriaOrder;
    QMap<QString, QString> _criteriaKeyLabelMap;
    QMap<QString, QAction*> _criteriaActions;

};

struct OrdererProfile
{
    QStringList criteriaOrder;
    int win;
    int draw;
    int loss;
    int winOT;
    int lossOT;
    bool isOT;

    OrdererProfile();
    ~OrdererProfile();
};


#endif // GORDERERSETTINGBOX_H
