#ifndef AEXPORTABLE_H
#define AEXPORTABLE_H

#include <QPixmap>
#include <QString>
#include <QWidget>

class GExportHelperWidget;
class QMenu;

class AExportable
{

public:
    enum Format {HTML, PNG, TXT, CSV};
    AExportable();
    virtual ~AExportable();

    virtual QString toHtml() = 0;
    virtual QPixmap toPixmap() = 0;
    virtual QString toCsv() = 0;
    virtual QString toPlainText() = 0;

    virtual QString getTitle() const;
    virtual QString getSubTitle() const;

    virtual bool hasMoreParts() const;
    virtual QList<AExportable*> getIndividualParts() const;
    void saveToFormat(Format format);
    void copyToFormat(Format format);

    int getRecomendedFormat() const;

    void setTitle(const QString &title);
    void setRecomendedFormat(const Format &recomendedFormat);
    void setDefaultPath(const QString &defaultPath);

protected:
    QString qtAlignToHtml(int qtAlign);


    QString _title;
    QString _subTitle;
    Format _recomendedFormat;
    QList<Format> _possibleFormats;
    QString _defaultPath;

private:
    void copyHtml();
    void copyPicture();

    GExportHelperWidget * _widget;


};

class GComplexExportable: public AExportable
{

public:
    GComplexExportable(const QList<AExportable*> parts);
    ~GComplexExportable() override;

    QString toHtml() override;
    QPixmap toPixmap() override;
    QString toCsv() override;
    QString toPlainText() override;

    bool hasMoreParts() const override;
    QList<AExportable*> getIndividualParts() const override;

private:
    QList<AExportable*> _parts;

};

class GExportHelperWidget: public QWidget
{
    Q_OBJECT

public:
    void openSavePictureDialog(const QPixmap& pixmap);
    void openSaveDialog(const QString &format, const QString &data);
};


#endif // AEXPORTABLE_H
