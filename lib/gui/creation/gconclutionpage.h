#ifndef GCONCLUTIONPAGE_H
#define GCONCLUTIONPAGE_H

#include <QWizardPage>
#include <QVariant>

class QLabel;
class QGroupBox;
class QTableWidget;

class GConclusionPage : public QWizardPage
{
    Q_OBJECT

public:
    GConclusionPage(QWidget *parent = nullptr);

protected:
    void initializePage() override;

private:
    QLabel* createTitle();
    QGroupBox* createNameBox();
    QGroupBox* createStartListBox();
    QGroupBox* createStagesBox();
    QTableWidget* createStagesTable();
    QGroupBox* createFieldsBox();
    QLayout *createYesFieldsLayout(int numFields);
    QLayout *createNoFieldsLayout();

    void setName();
    void setStartListParams();
    void setStagesData();
    void setStageValues(const QVariantMap& stage);
    void setStagesTableColumn(int col, const QStringList& cellTexts);
    void setStagesTableCellText(int row, int col, const QString& text);
    void setStagesTableHorizontalHeader(int col, const QString& text);
    void setLastStageIcons(int col);
    void setFieldsState();

    QLabel * laName;
    QPushButton * pbIsStartListRanked;
    QLabel * laNumTeams;
    QTableWidget* stagesTable;
    QGroupBox * boxFields;
};


#endif // GCONCLUTIONPAGE_H
