#include "controller.h"
#include "tournament.h"
#include "astagemodel.h"
#include "amodel.h"
#include "serialization.h"

#include "global_model.h"
#include "ordercommand.h"
#include "renamecommand.h"
#include "swapcommand.h"
#include "resultcommand.h"
#include "editationcommand.h"

#include <QMap>
#include <QVariant>
#include <QList>
#include <QUndoStack>
#include <QUndoCommand>


Controller::Controller(Tournament *model, SerializationController * serializer, QObject *parent)
    : QObject(parent)
{
    _model = model;
    _serializer = serializer;
}

void Controller::resetAll()
{
    _undoStack->clear();
    _model->clearTournament();
    _serializer->close();
}


void Controller::startTournament(const QString &name)
{
    resetAll();
    _model->newTournament(name);
}

void Controller::createStartList(const QVariant &teamsData, const QVariant &params)
{
    _model->createStartList(teamsData, params);
}

void Controller::createStages(const QStringList &types, const QVariantList &params, const QVariantList &settings)
{
    int stageId = 0;
    for(const auto &type : types){
        _model->addStage(type, settings[stageId].toMap()["name"].toString(), params[stageId], settings[stageId]);
        stageId++;
    }

}

void Controller::sheduleFields(const QVariant &setting)
{
     _model->sheduleFields(setting);
}

void Controller::finishTournament()
{
    resetAll();
}

void Controller::setUndoStack(QUndoStack *stack)
{
    _undoStack = stack;
}

Tournament* Controller::getModel() const
{
    return _model;
}

void Controller::resultEntered(const QVariant& identificator, const QString& newResult, const QString& oldResult)
{
    AMatchesModel * targetModel = nullptr;
    int identificatorIndex = 0;
    if(identificator.toMap().contains("stage_index")){
        identificatorIndex = identificator.toMap().value("stage_index").toInt();
        if(identificatorIndex < _model->getStagesModels().size()){
            targetModel = _model->getStagesModels()[identificatorIndex];
        }
    }
    else if (identificator.toMap().contains("field_index")){
        identificatorIndex = identificator.toMap().value("field_index").toInt();
        if(identificatorIndex < _model->getFieldsModels().size()){
            targetModel = _model->getFieldsModels()[identificatorIndex];
        }
    }
    else if (identificator.toMap().contains("matches")){
        targetModel = _model->getMatchesModel();
    }
    if(targetModel){
        _serializer->setModification(true);
        _undoStack->push(new ResultCommand(targetModel, identificator, newResult, oldResult));
    }
}

void Controller::startListViewEdit(ExecType type, const QVariant &identificator, const QVariant &data)
{
    AModel* targetModel = _model->getStartListModel();
    if(targetModel){
        switch (type) {
        case ExecType::Rename:
            _undoStack->push(new RenameCommand(targetModel, identificator, data));
            return;
        case ExecType::Swap:
            _undoStack->push(new SwapCommand(targetModel, identificator, data));
            return;
        case ExecType::Editation:
            _undoStack->push(new EditationCommand(targetModel, identificator, data));
            return;
        default:
            return;
        }
    }
}

void Controller::matchesViewEdit(ExecType type, const QVariant &identificator, const QVariant &data)
{
    AMatchesModel* targetModel = nullptr;
    if(identificator.toMap().contains("field_index")){
        int fieldIndex = identificator.toMap()["field_index"].toInt();
        if(fieldIndex < _model->getFieldsModels().size()){
            targetModel = _model->getFieldsModels()[fieldIndex];
        }
    }
    else if(identificator.toMap().contains("matches")){
        targetModel = _model->getMatchesModel();
    }
    if(targetModel){
        switch (type) {
        case ExecType::Rename:
            _undoStack->push(new RenameCommand(targetModel, identificator, data));
            return;
        default:
            return;
        }
    }
}

void Controller::stageViewEdit(ExecType type, const QVariant &identificator, const QVariant &data)
{
    if(identificator.toMap().contains("stage_index")){
        int identificatorIndex = identificator.toMap().value("stage_index").toInt();
        if(identificatorIndex < _model->getStagesModels().size()){
            AStageModel* targetModel = _model->getStagesModels()[identificatorIndex];
            switch (type) {
            case ExecType::Rename:
                _undoStack->push(new RenameCommand(targetModel, identificator, data));
                return;
            case ExecType::Swap:
                _undoStack->push(new SwapCommand(targetModel, identificator, data));
                return;
            case ExecType::Order:
                _undoStack->push(new OrderCommand(targetModel, identificator, data));
                return;
            default:
                return;
            }
        }
    }
}

