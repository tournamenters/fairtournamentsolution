#include "resultcommand.h"
#include "amodel.h"

ResultCommand::ResultCommand(AMatchesModel *model, const QVariant& identificator, const QString& newResult, const QString& oldResult, QUndoCommand *parent)
    : QUndoCommand(parent)
{
    _identificator = identificator;
    _newResult = newResult;
    _oldResult = oldResult;
    _model = model;
}

ResultCommand::~ResultCommand()
{

}

void ResultCommand::redo()
{
    _model->setResult(_identificator, _newResult);
}

void ResultCommand::undo()
{
    _model->setResult(_identificator, _oldResult);
}
