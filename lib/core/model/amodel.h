#ifndef AMODEL_H
#define AMODEL_H

#include <QObject>
#include <QVariant>
#include <QMap>

#include "global_model.h"

class CMatch;

class AModel: public QObject
{
    Q_OBJECT

public:
    explicit AModel(QObject *parent = nullptr);
    AModel(const QString& name, QObject *parent = nullptr);
    virtual ~AModel();

    void setName(const QString &name);
    virtual void setIdentificator(const QVariant &identificator);
    void addToIdentificator(const QString& key, const QVariant &value);

    QString getName() const;
    virtual QVariant getIdentificator() const;
    virtual QVariant getData() const = 0;

public slots:
    virtual bool execEdit(ExecType type, const QVariant& identificator, const QVariant& data) = 0;

signals:
    void dataChanged();
    void editRequested(const QVariant& data);

protected:
    bool isItForMe(const QVariant& identificator) const;

private:
    QString _name;
    QVariantMap _identificator;
};

struct Match;

class AMatchesModel: public AModel
{
    Q_OBJECT

public:
    explicit AMatchesModel(QObject *parent = nullptr);
    ~AMatchesModel() override;

    int matchesCount() const;
    bool allMatchesPlayed() const;
    CMatch * getNextMatchToPlay() const;
    QList<CMatch *> getMatches() const;

public slots:
    virtual bool setResult(const QVariant& identificator, const QString& result) = 0;

protected:
    void renameTeamInMatch(CMatch* match, const QVariant &data);
    void renameTeamInMatch(CMatch* match, const QVariant &identificator, const QVariant &data);
    virtual CMatch* getMatch(const QVariant& identificator) const;
    QVariant matchesToVariant() const;
    QVariant matchesToTable(bool withResults = true, bool withSources = false) const;
    void removeMatches();
    QList<Match> toSortererMatches() const;


    QList<CMatch* > _matches;
};


#endif // AMODEL_H

