#ifndef CSTAGECREATOR_H
#define CSTAGECREATOR_H

#include "astagemodel.h"

class LTeamsSorter;
class ADrawer;
class CFieldsManager;
class CFieldsManagerFactory;
class AStageCreator;

class CStagesFactory: public QObject
{

public:
    CStagesFactory();
    ~CStagesFactory();

    AStageModel *createStage(const QString& stageType, const QMap<QString, int>& params);
    AStageModel *createStage(const QString& stageType, const QString& name, const QMap<QString, int>& params);
    AStageModel *createStage(const QString& stageType, const QString& name, const QVariant &params);
    CFieldsManager * createFieldsManager();
    CFieldsManager * createFieldsManager(const QVariant &params);

    void reset();

    void changeSetting(const QString& stageType, const QVariant& setting);
    void registerStageType(const QString& stageType, AStageCreator* creator);

    QVariant toVariant();
    void fromVariant(const QVariant& variant);

private:
    int _nextStageIndex;
    QMap<QString, AStageCreator*> registeredTypes;
    CFieldsManagerFactory* _fieldsFactory;
};

class ASimpleStageCreator
{

public:
    ASimpleStageCreator(){_nextIndex=0;}
    virtual ~ASimpleStageCreator(){}

    ASimpleStageCreator(ASimpleStageCreator const& other) = delete;
    void operator=(ASimpleStageCreator const& other) = delete;

    virtual AStageModel* create();
    virtual AStageModel* create(const QString& name) = 0;

protected:
    QString letterFromIndex(int index);
    int _nextIndex;
    QString _typeName;
    QString _implicitName;
};

class AStageCreator
{

public:
    AStageCreator(){}
    virtual ~AStageCreator();
    virtual AStageModel* create(int globalIndex, const QString& name, const QMap<QString, int>& params) = 0;
    virtual void setSetting(const QVariant& setting) = 0;
    virtual QVariant toVariant();
    virtual void fromVariant(const QVariant& variant);

protected:
    QVariant _globalSetting;
    QVariantMap _usedSettingsByName;
};

class CRobinRoundCreator: public AStageCreator, public ASimpleStageCreator
{

public:
    CRobinRoundCreator();
    ~CRobinRoundCreator() override;
    AStageModel* create() override;
    AStageModel* create(int globalIndex);
    AStageModel* create(const QString& name) override;
    AStageModel* create(int globalIndex, const QString& name, const QMap<QString, int>& params) override;
    void setSetting(const QVariant& setting) override;
    LTeamsSorter * createSorter();

private:
    QString _drawerMethod;
    int _pointsWin;
    int _pointsLose;
    int _pointsDraw;
    int _pointsWinOT;
    int _pointsLossOT;
    bool _isOT;
    QStringList _criteriaOrder;
};

class CPlayOffCreator: public AStageCreator, public ASimpleStageCreator
{

public:
    CPlayOffCreator();
    AStageModel* create() override;
    AStageModel* create(const QString& name) override;
    AStageModel* create(int globalIndex, const QString& name, const QMap<QString, int>& params) override;
    void setSetting(const QVariant& setting) override;

private:
    QString _drawerMethod;

};

class CGroupsStageCreator: public AStageCreator
{

public:
    CGroupsStageCreator();
    ~CGroupsStageCreator() override;
    AStageModel* create(int globalIndex, const QString& name, const QMap<QString, int>& params) override;
    void setSetting(const QVariant& setting) override;

private:
    QString _drawerMethod;
    CRobinRoundCreator * creator;

};

class CFieldsManagerFactory
{

public:
    CFieldsManagerFactory();
    ~CFieldsManagerFactory();

    CFieldsManager* create();
    CFieldsManager* create(const QVariant& parameters);
    QVariant toVariant() const;
    void fromVariant(const QVariant& variant);

private:
    QVariant _parameters;
};

#endif // CSTAGECREATOR_H
