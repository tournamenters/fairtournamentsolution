#include "gconclutionpage.h"
#include "global_model.h"
#include "global_creation.h"

#include <QLabel>
#include <QTableWidget>
#include <QGroupBox>
#include <QPushButton>
#include <QBoxLayout>
#include <QFormLayout>

GConclusionPage::GConclusionPage(QWidget *parent)
    : QWizardPage(parent)
{
    setTitle(tr("Summary"));
    QVBoxLayout * layout = new QVBoxLayout();
    layout->addWidget(createTitle());
    layout->addStretch(1);
    layout->addWidget(createNameBox());
    layout->addStretch(1);
    layout->addWidget(createStartListBox());
    layout->addStretch(2);
    layout->addWidget(createStagesBox());
    layout->addStretch(2);
    layout->addWidget(createFieldsBox());
    layout->addStretch(4);
    setLayout(layout);
}

void GConclusionPage::initializePage()
{
    setName();
    setStartListParams();
    setStagesData();
    setFieldsState();
}

QLabel *GConclusionPage::createTitle()
{
    QLabel * label = new QLabel(tr("Please make sure you have entered all parameters correctly as"
                                  " making changes during a tournament may be problematic."), this);
    label->setWordWrap(true);
    return label;
}

QGroupBox *GConclusionPage::createNameBox()
{
    QGroupBox * boxName = new QGroupBox(tr("Tournament"), this);
    QFormLayout * nameLayout = new QFormLayout();
    laName = new QLabel(this);
    nameLayout->addRow(tr("Name:"), laName);
    boxName->setLayout(nameLayout);
    return boxName;
}

QGroupBox *GConclusionPage::createStartListBox()
{
    QGroupBox * boxStartList = new QGroupBox(tr("Start list"), this);
    QFormLayout * boxLayout = new QFormLayout();
    laNumTeams = new  QLabel(this);
    pbIsStartListRanked = new QPushButton(this);
    pbIsStartListRanked->setFlat(true);
    pbIsStartListRanked->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);

    boxLayout->addRow(tr("Participants number:"), laNumTeams);
    boxLayout->addRow(tr("Participants  are ranked:"), pbIsStartListRanked);
    boxStartList->setLayout(boxLayout);
    return boxStartList;
}

QGroupBox *GConclusionPage::createStagesBox()
{
    QGroupBox * boxStages = new QGroupBox(tr("Stages"), this);
    QVBoxLayout * boxLayout = new QVBoxLayout();
    boxLayout->addWidget(createStagesTable());
    boxStages->setLayout(boxLayout);
    return boxStages;
}

QTableWidget *GConclusionPage::createStagesTable()
{
    stagesTable = new QTableWidget(4,1, this);
    stagesTable->setVerticalHeaderItem(0, new QTableWidgetItem(tr("Type")));
    stagesTable->setVerticalHeaderItem(1, new QTableWidgetItem(tr("Incoming")));
    stagesTable->setVerticalHeaderItem(2, new QTableWidgetItem(tr("Eliminated")));
    stagesTable->setVerticalHeaderItem(3, new QTableWidgetItem(tr("Proceeding")));
    stagesTable->setAttribute(Qt::WA_TransparentForMouseEvents);
    stagesTable->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    stagesTable->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    stagesTable->setMinimumHeight((stagesTable->rowCount()+1)*stagesTable->rowHeight(0));
    return stagesTable;
}

QGroupBox *GConclusionPage::createFieldsBox()
{
    boxFields = new QGroupBox(tr("Fields schedule"), this);
    boxFields->setCheckable(true);
    boxFields->setAttribute(Qt::WA_TransparentForMouseEvents);
    boxFields->setFocusPolicy(Qt::NoFocus);

    return boxFields;
}

QLayout* GConclusionPage::createYesFieldsLayout(int numFields)
{
    QFormLayout* yesFieldsLayout = new QFormLayout();
    QLabel* laFields = new  QLabel("<b>" + QString::number(numFields) + "</b>");
    yesFieldsLayout->addRow(tr("Fields number:"), laFields);
    return yesFieldsLayout;
}

QLayout* GConclusionPage::createNoFieldsLayout()
{
    QLabel* laFields = new QLabel(tr("[No fields schedule.]"),this);
    QVBoxLayout* noFieldsLayout = new QVBoxLayout();
    noFieldsLayout->addWidget(laFields);
    noFieldsLayout->setAlignment(Qt::AlignCenter);
    return noFieldsLayout;
}

void GConclusionPage::setName()
{
    laName->setText("<b>" + field("introduction.name").toString() + "</b>");
}

void GConclusionPage::setStartListParams()
{
    laNumTeams->setText("<b>" + field("start_list.num_teams").toString() + "</b>");
    if(field("start_list.ranked").toBool()){
        pbIsStartListRanked->setIcon(QIcon(":/icons/check.png"));
    }
    else{
        pbIsStartListRanked->setIcon(QIcon(":/icons/end.png"));
    }
}

void GConclusionPage::setStagesData()
{
    QList<QVariant> stagesData = field("stages.data").toList();
    stagesTable->setColumnCount(stagesData.size());
    int col = 0;
    for(const auto& stage : stagesData){
        QVariantMap map = stage.toMap();
        setStagesTableHorizontalHeader(col, QString::number(col+1)
                                           + ". " + map["name"].toString());
        int numTeams = map[key_PARAMETERS].toMap()[key_NUM_TEAMS].toInt();
        QStringList columnCellTexts;
        QString type = CreationConstants::getInstance().getFormatsMap()[map[key_STAGE_TYPE].toString()];
        columnCellTexts.push_back(type);
        columnCellTexts.push_back(QString::number(numTeams));
        if(col<stagesData.size()-1){
            int nextStageTeams = stagesData[col+1].toMap()[key_PARAMETERS].toMap()[key_NUM_TEAMS].toInt();
            columnCellTexts.push_back(QString::number(numTeams - nextStageTeams));
            columnCellTexts.push_back(QString::number(nextStageTeams));          
        }
        setStagesTableColumn(col, columnCellTexts);
        col++;
    }
    setLastStageIcons(col-1);
}

void GConclusionPage::setStagesTableColumn(int col, const QStringList &cellTexts)
{
    for(int row = 0; row<cellTexts.size(); row++){
        setStagesTableCellText(row, col, cellTexts[row]);
    }
}

void GConclusionPage::setStagesTableCellText(int row, int col, const QString &text)
{
    if(stagesTable->item(row,col)){
        stagesTable->item(row,col)->setText(text);
    }
    else{
        stagesTable->setItem(row,col, new QTableWidgetItem(text));
        stagesTable->item(row,col)->setTextAlignment(Qt::AlignCenter);
    }
}

void GConclusionPage::setStagesTableHorizontalHeader(int col, const QString &text)
{
    if(stagesTable->horizontalHeaderItem(col)){
        stagesTable->horizontalHeaderItem(col)->setText(text);
    }
    else{
        stagesTable->setHorizontalHeaderItem(col, new QTableWidgetItem(text));
    }
}

void GConclusionPage::setLastStageIcons(int col)
{
    QIcon icon = QIcon(":icons/end.png");
    for(int row = 2; row<4;row++){
        QLabel *label = new QLabel(this);
        label->setAlignment(Qt::AlignCenter);
        label->setPixmap(icon.pixmap(stagesTable->rowHeight(0)*3/5,
                                     stagesTable->rowHeight(0)*3/5));
        stagesTable->setCellWidget(row, col, label);
    }
}

void GConclusionPage::setFieldsState()
{
    bool doFields = field("fields.do").toBool();
    boxFields->setChecked(doFields);
    if(boxFields->layout()){
        delete boxFields->layout();
    }
    if(doFields){
        boxFields->setLayout(createYesFieldsLayout(field("fields.num_fields").toInt()));
    }
    else{
        boxFields->setLayout(createNoFieldsLayout());
    }
}
