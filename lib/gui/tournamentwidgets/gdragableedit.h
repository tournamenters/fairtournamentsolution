#ifndef GDRAGABLEEDIT_H
#define GDRAGABLEEDIT_H

#include <QLineEdit>
#include <QPointer>

class QMimeData;
class GDragAndDropManager;

class GDragableEdit : public QLineEdit
{
    Q_OBJECT

public:
    explicit GDragableEdit(QWidget *parent = nullptr);
    virtual ~GDragableEdit() override;

    virtual void setText(const QString& text);
    void setOpen(bool open);
    void setDragAndDrop(bool enabled);

    virtual void dropEvent(QDropEvent *event) override = 0;
    void dragEnterEvent(QDragEnterEvent *event) override;
    void dragMoveEvent(QDragMoveEvent* event) override;

    void setDataToDrag(const QVariant &dataToDrag);
    void setGroupName(const QString &groupName);

protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent * event) override;
    void provideDragAction();

    QVariant createDragData();
    QVariant getDragData(const QVariant &drag);

    bool contentChanged();
    virtual bool isMyDrop(const QMimeData *data);

    QPointer<QMenu> mnuContext;
    QString _textContent;
    QString _textOld;
    QVariant _dataToDrag;
    QString _type;
    QString _groupName;

private:
    void handleDragEvent(QDragMoveEvent *event);

    GDragAndDropManager * _dragableManager;
    bool _dragAndDropEnabled;
    bool _openForAllDrops;

private slots:
    virtual void textEditation() = 0;
};

#endif // GDRAGABLEEDIT_H
