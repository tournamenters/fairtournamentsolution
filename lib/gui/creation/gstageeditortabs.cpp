#include "gstageeditortabs.h"
#include "global_creation.h"
#include <QLabel>
#include <QBoxLayout>
#include <QPushButton>
#include <QComboBox>
#include <QSpinBox>
#include <QDialog>
#include <QDialogButtonBox>
#include <QGroupBox>
#include <QFormLayout>
#include <QLineEdit>
#include <QToolButton>
#include <QMessageBox>
#include "global_model.h"
#include "gpointssettingbox.h"

AStageEditorTab::AStageEditorTab(QWidget *parent)
    :QScrollArea (parent),
    _index(-1)
{
    QVBoxLayout * layout = new QVBoxLayout();
    layout->addWidget(createNameGroupBox());
    createNumTeamsSpinBox();
    setLayout(layout);
}

AStageEditorTab::~AStageEditorTab()
{

}

int AStageEditorTab::getIndex() const
{
    return _index;
}

void AStageEditorTab::setIndex(int index)
{
    _index = index;
}

void AStageEditorTab::setName(const QString &name)
{
    leName->blockSignals(true);
    leName->setText(name);
    leName->blockSignals(false);
}

void AStageEditorTab::nameChange()
{
    if(!leName->text().isEmpty()){
        emit nameChanged(_index, leName->text());
    }
}

void AStageEditorTab::dataChange()
{
    emit dataChanged(_index, getData());
}

void AStageEditorTab::addItemsToComboBox(QComboBox* box, const QMap<QString, QString> &keyLabelPossibilities)
{
    for(const auto& key : keyLabelPossibilities.keys()){
        box->addItem(keyLabelPossibilities[key], key);
    }
}

QGroupBox *AStageEditorTab::createNameGroupBox()
{
    QGroupBox * boxName = new QGroupBox(this);
    QFormLayout * titLayout = new QFormLayout();
    boxName->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
    titLayout->addRow(tr("Name:"), createNameLineEdit());
    boxName->setLayout(titLayout);
    return boxName;
}

QSpinBox *AStageEditorTab::createNumTeamsSpinBox()
{
    sbNumTeams = new QSpinBox(this);
    connect(sbNumTeams, SIGNAL(valueChanged(int)),
            this, SIGNAL(numTeamsChanged()));
    return sbNumTeams;
}

QLineEdit *AStageEditorTab::createNameLineEdit()
{
    leName = new QLineEdit(this);
    leName->setClearButtonEnabled(true);
    leName->setPlaceholderText(tr("Please enter a name for your stage."));
    connect(leName,SIGNAL(editingFinished()),
            this,SLOT(nameChange()));
    return leName;
}

QVariant AStageEditorTab::getData() const
{
    return _data;
}

void AStageEditorTab::setNumTeams(int numTeams)
{
    sbNumTeams->setValue(numTeams);
}

void AStageEditorTab::setNumTeamsRanges(int min, int max)
{
    sbNumTeams->setRange(min,max);
}

int AStageEditorTab::numTeams()
{
    return sbNumTeams->value();
}


GGroupsEditorTab::GGroupsEditorTab(QWidget * parent)
    : AStageEditorTab(parent)
{
    layout()->addWidget(createParametersBox());
    layout()->addWidget(createAlgorithmsBox());
    layout()->addWidget(createAdvancedSettingBox());
    createAdvancedSettingDialog();
    connect(this, SIGNAL(nameChanged(int, QString)),
            this, SLOT(createDataVariant()));
}

GGroupsEditorTab::~GGroupsEditorTab()
{
    delete  otherSettingDialog;
}

void GGroupsEditorTab::setNumTeams(int numTeams)
{
    sbNumTeams->setValue(numTeams);
    sbNumGroups->setValue(numTeams/4);
}

void GGroupsEditorTab::setNumTeamsRanges(int minTeams, int maxTeams)
{
    int oldValue = sbNumTeams->value();
    sbNumTeams->setRange(minTeams, maxTeams);
    if(oldValue!=sbNumTeams->value()){
        sbNumGroups->setValue(sbNumTeams->value()/4);
    }
}

AStageEditorTab *GGroupsEditorTab::create()
{
    return new GGroupsEditorTab();
}

void GGroupsEditorTab::numTeamsChange()
{
    int numTeams = sbNumTeams->value();
    if(numTeams>3){
        sbNumGroups->setRange(2,numTeams/2+1);
        sbSizeGroups->setMaximum(numTeams/2+1);
    }
    else{
        sbNumGroups->setRange(1,numTeams/2);
        sbSizeGroups->setMaximum(numTeams);
    }
    numGroupsChange();
}

void GGroupsEditorTab::numGroupsChange()
{
    int numGroups = sbNumGroups->value();
    int groupSize = numTeams()/numGroups;
    if(numTeams()>numGroups*groupSize){
        groupSize++;
    }
    sbSizeGroups->blockSignals(true);
    sbSizeGroups->setValue(groupSize);
    sbSizeGroups->blockSignals(false);
    createDataVariant();
}

void GGroupsEditorTab::sizeGroupsChange()
{
    int sizeGroups = sbSizeGroups->value();
    int numGroups = numTeams()/sizeGroups;
    if(numTeams()>sizeGroups*numGroups){
        numGroups++;
    }
    sbNumGroups->blockSignals(true);
    sbNumGroups->setValue(numGroups);
    sbNumGroups->blockSignals(false);
    createDataVariant();
}

void GGroupsEditorTab::createDataVariant()
{
    _data.clear();
    QVariantMap dataMap;
    dataMap[key_PARAMETERS] = getParametersData();
    dataMap[key_SETTING] = getSettingData();
    dataMap[key_STAGE_TYPE] = key_GROUPS_STAGE;
    dataMap[key_NAME] = leName->text();
    _data = dataMap;
    dataChange();
}

void GGroupsEditorTab::showAdvancedSetting()
{
    QVariant oldData = ordererBox->getData();
    if(otherSettingDialog->exec() == QDialog::Accepted){
        createDataVariant();
        return;
    }
    ordererBox->setData(oldData);
}

void GGroupsEditorTab::createParametersSpinBoxes()
{
    sbNumGroups = new QSpinBox(this);
    sbNumGroups->setMinimum(1);
    connect(sbNumGroups, SIGNAL(valueChanged(int)),
            this, SLOT(numGroupsChange()));
    sbSizeGroups = new QSpinBox(this);
    sbSizeGroups->setValue(4);
    sbSizeGroups->setMinimum(2);
    connect(sbSizeGroups,SIGNAL(valueChanged(int)),
            this, SLOT(sizeGroupsChange()));
    connect(sbNumTeams,SIGNAL(valueChanged(int)),
            this, SLOT(numTeamsChange()));
}

void GGroupsEditorTab::createAdvancedSettingDialog()
{
    ordererBox = new GPointsSettingBox(this);
    otherSettingDialog = new QDialog(this);
    otherSettingDialog->setWindowTitle(leName->text() + tr("| Advance setting"));
    QVBoxLayout * layout = new QVBoxLayout();
    layout->addWidget(ordererBox);
    QDialogButtonBox * buttonBox = new QDialogButtonBox( {QDialogButtonBox::Ok, QDialogButtonBox::Cancel});
    connect(buttonBox, SIGNAL(accepted()),
            otherSettingDialog, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()),
            otherSettingDialog, SLOT(reject()));
    layout->addWidget(buttonBox);
    otherSettingDialog->setLayout(layout);
}

QGroupBox *GGroupsEditorTab::createParametersBox()
{
    createParametersSpinBoxes();
    QGroupBox * boxParams = new QGroupBox(tr("Parameters"),this);
    QFormLayout * parLayout = new QFormLayout();
    parLayout->addRow(tr("Participants number:"), sbNumTeams);
    parLayout->addRow(tr("Groups number:"), sbNumGroups);
    parLayout->addRow(tr("Groups size:"), sbSizeGroups);
    boxParams->setLayout(parLayout);
    return boxParams;
}

QGroupBox *GGroupsEditorTab::createAlgorithmsBox()
{
    QGroupBox * boxAlgorithms = new QGroupBox(tr("Algorithms"),this);
    QFormLayout * algLayout = new QFormLayout();
    algLayout->addRow(tr("Groups creation:"), createCreationComboBox());
    algLayout->addRow(tr("Matches order:"), createMatchesComboBox());
    boxAlgorithms->setLayout(algLayout);
    return boxAlgorithms;
}

QGroupBox *GGroupsEditorTab::createAdvancedSettingBox()
{
    QGroupBox * boxOtherSetting = new QGroupBox(tr("Advanced Setting"),this);
    QFormLayout * algLayout = new QFormLayout();
    algLayout->addRow(tr("Show advanced setting dialog: "), createAdvancedSettingButton());
    boxOtherSetting->setLayout(algLayout);
    return boxOtherSetting;
}

QComboBox *GGroupsEditorTab::createCreationComboBox()
{
    cbAlgorithmCreation = new QComboBox(this);
    addItemsToComboBox(cbAlgorithmCreation, CModelConstants::Instance().getKeyTextGroupsCreationMap());
    connect(cbAlgorithmCreation,SIGNAL(currentIndexChanged(int)),
            this, SLOT(createDataVariant()));
    return cbAlgorithmCreation;
}

QComboBox *GGroupsEditorTab::createMatchesComboBox()
{
    cbAlgorithmMatchesOrder = new QComboBox(this);
    addItemsToComboBox(cbAlgorithmMatchesOrder, CModelConstants::Instance().getKeyTextMatchesOrderMap());
    connect(cbAlgorithmMatchesOrder,SIGNAL(currentIndexChanged(int)),
            this, SLOT(createDataVariant()));;
    return cbAlgorithmMatchesOrder;
}

QPushButton *GGroupsEditorTab::createAdvancedSettingButton()
{
    QPushButton* btnOtherSetting = new QPushButton(tr("Advanced Setting"),this);
    connect(btnOtherSetting, SIGNAL(clicked()),
            this, SLOT(showAdvancedSetting()));
    return btnOtherSetting;
}

QVariantMap GGroupsEditorTab::getParametersData()
{
    QVariantMap paramsMap;
    paramsMap[key_NUM_TEAMS] = sbNumTeams->value();
    paramsMap[key_NUM_GROUPS] = sbNumGroups->value();
    paramsMap[key_SIZE_GROUPS] = sbSizeGroups->value();
    return paramsMap;
}

QVariantMap GGroupsEditorTab::getSettingData()
{
    QVariantMap settingMap;
    settingMap["alg_creation"] = cbAlgorithmCreation->currentData();
    settingMap["alg_matches_order"] = cbAlgorithmMatchesOrder->currentData();
    settingMap["orderer"] = ordererBox->getData();
    settingMap["name"] = leName->text();
    return settingMap;
}

GPlayOffEditorTab::GPlayOffEditorTab(QWidget *parent)
    : AStageEditorTab(parent)
{
    layout()->addWidget(createParametersBox());
    layout()->addWidget(createAlgorithmsBox());
    connect(this, SIGNAL(nameChanged(int, QString)),
            this, SLOT(createDataVariant()));
}

GPlayOffEditorTab::~GPlayOffEditorTab()
{

}

AStageEditorTab *GPlayOffEditorTab::create()
{
    return new GPlayOffEditorTab();
}

void GPlayOffEditorTab::createDataVariant()
{
    _data.clear();
    QVariantMap paramsMap;
    paramsMap[key_NUM_TEAMS] = sbNumTeams->value();

    QVariantMap settingMap;
    settingMap["alg_creation"] = cbAlgorithmCreation->currentData();
    settingMap["name"] = leName->text();

    QVariantMap dataMap;
    dataMap[key_PARAMETERS] = paramsMap;
    dataMap[key_SETTING] = settingMap;
    dataMap[key_STAGE_TYPE] = key_PLAY_OFF;
    dataMap[key_NAME] = leName->text();
    _data = dataMap;
    dataChange();
}

QGroupBox *GPlayOffEditorTab::createParametersBox()
{
    connect(sbNumTeams,SIGNAL(valueChanged(int)),
            this, SLOT(createDataVariant()));
    QGroupBox * boxParams = new QGroupBox(tr("Parameters"), this);
    QFormLayout * parLayout = new QFormLayout();
    parLayout->addRow(tr("Participants  number:"), sbNumTeams);
    boxParams ->setLayout(parLayout);
    return boxParams;
}

QGroupBox *GPlayOffEditorTab::createAlgorithmsBox()
{
    QGroupBox * boxAlgorithms = new QGroupBox(tr("Algorithms"), this);
    QFormLayout * algLayout = new QFormLayout();
    algLayout->addRow(tr("Play off creation:"), createCreationComboBox());
    boxAlgorithms->setLayout(algLayout);
    return boxAlgorithms;
}

QComboBox *GPlayOffEditorTab::createCreationComboBox()
{
    cbAlgorithmCreation = new QComboBox(this);
    addItemsToComboBox(cbAlgorithmCreation, CModelConstants::Instance().getKeyTextPlayOffMap());
    connect(cbAlgorithmCreation, SIGNAL(currentIndexChanged(int)),
            this, SLOT(createDataVariant()));;
    return cbAlgorithmCreation;
}

AStageEditorTab *GRobinRoundEditorTab::create()
{
    return new GRobinRoundEditorTab();
}

GRobinRoundEditorTab::GRobinRoundEditorTab(QWidget * parent)
    : AStageEditorTab(parent)
{
    layout()->addWidget(createParametersBox());
    layout()->addWidget(createAlgorithmsBox());
    layout()->addWidget(createAdvancedSettingBox());
    createAdvancedSettingDialog();
    connect(this, SIGNAL(nameChanged(int, QString)),
            this, SLOT(createDataVariant()));
}

GRobinRoundEditorTab::~GRobinRoundEditorTab()
{
    delete  otherSettingDialog;
}

void GRobinRoundEditorTab::createDataVariant()
{
    _data.clear();
    QVariantMap paramsMap;
    paramsMap[key_NUM_TEAMS] = QVariant(sbNumTeams->value());

    QVariantMap settingMap;
    settingMap["alg_matches_order"] = cbAlgorithmMatchesOrder->currentData();
    settingMap["orderer"] = ordererBox->getData();
    settingMap["name"] = leName->text();

    QVariantMap dataMap;
    dataMap[key_PARAMETERS] = paramsMap;
    dataMap[key_SETTING] = settingMap;
    dataMap[key_STAGE_TYPE] = key_ROBIN_ROUND;
    dataMap[key_NAME] = leName->text();
    _data = QVariant(dataMap);
    dataChange();
}

void GRobinRoundEditorTab::showAdvancedSetting()
{
    QVariant oldData = ordererBox->getData();
    if(otherSettingDialog->exec() == QDialog::Accepted){
        createDataVariant();
        return;
    }
    ordererBox->setData(oldData);
}

QGroupBox *GRobinRoundEditorTab::createParametersBox()
{
    connect(sbNumTeams,SIGNAL(valueChanged(int)),
            this, SLOT(createDataVariant()));
    QGroupBox * boxParams = new QGroupBox(tr("Parameters"));
    QFormLayout * parLayout = new QFormLayout();
    parLayout->addRow(tr("Participants number:"), sbNumTeams);
    boxParams ->setLayout(parLayout);
    layout()->addWidget(boxParams);
    return boxParams;
}

QGroupBox *GRobinRoundEditorTab::createAlgorithmsBox()
{
    QGroupBox * boxAlgorithms = new QGroupBox(tr("Algorithms"));
    QFormLayout * algLayout = new QFormLayout();
    algLayout->addRow(tr("Matches order:"), createMatchesComboBox());
    boxAlgorithms->setLayout(algLayout);
    return boxAlgorithms;
}

QComboBox *GRobinRoundEditorTab::createMatchesComboBox()
{
    cbAlgorithmMatchesOrder = new QComboBox();
    addItemsToComboBox(cbAlgorithmMatchesOrder, CModelConstants::Instance().getKeyTextMatchesOrderMap());
    connect(cbAlgorithmMatchesOrder, SIGNAL(currentIndexChanged(int)),
            this, SLOT(createDataVariant()));
    return cbAlgorithmMatchesOrder;
}

QGroupBox *GRobinRoundEditorTab::createAdvancedSettingBox()
{
    QGroupBox * boxOtherSetting = new QGroupBox(tr("Advanced Setting"),this);
    QFormLayout * algLayout = new QFormLayout();
    algLayout->addRow(tr("Show advanced setting dialog: "), createAdvancedSettingButton());
    boxOtherSetting->setLayout(algLayout);
    return boxOtherSetting;
}

QPushButton *GRobinRoundEditorTab::createAdvancedSettingButton()
{
    QPushButton* btnOtherSetting = new QPushButton(tr("Advanced Setting"),this);
    connect(btnOtherSetting, SIGNAL(clicked()),
            this, SLOT(showAdvancedSetting()));
    return btnOtherSetting;
}

void GRobinRoundEditorTab::createAdvancedSettingDialog()
{
    ordererBox = new GPointsSettingBox(this);
    otherSettingDialog = new QDialog(this);
    otherSettingDialog->setWindowTitle(leName->text() + tr("| Advance setting"));
    QVBoxLayout * layout = new QVBoxLayout();
    layout->addWidget(ordererBox);
    QDialogButtonBox * buttonBox = new QDialogButtonBox( {QDialogButtonBox::Ok, QDialogButtonBox::Cancel});
    connect(buttonBox, SIGNAL(accepted()),
            otherSettingDialog, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()),
            otherSettingDialog, SLOT(reject()));
    layout->addWidget(buttonBox);
    otherSettingDialog->setLayout(layout);
}

