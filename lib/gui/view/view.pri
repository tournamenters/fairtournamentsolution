include($$PWD/../../core/core.pri)
include($$PWD/../present/present.pri)
include($$PWD/../tournamentwidgets/tournamentwidgets.pri)

win32:CONFIG(release, debug|release): LIBS += -L$$TOP_BUILDDIR/lib/gui/view/release/ -lview
else:win32:CONFIG(debug, debug|release): LIBS += -L$$TOP_BUILDDIR/lib/gui/view/debug/ -lview
else:unix: LIBS += -L$$TOP_BUILDDIR/lib/gui/view/ -lview

INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/gui/view/release/libview.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/gui/view/debug/libview.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/gui/view/release/view.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/gui/view/debug/view.lib
else:unix: PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/gui/view/libview.a
