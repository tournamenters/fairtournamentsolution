#ifndef AORDERER_H
#define AORDERER_H

#include <QPair>
#include <QList>
#include <QString>
#include <QMap>

struct Element;
struct Match;

const QString NUM_POINTS = "num_points";
const QString MUTUAL_MATCH = "mutual_match";
const QString MINI_TABLE = "mini_table";
const QString SCORE_DIFF = "score_diff";
const QString SCORE_RATIO = "score_ratio";
const QString NUM_GOALS = "num_goals";
const QString NUM_WINS = "num_wins";

class LTeamsSorter
{

public:
    LTeamsSorter(const QList<QString>& criteriumsOrder);
    virtual ~LTeamsSorter();

    virtual QList<int> sortTeams(const QList<int>& teams, const QList<Match>& matches);
    virtual QList<int> sortTeams(const QList<int>& teams, const QList<QList<int> > &criteriaValues);

    QList<int> getOrder();
    QList<int> getPoints();
    QList<QPair<int,int>> getScore();

    void setPointsOverTime(int perWin, int perLoss);
    void setPoints(int perWin, int perDraw, int perLoss = 0);
    LTeamsSorter* copy();

protected:
    QList<Element*> createElemetsList(const QList<int>& teams, const QList<Match>& matches);

    bool arePerfectlySorted(const QList<Element *> &elements) const;

    Element *countElementPoints(int teamIndex, const QList<Match> &matches);

    void sortByCriterium(const QList<Element*>& elements, const QString& criterium);
    void sortByPoints(const QList<Element*>& elements);
    void sortByMutualPoints(const QList<Element*>& elements);
    void sortByMutualScore(const QList<Element*>& elements);
    void sortByScore(const QList<Element*>& elements);
    void sortByGoals(const QList<Element*>& elements);

    void sortElements(const QList<Element*>& elements, const QList<int> values, int minPosition = 1);

    QList<QList<Element*>> findUnsortedGroups(const QList<Element*>& elements);
    QList<Element*> findMutualStats(const QList<Element*>& elements);
    QList<Match> findMutualMatches(const QList<Element*>& elements);

private:
    int _pointsPerWin = 3;
    int _pointsPerDraw = 1;
    int _pointsPerLoss = 0;

    int _pointsPerWinOverTime = 2;
    int _pointsPerLossOverTime = 1;

    void clearElemets();

    QList<int> _lastOrder;
    QList<Element*> _allElements;
    QList<Match> _matches;
    QList<QString> _criteriumsOrder;
};

struct Element{

    int _index;
    int _myGoals;
    int _othersGoals;
    int _myPoints;
    int _currentPlace;

    Element();
    Element(int index);

};

struct Match
{
    int _hostIndex;
    int _guestIndex;
    int _hostScore;
    int _guestScore;
    bool _overTime;

    Match();
    Match(int host, int guest);
    Match(int host, int guest, int hostScore, int guestScore);

    bool playerWon(int teamIndex) const;
    bool wasPlayed() const;
    bool hostWin() const;
    bool guestWin() const;
    bool draw() const;

    bool contains(int index) const;
};

#endif // AORDERER_H
