#include "gmatchestableview.h"
#include "gmatchestable.h"
#include "amodel.h"

GMatchesTableView::GMatchesTableView(AMatchesModel *model, QObject *parent)
    :AMatchesView (parent)
{
    _title = model->getName();
    _model = model;
    _identificator = model->getIdentificator();
    _table = new GMatchesTable(_identificator);
    _table->setTitle(_title);
    update();
    connectTeamsTable(_table);
    connectMatchesTable(_table);
    connect(model, SIGNAL(dataChanged()),
            this, SLOT(update()));
}

GMatchesTableView::~GMatchesTableView()
{

}

QWidget *GMatchesTableView::getViewWidget()
{
    return _table;
}

AExportable *GMatchesTableView::getExportableWidget()
{
    return _table;
}

void GMatchesTableView::update()
{
    _table->setData(_model->getData());
}

void GMatchesTableView::findText(const QString &text)
{
    _table->findText(text);
}
