# Fair Tournament

The program is a tool designed for organizers of amateur sports tournaments. It helps end users to schedule tournament matches, draw participants, process results and export the tournaments data. The program is designed for PC running Windows 10.

## Installation

The easier way to install the program is download and run the installer available on (https://bitbucket.org/tournamenters/fairtournamentsolution/downloads/).

The source code can be obtained by cloning this [repository](https://bitbucket.org/tournamenters/fairtournamentsolution/).
The project is based on qmake, a makefiles generator, and Qt framework, ver. 5.12.1. Qt Creator ver. 4.9.1 is used for development.
Google test framework that is available on [googletest repository](https://github.com/google/googletest) is also necessary for running unit tests.

## Contributing

Changes and pull requests are welcome. 

## License

Copyright (C) 2019  Ale� Suchomel

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see [GNU licenses](https://www.gnu.org/licenses/>).