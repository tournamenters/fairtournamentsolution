<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ">
<context>
    <name>AppSetter</name>
    <message>
        <location filename="appsetter.cpp" line="179"/>
        <source>Reset Requered</source>
        <translation>Požadován restart</translation>
    </message>
    <message>
        <location filename="appsetter.cpp" line="180"/>
        <source>The change will take effect after restart of this application.</source>
        <translation>Změna bude provedena až po restartu aplikace.</translation>
    </message>
</context>
</TS>
