<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ">
<context>
    <name>Assistant</name>
    <message>
        <location filename="assistant.cpp" line="51"/>
        <source>Unable to launch Qt Assistant (%1)</source>
        <translation>Qt Assistant nemohl být otevřen.(%1)</translation>
    </message>
</context>
<context>
    <name>GOpenPage</name>
    <message>
        <location filename="gopenpage.cpp" line="50"/>
        <source>Recent </source>
        <translation>Nedávné </translation>
    </message>
    <message>
        <location filename="gopenpage.cpp" line="50"/>
        <source>s:</source>
        <translation>e:</translation>
    </message>
</context>
<context>
    <name>GThemeSettingDialog</name>
    <message>
        <location filename="gthemesettingdialog.cpp" line="10"/>
        <source>Theme Setting</source>
        <translation>Nastavení vzhledu</translation>
    </message>
    <message>
        <location filename="gthemesettingdialog.cpp" line="13"/>
        <source>Please select desing of this application.</source>
        <oldsource>Pleace select desing of this application.</oldsource>
        <translation>Prosím, zvolte nastavení vzhledu aplikace.</translation>
    </message>
    <message>
        <location filename="gthemesettingdialog.cpp" line="17"/>
        <source>Style</source>
        <translation>Styl</translation>
    </message>
    <message>
        <location filename="gthemesettingdialog.cpp" line="20"/>
        <source>Palette</source>
        <translation>Barevná paleta</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="39"/>
        <source>Main ToolBar</source>
        <translation>Hlavní toolbar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="40"/>
        <source>&amp;Find</source>
        <translation>&amp;Najít</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="41"/>
        <source>Drag and Drop &amp;Results</source>
        <translation>Táhnutelné &amp;výsledky</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="66"/>
        <source>Application successfully started!</source>
        <translation>Aplikace úspěšně spuštěna!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="94"/>
        <source>&amp;Start List</source>
        <translation>&amp;Startovní listina</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="95"/>
        <source>&amp;Matches</source>
        <translation>Seznam &amp;zápasů</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="96"/>
        <source>&amp;Fields</source>
        <translation>&amp;Hřiště</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="97"/>
        <source>&amp;Tournament Stats</source>
        <translation>&amp;Informace o turnaji</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="98"/>
        <source>Notes</source>
        <translation>Poznámky</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="107"/>
        <source>Find</source>
        <translation>Najít</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="108"/>
        <source>Find and highlight participants with a given name.</source>
        <oldsource>Find and highlight participants with a given name</oldsource>
        <translation>Najdi a zvýrazni účastníky daného jména.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="130"/>
        <source>&amp;File</source>
        <translation>&amp;Soubor</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="131"/>
        <source>&amp;New...</source>
        <translation>&amp;Nový...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="134"/>
        <source>Open new tournament creation dialog.</source>
        <translation>Otevři dialog pro vytváření nového turnaje.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="135"/>
        <source>&amp;Open...</source>
        <translation>&amp;Otevři...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="138"/>
        <source>Open native file dialog and browse for old tournament file.</source>
        <translation>Otevři souborový dialog a hledej starý turnajový soubor.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="139"/>
        <source>Open &amp;Recent</source>
        <translation>Otevři &amp;nedávné</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="146"/>
        <source>&amp;Save</source>
        <translation>&amp;Ulož</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="149"/>
        <source>Save changes of active tournament to its file.</source>
        <translation>Ulož změny aktivního turnaje do jeho souboru.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="150"/>
        <source>Save &amp;As...</source>
        <translation>Ulož &amp;jako...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="153"/>
        <source>Open native file dialog and chose where to save active tournament.</source>
        <translation>Otevři souborový dialog a urči kam uložit aktivní turnaj.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="155"/>
        <source>&amp;Close</source>
        <translation>&amp;Zavři</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="158"/>
        <source>Close active tournament.</source>
        <translation>Zavři aktivní turnaj.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="159"/>
        <source>&amp;Exit</source>
        <translation>&amp;Exit</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="162"/>
        <source>Quit Fair Tournament application.</source>
        <translation>Zavři aplikaci Fair Torunament.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="167"/>
        <source>&amp;Edit</source>
        <translation>&amp;Úpravy</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="168"/>
        <source>&amp;Undo</source>
        <translation>&amp;Zpět</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="171"/>
        <source>Undo last change.</source>
        <translation>Vrať poslední změnu.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="172"/>
        <source>&amp;Redo</source>
        <translation>&amp;Dopředu</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="175"/>
        <source>Redo last undo.</source>
        <translation>Vrať poslední zpět.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="180"/>
        <source>Show find toolbar.</source>
        <translation>Zobraz vyhledávání.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="184"/>
        <source>Edit &amp;Start List</source>
        <translation>Změň &amp;startovní listinu</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="186"/>
        <source>Open a dialog for the start list editing.</source>
        <translation>Otevři dialog pro editování startovní listiny.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="191"/>
        <source>&amp;View</source>
        <translation>&amp;Zobrazení</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="193"/>
        <source>Show or hide start list window.</source>
        <translation>Zobraz startovní listinu.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="195"/>
        <source>Show or hide matches window.</source>
        <translation>Zobraz seznam zápasů.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="197"/>
        <source>Show or hide fields window.</source>
        <translation>Zobraz hřiště.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="200"/>
        <source>Show or hide tournament stats window.</source>
        <translation>Zobraz informace o turnaji.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="202"/>
        <source>Show or hide notes window.</source>
        <translation>Zobraz poznámky.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="205"/>
        <source>Show or hide drag and drop results&apos; toolbar.</source>
        <translation>Zobraz výsledkový toolbar.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="210"/>
        <source>&amp;Setting</source>
        <translation>&amp;Nastavení</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="211"/>
        <source>Font...</source>
        <translation>Písmo...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="214"/>
        <source>Set the application&apos;s font.</source>
        <translation>Nastav písmo aplikace.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="215"/>
        <source>Appearance...</source>
        <translation>Vzhled...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="218"/>
        <source>Set the appearance of the application.</source>
        <translation>Nastav vzhled aplikace.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="220"/>
        <source>Language</source>
        <translation>Jazyk</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="226"/>
        <source>E&amp;xport</source>
        <translation>E&amp;xport</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="227"/>
        <source>Open &amp;Report Editor</source>
        <translation>Otevři &amp;textový editor</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="230"/>
        <source>Open a simple text editor, import tournament data into it and export the pdf.</source>
        <translation>Otevři jednoduchý textový editor, vlož turnajová data a exportuj je do PDF dokumentu.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="231"/>
        <source>Export Individual &amp;Components...</source>
        <translation>Exportuj jednotlivé komponenty...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="234"/>
        <source>Generate separate pictures and html files of the chosen tournament components and save them to the selected folder.</source>
        <translation>Vygeneruj obrázky a html soubory vybraných komponentů turnaje a ulož je do zvolené složky.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="239"/>
        <source>&amp;Help</source>
        <translation>&amp;Pomoc</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="240"/>
        <source>&amp;About...</source>
        <translation>&amp;O programu...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="243"/>
        <source>Show the application&apos;s About box.</source>
        <translation>O programu.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="244"/>
        <source>About &amp;Qt...</source>
        <translation>O &amp;Qt...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="247"/>
        <source>Show the Qt library&apos;s About box.</source>
        <translation>O Qt.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="433"/>
        <source>Fair Tournament is an open source desktop application for scheduling and organising sport events.

</source>
        <oldsource>Fair Tournament is an open source desctop application for scheduling and organising sport events.

</oldsource>
        <translation>Fair Tournament je volně dostupná aplikace pro podporu organizace sportovních akcí.

</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="434"/>
        <source>Copyright (C) 2019  Aleš Suchomel
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="879"/>
        <source>Unidentified</source>
        <translation>Nepojmenováno</translation>
    </message>
    <message>
        <source>Request New Feature...</source>
        <translation type="vanished">Požádat o novou funkci...</translation>
    </message>
    <message>
        <source>Open online service desk in native browser.</source>
        <translation type="vanished">Otevři v prohlížeči centrum online podpory.</translation>
    </message>
    <message>
        <source>Report Bug...</source>
        <translation type="vanished">Nahlásit chybu...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="258"/>
        <source>Get Help...</source>
        <translation>Pomoc...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="261"/>
        <source>Open the interactive user guide in a new window.</source>
        <translation>Otevři interaktivní centrum nápovědy.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="324"/>
        <source>Open Tournament</source>
        <translation>Otevři turnaj</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="326"/>
        <location filename="mainwindow.cpp" line="358"/>
        <source>Tournaments (*.turn)</source>
        <translation>Turnaje (*.turn)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="356"/>
        <source>Save Tournament</source>
        <translation>Ulož turnaj</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="432"/>
        <source>About Fair Tournament</source>
        <translation>O Fair Tournament</translation>
    </message>
    <message>
        <source>Fair Tournament is an open source desctop application for scheduling and organising sport events.
</source>
        <translation type="vanished">Fair Tournament je svobodný software pro podporu organizování sportovních akcí.
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="443"/>
        <source>The used icons set &apos;Must Have&apos; has been designed by Visual Pharm &lt;http://www.visualpharm.com&gt;.

</source>
        <translation>Základní použitý set ikon je &apos;Must Have&apos; vytvořený společností Visual Pharm &lt;http://www.visualpharm.com&gt;.

</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="444"/>
        <source>The text editor icons and PDF icon have been made by Freepik, &lt;https://www.freepik.com/&gt;.

</source>
        <oldsource>The used icons set &apos;Must Have&apos; has been designed by Visual Pharm &lt;http://www.visualpharm.com&gt;.</oldsource>
        <translation>Ikonky textového editoru a použitá PDF ikonka byly vytvořeny společností Freepik, &lt;https://www.freepik.com/&gt;.

</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="855"/>
        <source>Clear the list of recent tournaments.</source>
        <translation>Vymaž list nedávných turnajů.</translation>
    </message>
    <message>
        <source>Do you wish to quit? 
 Copyvrite,
 license, 
 no garranty</source>
        <translation type="vanished">Chcete ukončit aplikaci?</translation>
    </message>
    <message>
        <source>Something went wrong</source>
        <translation type="vanished">Něco se pokazilo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="450"/>
        <location filename="mainwindow.cpp" line="472"/>
        <source>Our online service desk was unable to reach.</source>
        <translation>Nebylo možné dosáhnout na online centrum podpory.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="450"/>
        <location filename="mainwindow.cpp" line="472"/>
        <source>Something went wrong.</source>
        <translation>Něco se pokazilo.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="501"/>
        <source>Welcome!</source>
        <translation>Vítejte!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="501"/>
        <source>We would be glad if you let our program help you organize your event.</source>
        <translation>Budeme rádi, pokud Vám náš program pomůže s přípravou Vaší akce.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="502"/>
        <source>Tournament</source>
        <translation>turnaj</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="503"/>
        <source>It would be our pleasure to help you organize your first tournaments.</source>
        <translation>Bude nám potěšením Vám pomoci s organizováním Vašich prvních turnajů.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="509"/>
        <source>Welcome Page</source>
        <translation>Uvítací stránka</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="531"/>
        <source>Empty Page</source>
        <translation>Prázdná stránka</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="728"/>
        <source>Set the language of the application.</source>
        <translation>Nastav jazyk aplikace.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="804"/>
        <source>The tournament has been modified.
Do you want to save the changes?</source>
        <translation>Turnaj byl změněn. Přejete si uložit změny?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="835"/>
        <source>%1[*] | %2</source>
        <translation>%1[*] | %2</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="854"/>
        <source>Clear Recent</source>
        <translation>Smaž nedávné</translation>
    </message>
    <message>
        <source>Clear the list of recent tournament.</source>
        <translation type="vanished">Smaž seznam nedávných turnajů.</translation>
    </message>
</context>
</TS>
