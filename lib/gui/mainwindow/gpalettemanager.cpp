#include "gpalettemanager.h"
#include <QPalette>
#include <QApplication>
#include <QStyle>

QStringList GPaletteFactory::keys()
{
    return {"Standard", "Dark", "Green"};
}

QPalette GPaletteFactory::create(const QString& themeString)
{
    Theme theme;
    if("Green" == themeString){
        theme = Theme::Green;
    }
    else if("Dark" == themeString){
        theme = Theme::Dark;
    }
    else {
        theme = Theme::Standard;
    }
    return GPaletteFactory::create(theme);
}

QPalette GPaletteFactory::create(GPaletteFactory::Theme theme)
{
    switch (theme) {
        case Theme::Standard:
            return QApplication::style()->standardPalette();
        case Theme::Dark:
            return GPaletteFactory::getDarkPalette();
        case Theme::Green:
            return GPaletteFactory::getGreenPalette();
    };
    return QApplication::style()->standardPalette();
}

QPalette GPaletteFactory::getDarkPalette()
{
    /*
###############################################################################
#                                                                             #
# The MIT License                                                             #
#                                                                             #
# Copyright (C) 2017 by Juergen Skrotzky (JorgenVikingGod@gmail.com)          #
#               >> https://github.com/Jorgen-VikingGod                        #
#                                                                             #
# Sources: https://github.com/Jorgen-VikingGod/Qt-Frameless-Window-DarkStyle  #
#                                                                             #
###############################################################################
*/
//    Permission is hereby granted, free of charge, to any person
//    obtaining a copy of this software and associated documentation
//    files (the "Software"), to deal in the Software without
//    restriction, including without limitation the rights to use,
//    copy, modify, merge, publish, distribute, sublicense, and/or sell
//    copies of the Software, and to permit persons to whom the
//    Software is furnished to do so, subject to the following
//    conditions:

//    The above copyright notice and this permission notice shall be
//    included in all copies or substantial portions of the Software.

//    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//    OTHER DEALINGS IN THE SOFTWARE.

    QPalette palette;
    palette.setColor(QPalette::Window,QColor(53,53,53));
    palette.setColor(QPalette::WindowText,Qt::white);
    palette.setColor(QPalette::Disabled,QPalette::WindowText,QColor(127,127,127));
    palette.setColor(QPalette::Base,QColor(42,42,42));
    palette.setColor(QPalette::AlternateBase,QColor(66,66,66));
    palette.setColor(QPalette::ToolTipBase,Qt::white);
    palette.setColor(QPalette::ToolTipText,Qt::white);
    palette.setColor(QPalette::Text,Qt::white);
    palette.setColor(QPalette::Disabled,QPalette::Text,QColor(127,127,127));
    palette.setColor(QPalette::Dark,QColor(35,35,35));
    palette.setColor(QPalette::Shadow,QColor(20,20,20));
    palette.setColor(QPalette::Button,QColor(53,53,53));
    palette.setColor(QPalette::ButtonText,Qt::white);
    palette.setColor(QPalette::Disabled,QPalette::ButtonText,QColor(127,127,127));
    palette.setColor(QPalette::BrightText,Qt::red);
    palette.setColor(QPalette::Link,QColor(42,130,218));
    palette.setColor(QPalette::Highlight,QColor(42,130,218));
    palette.setColor(QPalette::Disabled,QPalette::Highlight,QColor(80,80,80));
    palette.setColor(QPalette::HighlightedText,Qt::white);
    palette.setColor(QPalette::Disabled,QPalette::HighlightedText,QColor(127,127,127));
    return palette;
}

QPalette GPaletteFactory::getGreenPalette()
{
    QPalette palette;
    palette.setColor(QPalette::Window, QColor(76,175,80));
    palette.setColor(QPalette::WindowText, QColor(60,60,60));
    palette.setColor(QPalette::Disabled, QPalette::WindowText,QColor(189,189,189));
    palette.setColor(QPalette::Base, QColor(189,189,189));
    palette.setColor(QPalette::AlternateBase,QColor(189,189,189));
    palette.setColor(QPalette::ToolTipBase,Qt::white);
    palette.setColor(QPalette::ToolTipText,Qt::black);
    palette.setColor(QPalette::Text,QColor(20,20,20));
    palette.setColor(QPalette::Disabled, QPalette::Text,QColor(127,127,127));
    palette.setColor(QPalette::Dark,QColor(60,60,60));
    palette.setColor(QPalette::Shadow,QColor(20,20,20));
    palette.setColor(QPalette::Button,QColor(76,155,80));
    palette.setColor(QPalette::ButtonText,Qt::white);
    palette.setColor(QPalette::Disabled,QPalette::ButtonText,QColor(189,189,189));
    palette.setColor(QPalette::BrightText, Qt::green);
    palette.setColor(QPalette::Link,QColor(50,130,218));
    palette.setColor(QPalette::Highlight,QColor(40,120,220));
    palette.setColor(QPalette::Disabled,QPalette::Highlight,QColor(80,80,80));
    palette.setColor(QPalette::HighlightedText, Qt::black);
    palette.setColor(QPalette::Disabled, QPalette::HighlightedText,QColor(127,127,127));
    return palette;
}
