#include "aplayoffdrawer.h"
#include <QtMath>

#include <qglobal.h>
#include <ctime>
#include <algorithm>
#include <random>


const int MAX_ITERATION = 50;

APlayOffDrawer::APlayOffDrawer()
{
    std::srand(unsigned(std::time(nullptr)));
    _root = nullptr;
    _numTeams = 0;
}

APlayOffDrawer::~APlayOffDrawer()
{
    if(_root){
        deleteTree(_root);
    }
}

QList<QList<QList<int> > > APlayOffDrawer::getActualPlayOff()
{
    if(_root){
        return treeToListMatrix(_root, _numTeams);
    }
    return QList<QList<QList<int> > >();
}

QList<QList<QList<Entry> > > APlayOffDrawer::getActualPlayOffWithSources()
{
    if(_root){
        return treeToEntryMatrix(_root,_numTeams);
    }
    return QList<QList<QList<Entry> > >();
}

QList<QList<int> > APlayOffDrawer::getMatchesInOrder()
{
    if(_root){
        return roundToListList(sortMatches(_root));
    }
    return QList<QList<int> >();
}

QList<QList<Entry > >  APlayOffDrawer::getMatchesInOrderWithSources()
{
    if(_root){
        return roundToEtryList(sortMatches(_root));
    }
    return QList<QList<Entry> >();
}

MatchNode *APlayOffDrawer::createTree(int numTeams, APlayOffDrawer::Principle principle)
{
    QQueue<MatchNode*> queue;
    if(_root){
        deleteTree(_root);
    }
    _root = new MatchNode(1);
    _root->_upEntry = new Entry(1,1,0);
    _numTeams = numTeams;

    queue.push_back(_root);
    while(!queue.isEmpty()){
        MatchNode * match = queue.takeFirst();
        int betterIndex = match->getBetterIndex();
        int worstIndex = getAdvantageousWorstIndex(betterIndex, match->_myRoundSize);
        if(principle == Principle::Equal && match->_myRoundSize > 2){
            worstIndex = getEqualWorstIndex(betterIndex, numTeams, match->_myRoundSize);
        }
        if(match->_upEntry){
            match->_downEntry = new Entry(worstIndex,worstIndex,0);
        }
        else {
            match->_upEntry = new Entry(worstIndex,worstIndex,0);
        }
        if(predecessorNeeded(match->_upEntry->_rankOverall, numTeams, match->_myRoundSize)){
            queue.push_back(match->createUpPredecessor());
        }
        if(predecessorNeeded(match->_downEntry->_rankOverall, numTeams, match->_myRoundSize)){
            queue.push_back(match->createDownPredecessor());
        }
    }
    return _root;
}

MatchNode *APlayOffDrawer::createTree(const QList<int> &numTeamsInSources, APlayOffDrawer::Principle principle)
{
    if(_root){
        deleteTree(_root);
    }
    _root = new MatchNode(1);

    int iteration = 1;

    QList<Entry*> startList = createEntryList(numTeamsInSources);
    QList<MatchNode*> possibilities;
    QQueue<MatchNode*> queue;
    _numTeams = startList.size();

    _root->_upEntry = startList.takeFirst();
    while(!startList.isEmpty()){
        Entry* actualEntry = startList.takeFirst();
        queue.push_back(_root);
        while(!queue.isEmpty()){
            MatchNode * match = queue.takeFirst();
            if(match->hasPlace()){
                possibilities.push_back(match);
            }
            else{
                int upSources = countSourcesInTree(match->_upPredecessor, actualEntry->_sourceIndex);
                int downSources = countSourcesInTree(match->_downPredecessor, actualEntry->_sourceIndex);
                if(upSources<=downSources && match->_upPredecessor){
                    queue.push_back(match->_upPredecessor);
                }
                if(downSources<=upSources && match->_downPredecessor){
                    queue.push_back(match->_downPredecessor);
                }
            }
        }
        possibilities = matchesOfMinimalRound(possibilities);
        MatchNode* option = findBestPossibility(possibilities, actualEntry, principle);

        possibilities.clear();
        queue.clear();

        if(canBePlaced(option,_numTeams) || iteration>=MAX_ITERATION){
            placeEntry(actualEntry, option);
        }
        else{
            qDeleteAll(startList);
            startList.clear();
            deleteTree(_root);
            iteration++;

            startList = createEntryListRandom(numTeamsInSources, iteration);
            _root = new MatchNode(1);
            _root->_upEntry = startList.takeFirst();
        }

    }
    removeNotFullMatches(_root);
    return _root;
}

QList<MatchNode *> APlayOffDrawer::sortMatches(MatchNode *root)
{
    QQueue<MatchNode*> queue;
    QList<MatchNode *> matchesInOrder;
    queue.push_back(root);
    while(!queue.isEmpty()){
        MatchNode * match = queue.takeFirst();
        matchesInOrder.push_front(match);
        if(match->_upPredecessor){
            queue.push_back(match->_upPredecessor);
        }
        if(match->_downPredecessor){
            if(match->_upEntry->_rankOverall < match->_downEntry->_rankOverall && match->_upPredecessor){
                queue.insert(queue.size()-1, match->_downPredecessor);
            }
            else{
                queue.push_back(match->_downPredecessor);
            }
        }
    }
    return matchesInOrder;
}

QList<QList<MatchNode *> > APlayOffDrawer::treeToMatrix(MatchNode *root, int numTeams)
{
    QList<QList<MatchNode *> > matrix;
    matrix.push_front(QList<MatchNode*>());

    QQueue<MatchNode*> queue;
    queue.push_back(root);
    int matchesPlaced = 0;
    while(!queue.isEmpty()){
        MatchNode * match = queue.takeFirst();
        matrix.first().push_back(match);

        if(match){
            queue.push_back(match->_upPredecessor);
            queue.push_back(match->_downPredecessor);
            matchesPlaced++;
        }

        if(matrix.first().size() == static_cast<int>(qPow(2, matrix.size()-1))){
            if(matchesPlaced >= numTeams - 1){
                return matrix;
            }
            matrix.push_front(QList<MatchNode*>());
        }
    }
    return matrix;
}

QList<QList<QList<int> > > APlayOffDrawer::treeToListMatrix(MatchNode *root, int numTeams)
{
    QList<QList<MatchNode *> > matchMatrix = treeToMatrix(root, numTeams);
    QList<QList<QList<int> > > listMatrix;
    for(const auto& round : matchMatrix){
        listMatrix.push_back(roundToListList(round));
    }
    return listMatrix;
}

QList<QList<QList<Entry> > > APlayOffDrawer::treeToEntryMatrix(MatchNode *root, int numTeams)
{
    QList<QList<MatchNode *> > matchMatrix = treeToMatrix(root, numTeams);
    QList<QList<QList<Entry> > > listMatrix;
    for(const auto& round : matchMatrix){
        listMatrix.push_back(roundToEtryList(round));
    }
    return listMatrix;
}

QList<QList<int> > APlayOffDrawer::roundToListList(const QList<MatchNode *> &round)
{
    QList<QList<int> > list;
    for(const auto match : round){
        if(match){
            list.push_back({match->_upEntry->_rankOverall-1, match->_downEntry->_rankOverall-1});
        }
        else{
            list.push_back({});
        }
    }
    return list;
}

QList<QList<Entry> > APlayOffDrawer::roundToEtryList(const QList<MatchNode *>& round)
{
    QList<QList<Entry> > list;
    for(const auto match : round){
        if(match){
            list.push_back({*match->_upEntry, *match->_downEntry});
        }
        else{
            list.push_back({});
        }
    }
    return list;
}

QList<Entry *> APlayOffDrawer::createEntryList(const QList<int> &numTeamsInSources)
{
    QList<Entry *> entryList;
    int iteration = 0;
    while(entryList.size() < sumList(numTeamsInSources)){
        for(int sourceIndex = 0; sourceIndex < numTeamsInSources.size(); sourceIndex++){
            if(numTeamsInSources[sourceIndex] > iteration){
                entryList.push_back(new Entry(entryList.size()+1, iteration, sourceIndex));
            }
        }
        iteration++;
    }
    return entryList;
}

QList<Entry *> APlayOffDrawer::createEntryListRandom(const QList<int> &numTeamsInSources, int iteration)
{
    QList<Entry *> entryList = createEntryList(numTeamsInSources);
    QList<Entry *> randomPart;
    int randomPartRatio = 1 + iteration/5;
    for (int i = 0;i<_numTeams*randomPartRatio/(1+randomPartRatio);i++) {
        randomPart.push_front(entryList.takeLast());
    }
    std::random_shuffle(randomPart.begin(), randomPart.end());
    entryList.append(randomPart);
    return entryList;
}

int APlayOffDrawer::getAdvantageousWorstIndex(int betterIndex, int roundSize)
{
    return 1 + 2*roundSize - betterIndex;
}

int APlayOffDrawer::getEqualWorstIndex(int betterIndex, int numTeams, int roundSize)
{
    int realRoundSize = roundSize - qMax(0, 2*roundSize - numTeams);
    int worstIndex = betterIndex + realRoundSize + 1;
    if(worstIndex > qMin(numTeams, 2*roundSize)){
        worstIndex = betterIndex + 1;
    }
    return worstIndex;
}

int APlayOffDrawer::countSourcesInTree(MatchNode *root, int sourceIndex)
{
    if(!root){
        return 0;
    }
    int sourceCount = 0;
    QQueue<MatchNode*> queue;
    queue.push_back(root);
    while(!queue.isEmpty()){
        MatchNode* match = queue.takeFirst();
        if(match->_upEntry && !match->_upPredecessor){
            sourceCount += match->_upEntry->_sourceIndex == sourceIndex;
        }
        if(match->_downEntry && !match->_downPredecessor){
            sourceCount += match->_downEntry->_sourceIndex == sourceIndex;
        }
        if(match->_upPredecessor){
            queue.push_back(match->_upPredecessor);
        }
        if(match->_downPredecessor){
            queue.push_back(match->_downPredecessor);
        }
    }
    return sourceCount;
}

QList<MatchNode *> APlayOffDrawer::matchesOfMinimalRound(const QList<MatchNode *> &matches)
{
    if(matches.size()<=1){
        return matches;
    }
    int minRound = matches.first()->_myRoundSize;
    for(const auto match: matches){
        if(match->_myRoundSize < minRound){
            minRound = match->_myRoundSize;
        }
    }
    QList<MatchNode*> minMatches;
    for(const auto match: matches){
        if(match->_myRoundSize == minRound){
            minMatches.push_back(match);
        }
    }
    return minMatches;
}

MatchNode *APlayOffDrawer::findBestPossibility(const QList<MatchNode *> &posibilities, Entry *entry, APlayOffDrawer::Principle principle)
{
    MatchNode* bestChose = nullptr;
    int minError = INT_MAX;
    int rankOponent = -1;
    for(const auto match : posibilities){
        int betterIndex = match->getBetterIndex();
        int optimumWorstIndex = getAdvantageousWorstIndex(betterIndex, match->_myRoundSize);
        if(principle==Principle::Equal){
            optimumWorstIndex = getEqualWorstIndex(betterIndex, _numTeams, match->_myRoundSize);
        }
        int actualError = abs(optimumWorstIndex - entry->_rankOverall);
        if(minError > actualError || (minError == actualError && rankOponent < betterIndex)){
            minError = abs(optimumWorstIndex - entry->_rankOverall);
            rankOponent = betterIndex;
            bestChose = match;
        }
    }
    return bestChose;
}

bool APlayOffDrawer::canBePlaced(MatchNode *match, int numTeams)
{
    return match->_myRoundSize < numTeams;
}

void APlayOffDrawer::placeEntry(Entry *entry, MatchNode *match)
{
    if(match->_upEntry){
        match->_downEntry = entry;
    }
    else{
        match->_upEntry = entry;
    }
    if(!match->_upPredecessor && match->_upEntry){
        match->createUpPredecessor();
    }
    if(!match->_downPredecessor && match->_downEntry){
        match->createDownPredecessor();
    }
}

bool APlayOffDrawer::predecessorNeeded(int betterIndex, int numTeams, int roundSize)
{
    return (4 * roundSize - betterIndex < numTeams);
}

int APlayOffDrawer::sumList(const QList<int> &list)
{
    int sum = 0;
    for(int i : list){
        sum += i;
    }
    return sum;
}

void APlayOffDrawer::removeNotFullMatches(MatchNode *root)
{
    if(root->_upPredecessor){
        if(root->_upPredecessor->hasPlace()){
            root->_upPredecessor->nullSelf();
            delete root->_upPredecessor;
            root->_upPredecessor = nullptr;
        }
        else{
            removeNotFullMatches(root->_upPredecessor);
        }
    }
    if(root->_downPredecessor){
        if(root->_downPredecessor->hasPlace()){
            root->_downPredecessor->nullSelf();
            delete root->_downPredecessor;
            root->_downPredecessor = nullptr;
        }
        else{
            removeNotFullMatches(root->_downPredecessor);
        }
    }
}

void APlayOffDrawer::deleteTree(MatchNode *&root)
{
    if(root){
        if(root->_upPredecessor){
            deleteTree(root->_upPredecessor);
            root->_upEntry = nullptr;
            root->_upPredecessor = nullptr;
        }
        if(root->_downPredecessor){
            deleteTree(root->_downPredecessor);
            root->_downEntry = nullptr;
            root->_downPredecessor = nullptr;
        }        
        delete root;
        root = nullptr;
    }
}

LPlayOffDrawerAdvantageous::LPlayOffDrawerAdvantageous()
{

}

LPlayOffDrawerAdvantageous::~LPlayOffDrawerAdvantageous()
{

}

QList<QList<QList<int> > > LPlayOffDrawerAdvantageous::drawPlayOff(int numTeams)
{
    MatchNode* finale = createTree(numTeams, Principle::Advantageous);
    return treeToListMatrix(finale, numTeams);
}

QList<QList<QList<Entry> > > LPlayOffDrawerAdvantageous::drawPlayOffWithDifferentSources(const QList<int> &numTeamsInSources)
{
    MatchNode* finale = createTree(numTeamsInSources, Principle::Advantageous);
    return treeToEntryMatrix(finale, sumList(numTeamsInSources));
}

APlayOffDrawer *LPlayOffDrawerAdvantageous::create()
{
    return new LPlayOffDrawerAdvantageous();
}

LPlayOffDrawerEqual::LPlayOffDrawerEqual()
{

}

LPlayOffDrawerEqual::~LPlayOffDrawerEqual()
{

}

QList<QList<QList<int> > > LPlayOffDrawerEqual::drawPlayOff(int numTeams)
{
    MatchNode* finale = createTree(numTeams,Principle::Equal);
    return treeToListMatrix(finale, numTeams);
}

QList<QList<QList<Entry> > > LPlayOffDrawerEqual::drawPlayOffWithDifferentSources(const QList<int> &numTeamsInSources)
{
    MatchNode* finale = createTree(numTeamsInSources, Principle::Equal);
    return treeToEntryMatrix(finale, sumList(numTeamsInSources));
}

APlayOffDrawer *LPlayOffDrawerEqual::create()
{
    return new LPlayOffDrawerEqual();
}

MatchNode::MatchNode()
{
    _myRoundSize = -1;
    _upEntry = nullptr;
    _downEntry = nullptr;
    _upPredecessor = nullptr;
    _downPredecessor = nullptr;
}

MatchNode::~MatchNode()
{
    if(_upEntry){
        delete _upEntry;
    }
    if(_downEntry){
        delete _downEntry;
    }
}

MatchNode::MatchNode(int roundSize)
    :MatchNode()
{
    _myRoundSize = roundSize;
}

MatchNode *MatchNode::createUpPredecessor()
{
    _upPredecessor = new MatchNode(_myRoundSize*2);
    _upPredecessor->_upEntry = _upEntry;
    return _upPredecessor;
}

MatchNode *MatchNode::createDownPredecessor()
{
    _downPredecessor = new MatchNode(_myRoundSize*2);
    _downPredecessor->_downEntry = _downEntry;
    return _downPredecessor;
}

void MatchNode::nullSelf()
{
    _downPredecessor = nullptr;
    _upPredecessor = nullptr;
    _upEntry = nullptr;
    _downEntry = nullptr;
}

int MatchNode::getBetterIndex()
{
    if(_downEntry && _upEntry){
        return qMin(_downEntry->_rankOverall,_upEntry->_rankOverall);
    }
    if(_upEntry){
        return _upEntry->_rankOverall;
    }
    if(_downEntry){
        return _downEntry->_rankOverall;
    }
    return -1;
}

bool MatchNode::hasPlace()
{
    return !(_upEntry && _downEntry);
}

Entry::Entry()
{
    _sourceIndex = -1;
    _rankInSource = -1;
    _rankOverall = -1;
}

Entry::Entry(int rankOverall)
    :Entry()
{
    _rankOverall = rankOverall;
}

Entry::Entry(int rankOverall, int rankInSource, int sourceIndex)
    :Entry()
{
    _rankOverall = rankOverall;
    _rankInSource = rankInSource;
    _sourceIndex = sourceIndex;
}

bool Entry::isEmpty()
{
    return _rankOverall == -1;
}
