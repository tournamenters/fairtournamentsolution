#ifndef GGROUPSTAGEVIEW_H
#define GGROUPSTAGEVIEW_H

#include "aview.h"
#include <QPointer>

class GRobinRoundView;


class GGroupStageView: public AStageView
{
    Q_OBJECT

public:
    GGroupStageView(AStageModel* model, QWidget *parent = nullptr);
    ~GGroupStageView() override;

    static AStageView * __stdcall create(AStageModel* model);

    QWidget * getViewWidget() override;
    AExportable * getExportableWidget() override;   

public slots:
    void findText(const QString& text) override;
    void update() override;

private:
    QList<QColor> generateColorsList(int number);
    void createGroupsView();
    QWidget * createViewWidget();

    QList<GRobinRoundView*> _groupViews;
    QPointer<QSplitter> splGroups;
    AExportable* _exportable;
};

#endif // GGROUPSTAGEVIEW_H
