#include "agroupsdrawer.h"
#include <QList>
#include <QDebug>

#include <qglobal.h>
#include <ctime>
#include <algorithm>
#include <random>

AGroupsDrawer::AGroupsDrawer()
{

}

AGroupsDrawer::~AGroupsDrawer()
{

}

QList<QList<int> > AGroupsDrawer::drawFirstRound(int numGroups)
{
    QList<QList<int> > groups;
    for(int i = 0; i < numGroups; i++){
        groups.push_back({i});
    }
    return groups;
}

QList<QList<int> > AGroupsDrawer::drawNormalRound(const QList<QList<int>> &groups, int& nextToBePlaced, const int numTeams)
{
    QList<QList<int> > newGroups = groups;
    for (int j = 0; j<groups.size(); j++) {
        if(nextToBePlaced>=numTeams){
            return newGroups;
        }
        newGroups[j].push_back(nextToBePlaced);
        nextToBePlaced++;
    }
    return newGroups;
}

QList<QList<int> > AGroupsDrawer::drawReverseRound(const QList<QList<int> > &groups, int &nextToBePlaced, const int numTeams)
{
    QList<QList<int> > newGroups = groups;
    for (int j = groups.size()-1; j>=0; j--) {
        if(nextToBePlaced>=numTeams){
            return newGroups;
        }
        newGroups[j].push_back(nextToBePlaced);
        nextToBePlaced++;
    }
    return newGroups;
}

QList<QList<int> > AGroupsDrawer::drawRandomRound(const QList<QList<int> > &groups, int &nextToBePlaced, const int numTeams)
{
    QList<QList<int> > newGroups = groups;
    std::srand(unsigned(std::time(nullptr)));
    QList<int> randomList;
    for(int i = 0; i< groups.size(); i++){
        if(nextToBePlaced<numTeams){
            randomList.push_back(nextToBePlaced);
            nextToBePlaced++;
        }
    }
    std::random_shuffle(randomList.begin(), randomList.end());
    for(int i = 0; i< groups.size() && i<randomList.size(); i++){
        newGroups[i].push_back(randomList[i]);
    }
    return newGroups;
}

LGroupsDrawerBaskets::LGroupsDrawerBaskets()
{

}

LGroupsDrawerBaskets::~LGroupsDrawerBaskets()
{

}

QList<QList<int>> LGroupsDrawerBaskets::createGroups(int numGroups, int numTeams, int proceedPerGroup)
{
    QList<QList<int> > groups = drawFirstRound(numGroups);
    int nextToBePlaced = numGroups;
    while(nextToBePlaced < numTeams){
        groups = drawRandomRound(groups,nextToBePlaced,numTeams);
    }
    if(proceedPerGroup>groups.first().size()){
        qDebug() << "Problem with groups, too mutch proceedings";
    }
    return groups;
}

AGroupsDrawer *LGroupsDrawerBaskets::create()
{
    return new LGroupsDrawerBaskets();
}

LGroupsDrawerRandom::LGroupsDrawerRandom()
{
    std::srand(unsigned(std::time(nullptr)));
}

LGroupsDrawerRandom::~LGroupsDrawerRandom()
{

}

QList<QList<int> > LGroupsDrawerRandom::createGroups(int numGroups, int numTeams, int proceedPerGroup)
{
    QList<int> randomList;
    for(int i = 0; i < numTeams; i++){
        randomList.push_back(i);
    }
    std::random_shuffle(randomList.begin(), randomList.end());

    QList<QList<int> > groups;
    for(int i = 0; i < numGroups && i < randomList.size(); i++){
        groups.push_back({randomList[i]});
    }

    int nextToBePlaced = numGroups;
    while(nextToBePlaced<randomList.size()){
        for(int i = 0; i<groups.size(); i++){
            if(nextToBePlaced>=randomList.size()){
                return groups;
            }
            groups[i].push_back(randomList[nextToBePlaced]);
            nextToBePlaced++;
        }
    }
    if(proceedPerGroup>groups.first().size()){
        qDebug() << "Problem with groups, too mutch proceedings";
    }
    return groups;
}

AGroupsDrawer *LGroupsDrawerRandom::create()
{
    return new LGroupsDrawerRandom();
}

LGroupsDrawerAdvantageous::LGroupsDrawerAdvantageous()
{

}

LGroupsDrawerAdvantageous::~LGroupsDrawerAdvantageous()
{

}

QList<QList<int> > LGroupsDrawerAdvantageous::createGroups(int numGroups, int numTeams, int proceedPerGroup)
{
    QList<QList<int> > groups = drawFirstRound(numGroups);
    int nextToBePlaced = numGroups;
    int iterationCount = 1;
    while(nextToBePlaced<numTeams){
        if(iterationCount % 2){
            groups = drawReverseRound(groups,nextToBePlaced,numTeams);
        }
        else{
            groups = drawNormalRound(groups,nextToBePlaced,numTeams);
        }
        iterationCount++;
    }
    if(proceedPerGroup>iterationCount){
        qDebug() << "Problem with groups, too mutch proceedings";
    }
    return groups;
}

AGroupsDrawer *LGroupsDrawerAdvantageous::create()
{
    return new LGroupsDrawerAdvantageous();
}

LGroupsDrawerEqual::LGroupsDrawerEqual()
{

}

LGroupsDrawerEqual::~LGroupsDrawerEqual()
{

}

QList<QList<int> > LGroupsDrawerEqual::createGroups(int numGroups, int numTeams, int proceedPerGroup)
{
    QList<QList<int> > groups = drawFirstRound(numGroups);
    int nextToBePlaced = numGroups;
    while(nextToBePlaced<numTeams){
        groups = drawNormalRound(groups,nextToBePlaced,numTeams);
    }
    if(proceedPerGroup>groups.first().size()){
        qDebug() << "Problem with groups, too mutch proceedings";
    }
    return groups;
}

AGroupsDrawer *LGroupsDrawerEqual::create()
{
    return new LGroupsDrawerEqual();
}
LGroupsDrawerModified::~LGroupsDrawerModified()
{

}

QList<QList<int> > LGroupsDrawerModified::createGroups(int numGroups, int numTeams, int proceedPerGroup)
{
    QList<QList<int> > groups = drawFirstRound(numGroups);
    int nextToBePlaced = numGroups;
    int iterationCount = 1;
    while(nextToBePlaced<numTeams){
        if(iterationCount != proceedPerGroup){
            groups = drawNormalRound(groups,nextToBePlaced,numTeams);
        }
        else{
            groups.last().push_back(nextToBePlaced);
            nextToBePlaced++;
            groups = drawNormalRound(groups,nextToBePlaced,numTeams);
            if(nextToBePlaced<numTeams){
                groups.last().removeLast();
                nextToBePlaced--;
            }
        }
        iterationCount++;
    }
    if(proceedPerGroup>iterationCount){
        qDebug() << "Problem with groups, too mutch proceedings";
    }
    return groups;
}

AGroupsDrawer *LGroupsDrawerModified::create()
{
    return new LGroupsDrawerModified();
}

LGroupsDrawerModified::LGroupsDrawerModified()
{

}
