#ifndef GGROUPTABLE_H
#define GGROUPTABLE_H

#include <QDialog>
#include <QColor>

#include "gmatchestable.h"

class QVariant;
class GTeamEdit;
class QToolButton;

class GGroupTable : public GMatchesTable
{
    Q_OBJECT

public:
    explicit GGroupTable(QWidget *parent = nullptr);
    GGroupTable(int rows, QWidget *parent = nullptr);
    GGroupTable(int rows, const QVariant& identificator, QWidget *parent = nullptr);
    GGroupTable(const QVariant &identificator, QWidget *parent = nullptr);
    ~GGroupTable() override;

    void setData(const QVariant& data) override;
    void setHighlightColor(const QColor &color);
    void setResultsEditable(bool editable);

    void highliteNextMatch(const QVariant& data);

    void resetTable();

signals:
    void uniqueEditationRequested(const QString& header, const QString& value);

private slots:
    void uniqueEditationRequest(QAction *action);

private:
    void setDefaultSize();
    void setMatchColor(int row, int col, const QColor& color);
    void cleanEmptyRows();
    bool isRowEmpty(int row);

    void setupUniqueButtons(const QVariant& data);
    QToolButton* createUniqueButton(const QString& value);
    void removeDiagonaleResults();
    QTableWidgetItem* createDiagonalItem();

    QColor _highlightCellColor;
    int _highlightCellRow;
    int _highlightCellCol;
    int _editatedCol;
};

class GGroupEditationDialog: public QDialog
{
    Q_OBJECT

public:
    GGroupEditationDialog(const QVariant &data, const QString &header, QWidget *parent = nullptr);
    QStringList getEnteredValues();

    int getEditatedColumnIndex() const;
    void setEditatedColumnIndex(int value);

private slots:
    void tableCellChange();
    void drawValues();

private:
    QTableWidget* createTable(const QVariant &data);
    QLayout* createButtonsLayout();
    bool isEditationCorrect();
    void setTableValues(QList<int> values);

    GGroupTable* _table;
    QPushButton* saveChanges;
    int _editatedCol;

};


#endif // GGROUPTABLE_H
