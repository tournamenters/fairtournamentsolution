#ifndef GNEWDIALOG_H
#define GNEWDIALOG_H
#include <QWizard>
#include <QVariant>

class NewTournament: public QWizard
{
    Q_OBJECT

public:
    NewTournament(QWidget *parent = nullptr);
    ~NewTournament() override;

    QString getTournamentName();
    QVariant getTournamentSetting();
    QVariant getStartListSetting();
    QVariant getTeamsData();

    QVariant getFieldsSetting();

    QList<QString> getStagesTypes();
    QList<QVariant> getStagesSetting();
    QList<QVariant> getStagesParams();

    void accept() override;
    void closeEvent(QCloseEvent *event) override;

signals:
    void documentationNeeded(const QString& page);

private slots:
    void showHelp();

private:
    void writeSettings();
    void readSettings();

    void storeTournamentData();
    void storeFieldsData();
    void storeStagesData();
    void storeStartListData();

    QString _tournamentName;
    QVariant _tournamentSetting;
    QVariant _startListSetting;
    QVariant _teamsData;
    QVariantMap _fieldsSetting;
    QList<QString> _stagesTypes;
    QList<QVariant> _stagesSetting;
    QList<QVariant> _stagesParams;

};


#endif // GNEWDIALOG_H
