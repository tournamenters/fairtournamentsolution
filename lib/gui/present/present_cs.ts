<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ">
<context>
    <name>GExportDialog</name>
    <message>
        <location filename="gexportdialog.cpp" line="14"/>
        <source>| Export Dialog</source>
        <translation>| Export Dialog</translation>
    </message>
    <message>
        <location filename="gexportdialog.cpp" line="45"/>
        <source>Please, select elements that you wish to export.        </source>
        <translation>Prosím, vyberte data, která chcete exportovat.        </translation>
    </message>
    <message>
        <location filename="gexportdialog.cpp" line="51"/>
        <source>Objects to export</source>
        <translation>Objekty k exportování</translation>
    </message>
</context>
<context>
    <name>GExportHelperWidget</name>
    <message>
        <location filename="aexportable.cpp" line="141"/>
        <location filename="aexportable.cpp" line="161"/>
        <source>Save as</source>
        <translation>Uložit jako</translation>
    </message>
    <message>
        <location filename="aexportable.cpp" line="141"/>
        <location filename="aexportable.cpp" line="161"/>
        <source>Choose a filename</source>
        <translation>Vyber jméno souboru</translation>
    </message>
    <message>
        <location filename="aexportable.cpp" line="150"/>
        <location filename="aexportable.cpp" line="171"/>
        <source>File could not be saved</source>
        <translation>Soubor nemohl být uložen</translation>
    </message>
    <message>
        <location filename="aexportable.cpp" line="151"/>
        <location filename="aexportable.cpp" line="172"/>
        <source>Sorry, something went wrong.</source>
        <translation>Omlouváme se, něco se pokazilo.</translation>
    </message>
</context>
<context>
    <name>GImagesExporter</name>
    <message>
        <location filename="gimagesexporter.cpp" line="35"/>
        <source>Select Folder</source>
        <translation>Vyber složku</translation>
    </message>
    <message>
        <location filename="gimagesexporter.cpp" line="39"/>
        <source>Path of the directory where you wish to export the data.</source>
        <oldsource>Path of directory where you wish to export the files.</oldsource>
        <translation>Cesta složky, do které budou data exportovány.</translation>
    </message>
    <message>
        <location filename="gimagesexporter.cpp" line="43"/>
        <source>Browse</source>
        <translation>Hledej</translation>
    </message>
    <message>
        <location filename="gimagesexporter.cpp" line="47"/>
        <source>Path: </source>
        <translation>Cesta: </translation>
    </message>
    <message>
        <location filename="gimagesexporter.cpp" line="55"/>
        <source>Nothing to export</source>
        <translation>Nejsou žádná data k exportování.</translation>
    </message>
    <message>
        <location filename="gimagesexporter.cpp" line="56"/>
        <source>Nothing was found that could be exported.</source>
        <translation>Nebylo nalezeno nic, co by mohlo být exportováno.</translation>
    </message>
    <message>
        <location filename="gimagesexporter.cpp" line="66"/>
        <source>Open Directory</source>
        <translation>Otevřít složku</translation>
    </message>
    <message>
        <location filename="gimagesexporter.cpp" line="77"/>
        <source>/exported_data</source>
        <translation>/exportovaná-data</translation>
    </message>
    <message>
        <location filename="gimagesexporter.cpp" line="125"/>
        <source>Something went wrong</source>
        <translation>Něco se pokazilo.</translation>
    </message>
    <message>
        <location filename="gimagesexporter.cpp" line="125"/>
        <source>File: </source>
        <translation>Soubor: </translation>
    </message>
    <message>
        <location filename="gimagesexporter.cpp" line="126"/>
        <source> could not be saved.</source>
        <translation>nemohl být uložen.</translation>
    </message>
    <message>
        <location filename="gimagesexporter.cpp" line="145"/>
        <source>unidentified</source>
        <translation>unidentified</translation>
    </message>
</context>
<context>
    <name>ReportEditor</name>
    <message>
        <location filename="reporteditor.cpp" line="79"/>
        <source>&amp;File</source>
        <translation>&amp;Soubor</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="80"/>
        <source>&amp;New</source>
        <translation>&amp;Nový</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="82"/>
        <source>&amp;Export</source>
        <translation>&amp;Export</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="87"/>
        <source>Export &amp;As...</source>
        <translation>Export &amp;jako...</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="91"/>
        <source>&amp;Print...</source>
        <translation>&amp;Tisk...</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="94"/>
        <source>&amp;Exit</source>
        <translation>&amp;Exit</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="100"/>
        <source>&amp;Edit</source>
        <translation>&amp;Úpravy</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="110"/>
        <source>&amp;Format</source>
        <translation>&amp;Formát</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="111"/>
        <source>Font</source>
        <translation>Písmo</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="121"/>
        <source>&amp;Preview</source>
        <translation>Ná&amp;hled</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="124"/>
        <source>Refresh Preview</source>
        <translation>Obnov náhled</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="135"/>
        <source>&amp;Left</source>
        <translation>&amp;Levý okraj</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="138"/>
        <source>C&amp;enter</source>
        <translation>&amp;Střed</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="141"/>
        <source>&amp;Right</source>
        <translation>&amp;Pravý okraj</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="144"/>
        <source>&amp;Justify</source>
        <translation>&amp;Zarovnat</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="151"/>
        <source>Cu&amp;t</source>
        <translation>Vyjmi</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="154"/>
        <source>&amp;Copy</source>
        <translation>Vlož</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="157"/>
        <source>&amp;Paste</source>
        <translation>Kopíruj</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="169"/>
        <source>&amp;Undo</source>
        <translation>Znovu</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="173"/>
        <source>&amp;Redo</source>
        <translation>Zpět</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="181"/>
        <source>Table...</source>
        <translation>Tabulka...</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="184"/>
        <source>Insert</source>
        <translation>Vložit...</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="194"/>
        <source>&amp;Portrait</source>
        <translation>Na výšku</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="201"/>
        <source>&amp;Landscape</source>
        <translation>Na šířku</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="212"/>
        <source>Main Toolbar</source>
        <translation>Hlavní toolbar</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="289"/>
        <source>The report has been modified.
Do you want to export your changes?</source>
        <translation>Report byl modifikován.
Chcete exportovat provedené změny?</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="340"/>
        <source>Preview</source>
        <translation>Náhled</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="359"/>
        <source>Exported to </source>
        <translation>Exportováno do </translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="366"/>
        <source>Export PDF</source>
        <translation>Vytvořit PDF</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="384"/>
        <source>Print request was send.</source>
        <translation>Žádost o tisk byla odeslána.</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="412"/>
        <source>Insert Table Dialog</source>
        <translation>Dialog pro vložení tabulky</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="414"/>
        <source>Please select tournaments elements that you wish to export.</source>
        <oldsource>Pleace select what tournaments elements you wish to export.</oldsource>
        <translation>Prosím vyberte objekty, které chcete exportovat.</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="417"/>
        <source>Table Parameters</source>
        <translation>Tabulka parametrů</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="427"/>
        <source>Rows:</source>
        <translation>Řádky:</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="428"/>
        <source>Columns:</source>
        <translation>Sloupce:</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="452"/>
        <source>Nothing to insert</source>
        <translation>Není co importovat</translation>
    </message>
    <message>
        <location filename="reporteditor.cpp" line="453"/>
        <source>Nothing was found that could be inserted.</source>
        <translation>Nebylo nalezeno nic, co by mohlo být inportováno.</translation>
    </message>
</context>
</TS>
