#include "cmatch.h"
#include "cteam.h"
#include "cresult.h"
#include "ateamssorter.h"
#include <QString>
#include <QList>


CMatch::CMatch(int sourceId, QObject *parent)
    : QObject (parent)
{
    _hostIndex = -1;
    _guestIndex = -1;
    _sourceId = sourceId;
    _host = nullptr;
    _guest = nullptr;
}

CMatch::CMatch(int sourceId, CTeam *host, CTeam *guest, QObject* parent)
    : CMatch(sourceId, parent)
{
    change(_host, host);
    change(_guest, guest);
}

CMatch::CMatch(int sourceId, CTeam *host, CTeam *guest, const QString& result, QObject* parent)
    : CMatch(sourceId, host, guest, parent)
{
    _result.update(result);
}

CMatch::~CMatch()
{

}

void CMatch::setHost(CTeam *team)
{
    change(_host, team);
}

void CMatch::setGuest(CTeam *team)
{
    change(_guest, team);
}

void CMatch::setTeam(CTeam *team, bool host)
{
    if(host){
        setHost(team);
        return;
    }
    setGuest(team);
}

void CMatch::setSourceName(const QString &sourceName)
{
    _sourceName = sourceName;
}

void CMatch::setIndexes(int hostIndex, int guestIndex)
{
    _hostIndex = hostIndex;
    _guestIndex = guestIndex;
}

void CMatch::setPlaceHolders(const QString &placeHolderHost, const QString &placeHolderGuest)
{
    setPlaceHolderHost(placeHolderHost);
    setPlaceHolderGuest(placeHolderGuest);
}

void CMatch::setPlaceHolderHost(const QString &placeHolderHost)
{
    _hostPlaceholder = placeHolderHost;
}

void CMatch::setPlaceHolderGuest(const QString &placeHolderGuest)
{
    _guestPlaceholder = placeHolderGuest;
}

CTeam *CMatch::getHost() const
{
    return _host;
}

CTeam *CMatch::getGuest() const
{
    return _guest;
}

CTeam *CMatch::getTeam(bool host) const
{
    if(host){
        return  getHost();
    }
    return getGuest();
}

QList<int> CMatch::getTeamsIndexes() const
{
    QList<int> list;
    list.push_back(getHostIndex());
    list.push_back(getGuestIndex());
    return list;
}

QString CMatch::getResult() const
{
    return _result.toString();
}

QString CMatch::getResultReversed() const
{
    return _result.toStringReversed();
}

int CMatch::getHostIndex() const
{
    if(_host){
        return _host->getID();
    }
    return _hostIndex;
}

int CMatch::getFalseHostIndex() const
{
    return _hostIndex;
}

int CMatch::getGuestIndex() const
{
    if(_guest){
        return _guest->getID();
    }
    return _guestIndex;
}

int CMatch::getFalseGuestIndex() const
{
    return _guestIndex;
}

QString CMatch::getHostString() const
{
    if(_host){
        return _host->toString();
    }
    return _hostPlaceholder;
}

QString CMatch::getGuestString() const
{
    if(_guest){
        return _guest->toString();
    }
    return _guestPlaceholder;
}

QString CMatch::getSourceString() const
{
    return _sourceName;
}

QVariant CMatch::toVariant() const
{
    QVariantMap map;
    map["guest"] = teamToVariant(_guest, _guestPlaceholder);
    map["host"] = teamToVariant(_host, _hostPlaceholder);
    if(hasResult() || (map["host"].toMap()["active"].toBool() && map["guest"].toMap()["active"].toBool())){
        map["result"] = _result.toString();
    }
    map["source"] = _sourceId;
    map["source_string"] = _sourceName;
    return map;
}

Match CMatch::toMatchPackage() const
{
    Match match(getHostIndex(), getGuestIndex());
    if(_result.wasPlayed()){
        if(!_result.toIntList().isEmpty()){
            match._hostScore = _result.toIntList()[0];
            match._guestScore = _result.toIntList()[1];
        }
        match._overTime = _result.isOverTime();
    }
    return match;
}

void CMatch::setResult(const QString& result)
{
    if(_host && _guest){
        if(_host && _guest){
            if(_result.update(result)){
                emit resultUpdated();
            }
            return;
        }
    }
    _result.update("");
    emit resultUpdated();
}

void CMatch::setResultReversed(const QString& result)
{
    if(_host && _guest){
        _result.updateReversed(result);
        emit resultUpdated();
    }
}

bool CMatch::isIn(CTeam*team) const
{
    if(!team){
        return false;
    }
    return _host == team || _guest == team;
}

bool CMatch::isHost(CTeam *team) const
{
    if(!team){
        return false;
    }
    return _host == team;
}

bool CMatch::isGuest(CTeam *team) const
{
    if(!team){
        return false;
    }
    return _guest == team;
}

bool CMatch::isThisMatch(int hostIndex, int guestIndex)
{
    return hostIndex == getHostIndex() && guestIndex == getGuestIndex();
}

bool CMatch::equals(CMatch* other) const
{
    bool teamsOK = other->_guest == _guest && other->_host == _host;
    bool teamsRev = other->_guest == _host && other->_host == _guest;
    if(_result.equals(other->_result) && teamsOK){
        return true;
    }
    if(_result.equalsReverse(other->_result) && teamsRev){
        return true;
    }
    return false;
}

bool CMatch::hasResult() const
{
    return _result.wasPlayed() && _host && _guest;
}

bool CMatch::needsResult() const
{
    return !_result.wasPlayed() && _host && _guest;
}

CTeam* CMatch::getWinner() const
{
    if(_result.wonHost() && _host && _guest){
        return _host;
    }
    if(_result.wonGuest() && _guest && _host){
        return _guest;
    }
    return nullptr;
}

CTeam *CMatch::getLoser() const
{
    CTeam* team = getWinner();
    if(!team){
        return nullptr;
    }
    if(team == _host){
        return _guest;
    }
    if(team == _guest){
        return _host;
    }
    return nullptr;
}

void CMatch::change(CTeam *&originTeam, CTeam *newTeam)
{
    if(newTeam != originTeam){
        if(originTeam){
            disconnect(originTeam, SIGNAL(updated()),
                       this, SIGNAL(teamsUpdated()));
        }
        if(newTeam){
            connect(newTeam, SIGNAL(updated()),
                    this, SIGNAL(teamsUpdated()));
        }
        originTeam = newTeam;
        setResult("");
        emit teamsUpdated();
    }
}

QVariant CMatch::teamToVariant(CTeam *team, const QString &placeholder) const
{
    if(team){
        return team->toVariant();
    }
    QVariantMap host;
    host["name"] = placeholder;
    host["active"] = false;
    return host;
}


