#include "gnewdialog.h"
#include "global_creation.h"
#include "global_model.h"

#include "gintropage.h"
#include "gconclutionpage.h"
#include "gstagespage.h"
#include "gfieldspage.h"
#include "gformatpage.h"
#include "gstartlistpage.h"

#include <QMessageBox>
#include <QApplication>
#include <QVariant>
#include <QMap>
#include <QList>
#include <QIcon>
#include <QSettings>
#include <QTranslator>

NewTournament::NewTournament(QWidget *parent)
    :QWizard (parent)
{

    setWizardStyle(QWizard::ModernStyle);
    setWindowTitle(QApplication::applicationName() + tr(" | New Tournament Dialog"));
    setWindowIcon(QIcon(":/icons/icon.ico"));

    setPage(Page_Intro, new GIntroPage(this));
    setPage(Page_Teams, new GStartListInsertingPage(this));
    setPage(Page_Format, new GFormatPage(this));
    setPage(Page_Stages, new GStagesCreationPage(this));
    setPage(Page_Fields, new GFieldsSettingPage(this));
    setPage(Page_Conclusion, new GConclusionPage(this));

    setStartId(Page_Intro);

    setOption(HaveHelpButton, true);
    setOption(NoBackButtonOnStartPage, true);
    setOption(HaveNextButtonOnLastPage, false);
    setOption(HelpButtonOnRight, false);
    setOption(IndependentPages, false);
    connect(this, SIGNAL(helpRequested()),
            this, SLOT(showHelp()));
    readSettings();
}

NewTournament::~NewTournament()
{
    _tournamentSetting.clear();
    _startListSetting.clear();
    _teamsData.clear();
    _fieldsSetting.clear();
    _stagesTypes.clear();
    _stagesSetting.clear();
    _stagesParams.clear();
}

void NewTournament::accept()
{
    storeTournamentData();
    storeStartListData();
    storeStagesData();
    storeFieldsData();
    QDialog::accept();
}

void NewTournament::closeEvent(QCloseEvent *event)
{
    writeSettings();
    if(QMessageBox::question(this, QApplication::applicationName(),tr("Do you really want to quit?\n All entered data will be lost."))==QMessageBox::Yes){
        event->accept();
        return;
    }
    event->ignore();
}

void NewTournament::showHelp()
{
    switch(currentId()){
    case PageIndex::Page_Intro:
        emit documentationNeeded("uvod.html");
        return;
    case PageIndex::Page_Teams:
        emit documentationNeeded("startovni-listina.html");
        return;
    case PageIndex::Page_Format:
        emit documentationNeeded("format.html");
        return;
    case PageIndex::Page_Stages:
        emit documentationNeeded("faze.html");
        return;
    case PageIndex::Page_Fields:
        emit documentationNeeded("hriste.html");
        return;
    case PageIndex::Page_Conclusion:
        emit documentationNeeded("shrnuti.html");
        return;
    }
    QMessageBox::information(this, tr("New Tournament Help"), tr("Sorry, the help is not available yet."));
}

void NewTournament::writeSettings()
{
    QSettings settings;
    settings.beginGroup("new_dialog");
    settings.setValue("geometry", saveGeometry());
    settings.endGroup();
}

void NewTournament::readSettings()
{
    QSettings settings;
    settings.beginGroup("new_dialog");
    restoreGeometry(settings.value("geometry").toByteArray());
    settings.endGroup();

}

void NewTournament::storeTournamentData()
{
    QVariantMap tournamentSettingMap;
    tournamentSettingMap[key_NAME] = field("introduction.name");
    _tournamentName = field("introduction.name").toString();
    _tournamentSetting = tournamentSettingMap;
}

void NewTournament::storeFieldsData()
{
    QVariantMap fieldsSettingMap;
    fieldsSettingMap["fields"] = field("fields.do");
    fieldsSettingMap["alg_creation"] = field("fields.alg_creation");
    fieldsSettingMap["num_fields"] =  field("fields.num_fields");
    _fieldsSetting = fieldsSettingMap;
}

void NewTournament::storeStagesData()
{
    _stagesTypes.clear();
    _stagesSetting.clear();
    _stagesParams.clear();
    for(const auto & stage : field("stages.data").toList()){
        QVariantMap stageMap = stage.toMap();
        _stagesTypes.push_back(stageMap[key_STAGE_TYPE].toString());
        _stagesSetting.push_back(stageMap[key_SETTING]);
        _stagesParams.push_back(stageMap[key_PARAMETERS]);
    }
}

void NewTournament::storeStartListData()
{
    _startListSetting.clear();
    _teamsData.clear();
    QVariantMap startListSettingMap;
    startListSettingMap["ranked"] = field("start_list.ranked");
    startListSettingMap["descendinly"] = field("start_list.descendinly");
    startListSettingMap["clubs"] = field("start_list.clubs");
    _startListSetting = startListSettingMap;
    _teamsData = field("start_list.data");
}

QString NewTournament::getTournamentName()
{
    return _tournamentName;
}

QVariant NewTournament::getTournamentSetting()
{
    return _tournamentSetting;;
}

QVariant NewTournament::getStartListSetting()
{
    return _startListSetting;
}

QVariant NewTournament::getTeamsData()
{
    return _teamsData;
}

QVariant NewTournament::getFieldsSetting()
{
    return _fieldsSetting;
}

QList<QString> NewTournament::getStagesTypes()
{
    return _stagesTypes;
}

QList<QVariant> NewTournament::getStagesSetting()
{
    return _stagesSetting;
}

QList<QVariant > NewTournament::getStagesParams()
{
    return _stagesParams;
}

