//    Fair Tournament - desktop application for scheduling and organizing sport events.
//    Copyright (C) 2019  Aleš Suchomel

//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/gpl-3.0.html>.
//    You can also contact the authors by mail fairtournament@gmail.com


#include <QApplication>
#include <QString>
#include <QList>
#include <iostream>
#include "mainwindow.h"
#include "tournament.h"
#include <QTranslator>
#include <QSettings>
#include <QStringList>
#include <QPalette>
#include "gpalettemanager.h"
#include <QSplashScreen>
#include "controller.h"
#include "cstagesfactory.h"
#include "serialization.h"
#include "appsetter.h"


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QApplication::setOrganizationName("Aleš Suchomel");
    QApplication::setApplicationName("Fair Tournament");

    QPixmap pixmap(":/splash.png");
    QSplashScreen splash(pixmap);
    splash.show();
    a.processEvents();

    AppSetter *setter = AppSetter::getInstance();
    setter->readSettings();

    CStagesFactory creator;
    Tournament model(&creator);

    CXMLSerializer xmlSeriarizer;
    SerializationController serializer(&xmlSeriarizer, &model);
    Controller controller(&model, &serializer);

    MainWindow view(&controller, &serializer);

    setter->setUI(&view);
    view.setState(MainWindow::Welcome);
    view.show();
    splash.hide();
    return a.exec();
}


