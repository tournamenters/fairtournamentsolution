#include "gopenpage.h"

#include <QAction>
#include <QToolButton>
#include <QBoxLayout>
#include <QLabel>
#include <QScrollArea>
#include <QWidget>
#include <QFileInfo>

GOpenPage::GOpenPage(const QString& title, const QString& subTitle, QWidget *parent)
    :QWidget(parent),
    _titleFontSizeRatio(5),
    _buttonsFontSizeRatio(2),
    _recentFileFontRatio(static_cast<float>(1.3))
{
    createMainLayout();
    showTitlePart(title, subTitle);
}

void GOpenPage::showTitlePart(const QString& title, const QString& subTitle)
{
    QLabel* welcome = new QLabel(title);
    setWidgetsFontSize(welcome, _titleFontSizeRatio, true);
    titleLayout->addWidget(welcome);
    QLabel* welcomeText = new QLabel(subTitle);
    welcomeText->setWordWrap(true);
    titleLayout->addWidget(welcomeText);
}

void GOpenPage::showHelpPart(QAction *actHelp, const QString& text)
{
    QToolButton* help = createActionButton(actHelp,_buttonsFontSizeRatio);
    helpLayout->addWidget(help);
    QLabel* helpText = new QLabel(text);
    helpText->setWordWrap(true);
    helpLayout->addWidget(helpText);
}

void GOpenPage::showButtonsPart(const QList<QAction *>& actions)
{
    for(const auto action : actions){
        QToolButton * button = createActionButton(action, _buttonsFontSizeRatio, _filesTypeAlias);
        buttonsLayout->addWidget(button);
    }
}

void GOpenPage::showOpenRecentFilesPart(const QList<QAction *>& openRecentActions)
{
    QLabel* recentTournaments = new QLabel(tr("Recent ") + _filesTypeAlias + tr("s:"));
    setWidgetsFontSize(recentTournaments, _buttonsFontSizeRatio);
    recentTournaments->setAlignment(Qt::AlignTop);
    recentLayout->addWidget(recentTournaments);
    recentLayout->addWidget(createOpenRecentButtonsWidget(openRecentActions));
}

void GOpenPage::setFilesTypeLabel(const QString& filesTypeAlias, const QString& filesTypeIconPath)
{
    _filesTypeAlias = filesTypeAlias;
    _filesTypeIconPath = filesTypeIconPath;
}

void GOpenPage::setWidgetsFontSize(QWidget *widget, float ratio, bool bold)
{
    QFont font = widget->font();
    font.setBold(bold);
    font.setPointSize(static_cast<int>(font.pointSize()*ratio));
    widget->setFont(font);
}

QWidget* GOpenPage::createOpenRecentButtonsWidget(const QList<QAction *>& openRecentActions)
{
    QVBoxLayout *vLayout = new QVBoxLayout();
    for(const auto action : openRecentActions){
        vLayout->addWidget(createRecentButton(action));
    }
    vLayout->setSpacing(vLayout->spacing()/4);
    vLayout->addStretch(1);
    QScrollArea* scRecentButtons = new QScrollArea(this);
    scRecentButtons->setFrameShape(QFrame::NoFrame);
    scRecentButtons->setLayout(vLayout);
    scRecentButtons->setWidgetResizable(true);
    return scRecentButtons;
}

void GOpenPage::createMainLayout()
{
    titleLayout = new QVBoxLayout;
    helpLayout = new QVBoxLayout;
    QList<QLayout*> leftLayouts = {titleLayout, helpLayout};
    QList<int> leftStretches = {1, 4, 1};

    buttonsLayout = new QHBoxLayout;
    recentLayout = new QHBoxLayout;
    QList<QLayout*> rightLayouts = {buttonsLayout, recentLayout};
    QList<int> rightStretches = {1, 2, 4};

    QHBoxLayout* mainHLayout = new QHBoxLayout;
    mainHLayout->addStretch(1);
    mainHLayout->addLayout(createRowLayout(leftLayouts,leftStretches));
    mainHLayout->addStretch(2);
    mainHLayout->addLayout(createRowLayout(rightLayouts, rightStretches));
    mainHLayout->addStretch(4);
    setLayout(mainHLayout);
}

QToolButton *GOpenPage::createActionButton(QAction *action, float fontRatio, const QString& appendText)
{
    QToolButton* button = new QToolButton(this);
    setWidgetsFontSize(button, fontRatio);
    button->setDefaultAction(action);
    button->setIconSize(button->iconSize()*static_cast<qreal>(fontRatio));
    button->setToolButtonStyle(Qt::ToolButtonStyle::ToolButtonTextBesideIcon);
    button->setText(" " + button->text() + " " + appendText);
    button->setAutoRaise(true);
    return button;
}

QToolButton *GOpenPage::createRecentButton(QAction *action)
{
    QToolButton* tbOpenRecent = createActionButton(action);
    tbOpenRecent->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    tbOpenRecent->setText(createRecentFileButtonText(action->text()));
    tbOpenRecent->setIcon(QIcon(_filesTypeIconPath));
    setWidgetsFontSize(tbOpenRecent,_recentFileFontRatio);
    return tbOpenRecent;
}

QString GOpenPage::createRecentFileButtonText(const QString &actionText)
{
    QStringList list = actionText.split(QRegExp("\\s+"), QString::SkipEmptyParts);
    list.removeFirst();
    QString fileName = QFileInfo(list.join(" ")).fileName();
        if(!fileName.isEmpty() && fileName.contains(".")){
            return " " + fileName;
    }
    return actionText;
}

QVBoxLayout *GOpenPage::createRowLayout(const QList<QLayout *> &layouts, const QList<int> &stretches)
{
    QVBoxLayout* row = new QVBoxLayout();
    for(int i = 0; i< layouts.size(); i++){
        if(i<stretches.size()){
            row->addStretch(stretches[i]);
        }
        row->addLayout(layouts[i]);
    }
    if(stretches.size()>layouts.size()){
        row->addStretch(stretches.size());
    }
    return row;
}
