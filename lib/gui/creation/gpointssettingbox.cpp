#include "gpointssettingbox.h"
#include <QBoxLayout>
#include <QFormLayout>
#include <QSpinBox>
#include <QComboBox>
#include "global_model.h"
#include <QMenu>
#include <QAction>

GPointsSettingBox::GPointsSettingBox(QWidget *parent)
    : QGroupBox (parent)
{
    setTitle(tr("Points and criteria setting"));
    initializateFields();

    QFormLayout * ordLayout = new QFormLayout();
    ordLayout->addRow(tr("Sport Profile:"), createProfileComboBox());
    ordLayout->addRow(createPointsBox());
    ordLayout->addRow(tr("Order of Criteria:"), new QHBoxLayout());
    ordLayout->addRow(createCriteriaOrderTabBar());
    setLayout(ordLayout);
    setProfileValues(_possibleProfiles.first());
}

GPointsSettingBox::~GPointsSettingBox()
{
    _implicitCriteriaOrder.clear();
    _criteriaKeyLabelMap.clear();
    qDeleteAll(_profilesSetting.values());
    _profilesSetting.clear();
    _possibleProfiles.clear();
    _criteriaActions.clear();
}

void GPointsSettingBox::initializateFields()
{
    _implicitCriteriaOrder = CModelConstants::Instance().getKeyImplicitCriteriaOrder();
    _criteriaKeyLabelMap = CModelConstants::Instance().getKeyLabelOrdererCriteriaMap();

    QString footballKey = tr("Football");
    _possibleProfiles.push_back(footballKey);
    _profilesSetting[footballKey] = new OrdererProfile;

    QString hockeyKey = tr("Hockey");
    _possibleProfiles.push_back(hockeyKey);
    OrdererProfile * hockeyProfile = new OrdererProfile;
    hockeyProfile->isOT = true;
    _profilesSetting[hockeyKey] = hockeyProfile;

    QString tableTennisKey = tr("Table tennis");
    _possibleProfiles.push_back(tableTennisKey);
    OrdererProfile * tableTennisProfile = new OrdererProfile;
    tableTennisProfile->win = 1;
    tableTennisProfile->loss = 0;
    tableTennisProfile->draw = 0;
    _profilesSetting[tableTennisKey] = tableTennisProfile;

    QString customKey = tr("Custom");
    _possibleProfiles.push_back(customKey);

}

void GPointsSettingBox::setData(const QVariant &data)
{
    QMap<QString, QVariant> ordererMap = data.toMap();
    QMap<QString, QVariant> pointsMap = ordererMap["points"].toMap();
    blockSignals(true);
    sbWin->setValue(pointsMap["win"].toInt());
    sbDraw->setValue(pointsMap["draw"].toInt());
    sbLoss->setValue(pointsMap["loss"].toInt());
    sbLossOT->setValue(pointsMap["loss_ot"].toInt());
    sbWinOT->setValue(pointsMap["win_ot"].toInt());
    boxOverTime->setChecked(pointsMap["is_ot"].toInt());
    blockSignals(false);
    QStringList keyCriteriaOrder;
    for(const auto& variant : ordererMap["order_criteria"].toList()){
        keyCriteriaOrder.push_back(variant.toString());
    }
    setCriteriaOrder(keyCriteriaOrder);
    cbOrdererProfile->setCurrentIndex(_possibleProfiles.indexOf(ordererMap["profile"].toString()));
}

QVariant GPointsSettingBox::getData()
{
    QMap<QString, QVariant> ordererMap;
    ordererMap["profile"] = _possibleProfiles[cbOrdererProfile->currentIndex()];
    QMap<QString, QVariant> pointsMap;
    pointsMap["win"] = sbWin->value();
    pointsMap["draw"] = sbDraw->value();
    pointsMap["loss"] = sbLoss->value();
    pointsMap["loss_ot"] = sbLossOT->value();
    pointsMap["win_ot"] = sbWinOT->value();
    pointsMap["is_ot"] = boxOverTime->isChecked();
    ordererMap["points"] = QVariant(pointsMap);

    QVariantList keyCriteriaOrder;
    for(int i = 0; i<tabarCriteriaOrder->count(); i++){
        keyCriteriaOrder.push_back(_criteriaKeyLabelMap.key(tabarCriteriaOrder->tabText(i)));
    }
    ordererMap["order_criteria"] = QVariant(keyCriteriaOrder);
    return QVariant(ordererMap);
}


void GPointsSettingBox::criteriaContextMenuRequest()
{
    _criteriaActions.clear();
    QMenu menu(this);
    for(const auto& label : _criteriaKeyLabelMap.values()){
        QAction * actCriteria = new QAction(label, this);
        menu.addAction(label, this, &GPointsSettingBox::addCriteriaTab);
        actCriteria->setEnabled(!isChoosen(label));
        _criteriaActions[label] = actCriteria;
    }
    menu.exec(QCursor::pos());
}

void GPointsSettingBox::profileChange()
{
    setProfileValues(_possibleProfiles[cbOrdererProfile->currentIndex()]);
}

void GPointsSettingBox::addCriteriaTab()
{
    QAction * action = static_cast<QAction*>(sender());
    tabarCriteriaOrder->insertTab(tabarCriteriaOrder->currentIndex()+1, _criteriaActions.key(action));
    emit dataChanged();
}

void GPointsSettingBox::setProfileValues(QString profileKey)
{
    if(_profilesSetting.contains(profileKey)){
        blockSignals(true);
        OrdererProfile * profile = _profilesSetting[profileKey];
        sbWin->setValue(profile->win);
        sbDraw->setValue(profile->draw);
        sbWin->setValue(profile->win);
        sbLoss->setValue(profile->loss);
        sbWinOT->setValue(profile->winOT);
        sbLossOT->setValue(profile->lossOT);
        boxOverTime->setChecked(profile->isOT);
        blockSignals(false);
        cbOrdererProfile->blockSignals(true);
        cbOrdererProfile->setCurrentIndex(_possibleProfiles.indexOf(profileKey));
        cbOrdererProfile->blockSignals(false);
        setCriteriaOrder(_profilesSetting[profileKey]->criteriaOrder);
        emit dataChanged();
    }

}

void GPointsSettingBox::setCriteriaOrder(const QStringList &criteriaOrder)
{
    tabarCriteriaOrder->blockSignals(true);
    while (tabarCriteriaOrder->count()) {
        tabarCriteriaOrder->removeTab(0);
    }
    for(const auto& label : criteriaOrder){
        tabarCriteriaOrder->addTab(label);
    }
    tabarCriteriaOrder->blockSignals(false);
}

QComboBox *GPointsSettingBox::createProfileComboBox()
{
    cbOrdererProfile = new QComboBox(this);
    cbOrdererProfile->addItems(_possibleProfiles);
    connect(cbOrdererProfile,SIGNAL(currentIndexChanged(int)),
            this, SLOT(profileChange()));
    return cbOrdererProfile;
}

QGroupBox *GPointsSettingBox::createPointsBox()
{
    QGroupBox * boxPoints = new QGroupBox(tr("Points"));
    QHBoxLayout * pointsLayout = new QHBoxLayout();
    pointsLayout->addLayout(createMainPointsLayout());
    pointsLayout->addWidget(createOverTimeBox());
    boxPoints->setLayout(pointsLayout);
    return boxPoints;
}

QTabBar *GPointsSettingBox::createCriteriaOrderTabBar()
{
    tabarCriteriaOrder = new QTabBar(this);
    tabarCriteriaOrder->setTabsClosable(true);
    tabarCriteriaOrder->setMovable(true);
    tabarCriteriaOrder->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(tabarCriteriaOrder, SIGNAL(tabCloseRequested(int)),
            this, SLOT(criteriaCloseRequest(int)));
    connect(tabarCriteriaOrder, SIGNAL(tabMoved(int, int)),
            this, SLOT(orderCriteriaChange()));
    connect(tabarCriteriaOrder, SIGNAL(customContextMenuRequested(const QPoint&)),
            this, SLOT(criteriaContextMenuRequest()));
    return tabarCriteriaOrder;
}

QLayout *GPointsSettingBox::createMainPointsLayout()
{
    sbWin = new QSpinBox(this);
    sbWin->setRange(1,100);
    connect(sbWin, SIGNAL(valueChanged(int)),
            this, SLOT(sbWinChange()));
    sbDraw = new QSpinBox(this);
    connect(sbDraw, SIGNAL(valueChanged(int)),
            this, SLOT(sbDrawChange()));
    sbLoss = new QSpinBox(this);
    connect(sbLoss, SIGNAL(valueChanged(int)),
            this, SLOT(sbLossChange()));
    QFormLayout * layouts = new QFormLayout();
    layouts->addRow(tr("Win"), sbWin);
    layouts->addRow(tr("Draw"), sbDraw);
    layouts->addRow(tr("Loss"), sbLoss);
    return layouts;
}

QGroupBox *GPointsSettingBox::createOverTimeBox()
{
    boxOverTime = new QGroupBox(tr("Over Time"),this);
    boxOverTime->setCheckable(true);
    boxOverTime->setChecked(false);
    boxOverTime->setLayout(createOverTimeBoxLayout());
    connect(boxOverTime, SIGNAL(clicked(bool)),
            this, SLOT(isOTChange()));
    return boxOverTime;
}

QLayout *GPointsSettingBox::createOverTimeBoxLayout()
{
    sbWinOT = new QSpinBox(this);
    connect(sbWinOT, SIGNAL(valueChanged(int)),
            this, SLOT(sbWinOTChange()));
    sbLossOT = new QSpinBox(this);
    connect(sbLossOT, SIGNAL(valueChanged(int)),
            this, SLOT(sbLossOTChange()));

    QFormLayout * layout = new QFormLayout();
    layout->addRow(tr("Win OT"), sbWinOT);
    layout->addRow(tr("Loss OT"), sbLossOT);
    return layout;
}

bool GPointsSettingBox::isChoosen(const QString &criteria)
{
    for(int tab = 0; tab<tabarCriteriaOrder->count(); tab++){
        if(criteria == tabarCriteriaOrder->tabText(tab)){
            return true;
        }
    }
    return false;
}

void GPointsSettingBox::setCustomProfile()
{
    cbOrdererProfile->blockSignals(true);
    cbOrdererProfile->setCurrentIndex(_possibleProfiles.size()-1);
    cbOrdererProfile->blockSignals(false);

}

void GPointsSettingBox::sbWinChange()
{
    sbDraw->setRange(0, sbWin->value());
    sbWinOT->setRange(0, sbWin->value());
    sbLoss->setRange(0, sbWin->value());
    setCustomProfile();
    emit dataChanged();
}

void GPointsSettingBox::sbDrawChange()
{
    sbLoss->setRange(0, sbDraw->value());
    setCustomProfile();
    emit dataChanged();
}

void GPointsSettingBox::sbLossChange()
{
    setCustomProfile();
    emit dataChanged();
}

void GPointsSettingBox::sbLossOTChange()
{
    sbLoss->setRange(0,sbLossOT->value());
    setCustomProfile();
    emit dataChanged();
}

void GPointsSettingBox::sbWinOTChange()
{
    sbLossOT->setRange(sbLoss->value(), sbWinOT->value());
    setCustomProfile();
    emit dataChanged();
}

void GPointsSettingBox::isOTChange()
{
    setCustomProfile();
    emit dataChanged();
}

void GPointsSettingBox::orderCriteriaChange()
{
    setCustomProfile();
    emit dataChanged();
}

void GPointsSettingBox::criteriaCloseRequest(int index)
{
    if(tabarCriteriaOrder->count() > 1){
        tabarCriteriaOrder->removeTab(index);
    }
    orderCriteriaChange();
}


OrdererProfile::OrdererProfile()
{    
    QList<QString> orderKeys = CModelConstants::Instance().getKeyImplicitCriteriaOrder();
    QMap<QString, QString> keyLabelMap = CModelConstants::Instance().getKeyLabelOrdererCriteriaMap();
    for(const auto&key : orderKeys){
        criteriaOrder.push_back(keyLabelMap[key]);
    }

    win = 3;
    draw = 1;
    loss = 0;
    winOT = 2;
    lossOT = 1;
    isOT = 0;

}

OrdererProfile::~OrdererProfile()
{
    criteriaOrder.clear();
}
