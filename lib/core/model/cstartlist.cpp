#include "cstartlist.h"
#include "cteam.h"
#include <qglobal.h>
#include <ctime>
#include <algorithm>
#include <random>

CStartList::CStartList(QObject *parent)
    :CTeamsList (true, parent)
{
    _isRanked = false;
    _rankedDescendinly = false;
    _hasClubs = false;
    _nextTeamID = 0;
    setName(tr("Start List"));
    std::srand(unsigned(std::time(nullptr)));
    addToIdentificator("start_list", 1);
    addToIdentificator("group_name", "start_list");
}

CStartList::CStartList(const QStringList &columnNames, QObject *parent)
    :CStartList(parent)
{
    setColumnHeaders(columnNames);
}

CStartList::~CStartList()
{
    clear();
}

void CStartList::createTeam(const QVariant &teamData)
{
    QVariantMap data = teamData.toMap();
    if(data.contains("name") && !data["name"].toString().isEmpty()){
        int id = _nextTeamID;
        if(data.contains("id")){
            id = data["id"].toInt();
        }
        CTeam * team = new CTeam(id, data["name"].toString(), this);
        _nextTeamID++;
        team->setClub(data["club"].toString());
        team->setRank(data["rank"].toInt());
        addRow(team, data);
    }
}

void CStartList::createTeams(const QVariant &teamsData)
{
    QStringList headers = teamsData.toMap()["keys"].toStringList();
    headers.removeOne("name");
    headers.removeOne("club");
    headers.removeOne("rank");
    setColumnHeaders(headers);

    QVariantList rows = teamsData.toMap()["rows"].toList();
    for(const auto& team : rows){
        createTeam(team);
    }
}

void CStartList::setRanked(bool ranked)
{
    _isRanked = ranked;
}

void CStartList::setHaveClubs(bool clubs)
{
    _hasClubs = clubs;
}

void CStartList::setRankedType(bool descendinly)
{
    _rankedDescendinly = descendinly;
}

void CStartList::clear()
{
    qDeleteAll(_teams);
    CTeamsList::clear();
}

QList<CTeam *> CStartList::getTeamsInValidOrder() const
{
    if(_isRanked){
        return orderTeamsByRank(_teams);
    }
    return shuffleTeams(_teams);
}

QVariant CStartList::getData() const
{
    QVariantMap table = CTeamsList::getData().toMap();
    QStringList headers = table["keys"].toStringList();    
    if(_isRanked){
        headers.push_front(tr("Rank"));
    }
    if(_hasClubs){
        headers.push_front(tr("Club"));
    }
    headers.push_front(tr("Name"));
    QVariantList rows = table["rows"].toList();
    QVariantList newRows;
    for(int i = 0; i < _teams.size(); i++){
        if(_teams[i]){
            QVariantMap mapTeam = rows[i].toMap();
            mapTeam[tr("Name")] = _teams[i]->toString();
            if(_hasClubs){
                mapTeam[tr("Club")] = _teams[i]->getClub();
            }
            if(_isRanked){
                mapTeam[tr("Rank")] = _teams[i]->getRank();
            }
            newRows.push_back(mapTeam);
        }
    }
    QVariantMap data;
    data["keys"] = headers;
    data["rows"] = newRows;
    data["teams"] = teamsToVariant();
    return data;
}

QVariant CStartList::toVariant() const
{   
    QVariantMap parametersMap;
    parametersMap["clubs"] = _hasClubs;
    parametersMap["ranked"] = _isRanked;
    parametersMap["descendinly"] = _rankedDescendinly;

    QVariantMap map;
    map["parameters"] = parametersMap;
    map["data"] = CTeamsList::getData();
    return map;
}

void CStartList::fromVariant(const QVariant &variant)
{
    QVariantMap map = variant.toMap()["teams"].toMap();
    QVariantMap parametersMap = map["parameters"].toMap();
    map["parameters"] = parametersMap;
    setRanked(parametersMap["ranked"].toBool());
    setHaveClubs(parametersMap["clubs"].toBool());
    setRankedType(parametersMap["descendinly"].toBool());

    QVariantMap data = map["data"].toMap();

    for (const auto& teamData : data["teams"].toList()) {
        createTeam(teamData);
    }

    setColumnHeaders(data["keys"].toStringList());
    setColumnData(data["rows"]);
}

QMap<int, CTeam *> CStartList::getTeamsById() const
{
    QMap<int, CTeam *> map;
    for(const auto team : _teams){
        map[team->getID()] = team;
    }
    return map;
}

bool CStartList::execEdit(ExecType type, const QVariant& identificator, const QVariant& data)
{
    if(isItForMe(identificator)){
        switch (type) {
        case ExecType::Rename:
            return renameTeam(identificator, data);
        case ExecType::Swap:
            return swapTeams(data);
        case ExecType::Editation:
            return editStartList(data);
        default:
            return false;
        }
    }
    return true;
}

bool CStartList::swapTeams(const QVariant &data)
{
    QVariantMap map = data.toMap();
    if(map.contains("old") && map.contains("new")){        
        int firstTeam = map.value("old").toMap()["row"].toInt();
        int secondTeam = map.value("new").toMap()["row"].toInt();
        if(firstTeam<_teams.size() && secondTeam<_teams.size()){
            blockSignals(true);
            CTeam::swapData(_teams[firstTeam], _teams[secondTeam]);
            blockSignals(false);
            emit dataChanged();
            return true;
        }  
    }
    return false;
}

bool CStartList::editStartList(const QVariant &data)
{
    QStringList keys = data.toMap()["keys"].toStringList();
    QVariantList listNew = data.toMap()["rows"].toList();
    if(listNew.size() == _teams.size()){
        blockSignals(true);
        for (int row = 0; row<listNew.size(); row++){
            QVariantMap mapTeamNew = listNew[row].toMap();
            for (const auto& key : keys) {
                QString value = mapTeamNew[key].toString();
                if(!value.isEmpty()){
                    if(key == tr("Name")){
                        _teams[row]->rename(value);
                    }
                    else if(key == tr("Club")){
                        _teams[row]->setClub(value);
                    }
                    else if(key == tr("Rank")){
                        _teams[row]->setRank(value.toInt());
                    }
                    else{
                        changeValue(row, key, value);
                    }
                }
            }
        }
        blockSignals(false);
        emit dataChanged();
        return true;
    }
    return false;
}

QList<CTeam *> CStartList::orderTeamsByRank(const QList<CTeam *>& teams) const
{
    QList<QList<int>> sortableList;
    for(int i = 0; i<teams.size();i++){
        sortableList.push_back({_teams[i]->getRank(),i});
    }
    std::sort(sortableList.begin(), sortableList.end());
    if(!_rankedDescendinly){
        std::reverse(sortableList.begin(), sortableList.end());
    }
    QList<CTeam*> list;
    for (const auto& pair : sortableList) {
        list.push_back(teams.at(pair[1]));
    }
    return list;
}

QList<CTeam *> CStartList::shuffleTeams(const QList<CTeam *>& teams) const
{
    QList<CTeam *> list = teams;
    std::random_shuffle(list.begin(), list.end());
    return list;
}
