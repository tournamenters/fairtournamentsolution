#include "cteamstable.h"
#include "cteam.h"

CTeamsList::CTeamsList(bool connect, QObject *parent)
    : AModel(parent)
{
    _connect = connect;
}

CTeamsList::CTeamsList(const QList<CTeam *>& teams, QObject *parent)
    : CTeamsList(parent)
{
    _teams = teams;
    if(_connect){
        for(const auto team : teams){
            connectTeam(team);
        }
    }
}

CTeamsList::~CTeamsList()
{
    clear();
}

void CTeamsList::addColumn(const QString& header, const QStringList &values)
{
    _columnNames.push_back(header);
    _customColumns[header] = values;
}

void CTeamsList::addColumn(const QString &header, const QList<int> &values)
{
    _columnNames.push_back(header);
    _customColumns[header] = toStringList(values);
}

void CTeamsList::addRow(CTeam* team, const QVariantMap& values)
{
    _teams.push_back(team);
    connectTeam(team);
    for(const auto& header : _columnNames){
        _customColumns[header].push_back(values[header].toString());
    }
}

void CTeamsList::addRow(const QPair<CTeam *, QVariantMap>& row)
{
    addRow(row.first, row.second);
}

void CTeamsList::setColumnHeaders(const QStringList &columnHeaders)
{
    _columnNames = columnHeaders;
}

void CTeamsList::setColumnData(const QVariant &data)
{
    QVariantList rows = data.toList();
    for(const auto& row : rows){
        QVariantMap rowMap = row.toMap();
        for(const auto& header : _columnNames){
             _customColumns[header].push_back(rowMap[header].toString());
        }
    }
}

void CTeamsList::changeTeam(int row, CTeam *team)
{
    if(row < _teams.size() && _teams[row] != team){
        if(_connect){
            if(_teams[row]){
                disconnect(_teams[row], nullptr,
                           this, nullptr);
            }
            connectTeam(team);
        }
        _teams[row] = team;
    }
}

void CTeamsList::changeValue(int row, const QString &header, const QString &value)
{
    if(row < _teams.size() && _columnNames.contains(header)){
        _customColumns[header][row] = value;
    }
}

void CTeamsList::changeTeams(const QList<CTeam *> &teams)
{
    int row = 0;
    for (const auto team : teams) {
        changeTeam(row, team);
        row++;
    }
}

void CTeamsList::changeTeams(CTeamsList *teams)
{
    for (int row = 0; row<teams->getTeams().size(); row++) {
        auto pair = teams->getRow(row);
        changeRow(row, pair.first, pair.second);
    }
    delete teams;
}

void CTeamsList::changeTeams(const QList<CTeamsList*> &teams)
{
    CTeamsList* oneTable = new CTeamsList();
    oneTable->setColumnHeaders(teams.first()->getColumnHeaders());
    int round = 0;
    int maxSize = getTeams().size();
    while(oneTable->getTeams().size() < maxSize  && round < maxSize){
        for (const auto& table : teams) {
            if(round < table->getTeams().size()){
                oneTable->addRow(table->getRow(round));
            }
        }
        round++;
    }
    qDeleteAll(teams);
    changeTeams(oneTable);
}

void CTeamsList::changeRow(int row, CTeam *team, const QVariantMap &values)
{
    changeTeam(row, team);
    for (const auto& header : _columnNames) {
        if(values.contains(header)){
            changeValue(row, header, values[header].toString());
        }
    }
}

QList<CTeam *> CTeamsList::getTeams() const
{
    return _teams;
}

bool CTeamsList::hasColumn(const QString& header) const
{
    return _customColumns.contains(header);
}

void CTeamsList::clear()
{
    _teams.clear();
    _customColumns.clear();
    _columnNames.clear();
}

QList<QStringList> CTeamsList::toStringList() const
{
    QList<QStringList> table;

    return table;
}

QVariant CTeamsList::getData() const
{
    QVariantList rows;
    QStringList headers = getColumnHeaders();
    QVariantList keys;
    for(int row = 0; row<_teams.size(); row++){
        QVariantMap rowData;
        for(const auto& header : headers){
            if(row < _customColumns[header].size()){
                rowData[header] = _customColumns[header][row];
            }
            if(row==0){
                keys.push_back(header);
            }
        }
        rows.push_back(rowData);
    }
    QVariantMap data;
    data["keys"] = keys;
    data["rows"] = rows;
    data["teams"] = teamsToVariant();
    return data;
}

QStringList CTeamsList::getColumn(const QString &header) const
{
    return _customColumns[header];
}

QPair<CTeam *, QVariantMap> CTeamsList::getRow(int row) const
{
    QPair<CTeam *, QVariantMap> rowData(nullptr, QVariantMap());
    if(row<_teams.size()){
        rowData.first = _teams[row];
    }

    for (const auto& header : _columnNames) {
        if(row<_customColumns[header].size()){
            rowData.second[header] = _customColumns[header][row];
        }
    }
    return rowData;
}

QStringList CTeamsList::getColumnHeaders() const
{
    return _columnNames;
}

bool CTeamsList::execEdit(ExecType type, const QVariant& identificator, const QVariant& data)
{
    if(isItForMe(identificator)){
        if(type == ExecType::Rename){
            return renameTeam(identificator, data);
        }
    }
    return false;
}

bool CTeamsList::renameTeam(const QVariant &identificator, const QVariant &data)
{
    if(identificator.toMap().contains("row") && data.toMap().contains("name")){
        int row = identificator.toMap().value("row").toInt();
        if(row<_teams.size()){
            _teams[row]->rename(data.toMap()["name"].toString());
            return true;
        }
    }
    emit dataChanged();
    return false;
}

QVariant CTeamsList::teamsToVariant() const
{
    QVariantList teamsData;
    for(const auto team : _teams){
        if(team){
            teamsData.push_back(team->toVariant());
        }
        else{
            teamsData.push_back(QVariant());
        }
    }
    return teamsData;
}

QStringList CTeamsList::toStringList(const QList<int> &list)
{
    QStringList stringList;
    for(int value : list){
        stringList.push_back(QString::number(value));
    }
    return stringList;
}

void CTeamsList::connectTeam(CTeam *team)
{
    if(team){
        connect(team, SIGNAL(updated()),
                this, SIGNAL(teamUpdated()));

        connect(team, SIGNAL(updated()),
                this, SIGNAL(dataChanged()));
    }
}

