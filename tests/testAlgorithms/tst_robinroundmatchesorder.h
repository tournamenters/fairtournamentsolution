#pragma once

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "lrobinroundmatchesordererfactory.h"

#include <QList>
#include <QSet>

using namespace testing;

int maxRank(const QList<QList<int>>& matches)
{
    int max = 0;
    for (int i = 0; i<matches.size(); i++){
        for (int j = 0; j<matches[i].size(); j++){
            if(matches[i][j]>max){
                max = matches[i][j];
            }
        }
    }
    return max;
}

void nobodyPlaysItself(const QList<QList<int>>& matches)
{
    for (int i = 0; i<matches.size();i++){
        if(!matches[i].isEmpty()){
            ASSERT_NE(matches[i][0], matches[i][1]);
        }
    }
}

void everybodyPlaysEveryBody(const QList<QList<int>>& matches)
{
    int max = maxRank(matches);
    QSet<int> oponents;
    for(int i = 0; i<=max;i++){
        for(int j = 0; j<matches.size();j++){
            if (matches[j][0]==i){
                oponents.insert(matches[j][1]);
            }
            if (matches[j][1]==i){
                oponents.insert(matches[j][0]);
            }
        }
        ASSERT_EQ(oponents.size(), max);
        oponents.clear();
    }
}

void rightNumberOfMatches(const QList<QList<int>>& matches)
{
    int numTeams = 1 + maxRank(matches);
    ASSERT_EQ(numTeams*(numTeams-1)/2, matches.size());
}

void testMatchesPlan(const QList<QList<int>>& matches)
{
    nobodyPlaysItself(matches);
    rightNumberOfMatches(matches);
    everybodyPlaysEveryBody(matches);
}

void testOrderer(AMatchesOrderer* orderer)
{
    for (int numTeams = 2; numTeams < 50; numTeams++){
        auto matches = orderer->planMatches(numTeams);
        testMatchesPlan(matches);
    }
}

TEST(robinroundmatchesorder, rotation)
{
    AMatchesOrderer* rotation = LMatchesOrdererFactory::Instance()->createMatchesOrderer("rotation");
    testOrderer(rotation);
    QList<QList<int>> rightForFour = {{0,3},{1,2},
                                      {0,2},{3,1},
                                      {0,1},{2,3}};
    ASSERT_EQ(rightForFour, rotation->planMatches(4));
    QList<QList<int>> rightForSix= {{0,5},{1,4},{2,3},
                                     {0,4},{5,3},{1,2},
                                     {0,3},{4,2},{5,1},
                                     {0,2},{3,1},{4,5},
                                     {0,1},{2,5},{3,4}};
    ASSERT_EQ(rightForSix, rotation->planMatches(6));
    delete rotation;
}

TEST(robinroundmatchesorder, kirkman)
{
    AMatchesOrderer* kirkman = LMatchesOrdererFactory::Instance()->createMatchesOrderer("kirkman");
    testOrderer(kirkman);
    QList<QList<int>> rightForFour = {{3,0},{1,2},
                                      {2,0},{1,3},
                                      {0,1},{3,2}};
    ASSERT_EQ(rightForFour, kirkman->planMatches(4));
    QList<QList<int>> rightForSix= {{5,0},{1,4},{3,2},
                                     {2,0},{1,5},{4,3},
                                     {0,4},{5,2},{3,1},
                                     {1,0},{3,5},{4,2},
                                     {0,3},{5,4},{2,1}};
    ASSERT_EQ(rightForSix, kirkman->planMatches(6));
    delete kirkman;
}

TEST(robinroundmatchesorder, berger)
{
    AMatchesOrderer* berger = LMatchesOrdererFactory::Instance()->createMatchesOrderer("berger");
    testOrderer(berger);
    QList<QList<int>> rightForFour = {{0,3},{1,2},
                                      {3,2},{0,1},
                                      {1,3},{2,0}};
    ASSERT_EQ(rightForFour, berger->planMatches(4));
    QList<QList<int>> rightForEight = {{0,7},{1,6},{2,5},{3,4},
                                       {7,4},{5,3},{6,2},{0,1},
                                       {1,7},{2,0},{3,6},{4,5},
                                       {7,5},{6,4},{0,3},{1,2},
                                       {2,7},{3,1},{4,0},{5,6},
                                       {7,6},{0,5},{1,4},{2,3},
                                       {3,7},{4,2},{5,1},{6,0}};
    ASSERT_EQ(rightForEight, berger->planMatches(8));
    delete berger;
}
