#ifndef CRESULT_H
#define CRESULT_H
#include <QString>
#include <QList>

class CResult
{

public:
    CResult();
    CResult(int hostScore, int guestScore);
    CResult(const QString& string);

    bool update(int hostScore, int guestScore);
    bool update(const QString& string);
    void updateReversed(const QString& string);

    int getGuest() const;
    int getHost() const;
    bool wasPlayed() const;
    bool isOverTime() const;

    QString toString() const;
    QString toStringReversed() const;
    QList<int> toIntList() const;

    bool equals(const CResult& result) const;
    bool equalsReverse(const CResult& result) const;

    bool wonHost() const;
    bool wasDraw() const;
    bool wonGuest() const;

    static bool isResult(const QString& string);

private:
   void reset();
   QString getOverTimeMark() const;
   QStringList overTimeCheck(const QStringList &resultList);
   bool isOverTimeMark(const QString& text);

   int _hostScore;
   int _guestScore;
   bool _isSet;
   bool _overTime;
};

#endif // CRESULT_H
