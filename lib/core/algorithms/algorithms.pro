#-------------------------------------------------
#
# Project created by QtCreator 2019-04-30T20:49:10
#
#-------------------------------------------------

TARGET = algorithms
TEMPLATE = lib

QT += widgets
QT += core

CONFIG += c++11
CONFIG -= app_bundle
CONFIG += staticlib

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    afieldsassigner.cpp \
    aplayoffdrawer.cpp \
    arobinroundmatchesorderer.cpp \
    ateamssorter.cpp \
    lfieldsassignerfactory.cpp \
    agroupsdrawer.cpp \
    lgroupsdrawerfactory.cpp \
    lplayoffdrawerfactory.cpp \
    lrobinroundmatchesordererfactory.cpp \
    lteamssorterfactory.cpp

HEADERS += \
    aplayoffdrawer.h \
    arobinroundmatchesorderer.h \
    ateamssorter.h \
    afieldsassigner.h \
    agroupsdrawer.h \
    lfieldsassignerfactory.h \
    lgroupsdrawerfactory.h \
    lplayoffdrawerfactory.h \
    lrobinroundmatchesordererfactory.h \
    lteamssorterfactory.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES += \
    algorithms.pri

TRANSLATIONS += \
    algorithms_cs.ts
