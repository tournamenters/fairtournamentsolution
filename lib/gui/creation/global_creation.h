#ifndef GLOBAL_CREATION_H
#define GLOBAL_CREATION_H

#include <QList>
#include <QPair>
#include <QString>

enum PageIndex { Page_Intro,
                 Page_Teams,
                 Page_Format,
                 Page_Stages,
                 Page_Fields,
                 Page_Conclusion,
                 Page_Auto};

struct Pair {
    QString _key;
    QString _label;
    Pair(const QString& key, const QString& label){
        _key = key;
        _label = label;
    }
};

const QString keyRobinRound = "robin_round";
const QString keyPlayOff = "playoff";
const QString keyGroupsPlayOff = "groups_playoff";
const QString keyGroups = "groups";

class CreationConstants: QObject
{
    Q_OBJECT

public:
    static CreationConstants& getInstance()
    {
        static CreationConstants instance;
        return instance;
    }
    CreationConstants(CreationConstants const&) = delete;
    void operator=(CreationConstants const&) = delete;

    QList<Pair> getPreparedFormats() const
    {
        return preparedFormats;
    }

    QMap<QString, QString> getFormatsMap() const
    {
        return formatsMap;
    }

    QMap<QString, QList<QString> > getPreparedFormatsStructure() const
    {
        return preparedFormatsStructure;
    }

private:
    CreationConstants(){
        formatsMap[keyRobinRound] = tr("Robin-round");
        formatsMap[keyGroupsPlayOff] = tr("Groups and Play-off");
        formatsMap[keyPlayOff] = tr("Play-off");
        formatsMap[keyGroups] = tr("Groups");
        preparedFormats.push_back(Pair(keyRobinRound, formatsMap[keyRobinRound]));
        preparedFormats.push_back(Pair(keyGroupsPlayOff, formatsMap[keyGroupsPlayOff]));
        preparedFormats.push_back(Pair(keyPlayOff, formatsMap[keyPlayOff]));
        preparedFormatsStructure[keyRobinRound] = { keyRobinRound };
        preparedFormatsStructure[keyPlayOff] = { keyPlayOff };
        preparedFormatsStructure[keyGroupsPlayOff] = { keyGroups, keyPlayOff};

    }
    ~CreationConstants(){
        preparedFormats.clear();
    }

     QList<Pair> preparedFormats;
     QMap<QString, QString> formatsMap;
     QMap<QString, QList<QString>> preparedFormatsStructure;

};


#endif // GLOBAL_CREATION_H




