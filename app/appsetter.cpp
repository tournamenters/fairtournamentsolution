#include "appsetter.h"
#include <QApplication>
#include <QFont>
#include <QStyleFactory>
#include <QTranslator>
#include <QSettings>
#include <QLibraryInfo>
#include <QStringList>
#include <QFile>

#include "gpalettemanager.h"
#include "mainwindow.h"
#include "gthemesettingdialog.h"

const QList<QString> translationFiles = {"algorithms", "app", "creation", "present", "tournamentwidgets", "view", "model", "mainwindow"};

AppSetter::AppSetter(QObject *parent) : QObject(parent)
{
    _view = nullptr;
    _possibleLanguages = {"cs", "en"};
    _possibleStylesKeys = QStyleFactory::keys();
    if(_possibleStylesKeys.contains("windowsvista")){
        _possibleStylesKeys.replace(_possibleStylesKeys.indexOf("windowsvista"),"Default");
    }
    _possiblePalettesKeys = GPaletteFactory::keys();
}

AppSetter::~AppSetter()
{
    _customStylesPaths.clear();
    _possibleLanguages.clear();
    _possibleStylesKeys.clear();
    _possiblePalettesKeys.clear();
    qDeleteAll(_installedTranslators);
    _installedTranslators.clear();
}

void AppSetter::setUI(MainWindow *view)
{
    _view = view;
    _view->setPossibleLanguages(_possibleLanguages,_languageKey);
    connect(_view, SIGNAL(setFontRequest(QFont)),
            this, SLOT(changeFontRequest(QFont)));
    connect(_view, SIGNAL(setLanguageRequest(QString)),
            this, SLOT(changeLanguageRequest(QString)));
    connect(_view, SIGNAL(setAppearanceRequest()),
            this, SLOT(changeThemeRequest()));
}

QStringList AppSetter::getPossibleLanguages()
{
    return _possibleLanguages;
}

QStringList AppSetter::getPossiblePalettes()
{
    return _possiblePalettesKeys;
}

QStringList AppSetter::getPossibleStyles()
{
    return _possibleStylesKeys;
}

void AppSetter::readSettings()
{
    QSettings settings;
    settings.beginGroup("global");
    setLanguage(settings.value("language","").toString());
    setStyle(settings.value("style", "Windows").toString());
    setPalette(settings.value("palette", "Light").toString());
    setFont(settings.value("font", QApplication::font()).value<QFont>());
    settings.endGroup();
}

void AppSetter::writeSetting()
{
    QApplication::font();
    QSettings settings;
    settings.beginGroup("global");
    settings.setValue("language",_languageKey);
    settings.setValue("style",_styleKey);
    settings.setValue("palette", _paletteKey);
    settings.setValue("font", QVariant(QApplication::font()));
    settings.endGroup();
}

void AppSetter::registerCustomStyle(const QString& key, const QString& path)
{
    if(!_possibleStylesKeys.contains(key)){
        _possibleStylesKeys.push_back(key);
        _customStylesPaths[key] = path;
    }
}

void AppSetter::changeLanguageRequest(const QString& languageKey)
{
    _languageKey = languageKey;
    writeSetting();
    sendResetSuggestion();
}

void AppSetter::changeFontRequest(const QFont& font)
{
    setFont(font);
    writeSetting();
}

void AppSetter::changeThemeRequest()
{
    GThemeSettingDialog dialog(getPossiblePalettes(),getPossibleStyles(),_view);
    dialog.setCurrentStyle(_styleKey);
    dialog.setCurrentPalette(_paletteKey);
    if(dialog.exec() == QDialog::Accepted){
        setPalette(dialog.getCurrentPalette());
        setStyle(dialog.getCurrentStyle());
        writeSetting();
    }
}

void AppSetter::setLanguage(QString languageKey)
{
    if (languageKey.isEmpty() || !getPossibleLanguages().contains(languageKey)){
        languageKey = QLocale::system().name();
        switch (QLocale::system().language()) {
            case QLocale::English:
                languageKey = "en";
                break;
            case QLocale::Czech:
                languageKey = "cs";
                break;
            default:
                languageKey = "en";
            }
    }
    _languageKey = languageKey;

    auto path = QApplication::applicationDirPath()+ "/translations/";
    addTranslator("qt",path);
    for(const auto& translation : translationFiles){
        addTranslator(translation,path);
    }
}

void AppSetter::setStyle(QString styleKey)
{
    _styleKey = styleKey;
    if(styleKey == "Default"){
        styleKey = "windowsvista";
    }
    if(QStyleFactory::keys().contains(styleKey)){
        qApp->setStyle(QStyleFactory::create(styleKey));
    }
    else if(_customStylesPaths.contains(styleKey)){
        QFile styleFile(_customStylesPaths[styleKey]);
        if(styleFile.open(QFile::ReadOnly)){
            QString style(styleFile.readAll());
            qApp->setStyleSheet(style);
            styleFile.close();
        }
    }
}

void AppSetter::setPalette(const QString& paletteKey)
{
    QPalette palette = GPaletteFactory::create(paletteKey);
    qApp->setPalette(palette);
    _paletteKey = paletteKey;
}

void AppSetter::setFont(const QFont& font)
{
    QApplication::setFont(font);
}

void AppSetter::sendResetSuggestion()
{
    if(_view){
        _view->showMessage(tr("Reset Requered"),
                           tr("The change will take effect after restart "
                                    "of this application."));
    }
}

void AppSetter::addTranslator(const QString& name, const QString &path)
{
    QTranslator* translator = new QTranslator;
    translator->load(name +"_"+ _languageKey, path);
    QApplication::installTranslator(translator);
    _installedTranslators.push_back(translator);
}
