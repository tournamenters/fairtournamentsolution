#include "cgroupsstage.h"
#include "cmatch.h"
#include "cteamstable.h"
#include "global_model.h"
#include "ateamssorter.h"

CGroupsStage::CGroupsStage(int id, const QString& name, LTeamsSorter *orderer, AGroupsDrawer *drawer, QObject* parent)
    : AStageModel (id, StageType::Groups, parent)
{
    setName(name);
    _sorter = orderer;
    _drawer = drawer;
}

CGroupsStage::~CGroupsStage()
{
    delete _sorter;
    delete _drawer;
    qDeleteAll(_groups);
    _groups.clear();
}

void CGroupsStage::setTeamsCount(int numTeams)
{
    AStageModel::setTeamsCount(numTeams);
    _teamsGroups = _drawer->createGroups(_groups.size(), numTeams);
    for(int i = 0; i<_groups.size(); i++){
        _groups[i]->setTeamsCount(_teamsGroups[i].size());
    }
    coordinateMatches();
}

void CGroupsStage::setTeams(CTeamsList *teams)
{
    blockSignals(true);
    int groupIndex = 0;
    for(const auto& groupTeams : _teamsGroups){
        CTeamsList* teamsForGroup = new CTeamsList();
        teamsForGroup->setColumnHeaders(teams->getColumnHeaders());
        for(int teamIndex : groupTeams){
            teamsForGroup->addRow(teams->getRow(teamIndex));
        }
        _groups[groupIndex]->setTeams(teamsForGroup);
        groupIndex++;
    }
    _teams->changeTeams(teams);
    blockSignals(false);
    emit dataChanged();
}

void CGroupsStage::setTeams(const QList<CTeam *> &teams)
{
    setTeams(new CTeamsList(teams));
}

void CGroupsStage::setTeamsFromSources(const QList<CTeamsList *>& teams)
{
    CTeamsList* teamsTable = new CTeamsList();
    teamsTable->setColumnHeaders(teams.first()->getColumnHeaders());
    bool teamsPlaced = true;
    int row = 0;
    while(teamsPlaced){
        teamsPlaced = false;
        for(const auto table : teams){
            if(table->getTeams().size() > row){
                teamsTable->addRow(table->getRow(row));
                teamsPlaced = true;
            }
        }
        row++;
    }
    setTeams(teamsTable);
}

void CGroupsStage::setNextStage(AStageModel *nextStage)
{
    _nextStage = nextStage;
    if(_nextStage){
        nextStageCare();
    }
}

void CGroupsStage::setGroups(QList<AStageModel *> groups)
{
    qDeleteAll(_groups);
    _groups.clear();
    _groups = groups;
    int index = 0;
    for(const auto group : _groups){
        group->setName(generateGroupName(index));
        group->addToIdentificator("group_index", index);
        connect(group, SIGNAL(dataChanged()),
                this, SLOT(dataChange()));
        index++;
    }
}

void CGroupsStage::setTeamsFromVariant(const QVariant &variant, const QMap<int, CTeam *> &teamsById)
{
    for(const auto stage : _groups){
        stage->setTeamsFromVariant(variant.toMap()["sub_stages"].toList()[_groups.indexOf(stage)], teamsById);
    }
}

void CGroupsStage::resultsFromVariant(const QVariant &variant)
{
    for(const auto stage : _groups){
        stage->resultsFromVariant(variant.toMap()["sub_stages"].toList()[_groups.indexOf(stage)]);
    }
}

QVariant CGroupsStage::getData() const
{
    QVariantList list;
    for(auto const group : _groups){
        list.push_back(group->getData());
    }
    return list;
}

QVariant CGroupsStage::toVariant() const
{
    QVariantMap  map;
    map["name"] = getName();
    map["type"] = key_GROUPS_STAGE;

    QVariantMap mapParameters;
    mapParameters["num_groups"] = _groups.size();
    mapParameters["num_teams"] = teamsCount();
    map["parameters"] = mapParameters;

    QVariantList  list;
    for(auto const group : _groups){
        QVariant groupData = group->toVariant();
        QVariantMap map = groupData.toMap();
        list.push_back(map);
    }
    map["sub_stages"] = list;
    return QVariant(map);
}

void CGroupsStage::changeTeam(const QVariant &identificator, CTeam *team)
{
    if(isItForMe(identificator)){
        AStageModel * group = findGroup(identificator);
        if(group){
            group->changeTeam(identificator, team);
        }
    }
}

QList<AStageModel *> CGroupsStage::getSubStages() const
{
    return _groups;
}

CTeamsList *CGroupsStage::countStandings()
{
    QList<CTeamsList*> groupsStandings;
    for(auto group : _groups){
        groupsStandings.push_back(group->countStandings());
    }
    QList<QPair<QList<int>,int>> sortableList;
    QList<QPair<CTeam*,QVariantMap>> rowsList;
    int sourceIndex = 0;
    for (const auto groupStanding : groupsStandings) {
        for (int row = 0; row < groupStanding->getTeams().size(); row++) {
            auto rowData = groupStanding->getRow(row);
            QVariantMap rowMap = rowData.second;
            rowMap["source_index"] = sourceIndex;
            rowMap["row"] = row+1;
            rowsList.push_back(QPair<CTeam*,QVariantMap>(rowData.first, rowMap));
            QList<int> sortable;
            sortable.push_back(row);
            sortable.push_back(-1*rowMap["points"].toInt());
            sortable.push_back(-1*(rowMap["my_goals"].toInt() - rowMap["others_goals"].toInt()));
            sortableList.push_back(QPair<QList<int>,int>(sortable, sortableList.size()));           
        }
        sourceIndex++;
    }
    std::sort(sortableList.begin(), sortableList.end());
    CTeamsList * teamsTable = new CTeamsList();
    QStringList columnHeaders = groupsStandings.first()->getColumnHeaders();
    columnHeaders.push_back("source_index");
    columnHeaders.push_back("row");
    teamsTable->setColumnHeaders(columnHeaders);
    for (const auto & row : sortableList) {
        teamsTable->addRow(rowsList[row.second]);
    }
    qDeleteAll(groupsStandings);
    groupsStandings.clear();
    return teamsTable;
}

CTeam *CGroupsStage::getTeam(const QVariant &identificator) const
{
    if(isItForMe(identificator)){
        AStageModel * group = findGroup(identificator);
        if(group){
            return group->getTeam(identificator);
        }
    }
    return nullptr;
}

bool CGroupsStage::setResult(const QVariant& identificator, const QString& result)
{
    if(isItForMe(identificator)){
        AStageModel * group = findGroup(identificator);
        if(group){
            return group->setResult(identificator, result);
        }
    }
    return false;
}

bool CGroupsStage::execEdit(ExecType type, const QVariant &identificator, const QVariant &data)
{
    if(isItForMe(identificator)){
        switch (type) {
        case ExecType::Swap:
            return swapTeams(data.toMap());
        default:
            return sendExecToGroup(type, identificator, data);
        }
    }
    return false;
}

void CGroupsStage::dataChange()
{
    nextStageCare();
    emit dataChanged();
}

void CGroupsStage::coordinateMatches()
{
    _matches.clear();
    int maxTeams = teamsCount()/_groups.size()+1;
    int maxRoundSize = maxTeams/2;
    for(int round = 0; round < maxTeams; round++){
        for (int matchIndex = 0; matchIndex < maxRoundSize ;matchIndex++) {
            int groupIndex = 0;
            for(const auto group : _groups){
                int roundSize = group->teamsCount()/2;
                if(matchIndex<roundSize){
                    if(group->matchesCount() > matchIndex+round*roundSize){
                        CMatch* match = group->getMatches()[matchIndex+round*roundSize];
                        modifieMatch(match, groupIndex);
                        _matches.push_back(match);
                    }
                }
                groupIndex++;
            }
        }
    }
}

void CGroupsStage::modifieMatch(CMatch *match, int groupIndex)
{
    if(match && !match->getHost()){
        QList<int> indexes = match->getTeamsIndexes();
        match->setIndexes(_teamsGroups[groupIndex][indexes[0]],_teamsGroups[groupIndex][indexes[1]]);
    }
}

bool CGroupsStage::sendExecToGroup(ExecType type, const QVariant &identificator, const QVariant &data)
{
    if(identificator.toMap().contains("group_index")){
        int groupIndex = identificator.toMap()["group_index"].toInt();
        if(groupIndex<_groups.size()){
            return  _groups[groupIndex]->execEdit(type, identificator, data);
        }
    }
    return false;
}

bool CGroupsStage::swapTeams(const QVariantMap &data)
{
    if(data.contains("new") && data.contains("old")){
        QVariantMap newData = data.value("new").toMap();
        QVariantMap oldData = data.value("old").toMap();
        if(oldData.contains("group_index") && newData.contains("group_index")){
            int oldIndex = oldData["group_index"].toInt();
            int newIndex = newData["group_index"].toInt();
            if(oldIndex<_groups.size() && newIndex<_groups.size()){
                CTeam * newTeam = _groups[newIndex]->getTeam(newData);
                CTeam * oldTeam = _groups[oldIndex]->getTeam(oldData);
                _groups[newIndex]->changeTeam(newData,oldTeam);
                _groups[oldIndex]->changeTeam(oldData,newTeam);
                return true;
            }
        }
    }
    return false;
}

QString CGroupsStage::generateGroupName(int index) const
{
    return getName() + " - " + letterFromIndex(index);
}

QString CGroupsStage::letterFromIndex(int index) const
{
    if(index >= 0 && index < 26){
        return QString("ABCDEFGHIJKLMNOPQRSTUVWXYZ"[index]);
    }
    else{
        return letterFromIndex(index - 26) + letterFromIndex(index - 26);
    }
}

AStageModel *CGroupsStage::findGroup(const QVariant &identificator) const
{
    if(identificator.toMap().contains("group_index")){
        int groupIndex = identificator.toMap()["group_index"].toInt();
        if(groupIndex<_groups.size()){
            return _groups[groupIndex];
        }
    }
    return nullptr;
}

void CGroupsStage::nextStageCare()
{
    if (_nextStage){
        CTeamsList* myResults = countStandings();
        QStringList columnHeaders = myResults->getColumnHeaders();
        columnHeaders.push_back("place_holder");
        QList<CTeamsList*> groupsResults;
        for (int i = 0; i<_groups.size(); i++) {
            groupsResults.push_back(new CTeamsList);
            groupsResults.last()->setColumnHeaders(columnHeaders);
        }
        int totalProceedingTeams = _nextStage->teamsCount();
        for(int i = 0; i < totalProceedingTeams; i++){
            auto row = myResults->getRow(i);
            QVariantMap rowMap = row.second;
            rowMap["place_holder"] = rowMap["source"].toString() + rowMap["row"].toString();
            if(rowMap["order_is_final"].toBool()){
                groupsResults[rowMap["source_index"].toInt()]->addRow(row.first, rowMap);
            }
            else{
                groupsResults[rowMap["source_index"].toInt()]->addRow(nullptr, rowMap);
            }
        }
        _nextStage->setTeamsFromSources(groupsResults);
        delete myResults;
    }
}


