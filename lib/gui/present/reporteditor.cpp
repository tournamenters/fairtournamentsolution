#include "reporteditor.h"

#include <QtWidgets>
#include <QtPrintSupport/qtprintsupportglobal.h>
#include <QPrintDialog>
#include <QPrinter>
#include <QPrintPreviewDialog>
#include <QPrintPreviewWidget>

#include "gexportdialog.h"
#include "aexportable.h"

const QString iconsPath = ":/icons";

ReportEditor::ReportEditor(QWidget *parent)
    : QMainWindow(parent)
{
    _picturesCount = 0;
    _reportName = "";
    createCentralWidget();
    createPrinter();
    createPreview();

    makeFileMenu();
    makeEditMenu();
    makeFormatMenu();
    makePreviewMenu();
    makeMainToolBar();

    setWindowTitle("Report Editor | " + QCoreApplication::applicationName());
    showMaximized();

    teReport->setFocus();
    alignmentChanged(teReport->alignment());
    changeOrientation(actPortrait);
    printPreviewWidget->updatePreview();
}

ReportEditor::~ReportEditor()
{
    if(printer){
        delete printer;
    }
}

void ReportEditor::addExportable(AExportable *exportable)
{
    _exportables.push_back(exportable);
}

void ReportEditor::setExportables(QList<AExportable *> exportables)
{
    clearExportables();
    _exportables = exportables;
}

void ReportEditor::clearExportables()
{
    _exportables.clear();
}

void ReportEditor::closeEvent(QCloseEvent *event)
{
    if (maybeExport()){
        event->accept();
    }
    else{
        event->ignore();
    }
}

void ReportEditor::newReportSlot()
{
    newReport();
}

void ReportEditor::makeFileMenu()
{
    QMenu *menu = menuBar()->addMenu(tr("&File"));
    actNew = menu->addAction(QIcon(iconsPath + "/notes.png"),  tr("&New"),
                             this, &ReportEditor::newReportSlot, QKeySequence::New);
    actExport = menu->addAction(QIcon(iconsPath + "/pdf.png"), tr("&Export"),
                                this, &ReportEditor::exportReport, QKeySequence::Save);
    actExport->setEnabled(false);
    connect(teReport->document(), SIGNAL(modificationChanged(bool)),
            actExport, SLOT(setEnabled(bool)));
    actExportAs = menu->addAction(QIcon(iconsPath + "/pdf.png"), tr("Export &As..."),
                                  this, &ReportEditor::exportReportAs, QKeySequence::SaveAs);
    actExportAs->setPriority(QAction::LowPriority);
    menu->addSeparator();
    actPrint = menu->addAction(QIcon(iconsPath + "/print.png"), tr("&Print..."),
                               this, &ReportEditor::printReport, QKeySequence::Print);
    actPrint->setPriority(QAction::LowPriority);
    actExit = menu->addAction(QIcon(iconsPath + "/exit.ico"), tr("&Exit"),
                              this, &QWidget::close, QKeySequence::Quit);
}

void ReportEditor::makeEditMenu()
{
    QMenu *menu = menuBar()->addMenu(tr("&Edit"));
    makeUndoActions(menu);
    menu->addSeparator();
    makeClipboardActions(menu);
    menu->addSeparator();
    makeInsertActions(menu);
}

void ReportEditor::makeFormatMenu()
{
    QMenu *menu = menuBar()->addMenu(tr("&Format"));
    actFont = menu->addAction(QIcon(iconsPath + "/font.png"), tr("Font"),
                              this, &ReportEditor::changeFont);
    menu->addSeparator();
    makeAlignActons(menu);
    menu->addSeparator();
    makeOrientationActions(menu);
}

void ReportEditor::makePreviewMenu()
{
    QMenu *view = menuBar()->addMenu(tr("&Preview"));
    view->addAction(dwPreview->toggleViewAction());
    actRefreshPreview = view->addAction(QIcon(iconsPath + "/refresh.ico"),
                                        tr("Refresh Preview"), printPreviewWidget,
                                        &QPrintPreviewWidget::updatePreview);
    actRefreshPreview->setEnabled(teReport->document()->isModified());
}

void ReportEditor::makeAlignActons(QMenu* menu)
{
    alignGroup = new QActionGroup(this);
    connect(alignGroup, &QActionGroup::triggered,
            this, &ReportEditor::changeTextAlign);
    actAlignLeft = alignGroup->addAction(QIcon(iconsPath + "/left.png"),
                                         tr("&Left"));
    setupAlignAction(actAlignLeft, Qt::CTRL + Qt::Key_L);
    actAlignCenter = alignGroup->addAction(QIcon(iconsPath + "/center.png"),
                                           tr("C&enter"));
    setupAlignAction(actAlignCenter, Qt::CTRL + Qt::Key_E);
    actAlignRight = alignGroup->addAction(QIcon(iconsPath + "/right.png"),
                                          tr("&Right"));
    setupAlignAction(actAlignRight, Qt::CTRL + Qt::Key_R);
    actAlignJustify = alignGroup->addAction(QIcon(iconsPath + "/justify.png"),
                                            tr("&Justify"));
    setupAlignAction(actAlignJustify, Qt::CTRL + Qt::Key_J);
    menu->addActions(alignGroup->actions());
}

void ReportEditor::makeClipboardActions(QMenu *menu)
{
    actCut = menu->addAction(QIcon(iconsPath + "/cut.ico"), tr("Cu&t"),
                             teReport, &QTextEdit::cut, QKeySequence::Cut);
    actCut->setPriority(QAction::LowPriority);
    actCopy = menu->addAction(QIcon(iconsPath + "/copy.ico"), tr("&Copy"),
                              teReport, &QTextEdit::copy, QKeySequence::Copy);
    actCopy->setPriority(QAction::LowPriority);
    actPaste = menu->addAction(QIcon(iconsPath + "/paste.ico"), tr("&Paste"),
                               teReport, &QTextEdit::paste, QKeySequence::Paste);
    actPaste->setPriority(QAction::LowPriority);
    if (const QMimeData *mimeData = QApplication::clipboard()->mimeData()){
        actPaste->setEnabled(mimeData->hasText());
    }
    menu->addSeparator();
}

void ReportEditor::makeUndoActions(QMenu *menu)
{
    const QIcon undoIcon = QIcon(iconsPath + "/undo.png");
    actUndo = menu->addAction(undoIcon, tr("&Undo"),
                              teReport, &QTextEdit::undo, QKeySequence::Undo);
    connect(teReport->document(), SIGNAL(undoAvailable(bool)),
            actUndo, SLOT(setEnabled(bool)));
    actRedo = menu->addAction(QIcon(iconsPath + "/redo.png"), tr("&Redo"),
                              teReport, &QTextEdit::redo, QKeySequence::Redo);
    connect(teReport->document(), SIGNAL(redoAvailable(bool)),
            actRedo, SLOT(setEnabled(bool)));
}

void ReportEditor::makeInsertActions(QMenu *menu)
{
    actInsertTable = menu->addAction(QIcon(iconsPath + "/table.png"), tr("Table..."),
                                     this, &ReportEditor::insertNewTable);
    actInsertTable->setPriority(QAction::LowPriority);
    actInsertTournamentData = menu->addAction(QIcon(iconsPath + "/add.png"), tr("Insert"),
                                              this, &ReportEditor::insertTournamentData);
}

void ReportEditor::makeOrientationActions(QMenu *menu)
{
    orientationGroup = new QActionGroup(this);
    connect(orientationGroup, &QActionGroup::triggered,
            this, &ReportEditor::changeOrientation);
    actPortrait = orientationGroup->addAction(QIcon(iconsPath + "/notes.png"),
                                              tr("&Portrait"));
    connect(actPortrait,SIGNAL(triggered()),printPreviewWidget,
            SLOT(setPortraitOrientation()));
    actPortrait->setCheckable(true);
    actPortrait->setChecked(true);
    actPortrait->setPriority(QAction::LowPriority);
    actLandscape = orientationGroup->addAction(rotateIcon(iconsPath + "/notes.png"),
                                               tr("&Landscape"));
    connect(actLandscape,SIGNAL(triggered()),printPreviewWidget,
            SLOT(setLandscapeOrientation()));
    actLandscape->setCheckable(true);
    actLandscape->setPriority(QAction::LowPriority);
    menu->addActions(orientationGroup->actions());
}

void ReportEditor::makeMainToolBar()
{
    setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    tbMain = addToolBar(tr("Main Toolbar"));
    tbMain->setMovable(false);

    tbMain->addAction(actNew);
    tbMain->addAction(actExport);
    tbMain->addAction(actPrint);
    tbMain->addSeparator();

    tbMain->addAction(actCut);
    tbMain->addAction(actCopy);
    tbMain->addAction(actPaste);
    tbMain->addSeparator();

    tbMain->addAction(actUndo);
    tbMain->addAction(actRedo);
    tbMain->addSeparator();

    tbMain->addAction(actInsertTournamentData);
    tbMain->addAction(actInsertTable);
    tbMain->addSeparator();

    tbMain->addAction(actFont);
    tbMain->addSeparator();

    tbMain->addActions(alignGroup->actions());
    tbMain->addSeparator();

    tbMain->addActions(orientationGroup->actions());
    tbMain->addSeparator();

    tbMain->addAction(actRefreshPreview);
}

void ReportEditor::setupAlignAction(QAction *action, const QKeySequence& sequence)
{
    action->setShortcut(sequence);
    action->setCheckable(true);
    action->setPriority(QAction::LowPriority);
}

void ReportEditor::documentChanged()
{
    actRefreshPreview->setEnabled(true);
}

void ReportEditor::previewChanged()
{
    if(printPreviewWidget->orientation() == QPrinter::Portrait && !actPortrait->isChecked()){
        actPortrait->setChecked(true);
        changeOrientation(actPortrait);
    }
    else if(printPreviewWidget->orientation() == QPrinter::Landscape && !actLandscape->isChecked()){
        actLandscape->setChecked(true);
        changeOrientation(actLandscape);
    }
}

void ReportEditor::newReport(const QString& title)
{
    if (maybeExport()) {
        teReport->clear();
        insertTitle(title);
        if(!_exportables.isEmpty()){
            GExportDialog dialog(_exportables, this);
            if(dialog.exec()==QDialog::Accepted){
                insertChosenExportables(dialog);
            }
        }
        actRefreshPreview->trigger();
    }
}

bool ReportEditor::maybeExport()
{
    if (teReport->document()->isModified()){
        const QMessageBox::StandardButton maybeDialog =
            QMessageBox::warning(this, QCoreApplication::applicationName(),
                                 tr("The report has been modified.\n"
                                    "Do you want to export your changes?"),
                                 QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        if (maybeDialog == QMessageBox::Save){
            return exportReport();
        }
        else if (maybeDialog == QMessageBox::Cancel){
            return false;
        }
    }
    return true;
}

void ReportEditor::createCentralWidget()
{
    teReport = new QTextEditWithMargins();
    connect(teReport, SIGNAL(cursorPositionChanged()),
            this, SLOT(cursorPositionChanged()));

    QFont textFont("Helvetica");
    textFont.setStyleHint(QFont::Helvetica);
    teReport->setFont(textFont);

    QTextDocument * textDocument = teReport->document();
    textDocument->setDocumentMargin(50);
    textDocument->setModified(false);
    connect(textDocument,SIGNAL(contentsChanged()),
            this, SLOT(documentChanged()));

    QScrollArea* area = new QScrollArea;
    area->setWidget(teReport);
    area->setAlignment(Qt::AlignCenter);
    area->setWidgetResizable(true);
    setCentralWidget(area);
}

void ReportEditor::createPrinter()
{
    printer = new QPrinter(QPrinter::HighResolution);
    printer->setFullPage(true);
    printer->setPaperSize(QPrinter::A4);
    printer->setPageSize(QPrinter::A4);
    printer->setPageMargins(15,15,15,15,QPrinter::Millimeter);
}

void ReportEditor::createPreview()
{
    printPreviewWindow = new QPrintPreview(printer);
    connect(printPreviewWindow,SIGNAL(paintRequested(QPrinter*)),
            this, SLOT(renderReportForPreview(QPrinter*)));

    dwPreview = new QDockWidget(tr("Preview"),this);
    dwPreview->setWidget(printPreviewWindow);
    dwPreview->setAllowedAreas(Qt::RightDockWidgetArea | Qt::LeftDockWidgetArea);
    addDockWidget(Qt::RightDockWidgetArea, dwPreview);

    printPreviewWidget = printPreviewWindow->findChildren<QPrintPreviewWidget*>().first();
    connect(printPreviewWidget, SIGNAL(previewChanged()),
            this, SLOT(previewChanged()));
}

bool ReportEditor::exportReport()
{
    if (_reportName.isEmpty()){
        return exportReportAs();
    }
    printer->setOutputFormat(QPrinter::PdfFormat);
    printer->setOutputFileName(_reportName);
    teReport->document()->print(printer);
    teReport->document()->setModified(false);
    statusBar()->showMessage(tr("Exported to ") + QDir::toNativeSeparators(_reportName));
    QDesktopServices::openUrl(QUrl(_reportName));
    return true;
}

bool ReportEditor::exportReportAs()
{
    QFileDialog fileDialog(this, tr("Export PDF"));
    fileDialog.setAcceptMode(QFileDialog::AcceptSave);
    fileDialog.setMimeTypeFilters(QStringList("application/pdf"));
    fileDialog.setDefaultSuffix("pdf");
    if (fileDialog.exec() == QDialog::Accepted){
        _reportName = fileDialog.selectedFiles().first();
        return exportReport();
    }
    return false;
}

void ReportEditor::printReport()
{
    printer->setOutputFormat(QPrinter::NativeFormat);
    QPrintDialog printDialog(printer, this);
    if (printDialog.exec() == QDialog::Accepted) {
        teReport->document()->print(printer);
        teReport->document()->setModified(false);
        statusBar()->showMessage(tr("Print request was send."));
    }
}

void ReportEditor::renderReportForPreview(QPrinter *printer)
{
    teReport->document()->print(printer);
    if(actRefreshPreview){
        actRefreshPreview->setEnabled(false);
    }
}

void ReportEditor::changeFont()
{
    bool chosen = false;
    QFont font = QFontDialog::getFont(&chosen, teReport->currentFont(), this);
    if (chosen) {
        QTextCharFormat format;
        format.setFont(font);
        QTextCursor cursor = teReport->textCursor();
        cursor.mergeCharFormat(format);
        teReport->mergeCurrentCharFormat(format);
    }
}

void ReportEditor::insertNewTable()
{
    QDialog* dialog = new QDialog(this);
    dialog->setWindowTitle(tr("Insert Table Dialog"));
    QVBoxLayout * layout = new QVBoxLayout();
    QLabel* label = new QLabel(tr("Please select tournaments elements that you wish to export."));
    layout->addWidget(label);
    layout->addStretch(1);
    QGroupBox* parameters = new QGroupBox(tr("Table Parameters"));
    QFormLayout * parametersLayout = new QFormLayout();
    parameters->setLayout(parametersLayout);
    QSpinBox * spColumns = new QSpinBox(this);
    spColumns->setMinimum(1);
    spColumns->setValue(2);
    spColumns->setMaximum(30);
    QSpinBox * spRows = new QSpinBox(this);
    spRows->setMinimum(1);
    spRows->setValue(1);
    parametersLayout->addRow(tr("Rows:"), spRows);
    parametersLayout->addRow(tr("Columns:"), spColumns);
    layout->addWidget(parameters);
    layout->addStretch(2);
    QDialogButtonBox * buttonBox = new QDialogButtonBox( {QDialogButtonBox::Ok, QDialogButtonBox::Cancel});
    connect(buttonBox, SIGNAL(accepted()),
            dialog, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()),
            dialog, SLOT(reject()));
    layout->addWidget(buttonBox);
    dialog->setLayout(layout);
    if(dialog->exec() == QDialog::Accepted){
        QTextCursor cursor = teReport->textCursor();
        QTextTableFormat format;
        format.setCellSpacing(0);
        format.setCellPadding(15);
        format.setAlignment(Qt::AlignCenter);
        cursor.insertTable(spRows->value(),spColumns->value(),format);
    }
    delete dialog;
}

void ReportEditor::insertTournamentData()
{
    if(_exportables.isEmpty()){
        QMessageBox::warning(this,tr("Nothing to insert"),
                                 tr("Nothing was found that could be inserted."));
        return;
    }
    GExportDialog dialog(_exportables, this, GExportDialog::None);
    if(dialog.exec()==QDialog::Accepted){
        insertChosenExportables(dialog);
        actRefreshPreview->trigger();
    }
}

void ReportEditor::insertChosenExportables(GExportDialog &dialog, bool doPageBreaks)
{
    bool firstPrint = true;
    for(const auto exportable : _exportables){
        if(dialog.isExportableChosen(exportable)){
            if(exportable->hasMoreParts()){
                for(const auto part : exportable->getIndividualParts()){
                    if(dialog.isExportableChosen(part)){
                        if(doPageBreaks && !firstPrint){
                            insertPageBreak();
                            firstPrint = false;
                        }
                        insertExportable(part);
                    }
                }
            }
            else{
                if(doPageBreaks && !firstPrint){
                    insertPageBreak();
                    firstPrint = false;
                }
                insertExportable(exportable);
            }
        }
    }
}

void ReportEditor::insertPageBreak()
{
    QTextBlockFormat pageBreak;
    pageBreak.setPageBreakPolicy(QTextBlockFormat::PageBreak_AlwaysBefore);
    QTextCursor cursor = teReport->textCursor();
    cursor.insertBlock(pageBreak);
}

void ReportEditor::insertTitle(const QString& title)
{
    QTextBlockFormat center;
    center.setAlignment(Qt::AlignHCenter);
    center.setLeftMargin(10);
    center.setRightMargin(10);
    center.setTopMargin(10);
    center.setBottomMargin(10);
     if(!title.isEmpty()){
        QTextCursor cursor = teReport->textCursor();
        cursor.insertHtml("<h1>"+title+"</h1>");
        cursor.movePosition(QTextCursor::Left);
        cursor.setBlockFormat(center);
        cursor.movePosition(QTextCursor::End);
        cursor.insertHtml("<hr><br>");
     }
}

void ReportEditor::insertSmallTitle(const QString &title)
{
    if(!title.isEmpty()){
        QTextCursor cursor = teReport->textCursor();
        cursor.insertHtml("<h3>"+ title + "</h3><hr><br>");
    }
}

void ReportEditor::insertExportable(AExportable *widget)
{
    insertSmallTitle(widget->getTitle());
    if(widget->hasMoreParts()){
        for(const auto part : widget->getIndividualParts()){
            insertExportable(part);
        }
        return;
    }
    switch (widget->getRecomendedFormat()) {
    case AExportable::HTML:
        insertHtml(widget->toHtml());
        return;
    case AExportable::TXT:
        teReport->insertPlainText(widget->toPlainText());
        return;
    default:
        insetImage(widget->toPixmap().toImage(), generatePictureName());
        return;
    }
}

void ReportEditor::insetImage(const QImage& image, const QString& fileName)
{
    QTextDocument* document = teReport->document();
    QTextCursor cursor = teReport->textCursor();
    document->addResource(QTextDocument::ImageResource, fileName +".png", QVariant(image));
    QTextImageFormat imageFormat;
    imageFormat.setWidth(image.width());
    imageFormat.setHeight(image.height());
    imageFormat.setName(fileName + ".png");
    cursor.insertImage(imageFormat);
    cursor.insertHtml("<br>");
}

void ReportEditor::insertHtml(const QString &html)
{
    QTextCursor cursor = teReport->textCursor();
    cursor.insertHtml("<span>");
    cursor.insertHtml(html);
    cursor.insertHtml("</span>");
    cursor.insertHtml("<br>");
}

QIcon ReportEditor::rotateIcon(const QString& iconPath)
{
    QTransform transformator;
    transformator.rotate(90);
    QIcon icon = QIcon(iconPath);
    QPixmap pixmap;
    pixmap = icon.pixmap(iconSize()).transformed(transformator);
    icon = QIcon(pixmap);
    return icon;
}

QString ReportEditor::generatePictureName()
{
    QString name = _reportName + "number" + QString::number(_picturesCount);
    _picturesCount++;
    return name;
}

void ReportEditor::changeOrientation(QAction *action)
{
    QPrinter printer;
    printer.setPaperSize(QPrinter::A4);
    if(action == actLandscape){
        printer.setOrientation(QPrinter::Landscape);
    }
    else if(action == actPortrait){
        printer.setOrientation(QPrinter::Portrait);
    }
    else{
        return;
    }
    QSizeF sizeP = printer.paperSize(QPrinter::DevicePixel);
    qreal leftMargin = 50 * devicePixelRatioF();
    qreal topMargin = 20 * devicePixelRatioF();
    teReport->setMargins(leftMargin, topMargin);
    teReport->setFixedWidth(qRound(sizeP.toSize().width()+teReport->leftMargin()*2));
}

void ReportEditor::cursorPositionChanged()
{
    alignmentChanged(teReport->alignment());
}

void ReportEditor::clipboardDataChanged()
{
    const QMimeData * mimeData = QApplication::clipboard()->mimeData();
    if (mimeData){
        actPaste->setEnabled(mimeData->hasText());
    }
}

void ReportEditor::alignmentChanged(Qt::Alignment alignment)
{
    switch (alignment) {
    case Qt::AlignLeft:
        actAlignLeft->setChecked(true);
        return;
    case Qt::AlignHCenter:
        actAlignCenter->setChecked(true);
        return;
    case Qt::AlignRight:
        actAlignRight->setChecked(true);
        return;
    case Qt::AlignJustify:
        actAlignJustify->setChecked(true);
        return;
    }
}

void ReportEditor::changeTextAlign(QAction *action)
{
    if (action == actAlignCenter){
        teReport->setAlignment(Qt::AlignHCenter);
    }
    else if (action == actAlignLeft){
        teReport->setAlignment(Qt::AlignLeft);
    }
    else if (action == actAlignRight){
        teReport->setAlignment(Qt::AlignRight);
    }
    else if (action == actAlignJustify){
        teReport->setAlignment(Qt::AlignJustify);
    }
}
