#include "gtabstageseditor.h"
#include "global_creation.h"
#include "gstageeditortabs.h"
#include <QMimeData>
#include <QDropEvent>
#include <QDragEnterEvent>
#include <QLabel>
#include <QBoxLayout>
#include <QTabBar>
#include <QToolButton>
#include <QMessageBox>
#include "global_model.h"
#include "gpointssettingbox.h"
#include "gstagetabsfactory.h"

GTabStagesEditor::GTabStagesEditor(QWidget *parent)
    : QTabWidget (parent),
    _numTeams(0)
{    
    setMovable(true);
    setTabsClosable(true);

    setLayout(createPlaceHolderLayout());
    setCornerWidget(createClearAllButton());

    connect(this,SIGNAL(tabCloseRequested(int)),
            this,SLOT(closeTab(int)));
    connect(tabBar(),SIGNAL(tabMoved(int,int)),
            this,SLOT(pagesOrderActualization()));
}

GTabStagesEditor::~GTabStagesEditor()
{
    closeAll();
    delete laPlaceHolder;
}

void GTabStagesEditor::setNumTeams(int numTeams)
{
    _numTeams = numTeams;
    numTeamsRangesActualization();
}

int GTabStagesEditor::getStagesNumTeams(int index)
{
    if(index<tabsStages.size() && index >=0){
        return tabsStages[index]->numTeams();
    }
    return 0;
}

QList<QVariant> GTabStagesEditor::getStagesData() const
{
    QList<QVariant> stagesData;
    for(const auto tab: tabsStages){
        stagesData.push_back(tab->getData());
    }
    return stagesData;
}

void GTabStagesEditor::initialize(const QString &preparedFormat)
{
    closeAll();
    QMap<QString,QList<QString>> preparedFormatsStages = CreationConstants::getInstance().getPreparedFormatsStructure();
    if(preparedFormatsStages.contains(preparedFormat)){
        for(const auto& key : preparedFormatsStages[preparedFormat]){
            addNewTab(count(), key);
        }
    }
    setCurrentIndex(0);
}

void GTabStagesEditor::dropEvent(QDropEvent *event)
{    
    int index = currentIndex() + 1;
    if(tabsStages.isEmpty()){
        index = 0;
    }
    QMap<QString,QString> keyMap = CreationConstants::getInstance().getFormatsMap();
    addNewTab(index, keyMap.key(event->mimeData()->text()));
    event->acceptProposedAction();
}

void GTabStagesEditor::dragEnterEvent(QDragEnterEvent *event)
{
    dragEventCare(event);
}

void GTabStagesEditor::dragMoveEvent(QDragMoveEvent *event)
{
    dragEventCare(event);
}

void GTabStagesEditor::closeTab(int index)
{
    AStageEditorTab* tab = tabsStages[index];
    tabsStages.removeAt(index);
    tab->deleteLater();
    _stagesData.removeAt(index);
    removeTab(index);
    laPlaceHolder->setVisible(!count());
    pagesOrderActualization();

}

void GTabStagesEditor::addNewTab(int index, const QString &type)
{
    QMap<QString,QString> stagesMap = CreationConstants::getInstance().getFormatsMap();
    if(stagesMap.contains(type)){
        QString name = createStageName(type);
        AStageEditorTab* stageTab = GStageTabsFactory::getInstance()->create(type);
        stageTab->setName(name);
        if(index != 0){
            stageTab->setNumTeams(tabsStages[index-1]->numTeams()/2);
        }
        connect(stageTab, SIGNAL(numTeamsChanged()),
                this, SLOT(numTeamsRangesActualization()));
        connect(stageTab, SIGNAL(nameChanged(int, QString)),
                this, SLOT(nameChange(int, QString)));
        connect(stageTab, SIGNAL(dataChanged(int, QVariant)),
                this, SLOT(stageDataChange(int, QVariant)));
        QIcon icon = QIcon(":/icons/" + type + ".png");
        insertTab(index, stageTab, icon, name + " > ");
        _stagesData.insert(index, stageTab->getData());
        pagesOrderActualization();
        setCurrentIndex(index);
        emit currentChanged(index);
        laPlaceHolder->setVisible(false);
    }
}

void GTabStagesEditor::closeAllTabsRequest()
{
    if(QMessageBox::Yes == QMessageBox::question(this,tr("Are you sure?"),
                                                  tr("Do you really want to clear the tabs?"
                                                     "All entererd data will by lost."))){
        closeAll();
    }
}

void GTabStagesEditor::closeAll()
{
    clear();
    qDeleteAll(tabsStages);
    tabsStages.clear();
    _stagesData.clear();
    laPlaceHolder->setVisible(true);
}

void GTabStagesEditor::pagesOrderActualization()
{
    tabsStages.clear();
    for(int index = 0; index<count();index++){
        AStageEditorTab * tab = static_cast<AStageEditorTab *>(widget(index));
        if(tab){
            tabsStages.push_back(tab);
            tab->setIndex(index);
        }
    }
    numTeamsRangesActualization();
}

void GTabStagesEditor::numTeamsRangesActualization()
{
    if(!tabsStages.isEmpty()){
        tabsStages.first()->setNumTeamsRanges(_numTeams, _numTeams);
        for(int i = 1; i<tabsStages.size();i++){
            tabsStages[i]->setNumTeamsRanges(2, tabsStages[i-1]->numTeams());
        }
    }
    emit dataChanged();
}

void GTabStagesEditor::nameChange(int index, const QString& name)
{
    tabBar()->setTabText(index, createStageTabBarName(name));
    _stagesData.removeAt(index);

    emit dataChanged();
}

void GTabStagesEditor::stageDataChange(int index, const QVariant &data)
{
    _stagesData.removeAt(index);
    _stagesData.insert(index, data);
    emit dataChanged();
}

QToolButton *GTabStagesEditor::createClearAllButton()
{
    QToolButton * clearAll = new QToolButton(this);
    clearAll->setIcon(QIcon(":/icons/end.png"));
    clearAll->setAutoRaise(true);
    connect(clearAll,SIGNAL(clicked()),
            this, SLOT(closeAllTabsRequest()));
    return clearAll;
}

QLayout *GTabStagesEditor::createPlaceHolderLayout()
{
    laPlaceHolder = new QLabel(tr("[Drop stages here.]"), this);
    laPlaceHolder->setEnabled(false);
    QVBoxLayout * layout = new QVBoxLayout();
    layout->addWidget(laPlaceHolder);
    layout->setAlignment(Qt::AlignCenter);
    return layout;
}

QString GTabStagesEditor::createStageName(const QString &type)
{
    QString name = type;
    QMap<QString,QString> stagesMap = CreationConstants::getInstance().getFormatsMap();
    if(stagesMap.contains(type)){
        name = stagesMap[type];
    }
    int number = 1;
    while(tabNameExists(createStageTabBarName(name))){
        name = name + " (" + QString::number(number) + ")";
        number++;
    }
    return name;
}

QString GTabStagesEditor::createStageTabBarName(const QString &name)
{
    return name + " > ";
}

bool GTabStagesEditor::tabNameExists(const QString &name)
{
    for(int tab = 0; tab<count(); tab++){
        if(tabBar()->tabText(tab)==name){
            return true;
        }
    }
    return false;
}

bool GTabStagesEditor::isPossibleStageName(const QString &name)
{
    QMap<QString,QString> keyMap = CreationConstants::getInstance().getFormatsMap();
    return keyMap.values().contains(name);
}

void GTabStagesEditor::dragEventCare(QDragMoveEvent *event)
{
    if (event->mimeData()->hasText()
        && isPossibleStageName(event->mimeData()->text())) {
        if (event->source() == this) {
            event->setDropAction(Qt::MoveAction);
            event->accept();
        }
        else {
            event->acceptProposedAction();
        }
    }
    else {
        event->ignore();
    }
}

