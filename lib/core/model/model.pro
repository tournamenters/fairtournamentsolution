#-------------------------------------------------
#
# Project created by QtCreator 2019-04-30T20:49:10
#
#-------------------------------------------------

TARGET = model
TEMPLATE = lib

QT += widgets
QT += core

CONFIG += c++11
CONFIG -= app_bundle
CONFIG += staticlib

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    astagemodel.cpp \
    cfieldsmanager.cpp \
    cmatcheslist.cpp \
    cnotes.cpp \
    crobinround.cpp \
    cstagesfractory.cpp \
    cstats.cpp \
    cteam.cpp \
    cmatch.cpp \
    cresult.cpp \
    cteamstable.cpp \
    cgroupsstage.cpp \
    cplayoff.cpp \
    amodel.cpp \
    cstartlist.cpp \
    serialization.cpp \
    tournament.cpp

HEADERS += \
    cfieldsmanager.h \
    cmatcheslist.h \
    cnotes.h \
    crobinround.h \
    cstagesfactory.h \
    cstats.h \
    cteam.h \
    cmatch.h \
    cresult.h \
    cteamstable.h \
    cgroupsstage.h \
    cplayoff.h \
    astagemodel.h \
    amodel.h \
    cstartlist.h \
    global_model.h \
    serialization.h \
    tournament.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

include(../algorithms/algorithms.pri)

DISTFILES += \
    model.pri

TRANSLATIONS += \
    model_cs.ts
