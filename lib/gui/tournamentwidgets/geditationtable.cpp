#include "geditationtable.h"
#include <QAction>
#include <QMenu>
#include <QCheckBox>
#include <QLineEdit>
#include <QLabel>
#include <QVBoxLayout>
#include <QDialogButtonBox>

GEditationTable::GEditationTable(const QVariant& data, QWidget *parent)
    : GTournamentTable(parent)
{
    _originalData = data;
    QVariantMap map = data.toMap();
    _originalHeaders = map["keys"].toStringList();
    QVariantList rows = map["rows"].toList();

    setRowCount(rows.size());
    setColumnCount(_originalHeaders.size()*2);

    setColumnHeaders(map["keys"].toStringList());

    int row = 0;
    for(const auto& rowsData : rows){
        addRow(row, rowsData);
        row++;
    }
}

QVariant GEditationTable::getNewData() const
{
    QVariantList rows;
    for(int row = 0; row<rowCount(); row++){
        QVariantMap rowData;
        int col = 1;
        for(const auto& key : _originalHeaders){
            rowData[key] = getCellText(row, col);
            col += 2;
        }
        rows.push_back(rowData);
    }
    QVariantMap data;
    data["keys"] = _originalHeaders;
    data["rows"] = rows;
    return data;
}

QVariant GEditationTable::getOriginalData() const
{
    return _originalData;
}

void GEditationTable::setColumnHeaders(const QStringList &originalHeaders)
{
    _originalHeaders = originalHeaders;
    int newCol = 0;
    for(const auto& header : originalHeaders){
        setHorizontalHeaderItem(newCol, new QTableWidgetItem(header));
        newCol++;
        setHorizontalHeaderItem(newCol, new QTableWidgetItem(tr("New ") + header));
        newCol++;
    }
}

void GEditationTable::addRow(int row, const QVariant &data)
{
    QVariantMap rowMap = data.toMap();
    int col = 0;
    for(auto const& key : _originalHeaders){
        addDoubleCell(row, col, rowMap[key].toString());
        col+=2;
    }
}

void GEditationTable::addDoubleCell(int row, int col, const QString &text)
{
    addLabel(row, col, text);
    addLineEdit(row, col+1, text);
}

void GEditationTable::addLabel(int row, int col, const QString &text)
{
    QLabel * label = new QLabel();
    label->setText(text);
    label->setBackgroundRole(QPalette::Window);
    label->setAutoFillBackground(false);
    setCellWidget(row,col,label);
}

void GEditationTable::addLineEdit(int row, int col, const QString &text)
{
    QLineEdit * lineEdidCustom = new QLineEdit();
    lineEdidCustom->setPlaceholderText(text);
    lineEdidCustom->setClearButtonEnabled(true);
    lineEdidCustom->setFrame(QFrame::NoFrame);
    setCellWidget(row, col, lineEdidCustom);
}
