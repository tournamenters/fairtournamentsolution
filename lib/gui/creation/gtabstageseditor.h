#ifndef GTABSTAGESEDITOR_H
#define GTABSTAGESEDITOR_H

#include <QTabWidget>
#include <QScrollArea>
#include <QList>

class QLineEdit;
class QLabel;
class QSpinBox;
class QComboBox;
class QToolButton;
class AStageEditorTab;
class GPointsSettingBox;

class GTabStagesEditor: public QTabWidget
{
    Q_OBJECT

public:
    explicit GTabStagesEditor(QWidget* parent = nullptr);
    ~GTabStagesEditor() override;

    void initialize(const QString& preparedFormat);
    void setNumTeams(int numTeams);
    int getStagesNumTeams(int index);
    QList<QVariant> getStagesData() const;

signals:
    void dataChanged();

protected:
    void dropEvent(QDropEvent *event) override;
    void dragEnterEvent(QDragEnterEvent *event) override;
    void dragMoveEvent(QDragMoveEvent *event) override;

private slots:
    void closeTab(int index);
    void closeAllTabsRequest();

    void numTeamsRangesActualization();
    void pagesOrderActualization();

    void nameChange(int index, const QString& name);
    void stageDataChange(int index, const QVariant& data);

private:
    QToolButton * createClearAllButton();
    QLayout * createPlaceHolderLayout();
    QString createStageName(const QString& type);
    QString createStageTabBarName(const QString& name);

    void addNewTab(int index, const QString& type);
    void closeAll();

    bool tabNameExists(const QString& name);
    bool isPossibleStageName(const QString& name);
    void dragEventCare(QDragMoveEvent *event);

    QLabel * laPlaceHolder;
    QList<AStageEditorTab*> tabsStages;
    QList<QVariant> _stagesData;
    int _numTeams;
};

#endif // GTABSTAGESEDITOR_H
