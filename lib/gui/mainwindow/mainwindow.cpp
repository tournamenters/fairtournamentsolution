#include "mainwindow.h"

#include <QApplication>
#include <QtWidgets>
#include <QFileInfo>
#include <QSizePolicy>
#include <QUndoStack>
#include <QDrag>
#include <QDir>

#include "assistant.h"
#include "astagemodel.h"
#include "gopenpage.h"
#include "gstatsview.h"
#include "gnotesview.h"
#include "gnewdialog.h"
#include "ggroupstageview.h"
#include "gstartlistview.h"
#include "gmatchestableview.h"
#include "gstageviewfactory.h"
#include "gfieldsview.h"
#include "gimagesexporter.h"
#include "gdragabletoolbars.h"
#include "reporteditor.h"
#include "controller.h"
#include "serialization.h"
#include "tournament.h"

MainWindow::MainWindow(Controller *controller, SerializationController *serializer, QWidget *parent)
    : QMainWindow(parent)
{
    Q_INIT_RESOURCE(icons);

    setWindowTitle(QCoreApplication::applicationName());
    setWindowIcon(QIcon(":/icons/icon.ico"));
    setIconSize(iconSize()*4/3);
    setCorner(Qt::Corner::BottomLeftCorner,Qt::LeftDockWidgetArea);

    tbarMain = addToolBar(tr("Main ToolBar"));
    tbarFind = addToolBar(tr("&Find"));
    tbarResults = new GDragableResultsToolBar(tr("Drag and Drop &Results"), this);

    _assistant = new Assistant();
    _undoStack = new QUndoStack(this);
    _serializer = serializer;
    connectSerializer();
    _controller = controller;
    _controller->setUndoStack(_undoStack);

    createDockWidgets();
    createFindEdit();

    makeFileActions();
    makeEditActions();
    makeViewActions();
    makeSettingActions();
    makeExportActions();
    makeAboutActions();

    makeMainToolBar();
    makeFindToolBar();
    makeResultsToolBar();

    readSettings();

    statusBar()->showMessage(tr("Application successfully started!"), 2000);
}


MainWindow::~MainWindow()
{
    _exportableWidgets.clear();
    if(_assistant){
        delete _assistant;
    }
}

void MainWindow::showTournament()
{
    setState(State::Tournament);
    auto model = _controller->getModel();
    showInformation(model->getStatsModel());
    showStartList(model->getStartListModel());
    showMatches(model->getMatchesModel());
    showFields(model->getFieldsModels());
    for(const auto stage : model->getStagesModels()){
        showStage(stage);
    }
    showNotes(model->getNotesModel());
}

void MainWindow::createDockWidgets()
{
    dwTeams = new QDockWidget(tr("&Start List"), this);
    dwMatches = new QDockWidget(tr("&Matches"), this);
    dwFields = new QDockWidget(tr("&Fields"), this);
    dwInformation = new QDockWidget(tr("&Tournament Stats"), this);
    dwNotes = new QDockWidget(tr("Notes"), this);
    setImplicitDockWidgetsState();
}

void MainWindow::createFindEdit()
{
    leFind = new QLineEdit(this);
    leFind->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    leFind->setClearButtonEnabled(true);
    actFind = new QAction(" " + tr("Find") + ": ",this);
    setupAction(actFind,":/icons/find.png", tr("Find and highlight participants with a given name."));
    connect(actFind, SIGNAL(triggered()),
            this, SLOT(findText()));
}

void MainWindow::connectSerializer()
{
    connect(_serializer, SIGNAL(saveAsRequested()),
            this, SLOT(saveAsTournament()));
    connect(_serializer, SIGNAL(statusBarChangeRequested(QString)),
            this, SLOT(showStatusBarMessage(QString)));
    connect(_serializer, SIGNAL(modificationChanged(bool)),
            this, SLOT(setWindowModified(bool)));
    connect(_serializer, SIGNAL(currentFileChanged(QString)),
            this, SLOT(setCurrentFileName(QString)));
    connect(_serializer, SIGNAL(recentListChanged(QList<QString>)),
            this, SLOT(setRecentTournamentList(QList<QString>)));
}


void MainWindow::makeFileActions()
{
    QMenu *mnuFile = menuBar()->addMenu(tr("&File"));
    actNewTournament = mnuFile->addAction(tr("&New..."),
                                          this, &MainWindow::newTournament);
    setupAction(actNewTournament, ":/icons/new.png",
                tr("Open new tournament creation dialog."), QKeySequence::New, "document-new");
    actOpenTournament = mnuFile->addAction(tr("&Open..."),
                                           this, &MainWindow::openTournament);
    setupAction(actOpenTournament, ":/icons/open.png",
                tr("Open native file dialog and browse for old tournament file."), QKeySequence::Open, "document-open");
    mnuRecentTournaments = new QMenu(tr("Open &Recent"), this);
    mnuRecentTournaments->setIcon(QIcon(":/icons/open.png"));
    mnuRecentTournaments->setEnabled(false);
    mnuFile->addMenu(mnuRecentTournaments);
    actgrOpenRecent = new QActionGroup(this);
    setRecentTournamentList(_serializer->getRecentList());
    mnuFile->addSeparator();
    actSaveTournament = mnuFile->addAction(tr("&Save"),
                                           this, &MainWindow::saveTournament);
    setupAction(actSaveTournament, ":/icons/save.png",
                tr("Save changes of active tournament to its file."), QKeySequence::Save, "document-save");
    actSaveAsTournament = mnuFile->addAction(tr("Save &As..."),
                                             this, &MainWindow::saveAsTournament);
    setupAction(actSaveAsTournament, ":/icons/saveas.png",
                tr("Open native file dialog and chose where to save active tournament."), QKeySequence::SaveAs, "document-save");
    mnuFile->addSeparator();
    actCloseTournament = mnuFile->addAction(tr("&Close"),
                                            this, &MainWindow::closeTournament);
    setupAction(actCloseTournament, ":/icons/close.png",
                tr("Close active tournament."), QKeySequence::Close, "document-close");
    actExit = mnuFile->addAction(tr("&Exit"),
                                 this, &QWidget::close);
    setupAction(actExit, ":/icons/exit.ico",
                tr("Quit Fair Tournament application."), QKeySequence::Quit, "document-exit");
}

void MainWindow::makeEditActions()
{
    QMenu *mnuEdit = menuBar()->addMenu(tr("&Edit"));
    actUndo = _undoStack->createUndoAction(this,tr("&Undo"));
    mnuEdit->addAction(actUndo);
    setupAction(actUndo, ":/icons/undo.png",
                tr("Undo last change."), QKeySequence::Undo, "document-undo");
    actRedo = _undoStack->createRedoAction(this,tr("&Redo"));
    mnuEdit->addAction(actRedo);
    setupAction(actRedo, ":/icons/redo.png",
                tr("Redo last undo."), QKeySequence::Redo, "document-redo");
    mnuEdit->addSeparator();
    QAction * actShowFindToolBar = tbarFind->toggleViewAction();
    mnuEdit->addAction(actShowFindToolBar);
    setupAction(actShowFindToolBar, ":/icons/find.png",
                tr("Show find toolbar."), QKeySequence::Find, "document-find");
    connect(actShowFindToolBar, SIGNAL(toggled(bool)),
            leFind, SLOT(clear()));
    mnuEdit->addSeparator();
    actEditTeams = mnuEdit->addAction(tr("Edit &Start List"));
    setupAction(actEditTeams, ":/icons/edit.png",
                tr("Open a dialog for the start list editing."));
}

void MainWindow::makeViewActions()
{
    QMenu *mnuView = menuBar()->addMenu(tr("&View"));
    mnuView->addAction(dwTeams->toggleViewAction());
    setupAction(dwTeams->toggleViewAction(),"",tr("Show or hide start list window."));
    mnuView->addAction(dwMatches->toggleViewAction());
    setupAction(dwMatches->toggleViewAction(),"",tr("Show or hide matches window."));
    mnuView->addAction(dwFields->toggleViewAction());
    setupAction(dwFields->toggleViewAction(),"",tr("Show or hide fields window."));
    mnuView->addSeparator();
    mnuView->addAction(dwInformation->toggleViewAction());
    setupAction(dwInformation->toggleViewAction(),"",tr("Show or hide tournament stats window."));
    mnuView->addAction(dwNotes->toggleViewAction());
    setupAction(dwNotes->toggleViewAction(),"",tr("Show or hide notes window."));
    mnuView->addSeparator();
    mnuView->addAction(tbarResults->toggleViewAction());
    setupAction(tbarResults->toggleViewAction(),"",tr("Show or hide drag and drop results' toolbar."));
}

void MainWindow::makeSettingActions()
{
    QMenu *mnuSetting  = menuBar()->addMenu(tr("&Setting"));
    actSetFont = mnuSetting->addAction(tr("Font..."),
                                       this, &MainWindow::setFont);
    setupAction(actSetFont, ":/icons/font.png",
                tr("Set the application's font."));
    actSetAppearance = mnuSetting->addAction(tr("Appearance..."),
                                             this, &MainWindow::setAppearance);
    setupAction(actSetAppearance, "",
                tr("Set the appearance of the application."));
    QWidgetAction *actSetLanguage = createSetLanguageAction();
    QMenu *mnuSetLanguage = mnuSetting->addMenu(QIcon(":/icons/language.png"),tr("Language"));
    mnuSetLanguage->addAction(actSetLanguage);
}

void MainWindow::makeExportActions()
{
    QMenu *mnuExport = menuBar()->addMenu(tr("E&xport"));
    actOpenReportEditor = mnuExport->addAction(tr("Open &Report Editor"),
                                               this, &MainWindow::openReportEditor);
    setupAction(actOpenReportEditor, ":/icons/editor.png",
                tr("Open a simple text editor, import tournament data into it and export the pdf."));
    actExportIndividualFiles = mnuExport->addAction(tr("Export Individual &Components..."),
                                                    this, &MainWindow::exportIndividualFiles);
    setupAction(actExportIndividualFiles, ":/icons/export.png",
                tr("Generate separate pictures and html files of the chosen tournament components and save them to the selected folder."));
}

void MainWindow::makeAboutActions()
{
    QMenu *mnuHelp = menuBar()->addMenu(tr("&Help"));
    actAbout = mnuHelp->addAction(tr("&About..."),
                                  this, &MainWindow::about);
    setupAction(actAbout, ":/icons/icon.ico",
                tr("Show the application's About box."));
    QAction * actAboutQt = mnuHelp->addAction(tr("About &Qt..."),
                                             qApp, &QApplication::aboutQt);
    setupAction(actAboutQt, ":/icons/qt.png",
                tr("Show the Qt library's About box."));
    mnuHelp->addSeparator();
//    actNewFeature = mnuHelp->addAction(tr("Request New Feature..."),
//                                       this, &MainWindow::requestNewFeature);
//    setupAction(actNewFeature, "",
//                tr("Open online service desk in native browser."));
//    actReportBug = mnuHelp->addAction(tr("Report Bug..."),
//                                      this, &MainWindow::reportBug);
//    setupAction(actReportBug, "",
//                tr("Open online service desk in native browser."));
//    mnuHelp->addSeparator();
    actGetHelp = mnuHelp->addAction(tr("Get Help..."),
                                    this, &MainWindow::openUserGuide);
    setupAction(actGetHelp, ":/icons/help.png",
                tr("Open the interactive user guide in a new window."), QKeySequence::HelpContents);
}

void MainWindow::makeMainToolBar()
{
    tbarMain->addAction(actNewTournament);
    tbarMain->addAction(actOpenTournament);
    tbarMain->addAction(actSaveTournament);
    tbarMain->addAction(actCloseTournament);
    tbarMain->addSeparator();
    tbarMain->addAction(actUndo);
    tbarMain->addAction(actRedo);
    tbarMain->addSeparator();
    tbarMain->addAction(actOpenReportEditor);
    tbarMain->addAction(actExportIndividualFiles);
    tbarMain->setMovable(false);
    tbarMain->toggleViewAction()->setEnabled(false);
}

void MainWindow::makeFindToolBar()
{
    tbarFind->addAction(actFind);
    tbarFind->addWidget(leFind);
    tbarFind->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    setUseful(tbarFind);
}

void MainWindow::makeResultsToolBar()
{
    tbarResults->setAllowedAreas(Qt::LeftToolBarArea | Qt::RightToolBarArea);
    setUseful(tbarResults);
    connect(tbarResults, SIGNAL(orientationChanged(Qt::Orientation)),
            this, SLOT(dragToolBarOrientation(Qt::Orientation)));
    addToolBar(Qt::RightToolBarArea, tbarResults);
}


void MainWindow::newTournament()
{   
    NewTournament newDialog(this);
    connect(&newDialog, SIGNAL(documentationNeeded(QString)),
            this, SLOT(openUserGuidePage(QString)));
    if(newDialog.exec() == QDialog::Accepted){
       _controller->startTournament(newDialog.getTournamentName());
        QVariant teamsData = newDialog.getTeamsData();
        QVariant teamsParams = newDialog.getStartListSetting();
        _controller->createStartList(teamsData, teamsParams);

        QList<QString> stagesTypes = newDialog.getStagesTypes();
        QVariantList stagesParams = newDialog.getStagesParams();
        QVariantList stagesSetting = newDialog.getStagesSetting();
        _controller->createStages(stagesTypes, stagesParams, stagesSetting);

        if(newDialog.getFieldsSetting().toMap()["fields"].toBool()){
            _controller->sheduleFields(newDialog.getFieldsSetting());
        }
        showTournament();
        _serializer->setModification(true);
    }
}

void MainWindow::openTournament()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Tournament"),
                                                    QDir::toNativeSeparators(_openDialogPath),
                                                    tr("Tournaments (*.turn)"));
    if(!fileName.isEmpty()){
        _controller->finishTournament();
        _serializer->open(fileName);
        _openDialogPath = fileName;
        showTournament();
    }
}

void MainWindow::openRecentTournament(QAction* sender)
{
    if(sender){
        _controller->finishTournament();
        _serializer->open(sender->toolTip());
        showTournament();
    }
}

void MainWindow::clearRecentList()
{
    _serializer->clearRecentList();
}

void MainWindow::saveTournament()
{
    _serializer->save();
}

void MainWindow::saveAsTournament()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save Tournament"),
                                                    QDir::toNativeSeparators(QFileInfo(_saveDialogPath).path()),
                                                    tr("Tournaments (*.turn)"));
    if(!fileName.isEmpty()){
        if(!QFile::exists(fileName)){
            _serializer->saveAs(fileName);
        }
        _saveDialogPath = fileName;
    }
}

void MainWindow::closeTournament()
{
    if (isWindowModified() && !wishToSaveAndClose()){
        return;
    }
    _controller->finishTournament();
    setState(State::Welcome);
}

void MainWindow::openUserGuidePage(const QString &page)
{
    _assistant->showDocumentation(page);
}

void MainWindow::showStatusBarMessage(const QString &message)
{
    statusBar()->showMessage(message, 5000);
}

void MainWindow::findText()
{
    if(leFind){
        QString text = leFind->text();
        leFind->setText(text + " ");
        leFind->setText(text);
    }
}

void MainWindow::openReportEditor()
{
    ReportEditor* edit = new ReportEditor(this);
    edit->setAttribute(Qt::WA_DeleteOnClose, true);
    edit->setExportables(_exportableWidgets);
    edit->show();
    edit->newReport(_tournamentName);
}

void MainWindow::exportIndividualFiles()
{
    GImagesExporter exporter(_exportableWidgets,this);
    exporter.exportFiles();
}


void MainWindow::setFont()
{
    bool ok;
    QFont font = QFontDialog::getFont(&ok, QApplication::font(), this);
    if (ok){
        emit setFontRequest(font);
    }
}

void MainWindow::setLanguage(const QString &language)
{
    emit setLanguageRequest(modifiedLanguageKey(language));
}

void MainWindow::setAppearance()
{
    emit setAppearanceRequest();
}

void MainWindow::about()
{
    QMessageBox::about(this,tr("About Fair Tournament"),
     tr("Fair Tournament is an open source desktop application for scheduling and organising sport events.\n\n")
    +tr("Copyright (C) 2019  Aleš Suchomel\n"
    "This program is free software: you can redistribute it and/or modify "
    "it under the terms of the GNU General Public License as published by "
    "the Free Software Foundation, either version 3 of the License, or "
    "(at your option) any later version. "
    "This program is distributed in the hope that it will be useful, "
    "but WITHOUT ANY WARRANTY; without even the implied warranty of "
    "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the "
    "GNU General Public License for more details.\n\n")
    +tr("The used icons set 'Must Have' has been designed by Visual Pharm <http://www.visualpharm.com>.\n\n")
    +tr("The text editor icons and PDF icon have been made by Freepik, <https://www.freepik.com/>.\n\n"));
}

void MainWindow::requestNewFeature()
{
    if(!QDesktopServices::openUrl(QUrl("https://fairtournament.atlassian.net/servicedesk/customer/portal/2/group/2/create/27"))){
        QMessageBox::information(this,tr("Something went wrong."),tr("Our online service desk was unable to reach."));
    }
}

void MainWindow::openUserGuide()
{
    switch (_state) {
    case State::Welcome:
        _assistant->showDocumentation("uvodni-stranka.html");
        return;
    case State::Tournament:
        _assistant->showDocumentation("editovani-turnaje.html");
        return;
    case State::Empty:
        _assistant->showDocumentation("index.html");
        return;
    }
}

void MainWindow::reportBug()
{
    if(!QDesktopServices::openUrl(QUrl("https://fairtournament.atlassian.net/servicedesk/customer/portal/2/group/2/create/26"))){
        QMessageBox::information(this,tr("Something went wrong."),tr("Our online service desk was unable to reach."));
    }
}

void MainWindow::setState(MainWindow::State newState)
{
    _exportableWidgets.clear();
    qDeleteAll(_views);
    _views.clear();
    switch (newState) {
    case Empty:
        showEmptyPage();
        break;
    case Welcome:
        showWelcomePage();
        break;
    case Tournament:
        showTournamentPage();
        break;
    }
}

void MainWindow::showWelcomePage()
{
    _state = MainWindow::Welcome;
    suspendDockWidgets();
    enableAllActions(false);
    enableWelcomeActions(true);

    gopWelcomePage = new GOpenPage(tr("Welcome!"), tr("We would be glad if you let our program help you organize your event."), this);
    gopWelcomePage->setFilesTypeLabel(tr("Tournament"),":/icons/icon.ico");
    gopWelcomePage->showHelpPart(actGetHelp, tr("It would be our pleasure to help you organize your first tournaments."));
    gopWelcomePage->showButtonsPart({actNewTournament, actOpenTournament});
    if(!actgrOpenRecent->actions().isEmpty()){
        gopWelcomePage->showOpenRecentFilesPart(actgrOpenRecent->actions());
    }
    setCentralWidget(gopWelcomePage);
    setCurrentFileName(tr("Welcome Page"));

}

void MainWindow::showTournamentPage()
{
    _state = MainWindow::Tournament;
    enableAllActions(false);
    enableWelcomeActions(true);
    enableTournamnetActions(true);

    setImplicitDockWidgetsState();
    setUseful(tbarResults, true);
    tbarFind->toggleViewAction()->setEnabled(true);

    tabStages = new QTabWidget(this);
    tabStages->setMovable(true);
    setCentralWidget(tabStages);
}

void MainWindow::showEmptyPage()
{
    setCurrentFileName(tr("Empty Page"));
    suspendDockWidgets();
    enableAllActions(false);
    _state = MainWindow::Empty;
}

void MainWindow::removeCentralWidget()
{
    if(centralWidget()){
        delete takeCentralWidget();
    }
}


AView  *MainWindow::showStartList(AModel* model)
{
    GStartListView* tableOfTeams = new GStartListView(model, this);
    _views.push_back(tableOfTeams);
    connect(leFind,SIGNAL(textChanged(QString)),
            tableOfTeams,SLOT(findText(QString)));
    connect(actEditTeams, SIGNAL(triggered()),
            tableOfTeams, SLOT(editStartListRequest()));
    connect(tableOfTeams, SIGNAL(stateEdited(ExecType, QVariant, QVariant)),
            _controller, SLOT(startListViewEdit(ExecType, QVariant, QVariant)));
    actEditTeams->setEnabled(true);
    restoreDockWidget(dwTeams, tableOfTeams->getViewWidget());
    _exportableWidgets.push_back(tableOfTeams->getExportableWidget());
    return tableOfTeams;
}

AMatchesView *MainWindow::showMatches(AMatchesModel *model)
{
    GMatchesTableView* tableOfMatches = new GMatchesTableView(model, this);
    _views.push_back(tableOfMatches);
    connect(leFind,SIGNAL(textChanged(QString)),
            tableOfMatches,SLOT(findText(QString)));
    connect(tableOfMatches, SIGNAL(resultEntered(QVariant, QString, QString)),
            _controller, SLOT(resultEntered(QVariant, QString, QString)));
    connect(tableOfMatches, SIGNAL(stateEdited(ExecType, QVariant, QVariant)),
            _controller, SLOT(matchesViewEdit(ExecType, QVariant, QVariant)));

    restoreDockWidget(dwMatches, tableOfMatches->getViewWidget());
    _exportableWidgets.push_back(tableOfMatches->getExportableWidget());
    return tableOfMatches;
}

QList<AMatchesView *> MainWindow::showFields(const QList<AMatchesModel *> &models)
{
    if(models.isEmpty()){
        return {};
    }
    GFieldsView* fields = new GFieldsView(models, this);
    _views.push_back(fields);
    connect(leFind,SIGNAL(textChanged(QString)),
            fields,SLOT(findText(QString)));
    for(const auto field : fields->getFieldViews()){
        connect(field, SIGNAL(resultEntered(QVariant, QString, QString)),
                _controller, SLOT(resultEntered(QVariant, QString, QString)));
        connect(field, SIGNAL(stateEdited(ExecType, QVariant, QVariant)),
                _controller, SLOT(matchesViewEdit(ExecType, QVariant, QVariant)));
    }
    QScrollArea * scFields = new QScrollArea;
    scFields->setWidget(fields->getViewWidget());
    scFields->setWidgetResizable(true);
    QWidget*  widget = scFields->widget();
    widget->setFixedSize(static_cast<int>(widget->width()*1.2), static_cast<int>(widget->height()*1.1));
    restoreDockWidget(dwFields, scFields);
    _exportableWidgets.push_back(fields->getExportableWidget());
    return fields->getFieldViews();
}

AStageView *MainWindow::showStage(AStageModel *stage)
{
    AStageView* view = GStageViewFactory::getInstance()->create(stage);
    _views.push_back(view);
    _exportableWidgets.push_back(view->getExportableWidget());
    connect(leFind,SIGNAL(textChanged(QString)),
            view,SLOT(findText(QString)));
    connect(view, SIGNAL(resultEntered(QVariant, QString, QString)),
            _controller, SLOT(resultEntered(QVariant, QString, QString)));
    connect(view, SIGNAL(stateEdited(ExecType, QVariant, QVariant)),
            _controller, SLOT(stageViewEdit(ExecType, QVariant, QVariant)));

    QScrollArea * scStage = new QScrollArea();
    scStage->setWidget(view->getViewWidget());
    scStage->setAlignment(Qt::AlignCenter);
    scStage->setWidgetResizable(true);

    tabStages->addTab(scStage, getStageIcon(stage->getType()), view->getTitle());

    QWidget*  widget = scStage->widget();
    widget->setFixedSize(static_cast<int>(widget->width()*1.1), static_cast<int>(widget->height()*1.2));
    return view;
}

AView *MainWindow::showNotes(AModel *model)
{
    GNotesView* view = new GNotesView(model, this);
    _views.push_back(view);
    connect(view, SIGNAL(stateEdited(ExecType, QVariant, QVariant)),
            model, SLOT(execEdit(ExecType, QVariant, QVariant)));
    restoreDockWidget(dwNotes,view->getViewWidget());    
    return view;
}

AView* MainWindow::showInformation(AModel* model)
{
    GStatsView * view = new GStatsView(model, this);
    _views.push_back(view);
    connect(view, SIGNAL(tournamentNameChanged(QString)),
            this, SLOT(documentNameChange(QString)));
    connect(view, SIGNAL(stateEdited(ExecType, QVariant, QVariant)),
            model, SLOT(execEdit(ExecType, QVariant, QVariant)));

    _tournamentName = view->getTournamentName();
    restoreDockWidget(dwInformation, view->getViewWidget());
    return view;
}

void MainWindow::showMessage(const QString &title, const QString& message)
{
    QMessageBox::information(this, title, message);
}


void MainWindow::setImplicitDockWidgetsState()
{
    addDockWidget(Qt::LeftDockWidgetArea, dwInformation);
    addDockWidget(Qt::LeftDockWidgetArea, dwTeams);
    addDockWidget(Qt::RightDockWidgetArea, dwMatches);
    addDockWidget(Qt::BottomDockWidgetArea,dwFields);
    addDockWidget(Qt::RightDockWidgetArea, dwNotes);
    tabifyDockWidget(dwNotes, dwMatches);
    suspendDockWidgets();
}

void MainWindow::restoreDockWidget(QDockWidget *dock, QWidget *widget)
{
    if(dock->widget()){
        dock->widget()->disconnect();
    }
    dock->setWidget(widget);
    dock->show();
    dock->toggleViewAction()->setEnabled(true);
}

void MainWindow::setUseful(QDockWidget *dock, bool useful)
{
    dock->setVisible(useful);
    dock->toggleViewAction()->setEnabled(useful);
}

void MainWindow::setUseful(QToolBar *toolBar, bool useful)
{
    toolBar->setVisible(useful);
    toolBar->toggleViewAction()->setEnabled(useful);
}

void MainWindow::suspendDockWidgets()
{
    setUseful(dwTeams);
    setUseful(dwInformation);
    setUseful(dwNotes);
    setUseful(dwFields);
    setUseful(dwMatches);
    setUseful(tbarResults);
    setUseful(tbarFind);
}


void MainWindow::setupAction(QAction * action, const QString &iconPath,
                             const QString &tips, const QKeySequence shortCut,
                             const QString &iconThemeName, QAction::Priority priority)
{
    const QIcon icon = QIcon::fromTheme(iconThemeName, QIcon(iconPath));
    action->setIcon(icon);
    action->setShortcut(shortCut);
    action->setPriority(priority);
    action->setStatusTip(tips);
    action->setToolTip(tips);
}

void MainWindow::setupActionAsCheckable(QAction *action, bool enabled, bool state)
{
    action->setCheckable(true);
    action->setChecked(state);
    action->setEnabled(enabled);
}

QWidgetAction *MainWindow::createSetLanguageAction()
{
    cbSetLanguage = new QComboBox(this);
    cbSetLanguage->setAutoFillBackground(false);
    connect(cbSetLanguage, SIGNAL(currentIndexChanged(QString)),
            this, SLOT(setLanguage(QString)));
    QWidgetAction * action = new QWidgetAction(this);
    action->setDefaultWidget(cbSetLanguage);
    setupAction(action, ":/icons/language.png", tr("Set the language of the application."));
    return action;
}

void MainWindow::setPossibleLanguages(const QStringList& languageKeys, const QString& currentLanguage)
{
    cbSetLanguage->clear();
    for(const auto& key : languageKeys){
        cbSetLanguage->addItem(QIcon(":/icons/flag_" + key + ".png"), modifiedLanguageKey(key));
    }
    if(!currentLanguage.isEmpty()){
        cbSetLanguage->setCurrentText(modifiedLanguageKey(currentLanguage));
    }
}

QString MainWindow::modifiedLanguageKey(const QString &key)
{
    if(key.trimmed().isUpper()){
        return key.trimmed().toLower();
    }
    return " " + key.toUpper();
}

QIcon MainWindow::getStageIcon(const QString &key)
{
    if(key == key_PLAY_OFF){
        return QIcon(":/icons/playoff.png");
    }
    if(key == key_GROUPS_STAGE){
        return QIcon(":/icons/groups.png");
    }
    if(key == key_ROBIN_ROUND){
        return QIcon(":/icons/robin_round.png");
    }
    return QIcon();
}


void MainWindow::enableWelcomeActions(bool enable)
{
    actNewTournament->setEnabled(enable);
    actOpenTournament->setEnabled(enable);
}

void MainWindow::enableTournamnetActions(bool enable)
{
    actSaveTournament->setEnabled(enable);
    actSaveAsTournament->setEnabled(enable);
    actExportIndividualFiles->setEnabled(enable);
    actCloseTournament->setEnabled(enable);
}

void MainWindow::enableAllActions(bool enable)
{
    enableTournamnetActions(enable);
    enableWelcomeActions(enable);
    actEditTeams->setEnabled(enable);
}

void MainWindow::dragToolBarOrientation(Qt::Orientation orientation)
{
    QToolBar *toolBar = qobject_cast<QToolBar*>(sender());
    if(toolBar->isFloating() && orientation==Qt::Horizontal){
        toolBar->setOrientation(Qt::Vertical);
    }
}

void MainWindow::documentNameChange(const QString& newName)
{
    _tournamentName = newName;
}

bool MainWindow::wishToSaveAndClose()
{
    const QMessageBox::StandardButton ret =
        QMessageBox::warning(this, QCoreApplication::applicationName(),
                             tr("The tournament has been modified.\n"
                                "Do you want to save the changes?"),
                             QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
    if (ret == QMessageBox::Save){
        saveTournament();
        return true;
    }
    else if (ret == QMessageBox::Cancel){
        return false;
    }
    return true;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (!isWindowModified() || wishToSaveAndClose()){
        writeSettings();
        event->accept();
    }
    else{
        event->ignore();
    }
}

QUndoStack *MainWindow::startUndoStack()
{
    return _undoStack;
}

void MainWindow::setCurrentFileName(const QString &fileName)
{
    setWindowTitle(tr("%1[*] | %2").arg(fileName, QCoreApplication::applicationName()));
}

void MainWindow::setRecentTournamentList(const QList<QString>& fileNames)
{
    mnuRecentTournaments->clear();
    actgrOpenRecent->disconnect();
    actgrOpenRecent->deleteLater();
    actgrOpenRecent = new QActionGroup(this);
    connect(actgrOpenRecent, &QActionGroup::triggered,
            this, &MainWindow::openRecentTournament);
    for(const auto& fileName : fileNames){
        QAction * actOpenRecent = actgrOpenRecent->addAction("&"+QString::number(1+fileNames.indexOf(fileName)) + "  " +  fileName);
        actOpenRecent->setToolTip(fileName);
        actOpenRecent->setStatusTip(fileName);      
    }
    mnuRecentTournaments->addActions(actgrOpenRecent->actions());
    mnuRecentTournaments->setEnabled(!fileNames.isEmpty());
    mnuRecentTournaments->addSeparator();
    actClearRecentList = mnuRecentTournaments->addAction(tr("Clear Recent"), this, &MainWindow::clearRecentList);
    setupAction(actClearRecentList,"",tr("Clear the list of recent tournaments."));
    if(_state==MainWindow::Welcome){
        setState(_state);
    }
}

void MainWindow::writeSettings()
{
    QSettings settings;
    settings.beginGroup("main_window");
    settings.setValue("geometry", saveGeometry());
    settings.setValue("save_dialog_path",_saveDialogPath);
    settings.setValue("open_dialog_path",_openDialogPath);
    settings.endGroup();
}

void MainWindow::readSettings()
{
    QSettings settings;
    settings.beginGroup("main_window");
    restoreGeometry(settings.value("geometry").toByteArray());
    _saveDialogPath = settings.value("save_dialog_path","").toString();
    _openDialogPath = settings.value("open_dialog_path","").toString();
    settings.endGroup();
    _tournamentName = tr("Unidentified");
}
