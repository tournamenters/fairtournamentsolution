#ifndef GDRAGABLETOOLBAR_H
#define GDRAGABLETOOLBAR_H

#include <QToolBar>

class GDragableToolBar : public QToolBar
{
    Q_OBJECT

public:
    GDragableToolBar(const QString& title, QWidget* parent = nullptr);
    QToolButton* addToolButton(const QString& text, const QString& tips = "");
    QToolButton* addToolButton(const QIcon& icon, const QString& text, const QString& tips = "");
    void setAlign(Qt::Alignment align);

private:
    Qt::Alignment _align;
};

class GDragableResultsToolBar : public GDragableToolBar
{
    Q_OBJECT

public:
    GDragableResultsToolBar(const QString& title, QWidget* parent = nullptr, int maxScore = 6);

private:
    void createResultButtons(int maxScore);
};

#endif // GDRAGABLETOOLBAR_H
