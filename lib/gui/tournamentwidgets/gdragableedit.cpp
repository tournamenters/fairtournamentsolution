#include "gdragableedit.h"
#include "gdraganddropmanager.h"

#include <QSizePolicy>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMimeData>
#include <QMouseEvent>
#include <QApplication>
#include <QDrag>
#include <QMenu>

GDragableEdit::GDragableEdit(QWidget *parent)
    : QLineEdit (parent)
{
    _type = "all";
    _textContent = "";
    _groupName = "";
    _textOld = "";
    _openForAllDrops = true;
    _dragableManager = new GDragAndDropManager(this);
    setDragAndDrop(true);
    setFrame(false);
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
}

GDragableEdit::~GDragableEdit()
{

}

void GDragableEdit::setText(const QString &text)
{
    blockSignals(true);
    QLineEdit::setText(text);
    _textContent = text;
    _textOld = text;
    blockSignals(false);
}

void GDragableEdit::setDragAndDrop(bool enabled)
{
    _dragAndDropEnabled = enabled;
    setAcceptDrops(enabled);
}

void GDragableEdit::dragEnterEvent(QDragEnterEvent *event)
{
    handleDragEvent(event);
}

void GDragableEdit::dragMoveEvent(QDragMoveEvent *event)
{
    handleDragEvent(event);
}
void GDragableEdit::mousePressEvent(QMouseEvent *event)
{
    _dragableManager->mousePressEventCare(event);
}

void GDragableEdit::mouseMoveEvent(QMouseEvent *event)
{
    if(_dragAndDropEnabled && _dragableManager->mouseMoveEventCare(event)){
        provideDragAction();
    }
}

void GDragableEdit::provideDragAction()
{
    QDrag *drag = new QDrag(this);
    QMimeData *mimeData = new QMimeData();
    mimeData->setText(text());
    mimeData->setImageData(createDragData());
    drag->setMimeData(mimeData);
    drag->setPixmap(grab());
    Qt::DropAction dropAction = drag->exec(Qt::CopyAction | Qt::MoveAction);

    if(dropAction == Qt::IgnoreAction ||  Qt::MoveAction){
        drag->deleteLater();
        mimeData->deleteLater();
    }
}

QVariant GDragableEdit::createDragData()
{
    QVariantMap map;
    map["type"] = _type;
    map["group"] = _groupName;
    map["data"] = _dataToDrag;
    return map;
}

QVariant GDragableEdit::getDragData(const QVariant& drag)
{
    return drag.toMap()["data"];
}

bool GDragableEdit::contentChanged()
{
    QString helpCon = _textContent;
    QString helpText = text();
    if(helpCon.simplified().remove(" ") != helpText.simplified().remove(" ")){
        setModified(true);
        _textContent = text();
        return true;
    }
    if(_textContent != text()){
        blockSignals(true);
        QLineEdit::setText(_textContent);
        blockSignals(false);
    }
    return false;
}

bool GDragableEdit::isMyDrop(const QMimeData *data)
{
    if(_openForAllDrops && (data->hasImage() || data->hasText())){
        return true;
    }
    if(!data->hasImage()){
        return false;
    }
    QVariantMap mimeMap = data->imageData().toMap();
    return mimeMap["type"].toString() == _type &&
            mimeMap["group"].toString() == _groupName;
}

void GDragableEdit::setGroupName(const QString &groupName)
{
    _groupName = groupName;
}

void GDragableEdit::setDataToDrag(const QVariant &dataToDrag)
{
    _dataToDrag = dataToDrag;
}

void GDragableEdit::handleDragEvent(QDragMoveEvent *event)
{
    if (_dragAndDropEnabled && isMyDrop(event->mimeData())) {
        if (event->source() == this) {
            event->setDropAction(Qt::MoveAction);
            event->accept();
        }
        else {
            event->acceptProposedAction();
        }
    }
    else {
        event->ignore();
    }
}

void GDragableEdit::setOpen(bool open)
{
    _openForAllDrops = open;
}

