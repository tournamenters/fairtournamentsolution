#include "ateamssorter.h"
#include <algorithm>


LTeamsSorter::LTeamsSorter(const QList<QString> &criteriumsOrder)
{
    _criteriumsOrder = criteriumsOrder;
}

LTeamsSorter::~LTeamsSorter()
{
    clearElemets();
    _lastOrder.clear();
    _matches.clear();
    _criteriumsOrder.clear();
}

QList<int> LTeamsSorter::sortTeams(const QList<int> &teams, const QList<QList<int> >& criteriaValues)
{
    QList<QPair<QList<int>,int>> sortableList;
    for(int i = 0; i < teams.size() && i < criteriaValues.size(); i++){
        sortableList.push_back(QPair<QList<int>,int>(criteriaValues[i],teams[i]));
    }
    std::sort(sortableList.begin(), sortableList.end());
    std::reverse(sortableList.begin(), sortableList.end());
    QList<int> orderedList;
    for (const auto& team : sortableList) {
        orderedList.push_back(team.second);
    }
    if(orderedList.size()<teams.size()){
        for(int team : teams){
            if(!orderedList.contains(team)){
                orderedList.push_back(team);
            }
        }
    }
    return orderedList;
}

QList<int> LTeamsSorter::sortTeams(const QList<int> &teams, const QList<Match> &matches)
{
    clearElemets();
    _matches.clear();

    _allElements = createElemetsList(teams, matches);
    _matches = matches;

    int criteriumIndex = 0;
    while(!arePerfectlySorted(_allElements) && criteriumIndex<_criteriumsOrder.size()){
        sortByCriterium(_allElements, _criteriumsOrder[criteriumIndex]);
        criteriumIndex++;
    }
    return getOrder();
}


QList<int> LTeamsSorter::getOrder()
{
    QList<int> orderList;
    for (const auto element : _allElements) {
        orderList.push_back(element->_currentPlace);
    }
    return orderList;
}

QList<int> LTeamsSorter::getPoints()
{
    QList<int> orderList;
    for (const auto element : _allElements) {
        orderList.push_back(element->_myPoints);
    }
    return orderList;
}

QList<QPair<int, int> > LTeamsSorter::getScore()
{
    QList<QPair<int, int>> scoreList;
    for (const auto element : _allElements) {
        scoreList.push_back(QPair<int, int>(element->_myGoals, element->_othersGoals));
    }
    return scoreList;
}

void LTeamsSorter::setPointsOverTime(int perWin, int perLoss)
{
    _pointsPerWinOverTime = perWin;
    _pointsPerLossOverTime = perLoss;
}

void LTeamsSorter::setPoints(int perWin, int perDraw, int perLoss)
{
    _pointsPerWin = perWin;
    _pointsPerDraw = perDraw;
    _pointsPerLoss = perLoss;
}

LTeamsSorter *LTeamsSorter::copy()
{
    LTeamsSorter* sorter = new LTeamsSorter(_criteriumsOrder);
    sorter->setPoints(_pointsPerWin,_pointsPerDraw,_pointsPerLoss);
    sorter->setPointsOverTime(_pointsPerWinOverTime, _pointsPerLossOverTime);
    return sorter;
}

QList<Element*> LTeamsSorter::createElemetsList(const QList<int> &teams, const QList<Match> &matches)
{
    QList<Element*> elementList;
    for (int teamIndex : teams) {
        elementList.push_back(countElementPoints(teamIndex, matches));
    }
    return elementList;
}

bool LTeamsSorter::arePerfectlySorted(const QList<Element *>& elements) const
{
    QSet<int> set;
    for(const auto element : elements){
        if(set.contains(element->_currentPlace)){
            return false;
        }
        set.insert(element->_currentPlace);
    }
    return true;
}

Element *LTeamsSorter::countElementPoints(int teamIndex, const QList<Match> &matches)
{
    Element* team = new Element(teamIndex);
    for (const auto& match : matches) {
        if(match.contains(teamIndex) && match.wasPlayed()){
            if(match.draw()){
                team->_myPoints += _pointsPerDraw;
            }
            else if(match.playerWon(teamIndex)){
                if(match._overTime){
                    team->_myPoints += _pointsPerWinOverTime;
                }
                else{
                    team->_myPoints += _pointsPerWin;
                }
            }
            else{
                if(match._overTime){
                    team->_myPoints += _pointsPerLossOverTime;
                }
                else{
                    team->_myPoints += _pointsPerLoss;
                }
            }
            if(teamIndex == match._hostIndex){
                team->_myGoals += match._hostScore;
                team->_othersGoals += match._guestScore;
            }
            if(teamIndex == match._guestIndex){
                team->_myGoals += match._guestScore;
                team->_othersGoals += match._hostScore;
            }
        }
    }
    return team;
}

void LTeamsSorter::sortByCriterium(const QList<Element *>& elements, const QString &criterium)
{
    if(criterium == NUM_POINTS){
        sortByPoints(elements);
    }
    else if(criterium == MINI_TABLE){
        sortByMutualPoints(elements);
        sortByMutualScore(elements);
    }
    else if(criterium == SCORE_DIFF){
        sortByScore(elements);
    }
    else if(criterium == NUM_GOALS){
        sortByGoals(elements);
    }
}

void LTeamsSorter::sortByPoints(const QList<Element *> &elements)
{
    QList<QList<Element*>> unsortedGroups = findUnsortedGroups(elements);
    QList<int> pointsList;
    for (const auto& group : unsortedGroups) {
        for (const auto element : group) {
            pointsList.push_back(element->_myPoints);
        }
        sortElements(group, pointsList, group.first()->_currentPlace);
        pointsList.clear();
    }
}

void LTeamsSorter::sortByMutualPoints(const QList<Element *> &elements)
{
    QList<QList<Element*>> unsortedGroups = findUnsortedGroups(elements);
    QList<int>pointList;
    for (const auto& group : unsortedGroups) {
        QList<Element*> mutualStats = findMutualStats(group);
        for (const auto element : mutualStats) {
            pointList.push_back(element->_myPoints);
        }
        qDeleteAll(mutualStats);
        sortElements(group, pointList, group.first()->_currentPlace);
        pointList.clear();
    }
}

void LTeamsSorter::sortByMutualScore(const QList<Element *> &elements)
{
    QList<QList<Element*>> unsortedGroups = findUnsortedGroups(elements);
    QList<int> scoreList;
    for (const auto& group : unsortedGroups) {
        QList<Element*> mutualStats = findMutualStats(group);
        for (const auto element : mutualStats) {
            scoreList.push_back(element->_myGoals - element->_othersGoals);
        }
        qDeleteAll(mutualStats);
        sortElements(group, scoreList, group.first()->_currentPlace);
        scoreList.clear();
    }
}

void LTeamsSorter::sortByScore(const QList<Element *> &elements)
{
    QList<QList<Element*>> unsortedGroups = findUnsortedGroups(elements);
    QList<int>scoreList;
    for (const auto& group : unsortedGroups) {
        for (const auto element : group) {
            scoreList.push_back(element->_myGoals-element->_othersGoals);
        }
        sortElements(group, scoreList, group.first()->_currentPlace);
        scoreList.clear();
    }
}

void LTeamsSorter::sortByGoals(const QList<Element *> &elements)
{
    QList<QList<Element*>> unsortedGroups = findUnsortedGroups(elements);
    QList<int> goalsList;
    for (const auto& group : unsortedGroups) {
        for (const auto element : group) {
            goalsList.push_back(element->_myGoals);
        }
        sortElements(group, goalsList, group.first()->_currentPlace);
        goalsList.clear();
    }
}

void LTeamsSorter::sortElements(const QList<Element *> &elements, const QList<int> values, int minPosition)
{
    QList<QPair<int,int>> sortableList;
    for(int i = 0; i < elements.size() && i < values.size(); i++){
        sortableList.push_back(QPair<int,int>(values[i], i));
    }
    std::sort(sortableList.begin(), sortableList.end());
    std::reverse(sortableList.begin(), sortableList.end());
    int position = minPosition;
    int jump = 0;
    int lastValue = sortableList.first().first;
    for (const auto& order : sortableList) {
        if(lastValue > order.first){
            position += jump;
            jump = 1;
        }
        else{
            jump++;
        }
        elements[order.second]->_currentPlace = position;
        lastValue = order.first;
    }
}

QList<QList<Element *> > LTeamsSorter::findUnsortedGroups(const QList<Element *> &elements)
{
    QList<QList<Element *> > groups;
    int position = 1;
    int placed = 0;
    while(placed<elements.size()){
        groups.push_back({});
        for (const auto element : elements) {
            if(element->_currentPlace == position){
                groups.last().push_back(element);
                placed++;
            }
        }
        position++;
    }
    QList<QList<Element *> > interestingGroups;
    for (const auto& group : groups) {
        if(group.size()>1){
            interestingGroups.push_back(group);
        }
    }
    groups.clear();
    return interestingGroups;
}

QList<Element *> LTeamsSorter::findMutualStats(const QList<Element *> &elements)
{
    QList<Element *> newElementList;
    QList<Match> mutualMatches = findMutualMatches(elements);
    for(const auto origin : elements){
        newElementList.push_back(countElementPoints(origin->_index, mutualMatches));
    }
    return newElementList;
}

QList<Match> LTeamsSorter::findMutualMatches(const QList<Element *> &elements)
{
    QList<Match> mutualMatches;
    for(int i = 0; i < _matches.size(); i++){
        int containsCount = 0;
        for (const auto element : elements) {
            if(_matches[i].contains(element->_index)){
                containsCount++;
            }
        }
        if(containsCount==2){
            mutualMatches.push_back(_matches[i]);
        }
    }
    return mutualMatches;
}

void LTeamsSorter::clearElemets()
{
    qDeleteAll(_allElements);
    _allElements.clear();
}



Element::Element()
{
    _myGoals = 0;
    _othersGoals = 0;
    _myPoints = 0;
    _index = -1;
    _currentPlace = 1;
}

Element::Element(int index)
    :Element()
{
    _index = index;
}

Match::Match()
{
    _hostIndex = -1;
    _guestIndex = -1;

    _hostScore = -1;
    _guestScore = -1;

    _overTime = false;
}

Match::Match(int host, int guest)
    :Match()
{
    _hostIndex = host;
    _guestIndex = guest;
}

Match::Match(int host, int guest, int hostScore, int guestScore)
    :Match(host, guest)
{
    _hostScore = hostScore;
    _guestScore = guestScore;
}

bool Match::playerWon(int teamIndex) const
{
    if(wasPlayed()){
        if(teamIndex == _hostIndex){
            return _hostScore > _guestScore;
        }
        if(teamIndex == _guestIndex){
            return _hostScore < _guestScore;
        }
    }
    return false;
}

bool Match::wasPlayed() const
{
    return _hostScore != -1 && _guestScore != -1;
}

bool Match::hostWin() const
{
    return _hostScore > _guestScore;
}

bool Match::guestWin() const
{
    return _hostScore < _guestScore;
}

bool Match::draw() const
{
    return _hostScore == _guestScore;
}

bool Match::contains(int index) const
{
    return _hostIndex == index || _guestIndex == index;
}
