#include "lplayoffdrawerfactory.h"

LPlayOffDrawerFactory::LPlayOffDrawerFactory()
{
    registerOrdererType("advantageous", tr("Advantegeous"), LPlayOffDrawerAdvantageous::create);
    registerOrdererType("equal", tr("Equal"), LPlayOffDrawerEqual::create);
}

LPlayOffDrawerFactory::~LPlayOffDrawerFactory()
{
    _registeredTypes.clear();
    _typeLabelMap.clear();
}

QMap<QString, QString> LPlayOffDrawerFactory::getPossibleTypesKeyLabelMap()
{
    return _typeLabelMap;
}

void LPlayOffDrawerFactory::registerOrdererType(const QString &type, const QString &label, FcnCreatePlayOffDrawer create)
{
    _registeredTypes[type] = create;
    _typeLabelMap[type] = label;
}

APlayOffDrawer *LPlayOffDrawerFactory::createDrawer(const QString &type)
{
    if(_registeredTypes.contains(type)){
        return _registeredTypes[type]();
    }
    return nullptr;
}
