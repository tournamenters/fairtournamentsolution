#ifndef GSTARTLISTVIEW_H
#define GSTARTLISTVIEW_H

#include "aview.h"

class GTeamEditsTable;
class AModel;

class GStartListView : public ATeamsView
{
    Q_OBJECT

public:
    GStartListView(AModel * model, QObject *parent = nullptr);
    ~GStartListView() override;

    QWidget * getViewWidget() override;
    AExportable * getExportableWidget() override;

public slots:
    void update() override;
    void editStartListRequest();
    void findText(const QString& text) override;

private:
    void createStartListTable();

    GTeamEditsTable* gteStartList;
    AModel * _model;
};

#endif // GSTARTLISTVIEW_H
