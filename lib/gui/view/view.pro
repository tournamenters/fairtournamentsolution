#-------------------------------------------------
#
# Project created by QtCreator 2019-04-30T20:49:10
#
#-------------------------------------------------

TARGET = view
TEMPLATE = lib

QT += widgets
QT += core
QT += gui

CONFIG -= app_bundle
CONFIG += staticlib
CONFIG += c++11

DEFINES += VIEW_LIBRARY
DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
	aview.cpp \
	grobinroundview.cpp \
	gstatsview.cpp \
	gnotesview.cpp \
	gstartlistview.cpp \
	gfieldsview.cpp \
	gplayoffview.cpp \
	ggroupstageview.cpp \
	gstageviewfactory.cpp \
    gmatchestableview.cpp

HEADERS += \
	aview.h \
	grobinroundview.h \
	gstatsview.h \
	gnotesview.h \
	gstartlistview.h \
	gfieldsview.h \
	gplayoffview.h \
	ggroupstageview.h \
	gstageviewfactory.h \
    gmatchestableview.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

include($$PWD/../../core/core.pri)
include($$PWD/../tournamentwidgets/tournamentwidgets.pri)
include($$PWD/../present/present.pri)

DISTFILES += \
    view.pri

TRANSLATIONS += \
    view_cs.ts
