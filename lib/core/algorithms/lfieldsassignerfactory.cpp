#include "lfieldsassignerfactory.h"

LFieldsAssignerFactory::LFieldsAssignerFactory()
{
    registerOrdererType("simple", tr("No Added Requirements"), LFieldsAssignerSimple::create);
    registerOrdererType("changeover", tr("Max Changeovers"), LFieldsAssignerMaxChangeovers::create);
    registerOrdererType("transition", tr("Min Transitions"), LFieldsAssignerMinTransitions::create);
}

LFieldsAssignerFactory::~LFieldsAssignerFactory()
{
    _registeredTypes.clear();
    _typeLabelMap.clear();
}

QMap<QString, QString> LFieldsAssignerFactory::getPossibleTypesKeyLabelMap()
{
    return _typeLabelMap;
}

void LFieldsAssignerFactory::registerOrdererType(const QString& type, const QString& label, FcnCreateFieldsAssigner create)
{
    _registeredTypes[type] = create;
    _typeLabelMap[type] = label;
}

AFieldsAssigner *LFieldsAssignerFactory::createAssigner(const QString& type)
{
    if(_registeredTypes.contains(type)){
        return _registeredTypes[type]();
    }
    return nullptr;
}
