#ifndef GPLAYOFFWIDGET_H
#define GPLAYOFFWIDGET_H

#include <QVariant>
#include <QWidget>
#include <QPointer>
#include "aexportable.h"

class GTeamEdit;
class GResultEdit;
class GPlayOffMatch;

class GPlayOffWidget : public QWidget, public AExportable
{
    Q_OBJECT

public:
    explicit GPlayOffWidget(const QVariant& data, const QVariant& identificator, QWidget *parent = nullptr);
    void setData(const QVariant& data);
    void repaintPlayOff();

public slots:
    void findText(const QString &text);

signals:
    void teamChangeRequested(const QVariant& myIdentificator, const QString& text, const QString& oldText);
    void teamsReplacementRequested(const QVariant& myIdentificator, const QVariant& otherIdentificator);
    void resultChanged(int row, int column, const QString& result, const QString& oldResult);

protected:
    QString toHtml() override;
    QPixmap toPixmap() override;
    QString toPlainText() override;
    QString toCsv() override;

    virtual QMenu* createContextMenu();

private slots:
    void showMenu();

    void copyPicture();
    void savePicture();

private:
    void createWinner();

    void adjustMatchesSize(int numRounds);
    void adjustRoundsSize();
    void setSize(int numRounds);

    void placeTreeWidgets();
    void placeWinner();
    void placeMatch(int round, int match, int centerH, int centerW, int matchHeight);
    void paintTreeBackground();
    void paintMatchLines(QPainter* painter, int centerH, int centerW, int height);
    void paintTreeLines(QPainter* painter);

    bool matchExists(int roundIndex, int matchIndex);
    GPlayOffMatch* createMatch(int roundIdnex, int matchIndex);
    void addMatch(int round, int match, const QVariant& data);
    void connectTeamEdit(GTeamEdit* team);

    int countMatchHeight(int roundIndex);

    QList<QList<GPlayOffMatch*>> _matches;
    GTeamEdit * _winner;
    QPointer<QMenu> mnuContext;
    QVariant _identificator;
    int _columnWidth;
    int _rowHeight;
    int _treeLineWidth;
    int _numRounds;
    qreal _lineEditMarginCoef;

};







class GPlayOffMatch : public QWidget
{
    Q_OBJECT

public:
    GPlayOffMatch(int roundIndex, int matchIndex, const QVariant& identificator, QWidget *parent = nullptr);
    ~GPlayOffMatch() override;

    void setData(const QVariant &data, bool hostPlaceholder = true, bool guestPlaceholder = true);
    void findText(const QString& text);
    void moveTeamsLabels(int w, int h, int matchHeight);
    void moveResulEdit(int w, int h);

    GTeamEdit *host() const;
    GTeamEdit *guest() const;
    GResultEdit *result() const;

private:
    QVariant addKey(const QString& key, const QVariant& variant);
    void setTeamsData(GTeamEdit * edit, const QVariantMap& data, bool placeholder);

    GTeamEdit * _host;
    GTeamEdit * _guest;
    GResultEdit * _result;

};


#endif // GPLAYOFFWIDGET_H
