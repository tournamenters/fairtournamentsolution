#ifndef LFIELDSASSIGNERFACTORY_H
#define LFIELDSASSIGNERFACTORY_H
#include "afieldsassigner.h"

#include <QMap>
#include <QObject>

typedef AFieldsAssigner* (__stdcall * FcnCreateFieldsAssigner)(void);

class LFieldsAssignerFactory: public QObject
{
    Q_OBJECT

public:
    ~LFieldsAssignerFactory();

    static LFieldsAssignerFactory *Instance()
    {
        static LFieldsAssignerFactory instance;
        return &instance;
    }

    LFieldsAssignerFactory(LFieldsAssignerFactory& other) = delete;
    void operator=(LFieldsAssignerFactory const& other) = delete;

    QMap<QString, QString> getPossibleTypesKeyLabelMap();
    void registerOrdererType(const QString &type, const QString &label, FcnCreateFieldsAssigner create);
    AFieldsAssigner * createAssigner(const QString &type);

private:
    LFieldsAssignerFactory();
    QMap<QString, FcnCreateFieldsAssigner> _registeredTypes;
    QMap<QString, QString> _typeLabelMap;
};

#endif // LFIELDSASSIGNERFACTORY_H
