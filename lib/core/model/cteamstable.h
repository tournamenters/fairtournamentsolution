#ifndef CTABLE_H
#define CTABLE_H
#include "amodel.h"

class CTeam;

class CTeamsList: public AModel
{
    Q_OBJECT
    
public:
    explicit CTeamsList(bool connect = false, QObject *parent = nullptr);
    CTeamsList(const QList<CTeam*>& teams, QObject *parent = nullptr);
    ~CTeamsList() override;

    void addColumn(const QString& header, const QStringList& values);
    void addColumn(const QString& header, const QList<int>& values);
    void addRow(CTeam* team, const QVariantMap& values);
    void addRow(const QPair<CTeam *, QVariantMap> &row);
    void setColumnHeaders(const QStringList& columnHeaders);
    void setColumnData(const QVariant& data);

    void changeTeam(int row, CTeam* team);
    void changeValue(int row, const QString& header, const QString& value);
    void changeTeams(const QList<CTeam*>& teams);
    void changeTeams(CTeamsList *teams);
    void changeTeams(const QList<CTeamsList *> &teams);
    void changeRow(int row, CTeam* team, const QVariantMap& values);

    bool renameTeam(const QVariant& identificator, const QVariant& data);

    QList<CTeam*> getTeams() const;
    QVariant getData() const override;
    QStringList getColumn(const QString& header) const;
    QPair<CTeam*, QVariantMap> getRow(int row) const;
    QStringList getColumnHeaders() const;

    QList<QStringList> toStringList() const;
    QVariant teamsToVariant() const;

    bool hasColumn(const QString& header) const;

    virtual void clear();

public slots:
    bool execEdit(ExecType type, const QVariant& identificator, const QVariant& data) override;

signals:
    void teamUpdated();

protected:   
    QStringList toStringList(const QList<int>& list);

    QList<CTeam*> _teams;

private:
    void connectTeam(CTeam* team);

    QStringList _columnNames;
    QMap<QString, QStringList> _customColumns;
    bool _connect;

};



#endif // CTABLE_H



