#ifndef GMATCHESTABLEVIEW_H
#define GMATCHESTABLEVIEW_H

#include "aview.h"

class AMatchesModel;
class GMatchesTable;

class GMatchesTableView : public AMatchesView
{
    Q_OBJECT

public:
    GMatchesTableView(AMatchesModel *model, QObject *parent = nullptr);
    ~GMatchesTableView() override;
    QWidget * getViewWidget() override;
    AExportable * getExportableWidget() override;

public slots:
    void update() override;
    void findText(const QString& text) override;


private:
    AMatchesModel* _model;
    GMatchesTable * _table;
};

#endif // GMATCHESTABLEVIEW_H
