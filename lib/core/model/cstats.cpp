#include "cstats.h"

#include <QSet>
#include "cfieldsmanager.h"
#include "cstartlist.h"
#include "cteam.h"
#include "cmatch.h"

CStats::CStats(QObject *parent)
    :AModel(parent)
{
    _numTeams = 0;
    _numMatches = 0;
    _numMatchesPlayed = 0;
    _maxUnplayedMatches = 0;
    _maxSizeField = 0;
    addToIdentificator("stats", 1);
    _startList = nullptr;
}

CStats::~CStats()
{

}

void CStats::setTournamentName(const QString &name)
{
    _tournamentName = name;
    emit dataChanged();
}

void CStats::setMatches(const QList<AMatchesModel *> &fields)
{
    resetMatches();
    if(!fields.isEmpty()){
        _fields = fields;
        for(const auto field : _fields){
            for (const auto match : field->getMatches()) {
                if(match){
                    connect(match, SIGNAL(resultUpdated()),
                            this, SLOT(matchChange()));
                }
            }
        }
        updateNumbers();
    }
}

void CStats::setTeams(CStartList *startList)
{
    if(_startList){
        _startList->clear();
    }
    _startList = startList;
    for (const auto team : startList->getTeams()) {
        if(team){
            connect(team, SIGNAL(activeChanged()),
                    this, SLOT(teamChange()));
        }
    }
    _numTeams = startList->getTeams().size();
}

QVariant CStats::getData() const
{
    QVariantMap map;
    map["tournament_name"] = _tournamentName;
    map["num_teams"] = _numTeams;
    map["num_matches"] = _numMatches;
    map["num_matches_played"] = _numMatchesPlayed;
    map["tournament_progress"] =  (1.0 - (static_cast< double > (_maxUnplayedMatches) / static_cast< double > (_maxSizeField)))*100;
    return map;
}

QString CStats::getTournamentName() const
{
    return _tournamentName;
}

QVariant CStats::toVariant()
{
    return getData();
}

bool CStats::execEdit(ExecType type, const QVariant& identificator, const QVariant& data)
{
    if(isItForMe(identificator)){
        if(type == ExecType::Rewrite){
            if(!data.toString().isEmpty()){
                _tournamentName = data.toString();
                emit dataChanged();
                return true;
            }
        }
    }
    return false;
}

void CStats::matchChange()
{
    updateNumbers();
    emit dataChanged();

}

void CStats::teamChange()
{

}

void CStats::updateNumbers()
{
    _numMatches = 0;
    _maxSizeField = 0;
    _maxUnplayedMatches = 0;
    _numMatchesPlayed = 0;

    for(const auto field : _fields){
        int fieldSize = 0;
        int unplayedMatches = 0;
        for (const auto match : field->getMatches()) {
            if(match){
                _numMatches++;
                fieldSize++;
                if(!match->hasResult()){
                    unplayedMatches++;
                }
                else{
                    _numMatchesPlayed++;
                }
            }
            if(fieldSize > _maxSizeField){
                _maxSizeField = fieldSize;
            }
            if(_maxUnplayedMatches < unplayedMatches){
                _maxUnplayedMatches = unplayedMatches;
            }
        }
    }
}

void CStats::resetMatches()
{
    for(const auto field : _fields){
        for (const auto match : field->getMatches()) {
            if(match){
                disconnect(match, nullptr,
                           this, nullptr);
            }
        }
    }
    _fields.clear();
}
