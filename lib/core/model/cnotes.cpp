#include "cnotes.h"

CNotes::CNotes(QObject *parent)
    :AModel (parent)
{
    setName(tr("Notes"));
    QVariantMap map;
    map["type"] = "notes";
    setIdentificator(map);
}

QVariant CNotes::getData() const
{
    return _notes;
}

QVariant CNotes::toVariant() const
{
    return getData();
}

void CNotes::fromVariant(const QVariant &variant)
{
    _notes = variant.toString();
}

bool CNotes::execEdit(ExecType type, const QVariant &identificator, const QVariant &data)
{
    if(isItForMe(identificator) && type == ExecType::Rewrite){
        _notes = data.toString();
        return true;
    }
    return false;
}

