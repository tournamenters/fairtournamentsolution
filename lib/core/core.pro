TEMPLATE = subdirs

SUBDIRS += \
    algorithms \
    model

    algorithms.subdir = algorithms
    model.subdir = model

    model.depends = algorithms

DISTFILES += \
    core.pri
