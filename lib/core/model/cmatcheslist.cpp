#include "cmatcheslist.h"
#include "cmatch.h"

CMatchesList::CMatchesList(QObject *parent)
    : AMatchesModel (parent)
{
    setName(tr("Matches List"));
    createIdentificator();
}

CMatchesList::CMatchesList(const QList<CMatch *> &matches, QObject *parent)
    :CMatchesList(parent)
{
    addMatches(matches);
}

void CMatchesList::addMatch(CMatch *match)
{
    _matches.push_back(match);
    if(match){
        connect(match, SIGNAL(resultUpdated()),
                this, SIGNAL(dataChanged()));
        connect(match, SIGNAL(teamsUpdated()),
                this, SIGNAL(dataChanged()));
    }
}

void CMatchesList::addMatches(const QList<CMatch *>& matches)
{
    for(const auto match : matches){
        addMatch(match);
    }
}

void CMatchesList::setMatches(const QList<CMatch *> &matches)
{
    removeMatches();
    addMatches(matches);
}

QVariant CMatchesList::getData() const
{
    return matchesToTable(true, true);
}

QVariant CMatchesList::toVariant()
{
    return matchesToVariant();
}

bool CMatchesList::setResult(const QVariant &identificator, const QString &result)
{
    if(isItForMe(identificator)){
        CMatch* match = getMatch(identificator);
        if(match){
            match->setResult(result);
            return true;
        }
    }
    return false;
}

bool CMatchesList::execEdit(ExecType type, const QVariant &identificator, const QVariant &data)
{
    if(isItForMe(identificator)){
        if(type == ExecType::Rename){
            CMatch* match = getMatch(identificator);
            if(match){
                renameTeamInMatch(match, identificator, data);
                return true;
            }
        }
    }
    return false;
}
void CMatchesList::createIdentificator()
{
    QVariantMap map;
    map["matches"] = 1;
    setIdentificator(map);
}
