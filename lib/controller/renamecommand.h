#ifndef RENAMECOMMAND_H
#define RENAMECOMMAND_H

#include <QUndoCommand>
#include <QVariant>

class AModel;

class RenameCommand: public QUndoCommand
{

public:
    RenameCommand(AModel *model, const QVariant& identificator, const QVariant& data, QUndoCommand *parent = nullptr);
    ~RenameCommand() override;

    void undo() override;
    void redo() override;

private:
    AModel * _model;
    QVariant _identificator;
    QVariant _data;
};


#endif // RENAMECOMMAND_H
