TEMPLATE = subdirs

SUBDIRS += \
    view \
    mainwindow \
    tournamentwidgets \
    creation \
    present

    view.subdir = view
    mainwindow.subdir = mainwindow
    tournamentwidgets.subdir = tournamentwidgets
    creation.subdir = creation
    present.subdir = present

    mainwindow.depends = view present tournamentwidgets creation
    tournamentwidgets.depends = present
    view.depends = tournamentwidgets present
    creation.depends = tournamentwidgets view

DISTFILES += \
    gui.pri
