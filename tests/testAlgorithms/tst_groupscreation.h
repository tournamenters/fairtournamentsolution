#pragma once

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "lgroupsdrawerfactory.h"
#include "tst_robinroundmatchesorder.h"

#include <QList>

using namespace testing;

QList<int> toOneList(const QList<QList<int>>& groups){
    QList<int> list;
    for(const auto& group : groups)  {
        if(!group.isEmpty()){
            list.append(group);
        }
    }
    return list;
}

void rightNumberOfTeams(const QList<QList<int>>& groups, int numTeams)
{
    int number = 0;
    for(const auto& group : groups){
        number += group.size();
    }
    ASSERT_EQ(number, numTeams);
}

void everybodyPlaced(const QList<QList<int>>& groups, int numTeams)
{
    QList<int> oneList = toOneList(groups);
    for (int i =0;i<numTeams;i++) {
        ASSERT_TRUE(oneList.contains(i)) << "Some team is not in any group.";
    }
}

void everybodyJustOnes(const QList<QList<int>>& groups, int numTeams)
{
    QList<int> oneList = toOneList(groups);
    for (int i =0;i<numTeams;i++) {
        ASSERT_EQ(1,oneList.count(i)) << "More occurences of the same team.";
    }
}

void firstInFirstGroup(const QList<QList<int>>& groups)
{
    for (int i =0;i<groups.size();i++) {
        ASSERT_EQ(i, groups[i].first()) << "First teams not properly set.";
    }
}

void testBaskets(const QList<QList<int>>& groups, int numTeams)
{
    int minInBasket = 0;
    int maxInBasket = groups.size()-1;

    for(int round = 0; round<=numTeams/groups.size(); round++){
        for (int i =0;i<groups.size();i++) {
            if(round<groups[i].size()){
                ASSERT_TRUE(minInBasket<=groups[i][round]);
                ASSERT_TRUE(maxInBasket>=groups[i][round]);
            }
            else{
                ASSERT_TRUE(maxInBasket>=numTeams);//Last round
            }
        }
        minInBasket+=groups.size();
        maxInBasket+=groups.size();
    }
}

void testRandomDrawer(AGroupsDrawer* drawer)
{
    for (int numTeams = 4; numTeams < 50; numTeams++){
        for (int numGroups = 1; numGroups < numTeams/2; numGroups++){
            auto groups = drawer->createGroups(numGroups, numTeams);
            rightNumberOfTeams(groups, numTeams);
            everybodyPlaced(groups, numTeams);
            everybodyJustOnes(groups, numTeams);
        }
    }
}

void testExactDrawer(AGroupsDrawer* drawer)
{
    testRandomDrawer(drawer);
    for (int numTeams = 4; numTeams < 50; numTeams++){
        for (int numGroups = 1; numGroups < numTeams/2; numGroups++){
            auto groups = drawer->createGroups(numGroups, numTeams);
            firstInFirstGroup(groups);
        }
    }
}

void testBasketsDrawer(AGroupsDrawer* drawer)
{
    testExactDrawer(drawer);
    for (int numTeams = 4; numTeams < 50; numTeams++){
        for (int numGroups = 1; numGroups < numTeams/2; numGroups++){
            auto groups = drawer->createGroups(numGroups, numTeams);
            testBaskets(groups, numTeams);
        }
    }
}


TEST(groupscreation, advantageous)
{
    AGroupsDrawer* advantageous = LGroupsDrawerFactory::Instance()->createDrawer("advantageous");
    testExactDrawer(advantageous);
    QList<QList<int>> rightForFour = {{0,5,6},
                                      {1,4,7},
                                      {2,3,8}};
    ASSERT_EQ(rightForFour, advantageous->createGroups(3,9));
    QList<QList<int>> rightForSix = {{0,7,8},
                                     {1,6,9},
                                     {2,5,10},
                                     {3,4}};
    ASSERT_EQ(rightForSix, advantageous->createGroups(4,11));
    delete  advantageous;
}

TEST(groupscreation, equal)
{
    AGroupsDrawer* equal = LGroupsDrawerFactory::Instance()->createDrawer("equal");
    testExactDrawer(equal);
    QList<QList<int>> rightForFour = {{0,3,6},
                                      {1,4,7},
                                      {2,5,8}};
    ASSERT_EQ(rightForFour, equal->createGroups(3,9));
    QList<QList<int>> rightForSix = {{0,4,8},
                                     {1,5,9},
                                     {2,6,10},
                                     {3,7}};
    ASSERT_EQ(rightForSix, equal->createGroups(4,11));
    delete equal;
}

TEST(groupscreation, modified)
{
    AGroupsDrawer* modified = LGroupsDrawerFactory::Instance()->createDrawer("modified");
    testExactDrawer(modified);
    QList<QList<int>> rightForFour = {{0,3,7},
                                      {1,4,8},
                                      {2,5,6}};
    ASSERT_EQ(rightForFour, modified->createGroups(3,9));
    QList<QList<int>> rightForSix = {{0,4, 9},
                                     {1,5, 10},
                                     {2,6},
                                     {3,7,8}};
    ASSERT_EQ(rightForSix, modified->createGroups(4,11));
    delete modified;
}

TEST(groupscreation, random)
{
    AGroupsDrawer* random = LGroupsDrawerFactory::Instance()->createDrawer("random");
    testRandomDrawer(random);
    delete random;
}

TEST(groupscreation, baskets)
{
    AGroupsDrawer* baskets = LGroupsDrawerFactory::Instance()->createDrawer("baskets");
    testExactDrawer(baskets);
    testBasketsDrawer(baskets);
    delete baskets;
}



