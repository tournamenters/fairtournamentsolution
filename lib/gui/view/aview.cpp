#include "aview.h"
#include "gteameditstable.h"
#include "gmatchestable.h"
#include "gresultedit.h"

#include <QSplitter>

AView::AView(QObject *parent)
    : QObject(parent)
{
    _title = tr("Abstract");
}

AView::~AView()
{
    _identificator.clear();
}

QString AView::getTitle()
{
    return _title;
}

void AView::setTitle(const QString &title)
{
    _title = title;
}

AExportable *AView::getExportableWidget()
{
    return nullptr;
}

QVariant AView::createIdentificator(const QVariant &firstdentificator, const QVariant &secondIdentificator)
{
    QVariantMap identificatorMap = firstdentificator.toMap();
    identificatorMap.unite(secondIdentificator.toMap());
    return identificatorMap;
}

void ATeamsView::teamEditChange(const QVariant& editIdentificator, const QString& newName, const QString& oldName)
{
    QVariant identificator = createIdentificator(_identificator, editIdentificator);
    QVariantMap data;
    data["new"] = newName;
    data["old"] = oldName;
    emit stateEdited(ExecType::Rename, identificator, data);
}

ATeamsView::ATeamsView(QObject *parent)
    : AView(parent)
{

}

void ATeamsView::connectTeamsTable(GTeamEditsTable *table)
{
    connect(table, SIGNAL(teamChangeRequested(QVariant,QString,QString)),
            this, SLOT(teamEditChange(QVariant,QString,QString)));
    connect(table, SIGNAL(teamsReplacementRequested(QVariant,QVariant)),
            this, SLOT(teamsReplacement(QVariant,QVariant)));
}

void ATeamsView::teamsReplacement(const QVariant& thisEditIdentificator, const QVariant& otherEditIdentificator)
{
    QVariantMap data;
    data["new"] = thisEditIdentificator;
    data["old"] = otherEditIdentificator;
    emit stateEdited(ExecType::Swap, _identificator, data);
}

AMatchesView::AMatchesView(QObject *parent)
    : ATeamsView(parent)
{

}

void AMatchesView::connectMatchesTable(GMatchesTable *table)
{
    connect(table, SIGNAL(resultChanged(int,int,QString,QString)),
            this, SLOT(resultEditChange(int,int,QString,QString)));
}

void AMatchesView::resultEditChange(int row, int col, const QString &newResult, const QString &oldResult)
{
    QVariantMap identificator = _identificator.toMap();
    identificator["row"] = row;
    identificator["col"] = col;
    emit resultEntered(identificator, newResult, oldResult);
}

AStageView::AStageView(QObject *parent)
    : AMatchesView (parent)
{
    _model = nullptr;
}


void AStageView::addEmptySpace(QSplitter *splitter, int stretchFactor)
{
    addWidget(splitter, new QWidget(), stretchFactor);
}

void AStageView::addWidget(QSplitter *splitter, QWidget *widget, int stretchFactor)
{
    int index = splitter->count();
    splitter->addWidget(widget);
    splitter->setStretchFactor(index,stretchFactor);
    splitter->setCollapsible(index, false);
}

