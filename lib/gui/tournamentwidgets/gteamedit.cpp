#include "gteamedit.h"
#include <QMenu>
#include <QAction>
#include <QLineEdit>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMimeData>
#include <QMouseEvent>
#include <QApplication>
#include <QDrag>

GTeamEdit::GTeamEdit(int row, int column, const QVariant& identificator, QWidget *parent)
    : GDragableEdit (parent)
{
    setOpen(false);
    _removableActions = false;
    _type = "team";

    setToolTip(tr("Participant name"));
    setReadOnly(true);
    setContextMenuPolicy(Qt::CustomContextMenu);

    connect(this,SIGNAL(editingFinished()),
            this,SLOT(textEditation()));
    connect(this, SIGNAL(customContextMenuRequested(const QPoint&)),
            this, SLOT(showMenu()));

    setupDragData(row, column, identificator);
}

GTeamEdit::~GTeamEdit()
{

}

void GTeamEdit::dropEvent(QDropEvent *event)
{
    emit teamsReplacementRequested(_dataToDrag, getDragData(event->mimeData()->imageData()));
    event->acceptProposedAction();
}

void GTeamEdit::renameTeam()
{
    blockSignals(true);
    clear();
    setPlaceholderText(_textContent);
    setReadOnly(false);
    blockSignals(false);
}

void GTeamEdit::removeTeam()
{
    emit teamChangeRequested(_dataToDrag, "", text());
}

void GTeamEdit::restoreTeam()
{
    emit teamChangeRequested(_dataToDrag, text(), "");
}

void GTeamEdit::showMenu()
{
    if(!mnuContext){
        mnuContext = new QMenu(this);
        actRename = mnuContext->addAction(QIcon(":/icons/rename.ico"),
                                          tr("Rename"),this, &GTeamEdit::renameTeam);
        if(_removableActions){
            actRemove = mnuContext->addAction(QIcon(":/icons/remove.ico"),
                                              tr("Remove"),this, &GTeamEdit::removeTeam);
            actReturn = mnuContext->addAction(QIcon(":/icons/refresh.ico"),
                                              tr("Restore"),this, &GTeamEdit::restoreTeam);
        }
    }
    if(_removableActions){
        //Not yet fully implemented, so unenabled...
        actRemove->setEnabled(!isEnabled());
        actReturn->setEnabled(!isEnabled());
    }
    mnuContext->exec(QCursor::pos());
}

void GTeamEdit::setupDragData(int row, int col, const QVariant &identificator)
{
    QVariantMap map = identificator.toMap();
    map["row"] = row;
    map["col"] = col;
    _groupName = map["group_name"].toString();
    _dataToDrag = map;
}

void GTeamEdit::setRemovableActions(bool removableActions)
{
    _removableActions = removableActions;
}

void GTeamEdit::selectText(const QString &text)
{
    if (!text.isEmpty() && this->text().toLower().contains(text.toLower())){
        selectAll();
    }
    else {
        deselect();
    }
}

void GTeamEdit::textEditation()
{
    if(contentChanged()){
        emit teamChangeRequested(_dataToDrag, text(), _textContent);
    }
    setReadOnly(true);
}
