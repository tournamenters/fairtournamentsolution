<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ">
<context>
    <name>AView</name>
    <message>
        <location filename="aview.cpp" line="11"/>
        <source>Abstract</source>
        <translation>Abstraktní</translation>
    </message>
</context>
<context>
    <name>GFieldsView</name>
    <message>
        <location filename="gfieldsview.cpp" line="17"/>
        <source>Fields</source>
        <translation>Hřiště</translation>
    </message>
</context>
<context>
    <name>GNotesView</name>
    <message>
        <location filename="gnotesview.cpp" line="13"/>
        <source>Notes</source>
        <translation>Poznámky</translation>
    </message>
    <message>
        <location filename="gnotesview.cpp" line="46"/>
        <source>Write your notes here...</source>
        <translation>Zde můžete psát poznámky...</translation>
    </message>
</context>
<context>
    <name>GStartListView</name>
    <message>
        <location filename="gstartlistview.cpp" line="42"/>
        <source>Start List Editation</source>
        <translation>Editování startovní listiny</translation>
    </message>
</context>
<context>
    <name>GStatsView</name>
    <message>
        <location filename="gstatsview.cpp" line="13"/>
        <source>Information</source>
        <translation>Informace</translation>
    </message>
    <message>
        <location filename="gstatsview.cpp" line="35"/>
        <source>Number of participants:</source>
        <translation>Počet účastníků:</translation>
    </message>
    <message>
        <location filename="gstatsview.cpp" line="36"/>
        <source>Tournament: </source>
        <translation>Turnaj: </translation>
    </message>
    <message>
        <location filename="gstatsview.cpp" line="37"/>
        <source>Played Matches / Total Matches:</source>
        <translation>Odehrané zápasy/Všechny zápasy:</translation>
    </message>
    <message>
        <location filename="gstatsview.cpp" line="46"/>
        <source>Tournament progress: %p%</source>
        <translation>Průběh turnaje: %p%</translation>
    </message>
</context>
</TS>
