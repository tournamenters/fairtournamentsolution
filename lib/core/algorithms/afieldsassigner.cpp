#include "afieldsassigner.h"
#include <QSet>
#include <algorithm>

AFieldsAssigner::AFieldsAssigner()
{

}

AFieldsAssigner::~AFieldsAssigner()
{
    _fields.clear();
}

int AFieldsAssigner::fieldsCount()
{
    return _fields.size();
}

QList<QList<QList<int> > > AFieldsAssigner::getFieldsPlan()
{
    return _fields;
}

int AFieldsAssigner::getNumberOfUniqueTeams(const QList<QList<int> > &matches)
{
    QSet<int> set;
    for (const auto &match : matches) {
        for(int teamIndex : match){
            set.insert(teamIndex);
        }
    }
    return set.size();
}

void AFieldsAssigner::initializeFields(int numFields, int numTeams)
{
    numFields = qMin(numFields, numTeams/2);
    _fields.clear();
    for(int i = 0; i< numFields; i++){
        _fields.push_back({});
    }
}

bool AFieldsAssigner::canBePlaced(int fieldIndex, const QList<int> &match)
{
    int round = _fields[fieldIndex].size();
    for(const auto& field : _fields){
        for(int team : match){
            if(field.size() > round && field[round].contains(team)){
                return false;
            }
        }
    }
    return true;
}

void AFieldsAssigner::placeMatch(int fieldIndex, const QList<int> &match)
{
    _fields[fieldIndex].push_back(match);
}

QList<int> AFieldsAssigner::findPossibleFields()
{
    QList<int> possibleFieldsIndexes;
    int minSize = INT_MAX;
    for(const auto& field : _fields){
        if(field.size()<minSize){
            minSize = field.size();
        }
    }
    for(int i = 0; i<_fields.size();i++){
        if(_fields[i].size()==minSize){
            possibleFieldsIndexes.push_back(i);
        }
    }
    return possibleFieldsIndexes;
}

void AFieldsAssigner::roundFields()
{
    int maxSize = maxFieldSize();
    for(int i = 0; i<_fields.size(); i++){
        while(_fields[i].size()< maxSize){
            _fields[i].push_back({});
        }
    }
}

int AFieldsAssigner::maxFieldSize()
{
    int maxSize = 0;
    for(const auto& field : _fields){
        if(field.size() > maxSize){
            maxSize = field.size();
        }
    }
    return maxSize;
}

int AFieldsAssigner::countTeamsInField(int fieldIndex, const QList<int> &teams)
{
    int count = 0;
    for (const auto& match : _fields[fieldIndex]) {
        for (int team : teams) {
            count += match.count(team);
        }
    }
    return count;
}

QList<QList<int> > AFieldsAssigner::takeMatchesRound(QList<QList<int> > &matches)
{
    QList<QList<int> > actualRound;
    for(int i=0;i<_fields.size() && !matches.isEmpty();i++){
        actualRound.push_back(matches.takeFirst());
    }
    return actualRound;
}

QList<int> AFieldsAssigner::getLastMatch(int fieldIndex)
{
    if(_fields[fieldIndex].isEmpty()){
        return {};
    }
    return _fields[fieldIndex].last();
}

QList<int> AFieldsAssigner::getMatch(int fieldIndex, int indexFromBehind)
{
    if(_fields[fieldIndex].size()<indexFromBehind+1){
        return {};
    }
    return _fields[fieldIndex][_fields[fieldIndex].size()-indexFromBehind];
}

LFieldsAssignerSimple::LFieldsAssignerSimple()
{

}

LFieldsAssignerSimple::~LFieldsAssignerSimple()
{

}

QList<QList<QList<int> > > LFieldsAssignerSimple::planFields(const QList<QList<int> > &matchesInOrder, int numFields)
{
    initializeFields(numFields, getNumberOfUniqueTeams(matchesInOrder));
    createFieldsPlan(matchesInOrder);
    return getFieldsPlan();
}

QList<QList<QList<int> > > LFieldsAssignerSimple::addMatchesToPlan(const QList<QList<int> > &matchesInOrder)
{
    roundFields();
    createFieldsPlan(matchesInOrder);
    return getFieldsPlan();
}

AFieldsAssigner *LFieldsAssignerSimple::create()
{
    return new LFieldsAssignerSimple();
}

void LFieldsAssignerSimple::createFieldsPlan(const QList<QList<int> > &matchesInOrder)
{
    QList<QList<int>> matches = matchesInOrder;
    while(!matches.isEmpty()){
        QList<QList<int>> roundMatches = takeMatchesRound(matches);
        int fieldIndex = 0;
        while(!roundMatches.isEmpty()){
            if(canBePlaced(fieldIndex, roundMatches.first())){
                placeMatch(fieldIndex, roundMatches.takeFirst());
                fieldIndex++;
            }
            else{
                while(!roundMatches.isEmpty()){
                    matches.push_front(roundMatches.takeLast());
                }
            }
        }
        roundFields();
    }
}

LFieldsAssignerMinTransitions::LFieldsAssignerMinTransitions()
{

}

LFieldsAssignerMinTransitions::~LFieldsAssignerMinTransitions()
{

}

QList<QList<QList<int> > > LFieldsAssignerMinTransitions::planFields(const QList<QList<int> > &matchesInOrder, int numFields)
{
    initializeFields(numFields, getNumberOfUniqueTeams(matchesInOrder));
    createFieldsPlan(matchesInOrder);
    return getFieldsPlan();
}

QList<QList<QList<int> > > LFieldsAssignerMinTransitions::addMatchesToPlan(const QList<QList<int> > &matchesInOrder)
{
    roundFields();
    createFieldsPlan(matchesInOrder);
    return getFieldsPlan();
}

AFieldsAssigner *LFieldsAssignerMinTransitions::create()
{
    return new LFieldsAssignerMinTransitions();
}

void LFieldsAssignerMinTransitions::createFieldsPlan(const QList<QList<int> > &matchesInOrder)
{
    QList<QList<int>> matches = matchesInOrder;
    while(!matches.isEmpty()){
        QList<QList<int>> roundMatches = takeMatchesRound(matches);
        QList<int> possibilities = findPossibleFields();
        while(!roundMatches.isEmpty()){
            possibilities = sortOptions(roundMatches.first(), possibilities);
            if(tryToPlaceMatchRight(roundMatches.first(), possibilities)){
                roundMatches.removeFirst();
            }
            else{
                while(!roundMatches.isEmpty()){
                    matches.push_front(roundMatches.takeLast());
                }
            }
        }
        roundFields();
    }
}

QList<int> LFieldsAssignerMinTransitions::sortOptions(const QList<int> &match, const QList<int> &possibilities)
{
    QList<int> fieldIndexes = possibilities;
    QList<int> orderedPosibilities;
    for (int deep = 1; deep <= maxFieldSize(); deep++){
        for (int repeat = 2; repeat > 0; repeat--){
            for (int fieldIndex : possibilities) {
                int matchRepeat = 0;
                for (int team : match) {
                    matchRepeat += getMatch(fieldIndex, deep).contains(team);
                }
                if(matchRepeat == repeat && !orderedPosibilities.contains(fieldIndex)){
                    orderedPosibilities.push_back(fieldIndex);
                    if(orderedPosibilities.size() == possibilities.size()){
                        return orderedPosibilities;
                    }
                }
            }
        }
    }
    for (int fieldIndex : possibilities) {
        if(!orderedPosibilities.contains(fieldIndex)){
            orderedPosibilities.push_back(fieldIndex);
        }
    }
    return orderedPosibilities;
}

bool AFieldsAssigner::tryToPlaceMatchRight(const QList<int> &match, QList<int> &possibilities)
{
    for(int fieldIndex : possibilities){
        if(canBePlaced(fieldIndex, match)){
            placeMatch(fieldIndex, match);
            possibilities.removeOne(fieldIndex);
            return true;
        }
    }
    return false;
}

LFieldsAssignerMaxChangeovers::LFieldsAssignerMaxChangeovers()
{

}

LFieldsAssignerMaxChangeovers::~LFieldsAssignerMaxChangeovers()
{
    _teamsNumbersInFields.clear();
}

QList<QList<QList<int> > > LFieldsAssignerMaxChangeovers::planFields(const QList<QList<int> > &matchesInOrder, int numFields)
{
    int numTeams = getNumberOfUniqueTeams(matchesInOrder);
    initializeFields(numFields, numTeams);
    initializeTeamsNumbers(matchesInOrder);
    createFieldsPlan(matchesInOrder);
    return getFieldsPlan();
}

QList<QList<QList<int> > > LFieldsAssignerMaxChangeovers::addMatchesToPlan(const QList<QList<int> > &matchesInOrder)
{
    roundFields();
    updateTeamsNumbers(matchesInOrder);
    createFieldsPlan(matchesInOrder);
    return getFieldsPlan();
}

AFieldsAssigner *LFieldsAssignerMaxChangeovers::create()
{
    return new LFieldsAssignerMaxChangeovers();
}

void LFieldsAssignerMaxChangeovers::createFieldsPlan(const QList<QList<int> > &matchesInOrder)
{
    QList<QList<int>> matches = matchesInOrder;
    while(!matches.isEmpty()){
        QList<QList<int>> roundMatches = takeMatchesRound(matches);
        QList<int> possibilities = findPossibleFields();
        while(!roundMatches.isEmpty()){
            possibilities = sortPosibilities(roundMatches.first(), possibilities);
            if(tryPlaceMatch(roundMatches.first(), possibilities)){
                roundMatches.removeFirst();
            }
            else{
                while(!roundMatches.isEmpty()){
                    matches.push_front(roundMatches.takeLast());
                }
            }
        }
        roundFields();
    }
}

QList<int> LFieldsAssignerMaxChangeovers::orderOptions(const QList<int> &match, const QList<int> &possibilities)
{
    QList<int> orderedPosibilities;
    for (int repetition = 0; repetition<=maxFieldSize();repetition++) {
        for (int fieldIndex : possibilities) {
            int actualRepetition = countTeamsInField(fieldIndex, match);
            if (repetition == actualRepetition || actualRepetition > maxFieldSize()){
                 orderedPosibilities.push_back(fieldIndex);
             }
        }
    }
    for (int fieldIndex : possibilities) {
        if(!orderedPosibilities.contains(fieldIndex)){
            orderedPosibilities.push_back(fieldIndex);
        }
    }
    return orderedPosibilities;
}

void LFieldsAssignerMaxChangeovers::initializeTeamsNumbers(const QList<QList<int> > &matchesInOrder)
{
    _teamsNumbersInFields.clear();
    for (const auto &match : matchesInOrder) {
        for(int teamIndex : match){
            _teamsNumbersInFields[teamIndex] = {};
            for (int fieldIndex = 0; fieldIndex < fieldsCount(); fieldIndex++) {
                _teamsNumbersInFields[teamIndex].push_back(QPair<int,int>(0, fieldIndex));
            }
        }
    }
}

void LFieldsAssignerMaxChangeovers::updateTeamsNumbers(const QList<QList<int> > &matchesInOrder)
{
    for (const auto &match : matchesInOrder) {
        for(int teamIndex : match){
            if(!_teamsNumbersInFields.contains(teamIndex)){
                _teamsNumbersInFields[teamIndex] = {};
                for (int fieldIndex = 0; fieldIndex < fieldsCount(); fieldIndex++) {
                    _teamsNumbersInFields[teamIndex].push_back(QPair<int,int>(0, fieldIndex));
                }
            }
        }
    }
}

void LFieldsAssignerMaxChangeovers::matchAdded(int fieldIndex, const QList<int> &match)
{
    for(int team : match){
        _teamsNumbersInFields[team][fieldIndex].first++;
    }
}

QList<int> LFieldsAssignerMaxChangeovers::sortPosibilities(const QList<int> &match, const QList<int> &possibilities)
{
    if(match.isEmpty()){
        return possibilities;
    }
    QList<QPair<int,int>> bothNumbers;
    for (int i : possibilities) {
        int hostCount = _teamsNumbersInFields[match[0]][i].first;
        int guestCount = _teamsNumbersInFields[match[1]][i].first;
        bothNumbers.push_back(QPair<int,int>(hostCount+guestCount,i));
    }
    QList<int> orderedPossibilities;
    std::sort (bothNumbers.begin(), bothNumbers.end());
    for (const auto& pair : bothNumbers) {
        orderedPossibilities.push_back(pair.second);
    }
    return orderedPossibilities;
}

bool LFieldsAssignerMaxChangeovers::tryPlaceMatch(const QList<int> &match, QList<int> &possibilities)
{
    for(int fieldIndex : possibilities){
        if(canBePlaced(fieldIndex, match)){
            placeMatch(fieldIndex, match);
            possibilities.removeOne(fieldIndex);
            matchAdded(fieldIndex, match);
            return true;
        }
    }
    return false;
}
