#include "crobinround.h"
#include "cteam.h"
#include "cmatch.h"
#include "cresult.h"
#include "ateamssorter.h"
#include "global_model.h"

CRobinRound::CRobinRound(int id, const QString &name, LTeamsSorter *orderer, AMatchesOrderer *drawer, QObject *parent)
    : AStageModel(id, StageType::RobinRound, parent)
{
    setName(name);
    _planner = drawer;
    _sorter = orderer;
}

CRobinRound::~CRobinRound()
{
    delete _sorter;
    delete _planner;
    _order.clear();
    _matchesMatrix.clear();
    qDeleteAll(_matches);
    _matches.clear();
}

void CRobinRound::setTeamsCount(int numTeams)
{
    AStageModel::setTeamsCount(numTeams);
    createMatches(numTeams);
    resetOrder(numTeams);
}

void CRobinRound::setTeams(const QList<CTeam *> &teams)
{
    setTeams(new CTeamsList(teams));
}

void CRobinRound::setTeams(CTeamsList *teams)
{
    _teams->changeTeams(teams);
    placeTeamsToMatches(_teams);
}

void CRobinRound::setTeamsFromSources(const QList<CTeamsList *> &teams)
{
    _teams->changeTeams(teams);
    placeTeamsToMatches(_teams);
}

void CRobinRound::setNextStage(AStageModel *nextStage)
{
    _nextStage = nextStage;
    if(_nextStage){
        nextStageCare();
    }
}

void CRobinRound::setTeamsFromVariant(const QVariant &variant, const QMap<int, CTeam *> &teamsById)
{
    QList<CTeam*> teams;
    for(const auto& teamVariant : variant.toMap()["teams"].toList()){
        if(!teamVariant.toMap().isEmpty()){
            teams.push_back(teamsById[teamVariant.toMap()["id"].toInt()]);
        }
        else{
            teams.push_back(nullptr);
        }
    }
    setTeams(new CTeamsList(teams));
}

void CRobinRound::resultsFromVariant(const QVariant &variant)
{
    QVariantMap map = variant.toMap();
    for(int index = 0; index<_matches.size();index++){
        _matches[index]->setResult(map["matches"].toList()[index].toMap()["result"].toString());
    }
    _order.clear();
    for(const auto& string : map["order"].toStringList()){
        _order.push_back(string.toInt());
    }
}

void CRobinRound::changeTeam(const QVariant &identificator, CTeam *team)
{
    if(identificator.toMap().contains("row")){
        int row = identificator.toMap()["row"].toInt();
        if(row<_teams->getTeams().size()){
            CTeam * old = _teams->getTeams()[row];
            _teams->changeTeam(row, team);
            for(const auto match : _matches){
                if(match && match->isIn(old)){
                    match->setTeam(team, match->isHost(old));
                }
            }
            emit dataChanged();
        }
    }
}

void CRobinRound::setResult(int row, int col, const QString &result)
{
    if(row < teamsCount() && col < teamsCount() && _matchesMatrix[row][col]){
        if(_matchesMatrix[row][col]->isHost(_teams->getTeams()[row])){
            _matchesMatrix[row][col]->setResult(result);
        }
        else{
            _matchesMatrix[row][col]->setResultReversed(result);
        }
    }
}

void CRobinRound::nextStageCare()
{
    if (_nextStage){
        int proceedingTeamsNumber = _nextStage->teamsCount();
        CTeamsList* results = countStandings();
        CTeamsList* groupProceedingTable = new CTeamsList();
        groupProceedingTable->setColumnHeaders(results->getColumnHeaders());
        QList<QString> placeHolders;
        for (int i = 0; i<results->getTeams().size() && i< proceedingTeamsNumber;i++){
            groupProceedingTable->addRow(results->getRow(i));
            if(!finished()){
                groupProceedingTable->changeTeam(i, nullptr);
            }
            placeHolders.push_back(getName() + " " + QString::number(i+1));
        }
        groupProceedingTable->addColumn("place_holder", placeHolders);
        _nextStage->setTeams(groupProceedingTable);
        delete results;
    }
}

void CRobinRound::setOrder(int oldRank, const QList<int>& newOrder)
{
    if(newOrder.isEmpty()){
        recountOrder();
        return;
    }
    int newIndex = 0;
    for(int i = 0; i < _order.size();i++){
        if(_order[i] == oldRank){
            _order[i] = newOrder[newIndex] + oldRank - 1;
            newIndex++;
        }
    }
    nextStageCare();
    emit dataChanged();
}

void CRobinRound::resetOrder(int numTeams)
{
    _order.clear();
    for(int i = 0; i<numTeams;i++){
        _order.push_back(1);
    }
}

void CRobinRound::resetMatchesMatrix()
{
    _matchesMatrix.clear();
    for(int i = 0; i<teamsCount();i++){
        _matchesMatrix.push_back({});
        for(int j = 0; j<teamsCount();j++){
            _matchesMatrix.last().push_back(nullptr);
        }
    }
}

CTeam *CRobinRound::findTeam(const QVariantMap &identificator) const
{
    if(identificator.contains("row")){
        int row = identificator["row"].toInt();
        if(row<_teams->getTeams().size()){
            return _teams->getTeams()[row];
        }
    }
    return nullptr;
}

QPair<int, int> CRobinRound::findNextMatch() const
{
    CMatch* match = getNextMatchToPlay();
    if(match != nullptr){
        for (int row = 0; row<_matchesMatrix.size();row++) {
            for (int col = 0; col<_matchesMatrix.size();col++) {
                if(_matchesMatrix[row][col] == match){
                    return QPair<int,int>(row, col);
                }
            }
        }
    }
    return QPair<int,int>(-1, -1);
}

bool CRobinRound::isMatch(int row, int col) const
{
    if(row<_matchesMatrix.size() && col<_matchesMatrix[row].size()){
        return _matchesMatrix[row][col];
    }
    return false;
}

QVariant CRobinRound::createTableVariant() const
{
    QVariantMap map;
    if(sameRank() && finished()){
        QVariantMap uniqueMap;
        uniqueMap["header"] = tr("Order");
        uniqueMap["value"] = sameRank();
        map["unique_needed"] = uniqueMap;
    }
    if(!finished()){
        QPair<int,int> rowCol = findNextMatch();
        QVariantMap nextMatch;
        nextMatch["row"] = rowCol.first;
        nextMatch["col"] = rowCol.second + 1;
        map["next_match"] = nextMatch;
    }
    map["rows"] = createTableRows();
    map["greys"] = createTableGreys();
    map["keys"] = createTableHeaders();
    return map;
}

QVariant CRobinRound::getMiniTable(int order) const
{
    CRobinRound miniTable(getId(), tr("Same Order Table"), _sorter->copy(), _planner->copy());
    auto teams = getTeamsAtOrder(order);
    miniTable.setTeamsCount(teams.size());
    miniTable.setTeams(teams);
    int newRow = 0;
    for(int row = 0; row<teamsCount();row++){
        if(_order.size()>row && _order[row]==order){
            int newColumn = newRow + 1;
            for(int col = row+1; col<teamsCount();col++){
                if(_order.size()>col && _order[col]==order){
                    if(isMatch(row,col)){
                        miniTable.setResult(newRow, newColumn, _matchesMatrix[row][col]->getResult());
                    }
                    newColumn++;
                }
            }
            newRow++;
        }
    }
    return miniTable.createTableVariant();
}

QStringList CRobinRound::createTableHeaders() const
{
    QStringList headers;
    headers.push_back(getName());
    for (int i = 1; i<=teamsCount(); i++) {
        headers.push_back(QString::number(i));
    }
    headers.push_back(tr("Points"));
    headers.push_back(tr("Score"));
    headers.push_back(tr("Order"));
    return headers;
}

QVariantList CRobinRound::createTableRows() const
{
    QStringList points = toStringList(_sorter->getPoints());
    QVariantList score = toVariantList(_sorter->getScore());
    QVariantList rows;
    for(int row = 0; row < teamsCount();row++){
        QVariantMap rowData;
        if(_teams->getTeams()[row]){
            rowData[getName()] = _teams->getTeams()[row]->toString();
        }
        else{
            rowData[getName()] = _teams->getColumn("place_holder")[row];
        }
        for (int col = 0; col < teamsCount(); col++) {
            if(isMatch(row, col)){               
                if(_matchesMatrix[row][col]->isHost(_teams->getTeams()[row])){
                    rowData[QString::number(col+1)] = _matchesMatrix[row][col]->getResult();
                }
                else{
                    rowData[QString::number(col+1)] = _matchesMatrix[row][col]->getResultReversed();
                }
            }
        }
        if(score.size()>row){
            rowData[tr("Score")] = score[row].toString();
        }
        if(points.size()>row){
            rowData[tr("Points")] = points[row];
        }
        if(_order.size()>row){
            rowData[tr("Order")] = _order[row];
        }
        rows.push_back(rowData);
    }
    return rows;
}

QVariantList CRobinRound::createTableGreys() const
{
    QVariantList rows;
    for(int row = 0; row < teamsCount();row++){
        rows.push_back(!static_cast<bool>(_teams->getTeams()[row]));
    }
    return rows;
}

QVariant CRobinRound::getData() const
{
    QVariantMap map;
    map["matches_list"] = matchesToTable(false);
    map["teams"] = _teams->teamsToVariant();
    map["table"] = createTableVariant();
    if(finished() && sameRank()){
        map["draw_request"] = sameRank();
        map["mini_table"] = getMiniTable(sameRank());
    }
    return map;
}

QVariant CRobinRound::toVariant() const
{
    QVariantMap mapParameters;
    mapParameters["num_teams"] = teamsCount();
    QVariantMap map;
    map["parameters"] = mapParameters;
    map["name"] = getName();
    map["type"] = key_ROBIN_ROUND;
    map["order"] = toVariantList(_order);
    map["points"] = toVariantList(_sorter->getPoints());
    map["score"] = toVariantList(_sorter->getScore());
    map["matches"] = matchesToVariant();
    map["teams"] = _teams->teamsToVariant();
    return map;
}

void CRobinRound::createMatches(int numTeams)
{
    qDeleteAll(_matches);
     _matches.clear();
    QList<QList<int>>intMatchesList = _planner->planMatches(numTeams);
    QStringList placeHolders = _teams->getColumn("place_holder");
    resetMatchesMatrix();
    for(const auto& intMatch : intMatchesList){
        if(!intMatch.isEmpty()){
            CMatch* match = new CMatch(getId(), this);
            match->setIndexes(intMatch[0], intMatch[1]);
            match->setSourceName(getName());
            match->setPlaceHolderHost(placeHolders.at(intMatch[0]));
            match->setPlaceHolderGuest(placeHolders.at(intMatch[1]));
            connect(match, SIGNAL(resultUpdated()),
                    this, SLOT(recountOrder()));
            _matches.push_back(match);
            _matchesMatrix[intMatch[0]][intMatch[1]] = match;
            _matchesMatrix[intMatch[1]][intMatch[0]] = match;
        }
    }
}

void CRobinRound::updateMatches(const QList<QList<int> > &matches)
{
    resetMatchesMatrix();
    int round = 0;
    for(const auto& intMatch : matches){
        if(round<_matches.size() && !intMatch.isEmpty()){
            _matches[round]->setIndexes(intMatch[0], intMatch[1]);
            _matchesMatrix[intMatch[0]][intMatch[1]] = _matches[round];
            _matchesMatrix[intMatch[1]][intMatch[0]] = _matches[round];
        }
        round++;
    }
}

void CRobinRound::placeTeamsToMatches(CTeamsList *teams)
{
    auto matchIndexes = _planner->planMatches(teamsCount());
    int row = 0;
    blockSignals(true);
    for (auto match : _matches) {
        auto rowHost = teams->getRow(matchIndexes[row][0]);
        match->setHost(rowHost.first);
        match->setPlaceHolderHost(rowHost.second["place_holder"].toString());

        auto rowGuest = teams->getRow(matchIndexes[row][1]);
        match->setGuest(rowGuest.first);
        match->setPlaceHolderGuest(rowGuest.second["place_holder"].toString());
        row++;
    }
    blockSignals(false);
    emit dataChanged();
}

CTeamsList *CRobinRound::countStandings()
{
    QList<CTeam*> myTeams = _teams->getTeams();
    QList<QPair<int,int>> bothNumbers;
    for (int i = 0; i< myTeams.size(); i++) {
        bothNumbers.push_back(QPair<int,int>(_order[i], i));
    }
    std::sort(bothNumbers.begin(), bothNumbers.end());
    QList<QPair<int,int>> scoreList = _sorter->getScore();
    QList<int> points = _sorter->getPoints();
    CTeamsList * teamsTable = new CTeamsList();
    teamsTable->setColumnHeaders({"order", "source", "order_is_final", "points", "my_goals", "others_goals"});
    for (const auto& pair : bothNumbers) {
        QVariantMap teamData;
        teamData["order"] = pair.first;
        teamData["source"] = getName();
        teamData["order_is_final"] = finished() && _order.count(pair.first)==1;
        if(points.size() > pair.second){
            teamData["points"] = points[pair.second];
        }
        if(scoreList.size() > pair.second){
            teamData["my_goals"] = scoreList[pair.second].first;
            teamData["others_goals"] = scoreList[pair.second].second;
        }
        teamsTable->addRow(myTeams[pair.second], teamData);
    }
    return teamsTable;
}

CTeam *CRobinRound::getTeam(const QVariant &identificator) const
{
    if(isItForMe(identificator)){
        return findTeam(identificator.toMap());
    }
    return nullptr;
}

int CRobinRound::sameRank() const
{
    QList<int> list = _order;
    for (int value : _order) {
        if(_order.count(value)>1){
            return value;
        }
    }
    return 0;
}

bool CRobinRound::setResult(const QVariant &identificator, const QString& result)
{
    if(isItForMe(identificator)){
        if(identificator.toMap().contains("row") && identificator.toMap().contains("col")){
            int row = identificator.toMap()["row"].toInt();
            int col = identificator.toMap()["col"].toInt() - 1;
            setResult(row, col, result);
            return true;
        }
    }
    return false;
}

bool CRobinRound::execEdit(ExecType type, const QVariant &identificator, const QVariant &data)
{
    if(isItForMe(identificator)){
        switch (type) {
        case ExecType::Swap:
            return swapTeams(data.toMap());
        case ExecType::Rename:
            return _teams->renameTeam(identificator, data);
        case ExecType::Order:
            return setOrder(data.toMap());
        default:
            return false;
        }
    }
    return false;
}

void CRobinRound::recountOrder()
{    
    QList<int> newOrder = _sorter->sortTeams(getTeamsIndexes(), toSortererMatches());
    if(newOrder != _order){
        int size = newOrder.size();
        while(newOrder.size()<_order.size()){
            newOrder.push_back(size);
        }
        _order = newOrder;
        nextStageCare();
    }
    emit dataChanged();
}

bool CRobinRound::swapTeams(const QVariantMap &data)
{
    if(!data.contains("old")|| !data.contains("new")){
        return false;
    }
    QVariantMap firstData = data["old"].toMap();
    QVariantMap secondData = data["new"].toMap();
    CTeam* first = findTeam(firstData);
    CTeam* second = findTeam(secondData);
    if(first && second && first == second){
        blockSignals(true);
        changeTeam(firstData, second);
        changeTeam(secondData, first);
        blockSignals(false);
        emit dataChanged();
        return true;
    }
    return false;
}

bool CRobinRound::setOrder(const QVariantMap &data)
{
    if(data.contains("entered_values") && data.contains("value")){
        QVariantList newOrder = data.value("entered_values").toList();
        QList<int> newOrderInt;
        for(const auto& variant : newOrder){
            newOrderInt.push_back(variant.toInt());
        }
        setOrder(data.value("value").toInt(), newOrderInt);
        return true;
    }
    return false;
}

QStringList CRobinRound::toStringList(const QList<int>& list) const
{
    QStringList returnList;
    for (auto const integer : list){
        returnList.push_back(QString::number(integer));
    }
    return returnList;
}

QVariantList CRobinRound::toVariantList(const QList<int> &list) const
{
    QVariantList returnList;
    for (auto const integer : list){
        returnList.push_back(integer);
    }
    return returnList;
}

QVariantList CRobinRound::toVariantList(const QList<QPair<int, int> >& list) const
{
    QVariantList returnList;
    for (auto const& pair : list){
        returnList.push_back(QString::number(pair.first) + " : " + QString::number(pair.second));
    }
    return returnList;
}

QList<int> CRobinRound::getTeamsIndexes() const
{
    QList<int> list;
    for (const auto team : _teams->getTeams()) {
        if(team){
            list.push_back(team->getID());
        }
    }
    return list;
}

QList<CTeam *> CRobinRound::getTeamsAtOrder(int order) const
{
    QList<CTeam*> helpTeams;
    int row = 0;
    for(CTeam*team : _teams->getTeams()){
        if(_order.size()>row && _order[row] == order){
            helpTeams.push_back(team);
        }
        row++;
    }
    return helpTeams;
}
