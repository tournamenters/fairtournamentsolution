#ifndef GIMAGESEXPORTER_H
#define GIMAGESEXPORTER_H

#include <QWidget>

class QGroupBox;
class QLineEdit;

class AExportable;
class GExportDialog;

class GImagesExporter : public QWidget
{
    Q_OBJECT

public:
    GImagesExporter(QList<AExportable*> exportables, QWidget *parent = nullptr);
    ~GImagesExporter();
    void exportFiles();

public slots:
    void browse();

private:
    QGroupBox * createPathBox();

    void saveChoosenExportables();
    void saveExportable(AExportable *exportable, const QString& folderPath);
    void saveText(const QString &folderPath, const QString &fileName, const QString &data);
    void savePicture(const QString &folderPath, const QString &fileName, const QPixmap &data);
    void createFolder(const QString& path);
    void showErrorMessage(const QString& fileName);

    QString findFreeName(const QString &fileName);
    QString createFileName(const QString& title);

    void writeSetting();
    void readSetting();

    QLineEdit* lePath;
    GExportDialog* gedExport;
    QList<AExportable*> _exportables;
};

#endif // GIMAGESEXPORTER_H
