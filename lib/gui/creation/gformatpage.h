#ifndef GFORMATPAGE_H
#define GFORMATPAGE_H

#include <QWizardPage>
#include <QMap>
#include <QVariant>

class QLabel;
class QCheckBox;
class QGroupBox;
class QRadioButton;
class QLayout;

class GFormatPage : public QWizardPage
{
    Q_OBJECT
    Q_PROPERTY(QString _preparedType READ preparedType WRITE setPreparedType NOTIFY preparedTypeChanged)
    Q_PROPERTY(bool _formatPrepared READ formatPrepared WRITE setFormatPrepared NOTIFY formatCreationChanged)

public:
    GFormatPage(QWidget *parent = nullptr);
    ~GFormatPage() override;

    int nextId() const override;

    bool formatPrepared() const;
    void setFormatPrepared(bool formatPrepared);

    QString preparedType() const;
    void setPreparedType(const QString &preparedType);

protected:
    void initializePage() override;

signals:
    void preparedTypeChanged();
    void formatCreationChanged();

private slots:
    void checkChange();
    void changePreparedType();

private:
    QLabel* createTitle();
    QGroupBox* createPreparedGroupBox();
    QCheckBox* createCustomCheckBox();
    QCheckBox* createGenerateCheckBox();
    QLayout* createPreparedLayout();
    QRadioButton *createPreparedRadButton(const QString& title);
    void setDefaultPreparedFormat();

    QCheckBox* customFormat;
    QGroupBox* preparedFormatsBox;
    QCheckBox* generateFormat;
    QMap<QString, QRadioButton*> _preparedFormats;

    QString _preparedType;
    bool _formatPrepared;
    bool _firstVisit;

};

#endif // GFORMATPAGE_H
