#ifndef CNOTES_H
#define CNOTES_H
#include "amodel.h"

class CNotes: public AModel
{
    Q_OBJECT

public:
    explicit CNotes(QObject * parent = nullptr);
    QVariant getData() const override;
    QVariant toVariant() const;
    void fromVariant(const QVariant& variant);

public slots:
    bool execEdit(ExecType type, const QVariant& identificator, const QVariant& data) override;

private:
    QString _notes;
};

#endif // CNOTES_H
