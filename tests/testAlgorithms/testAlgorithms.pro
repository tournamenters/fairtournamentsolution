include(gtest_dependency.pri)
include($$PWD/../../lib/core/algorithms/algorithms.pri)

TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG += thread

CONFIG += testcase

TARGET = testAlgorithms

HEADERS += \
        tst_flieldsplanner.h \
        tst_groupscreation.h \
        tst_playoffcreation.h \
        tst_robinroundmatchesorder.h

SOURCES += \
        main.cpp
