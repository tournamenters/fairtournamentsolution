#-------------------------------------------------
#
# Project created by QtCreator 2019-04-30T21:30:49
#
#-------------------------------------------------

TARGET = creation
TEMPLATE = lib

QT += widgets
QT += core
QT += gui

CONFIG -= app_bundle
CONFIG += staticlib
CONFIG += c++11

DEFINES += CREATION_LIBRARY
DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    gnewdialog.cpp \
    gtabstageseditor.cpp \
    gformatpage.cpp \
    gstartlistpage.cpp \
    gstagespage.cpp \
    gfieldspage.cpp \
    gintropage.cpp \
    gconclutionpage.cpp \
    gstagetabsfactory.cpp \
    gstageeditortabs.cpp \
    gpointssettingbox.cpp

HEADERS += \
    gnewdialog.h \
    gtabstageseditor.h \
    gformatpage.h \
    gstartlistpage.h \
    gstagespage.h \
    gfieldspage.h \
    gintropage.h \
    gconclutionpage.h \
    global_creation.h \
    gstagetabsfactory.h \
    gstageeditortabs.h \
    gpointssettingbox.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

include($$PWD/../tournamentwidgets/tournamentwidgets.pri)
include($$PWD/../../core/core.pri)

DISTFILES += \
    creation.pri

RESOURCES += \
    icons.qrc

TRANSLATIONS += \
    creation_cs.ts
