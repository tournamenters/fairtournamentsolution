#include "ggroupstageview.h"
#include "grobinroundview.h"
#include "astagemodel.h"
#include <QSplitter>
#include <QList>
#include <QColor>
#include <QPainter>
#include "aexportable.h"

GGroupStageView::GGroupStageView(AStageModel *model, QWidget *parent)
    : AStageView(parent)
{
    _title = model->getName();
    _model = model;
    _exportable = nullptr;
    createGroupsView();
}

GGroupStageView::~GGroupStageView()
{
    if(_exportable){
        delete _exportable;
    }
    qDeleteAll(_groupViews);
}

AStageView *GGroupStageView::create(AStageModel *model)
{
    return new GGroupStageView(model);
}

QWidget *GGroupStageView::getViewWidget()
{
    if(!splGroups){
        return createViewWidget();
    }
    return splGroups;
}

AExportable *GGroupStageView::getExportableWidget()
{
    if(!_exportable){
        QList<AExportable*> parts;
        for(const auto group : _groupViews){
            parts.push_back(group->getExportableWidget());
        }
        _exportable = new GComplexExportable(parts);
    }
    _exportable->setTitle(_title);
    return _exportable;
}

void GGroupStageView::findText(const QString &text)
{
    for(const auto group : _groupViews){
        group->findText(text);
    }
}

void GGroupStageView::update()
{
    for(const auto& group : _groupViews){
        group->update();
    }
}

QList<QColor> GGroupStageView::generateColorsList(int size)
{
    QList<QColor> colors;
    int colorAngle = 0;
    int moveAngle = 360/(1+size);
    for(int i=0; i<=size;i++){
        if(i % 2){
            colors.push_back(QColor::fromHsv(colorAngle,255,255,70));
        }
        else{
            colors.push_front(QColor::fromHsv(colorAngle,255,255,70));
        }
        colorAngle += moveAngle;
    }
    return colors;
}

void GGroupStageView::createGroupsView()
{
    QList<AStageModel*> models = _model->getSubStages();
    int numGroups = models.size();
    QList<QColor> colorsList = generateColorsList(numGroups);
    for(int i = 0;i<numGroups;i++){
        GRobinRoundView * view = new GRobinRoundView(models[i]);
        view->setHighlightColor(colorsList[i]);
        connect(view, SIGNAL(resultEntered(QVariant, QString, QString)),
                this, SIGNAL(resultEntered(QVariant, QString, QString)));
        connect(view, SIGNAL(stateEdited(ExecType, QVariant,QVariant)),
                this, SIGNAL(stateEdited(ExecType, QVariant,QVariant)));
        _groupViews.push_back(view);
    }
}

QWidget *GGroupStageView::createViewWidget()
{
    splGroups = new QSplitter(Qt::Vertical);
    addEmptySpace(splGroups,1);
    for(const auto view : _groupViews){
        addWidget(splGroups, view->getViewWidget(),2);
        addEmptySpace(splGroups,1);
    }
    addEmptySpace(splGroups,6);
    return splGroups;
}
