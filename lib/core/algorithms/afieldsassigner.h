#ifndef AFIELDSASSIGNER_H
#define AFIELDSASSIGNER_H
#include<QList>

class AFieldsAssigner
{

public:
    AFieldsAssigner();
    virtual ~AFieldsAssigner();

    int fieldsCount();
    virtual QList<QList<QList<int>>> planFields(const QList<QList<int>>& matchesInOrder, int numFields)=0;
    QList<QList<QList<int>>> getFieldsPlan();
    virtual QList<QList<QList<int>>> addMatchesToPlan(const QList<QList<int>>& matchesInOrder) = 0;

protected:
    int getNumberOfUniqueTeams(const QList<QList<int> > &matches);

    void initializeFields(int numFields, int numTeams);
    bool canBePlaced(int fieldIndex, const QList<int>& match);
    void placeMatch(int fieldIndex, const QList<int>& match);
    bool tryToPlaceMatchRight(const QList<int>& match, QList<int>& possibilities);

    QList<int> findPossibleFields();
    void roundFields();
    int maxFieldSize();
    int countTeamsInField(int fieldIndex, const QList<int>& teams);

    QList<QList<int>> takeMatchesRound(QList<QList<int> > &matches);
    QList<int> getLastMatch(int fieldIndex);
    QList<int> getMatch(int fieldIndex, int indexFromBehind);

private:
    QList<QList<QList<int>>> _fields;
};

class LFieldsAssignerSimple: public AFieldsAssigner
{

public:
    LFieldsAssignerSimple();
    ~LFieldsAssignerSimple() override;

    QList<QList<QList<int>>> planFields(const QList<QList<int>>& matchesInOrder, int numFields) override;
    QList<QList<QList<int>>> addMatchesToPlan(const QList<QList<int>>& matchesInOrder) override;

    static AFieldsAssigner * __stdcall create();

private:
    void createFieldsPlan(const QList<QList<int>>& matchesInOrder);
};


class LFieldsAssignerMinTransitions: public AFieldsAssigner
{

public:
    LFieldsAssignerMinTransitions();
    ~LFieldsAssignerMinTransitions() override;

    QList<QList<QList<int>>> planFields(const QList<QList<int>>& matchesInOrder, int numFields) override;
    QList<QList<QList<int>>> addMatchesToPlan(const QList<QList<int>>& matchesInOrder) override;

    static AFieldsAssigner * __stdcall create();

private:
    void createFieldsPlan(const QList<QList<int>>& matchesInOrder);
    QList<int> sortOptions(const QList<int>& match, const QList<int>& possibilities);
};

class LFieldsAssignerMaxChangeovers: public AFieldsAssigner
{

public:
    LFieldsAssignerMaxChangeovers();
    ~LFieldsAssignerMaxChangeovers() override;

    QList<QList<QList<int>>> planFields(const QList<QList<int>>& matchesInOrder, int numFields) override;
    QList<QList<QList<int>>> addMatchesToPlan(const QList<QList<int>>& matchesInOrder) override;

    static AFieldsAssigner * __stdcall create();

private:
    void createFieldsPlan(const QList<QList<int>>& matchesInOrder);
    QList<int> orderOptions(const QList<int>& match, const QList<int>& possibilities);

    void initializeTeamsNumbers(const QList<QList<int> > &matchesInOrder);
    void updateTeamsNumbers(const QList<QList<int> > &matchesInOrder);
    void matchAdded(int numFields, const QList<int>& match);
    bool tryPlaceMatch(const QList<int> &match, QList<int> &possibilities);

    QMap<int, QList<QPair<int,int>>> _teamsNumbersInFields;
    QList<int> sortPosibilities(const QList<int>& match, const QList<int>& possibilities);
};


#endif // AFIELDSASSIGNER_H
