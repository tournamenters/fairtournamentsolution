#include "aexportable.h"

#include <QPainter>
#include <QPalette>
#include <QGuiApplication>
#include <QApplication>
#include <QClipboard>
#include <QMimeData>
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>
#include <QMenu>

AExportable::AExportable()
{
    _recomendedFormat = Format::PNG;
    _widget = new GExportHelperWidget();
}

AExportable::~AExportable()
{
    delete _widget;
}

QString AExportable::getTitle() const
{
    return _title;
}

QString AExportable::getSubTitle() const
{
    return _subTitle;
}

bool AExportable::hasMoreParts() const
{
    return false;
}

QList<AExportable *> AExportable::getIndividualParts() const
{
    return QList<AExportable *>();
}

void AExportable::saveToFormat(AExportable::Format format)
{
    QString data;
    switch (format) {
    case Format::CSV :
        _widget->openSaveDialog("csv", toCsv());
        break;
    case Format::TXT :
        _widget->openSaveDialog("txt", toPlainText());
        break;
    case Format::HTML :
        _widget->openSaveDialog("html", toHtml());
        return;
    case Format::PNG :
        _widget->openSavePictureDialog(toPixmap());
        return;
    }
    QApplication::clipboard()->setText(data, QClipboard::Clipboard);
}

void AExportable::copyToFormat(AExportable::Format format)
{
    QString data;
    switch (format) {
    case Format::CSV :
        data = toCsv();
        break;
    case Format::TXT :
        data = toPlainText();
        break;
    case Format::HTML :
        copyHtml();
        return;
    case Format::PNG :
        copyPicture();
        return;
    }
    QApplication::clipboard()->setText(data, QClipboard::Clipboard);
}

int AExportable::getRecomendedFormat() const
{
    return _recomendedFormat;
}

QString AExportable::qtAlignToHtml(int qtAlign)
{
    switch (qtAlign) {
    case Qt::AlignCenter :
        return "center";
    case Qt::AlignHCenter :
        return "center";
    case Qt::AlignLeft :
        return "left";
    case Qt::AlignRight :
        return "right";
    case Qt::AlignJustify :
        return "justify";
    default:
        return "left";
    }
}

void AExportable::setDefaultPath(const QString &defaultPath)
{
    _defaultPath = defaultPath;
}

void AExportable::setRecomendedFormat(const Format &recomendedFormat)
{
    _recomendedFormat = recomendedFormat;
}

void AExportable::setTitle(const QString &title)
{
    _title = title;
}

void AExportable::copyHtml()
{
    QMimeData *mimeData = new QMimeData;
    mimeData->setText(toHtml());
    mimeData->setHtml(toHtml());
    QApplication::clipboard()->setMimeData(mimeData, QClipboard::Clipboard);
}

void AExportable::copyPicture()
{
    QApplication::clipboard()->setPixmap(toPixmap(), QClipboard::Clipboard);
}



void GExportHelperWidget::openSavePictureDialog(const QPixmap& pixmap)
{
    QString fileName = QFileDialog::getSaveFileName(nullptr, tr("Save as"), tr("Choose a filename"), "PNG(*.png);; TIFF(*.tiff *.tif);; JPEG(*.jpg *.jpeg)");

    if(!fileName.isEmpty()){
        QString saveExtension = "PNG";
        int pos = fileName.lastIndexOf('.');
        if (pos >= 0){
            saveExtension = fileName.mid(pos + 1);
        }
        if(!pixmap.save(fileName, qPrintable(saveExtension))){
            QMessageBox::warning(this, tr("File could not be saved"),
                                 tr("Sorry, something went wrong."), QMessageBox::Ok);
        }
    }
}

void GExportHelperWidget::openSaveDialog(const QString &format, const QString &data)
{

    QString defaultFilter = format.toUpper() + " files (*."+ format.toLower()+")";
    QString filters = defaultFilter + ";;All files (*.*)";
    QString fileName = QFileDialog::getSaveFileName(nullptr, tr("Save as"), tr("Choose a filename"), filters, &defaultFilter);

    if(!fileName.isEmpty()){
        QFile file(fileName);
        if (file.open(QFile::WriteOnly | QFile::Truncate)) {
            QTextStream stream(&file);
            stream << data;
            file.close();
        }
        else{
             QMessageBox::warning(this, tr("File could not be saved"),
                                 tr("Sorry, something went wrong."), QMessageBox::Ok);
        }
    }
}



QString GComplexExportable::toHtml()
{
    QString html;
    for(const auto part : _parts){
        html += part->toHtml() + "<br>";
    }
    return html;
}

QPixmap GComplexExportable::toPixmap()
{
    QPixmap data;
    for(const auto part : _parts){
        QPixmap stagePixmap = part->toPixmap();
        int width = qMax(data.width(), stagePixmap.width());
        int space = 15;
        int height = data.height() + stagePixmap.height() + space;
        QPixmap newPixmap(width, height);
        newPixmap.fill(QGuiApplication::palette().color(QPalette::Base));
        QPainter* painter = new QPainter(&newPixmap);
        painter->drawImage(0,0, data.toImage());
        painter->drawImage(data.height(),0,stagePixmap.toImage());
        data = newPixmap;
    }
    return data;
}

QString GComplexExportable::toCsv()
{
    QString csv;
    for(const auto part : _parts){
        csv += part->toCsv() + "<br>";
    }
    return csv;
}

QString GComplexExportable::toPlainText()
{
    QString plainText;
    for(const auto part : _parts){
        plainText += part->toPlainText() + "<br>";
    }
    return plainText;
}

bool GComplexExportable::hasMoreParts() const
{
    return true;
}

GComplexExportable::GComplexExportable(const QList<AExportable *> parts)
{
    _parts = parts;
}

GComplexExportable::~GComplexExportable()
{
    _parts.clear();
}

QList<AExportable *> GComplexExportable::getIndividualParts() const
{
    return _parts;
}
