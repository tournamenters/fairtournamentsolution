#ifndef GTEAMEDIT_H
#define GTEAMEDIT_H

#include "gdragableedit.h"

class GTeamEdit : public GDragableEdit
{
    Q_OBJECT

public:
    GTeamEdit(int row, int column, const QVariant& identificator, QWidget *parent=nullptr);
    ~GTeamEdit() override;

    void dropEvent(QDropEvent *event) override;
    void setRemovableActions(bool removableActions);
    void selectText(const QString& text);

signals:
    void teamChangeRequested(const QVariant& myIdentificator, const QString& text, const QString& oldText);
    void teamsReplacementRequested(const QVariant& myIdentificator, const QVariant& otherIdentificator);

private slots:
    void textEditation() override;
    void renameTeam();
    void removeTeam();
    void restoreTeam();

    void showMenu();

private:
    void setupDragData(int row, int col, const QVariant& identificator);

    QPointer<QAction> actRename;
    QPointer<QAction> actRemove;
    QPointer<QAction> actReturn;
    bool _removableActions;

};

#endif // GTEAMEDIT_H
