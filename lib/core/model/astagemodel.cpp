#include "astagemodel.h"

AStageModel::AStageModel(int id, StageType type, QObject *parent)
    :AMatchesModel (parent)
{
    _uniqueId = id;
    addToIdentificator("stage_index", id);
    addToIdentificator("group_name", id);
    _type = type;
    _nextStage = nullptr;
    _teams = new CTeamsList(true, this);
    connect(_teams, SIGNAL(teamUpdated()),
            this, SIGNAL(dataChanged()));
}

AStageModel::~AStageModel()
{
    delete(_teams);
}

void AStageModel::setTeamsCount(int numTeams)
{
    prepareTeamsTable();
    for(int i = 0; i < numTeams; i++){
        QVariantMap teamData;
        teamData["index"] = QString::number(i);
        teamData["place_holder"] = getName() + " |" + tr(" Team ") + QString::number(i+1);
        _teams->addRow(nullptr, teamData);
    }
}

int AStageModel::getId() const
{
    return _uniqueId;
}

QString AStageModel::getType() const
{
    switch (_type) {
    case StageType::Groups:
        return key_GROUPS_STAGE;
    case StageType::RobinRound:
        return key_ROBIN_ROUND;
    case StageType::PlayOff:
        return key_PLAY_OFF;
    }
    return QString();
}

QList<AStageModel *> AStageModel::getSubStages() const
{
    return {};
}

bool AStageModel::hasSubStages() const
{
    return false;
}

int AStageModel::teamsCount() const
{
    return _teams->getTeams().size();
}

bool AStageModel::finished() const
{
    for(const auto team : _teams->getTeams()){
        if(!team){
            return false;
        }
    }
    return allMatchesPlayed();
}

bool AStageModel::isFinal() const
{
    return _nextStage;
}

void AStageModel::prepareTeamsTable()
{
    _teams->clear();
    _teams->setColumnHeaders({"place_holder", "index"});
}

void AStageModel::createIdentificator()
{
    QVariantMap map;
    map["stage_index"] = getId();
    setIdentificator(map);
}

bool AStageModel::isChange(int numTeams)
{
    if(_lastParams.size() == 1 && _lastParams.first() == numTeams){
        return false;
    }
    _lastParams.clear();
    _lastParams.push_back(numTeams);
    return true;
}

bool AStageModel::isChange(const QList<int> &numBySources)
{
    if(numBySources == _lastParams){
        return false;
    }
    _lastParams.clear();
    _lastParams = numBySources;
    return true;
}



