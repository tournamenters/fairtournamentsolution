#include "lteamssorterfactory.h"
#include "ateamssorter.h"

//slo by se zbavit factory a inicializovat to jako statik a venku, co to tr()...???
LTeamsSorterFactory::LTeamsSorterFactory()
{
    _implicitCriteriumsOrder.push_back(NUM_POINTS);
    _implicitCriteriumsOrder.push_back(MUTUAL_MATCH);
    _implicitCriteriumsOrder.push_back(MINI_TABLE);
    _implicitCriteriumsOrder.push_back(SCORE_DIFF);
    _implicitCriteriumsOrder.push_back(NUM_GOALS);

    _possibleCriteriums[NUM_POINTS] = tr("Points");
    _possibleCriteriums[MUTUAL_MATCH] = tr("Mutual match");
    _possibleCriteriums[MINI_TABLE] = tr("Mini table");
    _possibleCriteriums[SCORE_DIFF] = tr("Score diff");
    _possibleCriteriums[SCORE_RATIO] = tr("Score ratio");
    _possibleCriteriums[NUM_GOALS] = tr("Goals");
    _possibleCriteriums[NUM_WINS] = tr("Wins");
}

LTeamsSorterFactory::~LTeamsSorterFactory()
{
    _implicitCriteriumsOrder.clear();
    _possibleCriteriums.clear();
}

QList<QString> LTeamsSorterFactory::getImplicitCriteriumsOrder() const
{
    return _implicitCriteriumsOrder;
}

QMap<QString, QString> LTeamsSorterFactory::getPossibleCriteriumsKeyLabel() const
{
    return _possibleCriteriums;
}

LTeamsSorter *LTeamsSorterFactory::createOrderer()
{
    return new LTeamsSorter(_implicitCriteriumsOrder);
}

LTeamsSorter *LTeamsSorterFactory::createOrderer(QList<QString> criteriumsOrder)
{
    return new LTeamsSorter(criteriumsOrder);
}
