#-------------------------------------------------
#
# Project created by QtCreator 2019-04-30T21:20:42
#
#-------------------------------------------------

TARGET = mainwindow
TEMPLATE = lib

QT += widgets
QT += core
QT += gui
QT += printsupport

CONFIG -= app_bundle
CONFIG += staticlib
CONFIG += c++11

DEFINES += MAINWINDOW_LIBRARY
DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
	assistant.cpp \
	gopenpage.cpp \
	mainwindow.cpp \
	gpalettemanager.cpp \
	gthemesettingdialog.cpp \

HEADERS += \
	gopenpage.h \
	mainwindow.h \
	gpalettemanager.h \
	gthemesettingdialog.h \
	assistant.h \

unix {
    target.path = /usr/lib
    INSTALLS += target
}

include($$PWD/../../controller/controller.pri)
include($$PWD/../view/view.pri)
include($$PWD/../tournamentwidgets/tournamentwidgets.pri)
include($$PWD/../present/present.pri)
include($$PWD/../creation/creation.pri)

DISTFILES += \
    mainwindow.pri

RESOURCES += \
    icons.qrc

TRANSLATIONS += \
    mainwindow_cs.ts
