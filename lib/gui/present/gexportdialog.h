#ifndef GEXPORTDIALOG_H
#define GEXPORTDIALOG_H

#include <QDialog>
#include <QList>

class QLabel;
class QCheckBox;
class QGroupBox;
class QDialogButtonBox;
class AExportable;

class GExportDialog : public QDialog
{
    Q_OBJECT

public:
    enum DefaultSelection {All, None};
    GExportDialog(const QList<AExportable*>& exportables, QWidget* parent = nullptr, DefaultSelection selection=GExportDialog::All);
    ~GExportDialog();

    bool isExportableChosen(AExportable* exportable);

protected:
    QLabel* createTitle();
    QCheckBox* createCheckBox(AExportable * exportable);
    QGroupBox* createCheckableGroupBox(AExportable *exportable);
    QGroupBox* createMainGroupBox(QList<AExportable *> exportables);
    QDialogButtonBox * createButtonBox();

private:
    void setCheckStates(DefaultSelection selection);

    QMap<AExportable*, QGroupBox*> _complexExportables;
    QMap<AExportable*, QCheckBox*> _simpleExportables;

};

#endif // GEXPORTDIALOG_H
