#include "gdragabletoolbutton.h"
#include "gdraganddropmanager.h"

#include <QMimeData>
#include <QMouseEvent>
#include <QApplication>
#include <QDrag>

GDragableToolButton::GDragableToolButton(const QString &text, QWidget *parent)
    :QToolButton (parent)
{
    setText(text);
    _dragableManager = new GDragAndDropManager(this);
}

void GDragableToolButton::mousePressEvent(QMouseEvent *event)
{
    _dragableManager->mousePressEventCare(event);
}

void GDragableToolButton::mouseMoveEvent(QMouseEvent *event)
{
    if(_dragableManager->mouseMoveEventCare(event)){
        provideDragAction();
    }
}

void GDragableToolButton::provideDragAction()
{
    QDrag *drag = new QDrag(this);
    QMimeData *mimeData = new QMimeData();
    mimeData->setText(text());
    drag->setMimeData(mimeData);
    drag->setPixmap(grab());
    drag->exec(Qt::CopyAction | Qt::MoveAction);
}
