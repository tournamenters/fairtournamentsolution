#ifndef TOURNAMENTCONTROLLER_H
#define TOURNAMENTCONTROLLER_H
#include <QObject>

#include <QMap>
#include <QString>

#include "global_model.h"
#include "serializationcontroller.h"

class Tournament;
class QUndoStack;

class Controller : public QObject
{
    Q_OBJECT

public:
    Controller(Tournament * model, SerializationController * serializer, QObject * parent = nullptr);
    void startTournament(const QString& name);
    void createStartList(const QVariant& teamsData, const QVariant& params);
    void createStages(const QStringList& types, const QVariantList& params, const QVariantList& settings);
    void sheduleFields(const QVariant& setting);

    void finishTournament();
    void setUndoStack(QUndoStack* stack);

    Tournament *getModel() const;

public slots:
    void resultEntered(const QVariant &identificator, const QString &newResult, const QString &oldResult);
    void startListViewEdit(ExecType type, const QVariant& identificator, const QVariant& data);
    void matchesViewEdit(ExecType type, const QVariant& identificator, const QVariant& data);
    void stageViewEdit(ExecType type, const QVariant& identificator, const QVariant& data);

private:
    void resetAll();

private:
    Tournament * _model;
    SerializationController * _serializer;
    QUndoStack * _undoStack;
    
};

#endif // TOURNAMENTCONTROLLER_H
