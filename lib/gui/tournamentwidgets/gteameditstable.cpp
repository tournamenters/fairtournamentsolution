#include "gteameditstable.h"

#include <QVariant>
#include "gteamedit.h"

GTeamEditsTable::GTeamEditsTable(QWidget *parent, int teamEditColumnsCount)
    : GTournamentTable(parent)
{
    setTeamEditRemovable(false);
    setTeamEditDragAndDrop(false);
    _teamEditColumnsCount = teamEditColumnsCount;
}

GTeamEditsTable::GTeamEditsTable(const QVariant &identificator, QWidget *parent, int teamEditColumnsCount)
    : GTeamEditsTable(parent, teamEditColumnsCount)
{
    _identificator = identificator;
}

GTeamEditsTable::~GTeamEditsTable()
{
    _teamEditCells.clear();
    _identificator.clear();
}

void GTeamEditsTable::setData(const QVariant &data)
{
    int rows = data.toMap()["rows"].toList().size();
    if(rows > rowCount()){
        setRowCount(rows);
        prepareTeamCells(rows, _teamEditColumnsCount);
    }   
    GTournamentTable::setData(data);
}

void GTeamEditsTable::setTeamEditRemovable(bool teamEditRemovable)
{
    for(const auto teamEdit : _teamEditCells){
        teamEdit->setRemovableActions(teamEditRemovable);
    }
    _teamEditsRemovable = teamEditRemovable;
}

void GTeamEditsTable::setTeamEditDragAndDrop(bool teamEditDragAndDrop)
{
    for(const auto teamEdit : _teamEditCells){
        teamEdit->setDragAndDrop(teamEditDragAndDrop);
    }
    _teamEditsDragAndDrop = teamEditDragAndDrop;
}

void GTeamEditsTable::setTeamEditColumnsCount(int teamColumns)
{
    if(teamColumns == _teamEditColumnsCount){
        return;
    }
    prepareTeamCells(rowCount(), teamColumns);
    _teamEditColumnsCount = teamColumns;
}

void GTeamEditsTable::prepareTeamCells(int rows, int columns)
{
    if(columns > columnCount()){
        setColumnCount(columns);
    }
    for(int row = 0; row<rows; row++){
        for(int col = 0 ; col<columnCount(); col++){
            if(col<columns){
                addTeamEdit(row, col);
            }
            else{
                removeTeamEdit(row, col);
            }
        }
    }
    int standardWidth = columnWidth(0);
    for(int col = 0 ; col<columns; col++){
        setColumnWidth(col, standardWidth*4/3);
    }
}

void GTeamEditsTable::addTeamEdit(int row, int col)
{
    if(qobject_cast<GTeamEdit*>(cellWidget(row,col))){
        return;
    }
    GTeamEdit* teamCell = new GTeamEdit(row, col, _identificator, this);
    teamCell->setRemovableActions(_teamEditsRemovable);
    teamCell->setDragAndDrop(_teamEditsDragAndDrop);
    _teamEditCells.push_back(teamCell);
    setCellWidget(row,col,teamCell);
    connect(teamCell, SIGNAL(teamChangeRequested(QVariant, QString, QString)),
            this, SIGNAL(teamChangeRequested(QVariant, QString, QString)));
    connect(teamCell, SIGNAL(teamsReplacementRequested(QVariant, QVariant)),
            this, SIGNAL(teamsReplacementRequested(QVariant, QVariant)));
}

void GTeamEditsTable::removeTeamEdit(int row, int col)
{
    GTeamEdit* teamEdit = qobject_cast<GTeamEdit*>(cellWidget(row,col));
    if(teamEdit){
        removeCellWidget(row, col);
        _teamEditCells.removeOne(teamEdit);
        delete teamEdit;
    }
}
