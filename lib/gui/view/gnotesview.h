#ifndef GNOTESVIEW_H
#define GNOTESVIEW_H

#include "aview.h"

class AModel;
class QTextEdit;

class GNotesView : public AView
{
    Q_OBJECT

public:
    GNotesView(AModel* model, QWidget *parent = nullptr);
    ~GNotesView() override;
    QWidget * getViewWidget() override;

public slots:
    void update() override;

private slots:
    void dataChange();

private:
    QTextEdit* createTextEdit();

    QTextEdit* teNotes;
    AModel *_model;
};

#endif // GNOTESVIEW_H
