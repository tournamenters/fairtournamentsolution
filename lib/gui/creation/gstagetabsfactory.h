#ifndef GSTAGETABSFACTORY_H
#define GSTAGETABSFACTORY_H

#include <QList>

class AStageEditorTab;

typedef AStageEditorTab* (__stdcall * FcnCreateStageTab)();

class GStageTabsFactory
{

public:
    ~GStageTabsFactory();
    static GStageTabsFactory *getInstance();

    AStageEditorTab * create(const QString& key);
    QStringList keys();
    void registerType(const QString& key, FcnCreateStageTab create);

    GStageTabsFactory(GStageTabsFactory const& other) = delete;
    void operator=(GStageTabsFactory const& other) = delete;

private:
    GStageTabsFactory();
    QMap<QString, FcnCreateStageTab> registeredTypes;
};


#endif // GSTAGETABSFACTORY_H
