#ifndef GINTROPAGE_H
#define GINTROPAGE_H

#include <QWizardPage>
#include <QMap>
#include <QVariant>

class QLabel;
class QLineEdit;

class GIntroPage : public QWizardPage
{
    Q_OBJECT

public:
    GIntroPage(QWidget *parent = nullptr);
    int nextId() const override;

private:
    QLabel* createTitle();
    QLayout* createNameLayout();
    QLineEdit* createNameEdit();

    QLineEdit * _tournamentName;

};

#endif // GINTROPAGE_H
