#ifndef GDRAGANDDROPMANAGER_H
#define GDRAGANDDROPMANAGER_H

#include <QObject>
#include <QPoint>

class QMouseEvent;

class GDragAndDropManager : public QObject//zatim pouze drag...
{
    Q_OBJECT

public:
    explicit GDragAndDropManager(QObject *parent = nullptr);
    void mousePressEventCare(QMouseEvent *event);
    bool mouseMoveEventCare(QMouseEvent * event);

private:
    bool _mousePressed;
    QPoint _dragStartPosition;
};

#endif // GDRAGANDDROPMANAGER_H
