TEMPLATE = subdirs

SUBDIRS += \
    core \
    controller \
    gui

    guisubdir = gui
    core.subdir = core
    controller.sudbir = controller

    gui.depends = core controller
    controller.depends = core

DISTFILES += \
    lib.pri
