#include "editationcommand.h"
#include "amodel.h"

EditationCommand::EditationCommand(AModel *model, const QVariant &identificator, const QVariant &data, QUndoCommand *parent)
    :QUndoCommand (parent)
{
    _model = model;
    _identificator = identificator;
    _dataNew = data.toMap()["new"];
    _dataOld = data.toMap()["old"];
}

EditationCommand::~EditationCommand()
{

}

void EditationCommand::undo()
{
    _model->execEdit(ExecType::Editation, _identificator, _dataOld);
}

void EditationCommand::redo()
{
    _model->execEdit(ExecType::Editation, _identificator, _dataNew);
}
