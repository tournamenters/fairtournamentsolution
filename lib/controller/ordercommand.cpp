#include "ordercommand.h"
#include "astagemodel.h"

OrderCommand::OrderCommand(AStageModel *model, const QVariant& identificator, const QVariant& data, QUndoCommand *parent)
    :QUndoCommand (parent)
{
    _model = model;
    _identificator = identificator;
    _data = data;
}

void OrderCommand::undo()
{
    _model->execEdit(ExecType::Order, _identificator, QVariant());
}

void OrderCommand::redo()
{
    _model->execEdit(ExecType::Order, _identificator, _data);
}

