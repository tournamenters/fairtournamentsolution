#ifndef CTOURNAMENT_H
#define CTOURNAMENT_H
#include <QObject>
#include "serialization.h"

#include <QList>
#include <QPointer>

class AModel;
class AMatchesModel;
class AStageModel;
class CNotes;
class CStats;
class CFieldsManager;
class CTeamsList;
class CMatchesList;

class CStagesFactory;
class CStartList;

class Tournament: public QObject, public ASerializable
{
    Q_OBJECT

public:
    Tournament(CStagesFactory* creator, QObject* parent = nullptr);
    ~Tournament() override;

    void newTournament(const QString& name);
    void clearTournament();

    void createStartList(const QVariant &teamsData, const QVariant &parameters);
    bool addStage(const QString& type, const QString& name, const QVariant& parameters, const QVariant& setting = QVariant());
    void sheduleFields(const QVariant& parameters);

    AModel * getStatsModel() const;
    AModel * getNotesModel() const;
    AModel * getStartListModel() const;
    AMatchesModel * getMatchesModel() const;
    QList<AStageModel *> getStagesModels() const;
    QList<AMatchesModel *> getFieldsModels() const;

    void fromVariant(const QVariant& variant) override;
    QVariant toVariant() const override;

private:
    QPointer<CStagesFactory> _creator;

    QList<AStageModel *> _stages;
    QList<AModel*> _models;

    QPointer<CStartList> _startList;
    QPointer<CMatchesList> _matches;
    QPointer<CFieldsManager> _fields;

    QPointer<CStats> _stats;
    QPointer<CNotes> _notes;

};

#endif // CTOURNAMENT_H
