#ifndef GTEAMEDITSTABLE_H
#define GTEAMEDITSTABLE_H

#include "gtournamenttable.h"

class GTeamEdit;

class GTeamEditsTable : public GTournamentTable
{
    Q_OBJECT

public:
    GTeamEditsTable(QWidget *parent = nullptr, int teamEditColumnsCount = 1);
    GTeamEditsTable(const QVariant& identificator, QWidget *parent = nullptr, int teamEditColumns = 1);
    ~GTeamEditsTable() override;

    void setData(const QVariant& data) override;
    void setTeamEditColumnsCount(int teamColumns);
    void setTeamEditRemovable(bool teamEditRemovable);
    void setTeamEditDragAndDrop(bool teamEditDragAndDrop);

signals:
    void teamChangeRequested(const QVariant& myIdentificator, const QString& text, const QString& oldText);
    void teamsReplacementRequested(const QVariant& myIdentificator, const QVariant& otherIdentificator);

protected:
    void addTeamEdit(int row, int col);
    void removeTeamEdit(int row, int col);
    void prepareTeamCells(int rows, int columns);

    QList<GTeamEdit*> _teamEditCells;
    QVariant _identificator;
    int _teamEditColumnsCount;

    bool _teamEditsDragAndDrop;
    bool _teamEditsRemovable;

};

#endif // GTEAMEDITSTABLE_H
