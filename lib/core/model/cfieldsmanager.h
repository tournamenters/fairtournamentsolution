#ifndef CFIELDSSTAGE_H
#define CFIELDSSTAGE_H
#include "amodel.h"

#include <QVariant>
#include <QList>

class AFieldsAssigner;
class CMatchesList;

class CFieldsManager: public AModel
{
    Q_OBJECT

public:
    CFieldsManager(int numFields, AFieldsAssigner* assigner, QObject *parent = nullptr);
    ~CFieldsManager() override;

    void addMatches(const QList<CMatch *> &matches);

    int maxFieldSize() const;
    QList<AMatchesModel* > getFields() const;
    QList<QList<CMatch *> > getMatches() const;
    QVariant getData() const override;

    QVariant toVariant() const;

public slots:
    bool execEdit(ExecType type, const QVariant& identificator, const QVariant& data) override;

private:
    QList<QList<int> > getIntList(const QList<CMatch *>& matches);
    void placeMatches(const QList<QList<QList<int> > > &intMatches, const QList<CMatch *>& realMatches);

    AFieldsAssigner* _assigner;
    QList<CMatchesList*> _fields;
};

#endif // CFIELDSSTAGE_H
