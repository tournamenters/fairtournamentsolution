#ifndef GEDITATIONDIALOG_H
#define GEDITATIONDIALOG_H

#include <QDialog>
class GEditationTable;

class GEditDialog : public QDialog
{
    Q_OBJECT

public:
    GEditDialog(const QVariant& data, QWidget* parent = nullptr);

    QVariant getNewData();
    QVariant getOriginalData();

private:
    GEditationTable* twEditation;

};
#endif // GEDITATIONDIALOG_H
