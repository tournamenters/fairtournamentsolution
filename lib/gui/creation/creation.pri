include($$PWD/../tournamentwidgets/tournamentwidgets.pri)

win32:CONFIG(release, debug|release): LIBS += -L$$TOP_BUILDDIR/lib/gui/creation/release/ -lcreation
else:win32:CONFIG(debug, debug|release): LIBS += -L$$TOP_BUILDDIR/lib/gui/creation/debug/ -lcreation
else:unix: LIBS += -L$$TOP_BUILDDIR/lib/gui/creation/ -lcreation

INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/gui/creation/release/libcreation.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/gui/creation/debug/libcreation.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/gui/creation/release/creation.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/gui/creation/debug/creation.lib
else:unix: PRE_TARGETDEPS += $$TOP_BUILDDIR/lib/gui/creation/libcreation.a
