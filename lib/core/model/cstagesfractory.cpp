#include "cstagesfactory.h"

#include "global_model.h"

#include "crobinround.h"
#include "cplayoff.h"
#include "cgroupsstage.h"
#include "cfieldsmanager.h"

#include "lrobinroundmatchesordererfactory.h"
#include "lgroupsdrawerfactory.h"
#include "lteamssorterfactory.h"
#include "lplayoffdrawerfactory.h"
#include "lfieldsassignerfactory.h"

#include "ateamssorter.h"

CStagesFactory::CStagesFactory()
{
    _nextStageIndex = 0;
    _fieldsFactory = new CFieldsManagerFactory();
    registerStageType(key_GROUPS_STAGE, new CGroupsStageCreator);
    registerStageType(key_PLAY_OFF, new CPlayOffCreator);
    registerStageType(key_ROBIN_ROUND, new CRobinRoundCreator);
}

CStagesFactory::~CStagesFactory()
{
    qDeleteAll(registeredTypes.values());
    registeredTypes.clear();
    delete _fieldsFactory;
}

AStageModel *CStagesFactory::createStage(const QString& stageType, const QMap<QString, int>& params)
{
    return createStage(stageType, stageType, params);
}

AStageModel *CStagesFactory::createStage(const QString& stageType, const QString& name, const QMap<QString, int>& params)
{
    if(registeredTypes.contains(stageType)){
        _nextStageIndex++;
        return registeredTypes[stageType]->create(_nextStageIndex-1, name, params);
    }
    return nullptr;
}

AStageModel *CStagesFactory::createStage(const QString& stageType, const QString& name, const QVariant & params)
{
    QMap<QString, int> paramsInt;
    for(const auto& key : params.toMap().keys()){
        paramsInt[key] = params.toMap()[key].toInt();
    }
    return createStage(stageType, name, paramsInt);
}

CFieldsManager *CStagesFactory::createFieldsManager()
{
    return _fieldsFactory->create();
}

CFieldsManager *CStagesFactory::createFieldsManager(const QVariant &params)
{
    return _fieldsFactory->create(params);
}

void CStagesFactory::reset()
{
    _nextStageIndex = 0;
}

void CStagesFactory::changeSetting(const QString& stageType, const QVariant &setting)
{
    if(registeredTypes.contains(stageType)){
        registeredTypes[stageType]->setSetting(setting);
        return;
    }
}

void CStagesFactory::registerStageType(const QString& stageType, AStageCreator *creator)
{
    registeredTypes[stageType] = creator;
}

QVariant CStagesFactory::toVariant()
{
    QVariantMap map;
    for(const auto & key : registeredTypes.keys()){
        map[key] = registeredTypes[key]->toVariant();
    }
    map["fields"] = _fieldsFactory->toVariant();
    return map;
}

void CStagesFactory::fromVariant(const QVariant &variant)
{
    QVariantMap map = variant.toMap();
    for(const auto & key : registeredTypes.keys()){
        registeredTypes[key]->fromVariant(map[key]);
    }
    _fieldsFactory->fromVariant(map["fields"]);
}


AStageModel *ASimpleStageCreator::create()
{
    return create(_implicitName + " " + letterFromIndex(_nextIndex));
}

QString ASimpleStageCreator::letterFromIndex(int index)
{
    if(index >= 0 && index < 26){
        return QString("ABCDEFGHIJKLMNOPQRSTUVWXYZ"[index]);
    }
    else{
        return letterFromIndex(index - 26) + letterFromIndex(index - 26);
    }
}

CRobinRoundCreator::CRobinRoundCreator()
{
    _nextIndex = 0;
    _typeName = key_ROBIN_ROUND;
    _implicitName = "Group";
    _drawerMethod = CModelConstants::Instance().getKeyTextMatchesOrderMap().keys()[0];

    _pointsWin = 3;
    _pointsLose = 0;
    _pointsDraw = 1;
    _pointsWinOT = 2;
    _pointsLossOT = 1;
    _isOT = false;
    _criteriaOrder = CModelConstants::Instance().getKeyImplicitCriteriaOrder();
}

CRobinRoundCreator::~CRobinRoundCreator()
{
    _criteriaOrder.clear();
}

AStageModel *CRobinRoundCreator::create()
{
    return ASimpleStageCreator::create();
}

AStageModel *CRobinRoundCreator::create(int globalIndex)
{
    if(_usedSettingsByName.contains(_implicitName)){
        setSetting(_usedSettingsByName[_implicitName]);
    }
    else{
        _usedSettingsByName[_implicitName] = _globalSetting;
    }
    AMatchesOrderer * drawer =  LMatchesOrdererFactory::Instance()->createMatchesOrderer(_drawerMethod);
    CRobinRound * group = new CRobinRound(globalIndex, _implicitName, createSorter(), drawer);
    return group;
}
AStageModel *CRobinRoundCreator::create(const QString& name)
{
    if(_usedSettingsByName.contains(name)){
        setSetting(_usedSettingsByName[name]);
    }
    else{
        _usedSettingsByName[name]=_globalSetting;
    }
    AMatchesOrderer * drawer =  LMatchesOrdererFactory::Instance()->createMatchesOrderer(_drawerMethod);
    CRobinRound * group = new CRobinRound(_nextIndex, name, createSorter(), drawer);
    _nextIndex++;
    return group;
}

AStageModel *CRobinRoundCreator::create(int globalIndex, const QString& name, const QMap<QString, int>& params)
{
    if(_usedSettingsByName.contains(name)){
        setSetting(_usedSettingsByName[name]);
    }
    else{
        _usedSettingsByName[name]=_globalSetting;
    }
    AMatchesOrderer * drawer =  LMatchesOrdererFactory::Instance()->createMatchesOrderer(_drawerMethod);
    CRobinRound * group = new CRobinRound(globalIndex, name, createSorter(), drawer);
    group->setTeamsCount(params[key_NUM_TEAMS]);
    return group;
}

void CRobinRoundCreator::setSetting(const QVariant & setting)
{
    QMap<QString, QVariant> orderer = setting.toMap()["orderer"].toMap();
    QMap<QString, QVariant> points = orderer["points"].toMap();
    _pointsWin = points["win"].toInt();
    _pointsLose = points["lose"].toInt();
    _pointsDraw = points["draw"].toInt();
    _pointsWinOT = points["win_ot"].toInt();
    _pointsLossOT = points["loss_ot"].toInt();
    _isOT = points["is_ot"].toBool();

    _drawerMethod = setting.toMap()["alg_matches_order"].toString();

    _criteriaOrder.clear();
    for(const auto& variant : orderer["order_criteria"].toList()){
        _criteriaOrder.push_back(variant.toString());
    }

    _globalSetting = setting;
}

LTeamsSorter *CRobinRoundCreator::createSorter()
{
    LTeamsSorter * orderer = LTeamsSorterFactory::Instance()->createOrderer(_criteriaOrder);
    orderer->setPoints(_pointsWin, _pointsDraw, _pointsLose);
    if(_isOT){
        orderer->setPointsOverTime(_pointsWinOT, _pointsLossOT);
    }
    return orderer;
}

CPlayOffCreator::CPlayOffCreator()
{
    _drawerMethod = "advantageous";
}

AStageModel *CPlayOffCreator::create()
{
    return ASimpleStageCreator::create();
}

AStageModel *CPlayOffCreator::create(const QString& name)
{
    if(_usedSettingsByName.contains(name)){
        setSetting(_usedSettingsByName[name]);
    }
    else{
        _usedSettingsByName[name]=_globalSetting;
    }
    APlayOffDrawer * drawer =  LPlayOffDrawerFactory::Instance()->createDrawer(_drawerMethod);
    CPlayOff * playOff = new CPlayOff(_nextIndex, name, drawer);
    _nextIndex++;
    return playOff;
}

AStageModel *CPlayOffCreator::create(int globalIndex, const QString& name, const QMap<QString, int>& params)
{
    if(_usedSettingsByName.contains(name)){
        setSetting(_usedSettingsByName[name]);
    }
    else{
        _usedSettingsByName[name]=_globalSetting;
    }
    APlayOffDrawer * drawer =  LPlayOffDrawerFactory::Instance()->createDrawer(_drawerMethod);
    CPlayOff * playOff = new CPlayOff(globalIndex, name, drawer);
    playOff->setTeamsCount(params["num_teams"]);
    return playOff;
}

void CPlayOffCreator::setSetting(const QVariant & setting)
{
    _drawerMethod = setting.toMap()["alg_creation"].toString();
    _globalSetting = setting;
}

CGroupsStageCreator::CGroupsStageCreator()
{
    _drawerMethod = CModelConstants::Instance().getKeyTextGroupsCreationMap().keys()[0];
    creator = new CRobinRoundCreator();
}

CGroupsStageCreator::~CGroupsStageCreator()
{
    delete creator;
}

AStageModel *CGroupsStageCreator::create(int globalIndex, const QString& name, const QMap<QString, int>& params)
{
    if(_usedSettingsByName.contains(name)){
        setSetting(_usedSettingsByName[name]);
    }
    else{
        _usedSettingsByName[name]=_globalSetting;
    }
    AGroupsDrawer* drawer = LGroupsDrawerFactory::Instance()->createDrawer(_drawerMethod);
    CGroupsStage* groupsStage = new CGroupsStage(globalIndex, name, creator->createSorter(), drawer);

    QList<AStageModel*> groups;
    for (int i = 0;i<params["num_groups"];i++) {
        groups.push_back(creator->create(globalIndex));
    }
    groupsStage->setGroups(groups);
    groupsStage->setTeamsCount(params["num_teams"]);
    return groupsStage;
}

void CGroupsStageCreator::setSetting(const QVariant &setting)
{
    creator->setSetting(setting);
    _drawerMethod = setting.toMap()["alg_creation"].toString();
    _globalSetting = setting;
}

AStageCreator::~AStageCreator()
{
    _globalSetting.clear();
    _usedSettingsByName.clear();
}

QVariant AStageCreator::toVariant()
{
    QVariantMap map;
    map["global_setting"] = _globalSetting;
    map["used_setting"] = _usedSettingsByName;
    return map;
}

void AStageCreator::fromVariant(const QVariant &variant)
{
    QVariantMap map = variant.toMap();
    _usedSettingsByName = map["used_setting"].toMap();
    setSetting(map["global_setting"]);
}

CFieldsManagerFactory::CFieldsManagerFactory()
{
    QVariantMap map;
    map["alg_creation"] = 0;
    map["num_fields"] = 2;
    _parameters = map;
}

CFieldsManagerFactory::~CFieldsManagerFactory()
{

}

CFieldsManager *CFieldsManagerFactory::create()
{
    int keyIndex = _parameters.toMap()["alg_creation"].toInt();
    QString key = CModelConstants::Instance().getKeyTextFieldsMap().keys()[keyIndex];
    auto keyLabelMap = CModelConstants::Instance().getKeyTextFieldsMap();
    AFieldsAssigner * assigner = LFieldsAssignerFactory::Instance()->createAssigner(key);
    return new CFieldsManager(_parameters.toMap()["num_fields"].toInt(), assigner);

}

CFieldsManager *CFieldsManagerFactory::create(const QVariant &parameters)
{
    _parameters = parameters;
    return create();
}

QVariant CFieldsManagerFactory::toVariant() const
{
    return _parameters;
}

void CFieldsManagerFactory::fromVariant(const QVariant &variant)
{
    _parameters = variant;
}
