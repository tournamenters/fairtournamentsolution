#ifndef GRESULTEDIT_H
#define GRESULTEDIT_H

#include "gdragableedit.h"

class QActionGroup;

class GResultEdit : public GDragableEdit
{
    Q_OBJECT

public:
    GResultEdit(int row, int col, QWidget *parent=nullptr);
    ~GResultEdit() override;

    void dropEvent(QDropEvent *event) override;

signals:
    void resultChanged(int row, int col, const QString& result, const QString& oldResult);

private slots:
    void textEditation() override;
    void resultActionTriggered(QAction* action);
    void addOwnResult(const QString& result);

    void showMenu();

private:
    void addDrawActions();
    void addWinnerActions();

    QPointer<QActionGroup> actgrOwnResults;
    int _row;
    int _col;
};

#endif // GRESULTEDIT_H
