#include "gfieldspage.h"
#include "global_creation.h"
#include "global_model.h"

#include <QLabel>
#include <QSpinBox>
#include <QComboBox>
#include <QGroupBox>
#include <QBoxLayout>
#include <QFormLayout>


GFieldsSettingPage::GFieldsSettingPage(QWidget *parent)
    : QWizardPage(parent)
{
    setTitle(tr("Schedule Fields"));
    setDoFields(true);

    QVBoxLayout * layout = new QVBoxLayout();
    layout->addWidget(createTitle());
    layout->addStretch(1);
    layout->addWidget(createMainBox());
    layout->addStretch(3);
    setLayout(layout);
}

void GFieldsSettingPage::initializePage()
{
    sbNumFields->setRange(1, field("start_list.num_teams").toInt()/2);
    sbNumFields->setValue(field("start_list.num_teams").toInt()/6);
}

void GFieldsSettingPage::setDoFields(bool doFields)
{
    _doFields = doFields;
}

bool GFieldsSettingPage::doFields() const
{
    return _doFields;
}

int GFieldsSettingPage::nextId() const
{
    return PageIndex::Page_Conclusion;
}

void GFieldsSettingPage::fieldsCheckChange()
{
    if(doFields() != boxFields->isChecked()){
        setDoFields(boxFields->isChecked());
        emit fieldsCheckChanged();
    }
}

QLabel* GFieldsSettingPage::createTitle()
{
    QLabel * label = new QLabel(tr("Here you can set up matches scheduling on individual"
                                  " fields to precisely plan your tournament."), this);
    label->setWordWrap(true);
    return label;
}

QGroupBox *GFieldsSettingPage::createMainBox()
{
    boxFields = new QGroupBox(tr("Fields"), this);
    boxFields->setCheckable(true);
    boxFields->setChecked(doFields());
    connect(boxFields, SIGNAL(clicked(bool)),
            this, SLOT(fieldsCheckChange()));
    registerField("fields.do", this, "_doFields", SIGNAL(fieldsCheckChanged()));
    boxFields->setLayout(createMainBoxLayout());
    return boxFields;
}

QLayout *GFieldsSettingPage::createMainBoxLayout()
{
    sbNumFields = new QSpinBox(this);
    sbNumFields->setKeyboardTracking(false);
    registerField("fields.num_fields", sbNumFields);
    cbAlgorithmCreation = new QComboBox(this);
    auto keyLabelMap = CModelConstants::Instance().getKeyTextFieldsMap();
    for(const auto& key : keyLabelMap .keys()){
        cbAlgorithmCreation->addItem(keyLabelMap[key], key);
    }
    registerField("fields.alg_creation", cbAlgorithmCreation);
    QFormLayout * layout = new QFormLayout(this);
    layout->addRow(tr("Fields number:"), sbNumFields);
    layout->addRow(tr("Creation method:"), cbAlgorithmCreation);
    return layout;
}

