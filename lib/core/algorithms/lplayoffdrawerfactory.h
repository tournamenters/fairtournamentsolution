#ifndef LPLAYOFFDRAWERFACTORY_H
#define LPLAYOFFDRAWERFACTORY_H
#include "aplayoffdrawer.h"

#include <QMap>
#include <QObject>

typedef APlayOffDrawer* (__stdcall * FcnCreatePlayOffDrawer)(void);

class LPlayOffDrawerFactory: public QObject
{
    Q_OBJECT

public:
    ~LPlayOffDrawerFactory();

    static LPlayOffDrawerFactory *Instance()
    {
        static LPlayOffDrawerFactory instance;
        return &instance;
    }

    LPlayOffDrawerFactory(LPlayOffDrawerFactory const& other) = delete;
    void operator=(LPlayOffDrawerFactory const& other) = delete;

    QMap<QString, QString> getPossibleTypesKeyLabelMap();
    void registerOrdererType(const QString &type, const QString &label, FcnCreatePlayOffDrawer create);
    APlayOffDrawer * createDrawer(const QString &type);

private:
    LPlayOffDrawerFactory();
    QMap<QString, FcnCreatePlayOffDrawer> _registeredTypes;
    QMap<QString, QString> _typeLabelMap;

};

#endif // LPLAYOFFDRAWERFACTORY_H
