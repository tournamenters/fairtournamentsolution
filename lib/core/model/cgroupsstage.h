#ifndef CGROUPSTAGE_H
#define CGROUPSTAGE_H
#include "astagemodel.h"

class CRobinRound;
class AGroupsDrawer;
class LTeamsSorter;

class CGroupsStage: public AStageModel
{
    Q_OBJECT

public:
    CGroupsStage(int id, const QString& name, LTeamsSorter* orderer, AGroupsDrawer * drawer, QObject* parent = nullptr);
    ~CGroupsStage() override;

    void setGroups(QList<AStageModel*> groups);
    void setTeamsCount(int numTeams) override;
    void setTeams(CTeamsList* teams) override;
    void setTeams(const QList<CTeam*>& teams) override;
    void setTeamsFromSources(const QList<CTeamsList*>& teams) override;
    void setTeamsFromVariant(const QVariant& variant, const QMap<int,CTeam*>& teamsById) override;

    void setNextStage(AStageModel* nextStage) override;

    CTeamsList * countStandings() override;

    CTeam* getTeam(const QVariant& identificator) const override;
    QVariant getData() const override;
    QList<AStageModel*> getSubStages() const override;

    void resultsFromVariant(const QVariant& variant) override;
    QVariant toVariant() const override;

    void changeTeam(const QVariant& identificator, CTeam* team) override;
    void nextStageCare() override;

public slots:
    bool setResult(const QVariant& identificator, const QString& result) override;
    bool execEdit(ExecType type, const QVariant& identificator, const QVariant& data) override;

private slots:
    void dataChange();

protected:
    void coordinateMatches();
    void modifieMatch(CMatch* match, int groupIndex);
    bool sendExecToGroup(ExecType type, const QVariant &identificator, const QVariant &data);
    bool swapTeams(const QVariantMap& data) override;

private:
    QString generateGroupName(int index) const;
    QString letterFromIndex(int index) const;
    AStageModel* findGroup(const QVariant& identificator) const;

    AGroupsDrawer* _drawer;
    LTeamsSorter* _sorter;

    QList<QList<int>> _teamsGroups;
    QList<AStageModel*> _groups;
};

#endif // CGROUPSTAGE_H
