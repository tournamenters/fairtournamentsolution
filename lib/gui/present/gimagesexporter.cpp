#include "gimagesexporter.h"
#include <QFileDialog>

#include <QLayout>
#include <QGroupBox>
#include <QFormLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QSettings>
#include <QTextStream>
#include <QMessageBox>
#include <QDir>

#include "aexportable.h"
#include "gexportdialog.h"

GImagesExporter::GImagesExporter(QList<AExportable*> exportables, QWidget *parent)
    : QWidget (parent),
    _exportables(exportables)
{
    gedExport = new GExportDialog(exportables, this);
    QVBoxLayout * dialogLayout = qobject_cast<QVBoxLayout*>(gedExport->layout());
    dialogLayout->insertWidget(dialogLayout->count()-1,createPathBox());
    readSetting();
}

GImagesExporter::~GImagesExporter()
{
    delete gedExport;
    _exportables.clear();
}

QGroupBox *GImagesExporter::createPathBox()
{
    QGroupBox* box = new QGroupBox(tr("Select Folder"));
    QFormLayout* boxLayout = new QFormLayout();
    QHBoxLayout* buttonLayout = new QHBoxLayout();
    lePath = new QLineEdit(this);
    lePath->setPlaceholderText(tr("Path of the directory where you"
                                  " wish to export the data."));
    lePath->setClearButtonEnabled(true);
    buttonLayout->addWidget(lePath);
    QPushButton* browseButton = new QPushButton(tr("Browse"));
    buttonLayout->addWidget(browseButton);
    connect(browseButton, SIGNAL(clicked()),
            this, SLOT(browse()));
    boxLayout->addRow(tr("Path: "),buttonLayout);
    box->setLayout(boxLayout);
    return box;
}

void GImagesExporter::exportFiles()
{
    if(_exportables.isEmpty()){
        QMessageBox::warning(this,tr("Nothing to export"),
                             tr("Nothing was found that could be exported."));
        return;
    }
    if(gedExport->exec() == QDialog::Accepted){
        saveChoosenExportables();
    }
}

void GImagesExporter::browse()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                    lePath->text(),
                                                    QFileDialog::ShowDirsOnly
                                                        | QFileDialog::DontResolveSymlinks);
    if(!dir.isEmpty()){
        lePath->setText(dir);
    }
}

void GImagesExporter::saveChoosenExportables()
{
    QString mainFolderPath = lePath->text() + tr("/exported_data");
    createFolder(mainFolderPath);
    for(const auto exportable : _exportables){
        saveExportable(exportable, mainFolderPath);
    }
    writeSetting();
}

void GImagesExporter::saveExportable(AExportable *exportable, const QString& folderPath)
{
    if(gedExport->isExportableChosen(exportable)){
        QString currentFolderPath = folderPath + "/" + createFileName(exportable->getTitle());
        createFolder(currentFolderPath);
        if(!exportable->hasMoreParts()){
            saveText(currentFolderPath, createFileName(exportable->getTitle()), exportable->toHtml());
            savePicture(currentFolderPath, createFileName(exportable->getTitle()), exportable->toPixmap());
            return;
        }
        for(const auto subStage : exportable->getIndividualParts()){
            saveExportable(subStage, currentFolderPath);
        }
    }
}

void GImagesExporter::saveText(const QString& folderPath, const QString& fileName, const QString& data)
{
    QString newFileName = findFreeName(folderPath + "/" + fileName + ".html");
    QFile file(newFileName);
    if (file.open(QFile::WriteOnly | QFile::Truncate)){
        QTextStream stream(&file);
        stream << data;
        file.close();
    }
    else{
        showErrorMessage(newFileName);
    }
}

void GImagesExporter::savePicture(const QString& folderPath, const QString& fileName, const QPixmap& data)
{
    QString newFileName = findFreeName(folderPath + "/" + fileName + ".png");
    if(!data.save(newFileName)){
        showErrorMessage(newFileName);
    }
}

void GImagesExporter::showErrorMessage(const QString &fileName)
{
    QMessageBox::warning(this, tr("Something went wrong"), tr("File: ")
                             + fileName + tr(" could not be saved."), QMessageBox::Ok);
}

QString GImagesExporter::findFreeName(const QString& fileName)
{
    QString newFileName = fileName;
    int fileNameCount = 1;
    while(QFileInfo::exists(newFileName)){
        newFileName = fileName.split(".")[0] + "("
            + QString::number(fileNameCount) + ")"
            + fileName.split(".")[1];
        fileNameCount++;
    }
    return newFileName;
}

QString GImagesExporter::createFileName(const QString &title)
{
    if(title.isEmpty()){
        return tr("unidentified");
    }
    return title.trimmed();
}

void GImagesExporter::writeSetting()
{
    QSettings settings;
    settings.beginGroup("componets_export");
    settings.setValue("directory_path",lePath->text());
    settings.endGroup();
}

void GImagesExporter::readSetting()
{
    QSettings settings;
    settings.beginGroup("componets_export");
    lePath->setText(settings.value("directory_path",QDir::homePath()).toString());
    settings.endGroup();
}

void GImagesExporter::createFolder(const QString& path)
{
    QDir dir;
    if(!dir.exists(path)){
        dir.mkpath(path);
    }
}

