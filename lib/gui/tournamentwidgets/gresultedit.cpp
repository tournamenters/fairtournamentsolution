#include "gresultedit.h"
#include <QMenu>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMimeData>
#include <QAction>

GResultEdit::GResultEdit(int row, int col, QWidget *parent)
    : GDragableEdit (parent)
{
    _row = row;
    _col = col;
    _type = "result";
    setOpen(true);

    setToolTip(tr("Result format: 2 : 0"));
    setStatusTip(tr("Result format: 2 : 0"));
    setContextMenuPolicy(Qt::CustomContextMenu);
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    setAlignment(Qt::AlignCenter);
    connect(this, SIGNAL(editingFinished()),
            this, SLOT(textEditation()));
    connect(this, SIGNAL(customContextMenuRequested(const QPoint&)),
            this, SLOT(showMenu()));
}

GResultEdit::~GResultEdit()
{

}

void GResultEdit::dropEvent(QDropEvent *event)
{
    addOwnResult(event->mimeData()->text());
    event->acceptProposedAction();
}

void GResultEdit::addOwnResult(const QString &result)
{
    blockSignals(true);
    QLineEdit::setText(result);
    setModified(true);
    blockSignals(false);
    textEditation();
}

void GResultEdit::textEditation()
{
    if(contentChanged()){
        emit resultChanged(_row, _col, text(), _textOld);
    }
}

void GResultEdit::resultActionTriggered(QAction *action)
{
    addOwnResult(action->text());
}

void GResultEdit::showMenu()
{
    if(!mnuContext){
        mnuContext = new QMenu(this);
        actgrOwnResults = new QActionGroup(this);
        connect(actgrOwnResults, SIGNAL(triggered(QAction*)),
                this, SLOT(resultActionTriggered(QAction*)));
        addWinnerActions();
        addDrawActions();
        mnuContext->addAction(actgrOwnResults->addAction(tr("Clear")));
        QMenu* standardMenu = createStandardContextMenu();
        standardMenu->setTitle(tr("Standard Menu"));
        mnuContext->addMenu(standardMenu);
    }
    mnuContext->exec(QCursor::pos());
}

void GResultEdit::addDrawActions()
{
    QMenu* draw = mnuContext->addMenu(tr("Draw"));
    for (int j = 0; j<6;j++){
        QString result = QString::number(j) + " : " + QString::number(j);
        draw->addAction(actgrOwnResults->addAction(result));
    }
}

void GResultEdit::addWinnerActions()
{
    QMenu* host = mnuContext->addMenu(tr("Host win"));
    QMenu* guest = mnuContext->addMenu(tr("Guest win"));
    for (int i = 0; i < 6;i++){
        for (int j = 0; j < i; j++){
            QString result = QString::number(i) + " : " + QString::number(j);
            host->addAction(actgrOwnResults->addAction(result));
            result = QString::number(j) + " : " + QString::number(i);
            guest->addAction(actgrOwnResults->addAction(result));
        }
    }
}
