#-------------------------------------------------
#
# Project created by QtCreator 2019-04-30T21:21:55
#
#-------------------------------------------------

TARGET = tournamentwidgets
TEMPLATE = lib

QT += widgets
QT += core
QT += gui

CONFIG -= app_bundle
CONFIG += staticlib
CONFIG += c++11

DEFINES += TOURNAMENTWIDGETS_LIBRARY
DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
	gtournamenttable.cpp \
	ggrouptable.cpp \
	gteameditstable.cpp \
	gmatchestable.cpp \
	gresultedit.cpp \
	gteamedit.cpp \
	gdragabletoolbutton.cpp \
	geditationtable.cpp \
	gdragableedit.cpp \
	gcreationtable.cpp \
    gdraganddropmanager.cpp \
    gdragabletoolbars.cpp \
    geditationdialog.cpp \
    gplayoffwidget.cpp

HEADERS += \
	gtournamenttable.h \
	gteamedit.h \
	ggrouptable.h  \
	gteameditstable.h \
	gmatchestable.h \
	gresultedit.h \
	gdragabletoolbutton.h \
	geditationtable.h \
	gdragableedit.h \
	gcreationtable.h \
    gdraganddropmanager.h \
    gdragabletoolbars.h \
    geditationdialog.h \
    gplayoffwidget.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

    include($$PWD/../present/present.pri)

DISTFILES += \
    tournamentwidgets.pri

TRANSLATIONS += \
    tournamentwidgets_cs.ts
