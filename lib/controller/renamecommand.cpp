#include "renamecommand.h"
#include "amodel.h"

RenameCommand::RenameCommand(AModel *model, const QVariant &identificator, const QVariant& data, QUndoCommand *parent)
    :QUndoCommand (parent)
{
    _model = model;
    _identificator = identificator;
    _data = data;
}

RenameCommand::~RenameCommand()
{

}

void RenameCommand::undo()
{
    QVariantMap sendData;
    sendData["name"] = _data.toMap()["old"];
    _model->execEdit(ExecType::Rename, _identificator, sendData);
}

void RenameCommand::redo()
{
    QVariantMap sendData;
    sendData["name"] = _data.toMap()["new"];
    _model->execEdit(ExecType::Rename, _identificator, sendData);
}
