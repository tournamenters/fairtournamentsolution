#ifndef SWAPCOMMAND_H
#define SWAPCOMMAND_H

#include <QUndoCommand>
#include <QVariant>

class AModel;

class SwapCommand: public QUndoCommand
{

public:
    SwapCommand(AModel *model, const QVariant& identificator, const QVariant& data, QUndoCommand *parent = nullptr);
    ~SwapCommand() override;

    void undo() override;
    void redo() override;

private:
    AModel * _model;
    QVariant _identificator;
    QVariant _data;
};

#endif // SWAPCOMMAND_H
