#ifndef CMATCHESLIST_H
#define CMATCHESLIST_H
#include "amodel.h"

class CMatchesList: public AMatchesModel
{
    Q_OBJECT

public:
    CMatchesList(QObject *parent = nullptr);
    CMatchesList(const QList<CMatch*>& matches, QObject *parent = nullptr);

    void addMatch(CMatch* match);
    void addMatches(const QList<CMatch* >& matches);
    void setMatches(const QList<CMatch* >& matches);

    QVariant getData() const override;
    QVariant toVariant();

public slots:
    bool setResult(const QVariant& identificator, const QString& result) override;
    bool execEdit(ExecType type, const QVariant& identificator, const QVariant& data) override;

private:
    void createIdentificator();
};

#endif // CMATCHESLIST_H
