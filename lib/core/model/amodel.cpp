#include "amodel.h"
#include "cmatch.h"
#include "cteam.h"
#include "cteamstable.h"
#include "cstartlist.h"
#include "cfieldsmanager.h"
#include "ateamssorter.h"

AModel::AModel(QObject *parent)
    : QObject (parent)
{

}

AModel::AModel(const QString &name, QObject *parent)
    : AModel(parent)
{
    _name = name;
}

AModel::~AModel()
{

}

void AModel::setName(const QString& name)
{
    _name = name;
}

QString AModel::getName() const
{
    return _name;
}

void AModel::setIdentificator(const QVariant& identificator)
{
    _identificator = identificator.toMap();
}

void AModel::addToIdentificator(const QString &key, const QVariant &value)
{
    _identificator[key] = value;
}

QVariant AModel::getIdentificator() const
{
    return _identificator;
}

bool AModel::isItForMe(const QVariant &identificator) const
{
    QVariantMap map = identificator.toMap();
    bool isMe = false;
    for(const auto& key : _identificator.keys()){
        if(map.contains(key)){
            if(map[key] != _identificator[key]){
                return false;
            }
            else{
                isMe = true;
            }
        }
    }
    return isMe;
}

AMatchesModel::AMatchesModel(QObject *parent)
    :AModel (parent)
{

}

AMatchesModel::~AMatchesModel()
{
    _matches.clear();
}

QList<CMatch *> AMatchesModel::getMatches() const
{
    return _matches;
}

int AMatchesModel::matchesCount() const
{
    return _matches.size();
}

bool AMatchesModel::allMatchesPlayed() const
{
    for(const auto match : _matches){
        if(!match || !match->hasResult()){
            return false;
        }
    }
    return true;
}

CMatch *AMatchesModel::getNextMatchToPlay() const
{
    for(const auto match : _matches){
        if(match && match->needsResult()){
            return match;
        }
    }
    return nullptr;
}

void AMatchesModel::renameTeamInMatch(CMatch *match, const QVariant &identificator, const QVariant &data)
{
    if(match){
        if(identificator.toMap()["col"].toInt()){
            match->getGuest()->rename(data.toMap()["name"].toString());
        }
        else{
            match->getHost()->rename(data.toMap()["name"].toString());
        }
    }
}

void AMatchesModel::renameTeamInMatch(CMatch *match, const QVariant &data)
{
    if(match){
        if(data.toMap().contains("guest")){
            match->getGuest()->rename(data.toMap()["guest"].toString());
        }
        if(data.toMap().contains("host")){
            match->getHost()->rename(data.toMap()["host"].toString());
        }
    }
}

CMatch *AMatchesModel::getMatch(const QVariant &identificator) const
{
    if(identificator.toMap().contains("row")){
        int matchIndex = identificator.toMap().value("row").toInt();
        if(matchIndex<_matches.size()){
            return _matches[matchIndex];
        }
    }
    return nullptr;
}

QVariant AMatchesModel::matchesToVariant() const
{
    QVariantList list;
    for(const auto match : _matches){
        if(match){
            list.push_back(match->toVariant());
        }
        else{
            list.push_back(QVariant());
        }
    }
    return list;
}

void AMatchesModel::removeMatches()
{
    for(const auto match : _matches){
        if(match){
            disconnect(match, nullptr,
                       this, nullptr);
        }
    }
    _matches.clear();
}

QList<Match> AMatchesModel::toSortererMatches() const
{
    QList<Match> list;
    for(const auto cmatch : _matches){
        list.push_back(cmatch->toMatchPackage());
    }
    return list;
}

QVariant AMatchesModel::matchesToTable(bool withResults, bool withSources) const
{
    QStringList keys = {tr("Host"), tr("Guest")};
    if(withResults){
        keys.push_back(tr("Result"));
    }
    if(withSources){
        keys.push_back(tr("Source"));
    }
    QVariantList rows;
    QVariantList greys;    
    for(const auto match : _matches){
        QVariantMap row;
        bool grey = true;
        if(match){           
            row[tr("Host")] = match->getHostString();
            row[tr("Guest")] = match->getGuestString();
            row[tr("Result")] = match->getResult();
            row[tr("Source")] = match->getSourceString();
            grey = match->hasResult() || !match->getHost() || !match->getGuest();
        }
        rows.push_back(row);
        greys.push_back(grey);
    }
    QVariantMap table;
    table["rows"] = rows;
    table["keys"] = keys;
    table["greys"] = greys;
    return table;
}


