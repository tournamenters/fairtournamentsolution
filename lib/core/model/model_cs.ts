<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ">
<context>
    <name>AMatchesModel</name>
    <message>
        <location filename="amodel.cpp" line="180"/>
        <location filename="amodel.cpp" line="193"/>
        <source>Host</source>
        <translation>Domácí</translation>
    </message>
    <message>
        <location filename="amodel.cpp" line="180"/>
        <location filename="amodel.cpp" line="194"/>
        <source>Guest</source>
        <translation>Hostující</translation>
    </message>
    <message>
        <location filename="amodel.cpp" line="182"/>
        <location filename="amodel.cpp" line="195"/>
        <source>Result</source>
        <translation>Výsledek</translation>
    </message>
    <message>
        <location filename="amodel.cpp" line="185"/>
        <location filename="amodel.cpp" line="196"/>
        <source>Source</source>
        <translation>Zdroj</translation>
    </message>
</context>
<context>
    <name>AStageModel</name>
    <message>
        <location filename="astagemodel.cpp" line="27"/>
        <source> Team </source>
        <translation> Účastník </translation>
    </message>
</context>
<context>
    <name>CFieldsManager</name>
    <message>
        <location filename="cfieldsmanager.cpp" line="16"/>
        <source>Field</source>
        <translation>Hřiště</translation>
    </message>
</context>
<context>
    <name>CMatchesList</name>
    <message>
        <location filename="cmatcheslist.cpp" line="7"/>
        <source>Matches List</source>
        <translation>Seznam zápasů</translation>
    </message>
</context>
<context>
    <name>CModelConstants</name>
    <message>
        <location filename="global_model.h" line="88"/>
        <source>Playoff</source>
        <translation>Play off</translation>
    </message>
    <message>
        <location filename="global_model.h" line="89"/>
        <source>Robin round</source>
        <translation>Každý s každým</translation>
    </message>
    <message>
        <location filename="global_model.h" line="90"/>
        <source>Groups</source>
        <translation>Skupiny</translation>
    </message>
</context>
<context>
    <name>CNotes</name>
    <message>
        <location filename="cnotes.cpp" line="6"/>
        <source>Notes</source>
        <translation>Poznámky</translation>
    </message>
</context>
<context>
    <name>CPlayOff</name>
    <message>
        <location filename="cplayoff.cpp" line="474"/>
        <source>Final</source>
        <translation>Finále</translation>
    </message>
    <message>
        <location filename="cplayoff.cpp" line="476"/>
        <source>Semifinal</source>
        <translation>Semifinále</translation>
    </message>
    <message>
        <location filename="cplayoff.cpp" line="478"/>
        <source>Quaterfinal</source>
        <translation>Čtvrtfinále</translation>
    </message>
    <message>
        <location filename="cplayoff.cpp" line="480"/>
        <source>. round</source>
        <translation>. kolo</translation>
    </message>
    <message>
        <location filename="cplayoff.cpp" line="488"/>
        <source>Winner</source>
        <translation>Vítěz</translation>
    </message>
    <message>
        <location filename="cplayoff.cpp" line="490"/>
        <source>Beaten Finalist</source>
        <translation>Poražený finalista</translation>
    </message>
    <message>
        <location filename="cplayoff.cpp" line="492"/>
        <location filename="cplayoff.cpp" line="494"/>
        <source>Beaten Semi-Finalist</source>
        <translation>Poražený semifinalista</translation>
    </message>
    <message>
        <location filename="cplayoff.cpp" line="496"/>
        <source>Beaten in </source>
        <translation>Poražený v </translation>
    </message>
    <message>
        <location filename="cplayoff.cpp" line="496"/>
        <source> round</source>
        <translation> kolo</translation>
    </message>
</context>
<context>
    <name>CRobinRound</name>
    <message>
        <location filename="crobinround.cpp" line="209"/>
        <location filename="crobinround.cpp" line="259"/>
        <location filename="crobinround.cpp" line="293"/>
        <source>Order</source>
        <translation>Pořadí</translation>
    </message>
    <message>
        <location filename="crobinround.cpp" line="228"/>
        <source>Same Order Table</source>
        <translation>Tabulka neseřaditelných účastníků</translation>
    </message>
    <message>
        <location filename="crobinround.cpp" line="257"/>
        <location filename="crobinround.cpp" line="290"/>
        <source>Points</source>
        <translation>Body</translation>
    </message>
    <message>
        <location filename="crobinround.cpp" line="258"/>
        <location filename="crobinround.cpp" line="287"/>
        <source>Score</source>
        <translation>Skóre</translation>
    </message>
</context>
<context>
    <name>CStartList</name>
    <message>
        <location filename="cstartlist.cpp" line="15"/>
        <source>Start List</source>
        <translation>Startovní listina</translation>
    </message>
    <message>
        <location filename="cstartlist.cpp" line="96"/>
        <location filename="cstartlist.cpp" line="112"/>
        <location filename="cstartlist.cpp" line="216"/>
        <source>Rank</source>
        <translation>Nasazení</translation>
    </message>
    <message>
        <location filename="cstartlist.cpp" line="99"/>
        <location filename="cstartlist.cpp" line="109"/>
        <location filename="cstartlist.cpp" line="213"/>
        <source>Club</source>
        <translation>Klub</translation>
    </message>
    <message>
        <location filename="cstartlist.cpp" line="101"/>
        <location filename="cstartlist.cpp" line="107"/>
        <location filename="cstartlist.cpp" line="210"/>
        <source>Name</source>
        <translation>Jméno</translation>
    </message>
</context>
</TS>
