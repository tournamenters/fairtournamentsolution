#include "gmatchestable.h"
#include <QTableWidgetItem>
#include <QList>
#include <QScrollBar>
#include <QHeaderView>
#include <QPalette>
#include <QVariant>

#include "gresultedit.h"

GMatchesTable::GMatchesTable(QWidget *parent, int teamEditColumnsCount, int resultColumns)
    : GTeamEditsTable(parent, teamEditColumnsCount)
{
    _resultEditColumnsCount = resultColumns;
    _teamEditsDragAndDrop = true;
    _teamEditsRemovable = true;
}

GMatchesTable::GMatchesTable(const QVariant &identificator, QWidget *parent, int teamEditColumnsCount, int resultColumns)
    : GTeamEditsTable(identificator, parent, teamEditColumnsCount)
{
    _resultEditColumnsCount = resultColumns;
}

GMatchesTable::~GMatchesTable()
{

}

void GMatchesTable::setData(const QVariant &data)
{
    int rows = data.toMap()["rows"].toList().size();
    if(rows > rowCount()){
        setRowCount(rows);
        createVerticalHeader();
        prepareTeamCells(rows, _teamEditColumnsCount);
        prepareResultCells(rows, _resultEditColumnsCount);
    }   
    GTeamEditsTable::setData(data);
    if(data.toMap().contains("greys")){
        setGreyRows(data.toMap()["greys"].toList());
    }
}

void GMatchesTable::setResultEditColumnsCount(int resultColumns)
{
    if(resultColumns==_resultEditColumnsCount){
        return;
    }
    prepareResultCells(rowCount(), resultColumns);
    _resultEditColumnsCount = resultColumns;
}

void GMatchesTable::addResultEdit(int row, int col)
{
    if(!(qobject_cast<GResultEdit*>(cellWidget(row,col)))){
        GResultEdit* resultCell = new GResultEdit(row, col, this);       
        _resultEditCells.push_back(resultCell);
        setCellWidget(row,col,resultCell);
        connect(resultCell, SIGNAL(resultChanged(int,int,QString,QString)),
                this, SIGNAL(resultChanged(int,int,QString,QString)));
    }

}

void GMatchesTable::removeResultEdit(int row, int col)
{
    GResultEdit* resultEdit = qobject_cast<GResultEdit*>(cellWidget(row,col));
    if(resultEdit){
        removeCellWidget(row, col);
        _resultEditCells.removeOne(resultEdit);
        delete resultEdit;
    }
}

void GMatchesTable::prepareResultCells(int rows, int columns)
{
    if(columns+_teamEditColumnsCount>columnCount()){
        setColumnCount(columns+_teamEditColumnsCount);
    }
    for(int row = 0; row<rows; row++){
        for(int col = _teamEditColumnsCount; col<columnCount(); col++){
            if(col < columns + _teamEditColumnsCount){
                addResultEdit(row, col);               
            }
            else{
                removeResultEdit(row, col);
            }
        }
    }
    for(int col = _teamEditColumnsCount; col<columns + _teamEditColumnsCount; col++){
        setColumnWidth(col, columnWidth(0)/5*3);
    }
}

void GMatchesTable::setGreyRows(const QVariantList &greyData)
{
    int row = 0;
    int value = verticalScrollBar()->minimum();
    bool first = true;
    for(const auto& grey : greyData){
        if(grey.toBool()){
            setRowTextColor(row, palette().color(QPalette::PlaceholderText));
            if(first){
                value++;
            }
        }
        else{
            setRowTextColor(row, palette().color(QPalette::Text));
            first = false;
        }
        row++;
    }
    verticalScrollBar()->setValue(value);
}

void GMatchesTable::createVerticalHeader()
{
    for(int row=0;row<rowCount();row++){
        QTableWidgetItem *header = new QTableWidgetItem(QString::number(row+1));
        setVerticalHeaderItem(row,header);
    }
}





