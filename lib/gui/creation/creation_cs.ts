<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ">
<context>
    <name>AStageEditorTab</name>
    <message>
        <location filename="gstageeditortabs.cpp" line="74"/>
        <source>Name:</source>
        <translation>Jméno:</translation>
    </message>
    <message>
        <location filename="gstageeditortabs.cpp" line="91"/>
        <source>Please enter a name for your stage.</source>
        <oldsource>Please enter name for your stage.</oldsource>
        <translation>Prosím vložte jméno dané fáze.</translation>
    </message>
</context>
<context>
    <name>CreationConstants</name>
    <message>
        <location filename="global_creation.h" line="60"/>
        <source>Robin-round</source>
        <translation>Každý s každým</translation>
    </message>
    <message>
        <location filename="global_creation.h" line="61"/>
        <source>Groups and Play-off</source>
        <translation>Skupiny a play-off</translation>
    </message>
    <message>
        <location filename="global_creation.h" line="62"/>
        <source>Play-off</source>
        <translation>Vyřazovací</translation>
    </message>
    <message>
        <location filename="global_creation.h" line="63"/>
        <source>Groups</source>
        <translation>Skupiny</translation>
    </message>
</context>
<context>
    <name>GConclusionPage</name>
    <message>
        <location filename="gconclutionpage.cpp" line="15"/>
        <source>Summary</source>
        <translation>Shrnutí</translation>
    </message>
    <message>
        <location filename="gconclutionpage.cpp" line="40"/>
        <source>Please make sure you have entered all parameters correctly as making changes during a tournament may be problematic.</source>
        <translation>Prosím, ujistěte se, že jste vše nastavili správně. Dodatečné změny mohou být problematické.</translation>
    </message>
    <message>
        <location filename="gconclutionpage.cpp" line="48"/>
        <source>Tournament</source>
        <translation>Turnaj</translation>
    </message>
    <message>
        <location filename="gconclutionpage.cpp" line="51"/>
        <source>Name:</source>
        <translation>Jméno:</translation>
    </message>
    <message>
        <location filename="gconclutionpage.cpp" line="58"/>
        <source>Start list</source>
        <translation>Startovní listina</translation>
    </message>
    <message>
        <location filename="gconclutionpage.cpp" line="65"/>
        <source>Participants number:</source>
        <oldsource>Teams number:</oldsource>
        <translation>Počet účastníků:</translation>
    </message>
    <message>
        <location filename="gconclutionpage.cpp" line="66"/>
        <source>Participants  are ranked:</source>
        <oldsource>Teams are ranked:</oldsource>
        <translation>Účastníci jsou nasazeni:</translation>
    </message>
    <message>
        <location filename="gconclutionpage.cpp" line="73"/>
        <source>Stages</source>
        <translation>Fáze</translation>
    </message>
    <message>
        <location filename="gconclutionpage.cpp" line="83"/>
        <source>Type</source>
        <translation>Druh</translation>
    </message>
    <message>
        <location filename="gconclutionpage.cpp" line="84"/>
        <source>Incoming</source>
        <translation>Vstupující</translation>
    </message>
    <message>
        <location filename="gconclutionpage.cpp" line="85"/>
        <source>Eliminated</source>
        <translation>Eliminovaní</translation>
    </message>
    <message>
        <location filename="gconclutionpage.cpp" line="86"/>
        <source>Proceeding</source>
        <translation>Postupující</translation>
    </message>
    <message>
        <location filename="gconclutionpage.cpp" line="96"/>
        <source>Fields schedule</source>
        <translation>Rozvrh hřišť</translation>
    </message>
    <message>
        <location filename="gconclutionpage.cpp" line="108"/>
        <source>Fields number:</source>
        <translation>Počet hřišť:</translation>
    </message>
    <message>
        <location filename="gconclutionpage.cpp" line="114"/>
        <source>[No fields schedule.]</source>
        <translation>[Žádný plán hřišť.]</translation>
    </message>
</context>
<context>
    <name>GFieldsSettingPage</name>
    <message>
        <source>Shedule Fields</source>
        <translation type="vanished">Plánování hřišť</translation>
    </message>
    <message>
        <location filename="gfieldspage.cpp" line="16"/>
        <source>Schedule Fields</source>
        <translation>Plánování hřišť</translation>
    </message>
    <message>
        <location filename="gfieldspage.cpp" line="58"/>
        <source>Here you can set up matches scheduling on individual fields to precisely plan your tournament.</source>
        <oldsource>Here you can set up match scheduling on individual fields to precasely plan your tournament.</oldsource>
        <translation>Zde můžete nastavit způsob plánování zápasů na jednotlivá hřiště.</translation>
    </message>
    <message>
        <location filename="gfieldspage.cpp" line="66"/>
        <source>Fields</source>
        <translation>Hřiště</translation>
    </message>
    <message>
        <location filename="gfieldspage.cpp" line="88"/>
        <source>Fields number:</source>
        <translation>Počet hřišť:</translation>
    </message>
    <message>
        <location filename="gfieldspage.cpp" line="89"/>
        <source>Creation method:</source>
        <translation>Způsob plánování:</translation>
    </message>
</context>
<context>
    <name>GFormatPage</name>
    <message>
        <location filename="gformatpage.cpp" line="16"/>
        <source>Tournament Format</source>
        <translation>Turnajový formát</translation>
    </message>
    <message>
        <location filename="gformatpage.cpp" line="108"/>
        <source>Please choose your tournament format. More detailed settings and editation can be both made on the following page.</source>
        <translation>Prosím zvolte turnajový formát. Další úpravy formátu je možné provést na následující stránce.</translation>
    </message>
    <message>
        <location filename="gformatpage.cpp" line="117"/>
        <source>Prepared formats</source>
        <translation>Připravené formáty</translation>
    </message>
    <message>
        <location filename="gformatpage.cpp" line="129"/>
        <source>Custom format</source>
        <translation>Vlastní formát</translation>
    </message>
    <message>
        <location filename="gformatpage.cpp" line="138"/>
        <source>Generate format [Experimental]</source>
        <translation>Vygeneruj formát [Experimentální]</translation>
    </message>
</context>
<context>
    <name>GGroupsEditorTab</name>
    <message>
        <location filename="gstageeditortabs.cpp" line="235"/>
        <source>| Advance setting</source>
        <translation>| Pokročilé nastavení</translation>
    </message>
    <message>
        <location filename="gstageeditortabs.cpp" line="250"/>
        <source>Parameters</source>
        <translation>Parametry</translation>
    </message>
    <message>
        <location filename="gstageeditortabs.cpp" line="252"/>
        <source>Participants number:</source>
        <oldsource>Teams number:</oldsource>
        <translation>Počet účastníků:</translation>
    </message>
    <message>
        <location filename="gstageeditortabs.cpp" line="253"/>
        <source>Groups number:</source>
        <translation>Počet skupin:</translation>
    </message>
    <message>
        <location filename="gstageeditortabs.cpp" line="254"/>
        <source>Groups size:</source>
        <translation>Velikost skupin:</translation>
    </message>
    <message>
        <location filename="gstageeditortabs.cpp" line="261"/>
        <source>Algorithms</source>
        <translation>Algoritmy</translation>
    </message>
    <message>
        <location filename="gstageeditortabs.cpp" line="263"/>
        <source>Groups creation:</source>
        <translation>Losování skupin:</translation>
    </message>
    <message>
        <location filename="gstageeditortabs.cpp" line="264"/>
        <source>Matches order:</source>
        <translation>Pořadí zápasů:</translation>
    </message>
    <message>
        <location filename="gstageeditortabs.cpp" line="271"/>
        <location filename="gstageeditortabs.cpp" line="298"/>
        <source>Advanced Setting</source>
        <oldsource>Advance Setting</oldsource>
        <translation>Pokročilé nastavení</translation>
    </message>
    <message>
        <location filename="gstageeditortabs.cpp" line="273"/>
        <source>Show advanced setting dialog: </source>
        <oldsource>Show advance setting dialog: </oldsource>
        <translation>Ukaž dialog pro pokročilé nastavení:</translation>
    </message>
</context>
<context>
    <name>GIntroPage</name>
    <message>
        <location filename="gintropage.cpp" line="10"/>
        <source>Introduction</source>
        <translation>Úvod</translation>
    </message>
    <message>
        <location filename="gintropage.cpp" line="26"/>
        <source>You have decided to create a new tournament. This dialog will guide you through setting its data and parameters.</source>
        <translation>Rozhodli jste se naplánovat nový turnaj. Tento dialog Vás provede zadáváním dat účastníků a nastavováním parametrů turnaje.</translation>
    </message>
    <message>
        <location filename="gintropage.cpp" line="36"/>
        <source>Tournament name:</source>
        <translation>Název turnaje:</translation>
    </message>
    <message>
        <location filename="gintropage.cpp" line="45"/>
        <source>Please, insert a name for your new tournament.</source>
        <oldsource>Please, insert name for your new tournament.</oldsource>
        <translation>Prosím, vložte označení Vašeho turnaje.</translation>
    </message>
</context>
<context>
    <name>GPlayOffEditorTab</name>
    <message>
        <location filename="gstageeditortabs.cpp" line="365"/>
        <source>Parameters</source>
        <translation>Parametry</translation>
    </message>
    <message>
        <location filename="gstageeditortabs.cpp" line="367"/>
        <source>Participants  number:</source>
        <oldsource>Teams number:</oldsource>
        <translation>Počet účastníků:</translation>
    </message>
    <message>
        <location filename="gstageeditortabs.cpp" line="374"/>
        <source>Algorithms</source>
        <translation>Algoritmy</translation>
    </message>
    <message>
        <location filename="gstageeditortabs.cpp" line="376"/>
        <source>Play off creation:</source>
        <translation>Losování pavouka:</translation>
    </message>
</context>
<context>
    <name>GPointsSettingBox</name>
    <message>
        <location filename="gpointssettingbox.cpp" line="13"/>
        <source>Points and criteria setting</source>
        <translation>Nastavení bodů a kritérií</translation>
    </message>
    <message>
        <location filename="gpointssettingbox.cpp" line="17"/>
        <source>Sport Profile:</source>
        <translation>Profil sportu:</translation>
    </message>
    <message>
        <location filename="gpointssettingbox.cpp" line="19"/>
        <source>Order of Criteria:</source>
        <translation>Pořadí kritérií:</translation>
    </message>
    <message>
        <location filename="gpointssettingbox.cpp" line="40"/>
        <source>Football</source>
        <translation>Fotbal</translation>
    </message>
    <message>
        <location filename="gpointssettingbox.cpp" line="44"/>
        <source>Hockey</source>
        <translation>Hokej</translation>
    </message>
    <message>
        <location filename="gpointssettingbox.cpp" line="50"/>
        <source>Table tennis</source>
        <translation>Stolní tenis</translation>
    </message>
    <message>
        <location filename="gpointssettingbox.cpp" line="58"/>
        <source>Custom</source>
        <translation>Vlastní</translation>
    </message>
    <message>
        <location filename="gpointssettingbox.cpp" line="175"/>
        <source>Points</source>
        <translation>Body</translation>
    </message>
    <message>
        <location filename="gpointssettingbox.cpp" line="211"/>
        <source>Win</source>
        <translation>Výhra</translation>
    </message>
    <message>
        <location filename="gpointssettingbox.cpp" line="212"/>
        <source>Draw</source>
        <translation>Remíza</translation>
    </message>
    <message>
        <location filename="gpointssettingbox.cpp" line="213"/>
        <source>Loss</source>
        <translation>Prohra</translation>
    </message>
    <message>
        <location filename="gpointssettingbox.cpp" line="219"/>
        <source>Over Time</source>
        <translation>Prodloužení</translation>
    </message>
    <message>
        <location filename="gpointssettingbox.cpp" line="238"/>
        <source>Win OT</source>
        <translation>Výhra pp.</translation>
    </message>
    <message>
        <location filename="gpointssettingbox.cpp" line="239"/>
        <source>Loss OT</source>
        <translation>Prohra pp.</translation>
    </message>
</context>
<context>
    <name>GRobinRoundEditorTab</name>
    <message>
        <location filename="gstageeditortabs.cpp" line="445"/>
        <source>Parameters</source>
        <translation>Parametry</translation>
    </message>
    <message>
        <location filename="gstageeditortabs.cpp" line="447"/>
        <source>Participants number:</source>
        <oldsource>Teams number:</oldsource>
        <translation>Počet účastníků:</translation>
    </message>
    <message>
        <location filename="gstageeditortabs.cpp" line="455"/>
        <source>Algorithms</source>
        <translation>Algoritmy</translation>
    </message>
    <message>
        <location filename="gstageeditortabs.cpp" line="457"/>
        <source>Matches order:</source>
        <translation>Pořadí zápasů:</translation>
    </message>
    <message>
        <location filename="gstageeditortabs.cpp" line="473"/>
        <location filename="gstageeditortabs.cpp" line="482"/>
        <source>Advanced Setting</source>
        <translation>Pokročilé nastavení</translation>
    </message>
    <message>
        <location filename="gstageeditortabs.cpp" line="475"/>
        <source>Show advanced setting dialog: </source>
        <translation>Ukaž dialog pro pokročilé nastavení: </translation>
    </message>
    <message>
        <location filename="gstageeditortabs.cpp" line="492"/>
        <source>| Advance setting</source>
        <translation>| Pokročilé nastavení</translation>
    </message>
</context>
<context>
    <name>GStagesCreationPage</name>
    <message>
        <location filename="gstagespage.cpp" line="12"/>
        <source>Tournament Stages</source>
        <translation>Turnajové fáze</translation>
    </message>
    <message>
        <location filename="gstagespage.cpp" line="13"/>
        <source>You can create your own tournament format or edit prepared one here.</source>
        <translation>Zde můžete vytvořit svůj vlastní turnajový formát nebo upravit připravené.</translation>
    </message>
    <message>
        <location filename="gstagespage.cpp" line="47"/>
        <source>Please enter at least one stage of your choice from the menu below to the frame where you can set its properties.</source>
        <translation>Prosím zvolte alespoň jednu fázi z menu níže vlevo a přeneste ji do rámce, ve kterém budete moci nastavit její parametry.</translation>
    </message>
    <message>
        <location filename="gstagespage.cpp" line="57"/>
        <source>Stages</source>
        <translation>Fáze</translation>
    </message>
    <message>
        <location filename="gstagespage.cpp" line="59"/>
        <source>Robin-Round
All participants will play matches with each other.</source>
        <translation>Každý s každým
Každý účastník hraje jeden zápas proti všem ostatním.</translation>
    </message>
    <message>
        <location filename="gstagespage.cpp" line="61"/>
        <source>Groups
Divide participants into several groups.
Each group will play independent robin-round tournament.</source>
        <translation>Skupiny
Účastníci jsou rozděleni do skupin,
 ve kterých odehrají nezávislé turnaje každý s každým.</translation>
    </message>
    <message>
        <location filename="gstagespage.cpp" line="64"/>
        <source>Play Off
All participants will play one match,
the losers are eliminated and the winners play
another game with each other until only on remains
undefeated.</source>
        <translation>Vyřazovací
Všichni účastníci odehrají jeden zápas,
poražení končí a vítězové pokračují do dalšího kola.
Turnaj pokračuje dokud nezbývá pouze jeden neporažený účastník.</translation>
    </message>
</context>
<context>
    <name>GStartListInsertingPage</name>
    <message>
        <location filename="gstartlistpage.cpp" line="32"/>
        <source>Start List</source>
        <translation>Startovní listina</translation>
    </message>
    <message>
        <location filename="gstartlistpage.cpp" line="33"/>
        <source>Please enter your tournament participants&apos; data in the table below or set the number of them at least.</source>
        <translation>Prosím vložte data účastníků turnaje do tabulky níže nebo alespoň určete jejich počet.</translation>
    </message>
    <message>
        <location filename="gstartlistpage.cpp" line="59"/>
        <source>You can edit the names and data later. However the total number of participants cannot be increased after the creation of the tournament.</source>
        <translation>Pozor, data účastníků je možné v průběhu turnaje měnit. Nicméně jejich celkový počet již měnit nelze.</translation>
    </message>
    <message>
        <location filename="gstartlistpage.cpp" line="100"/>
        <source>Import start list from a file</source>
        <translation>Vlož startovní listinu ze souboru</translation>
    </message>
    <message>
        <location filename="gstartlistpage.cpp" line="108"/>
        <source>Enter only size of the list</source>
        <translation>Zadat pouze počet účastníků</translation>
    </message>
    <message>
        <location filename="gstartlistpage.cpp" line="116"/>
        <source>Seed participants</source>
        <translation>Nasazení účastníků</translation>
    </message>
    <message>
        <location filename="gstartlistpage.cpp" line="130"/>
        <source>Rankings [1,2,...]</source>
        <translation>Pořadím [1,2,...]</translation>
    </message>
    <message>
        <location filename="gstartlistpage.cpp" line="134"/>
        <source>Coefficient [ELO,...]</source>
        <translation>Koeficientem [Elo,...]</translation>
    </message>
    <message>
        <location filename="gstartlistpage.cpp" line="142"/>
        <source>Classify participants by clubs</source>
        <translation>Dělení účastníků dle klubů</translation>
    </message>
    <message>
        <location filename="gstartlistpage.cpp" line="152"/>
        <source>Clear start list</source>
        <translation>Smaž startovní listinu</translation>
    </message>
    <message>
        <location filename="gstartlistpage.cpp" line="162"/>
        <source>Add a custom column</source>
        <translation>Přidej sloupec</translation>
    </message>
    <message>
        <location filename="gstartlistpage.cpp" line="203"/>
        <source>Name</source>
        <translation>Jméno</translation>
    </message>
    <message>
        <location filename="gstartlistpage.cpp" line="204"/>
        <source>Rank</source>
        <translation>Nasazení</translation>
    </message>
    <message>
        <location filename="gstartlistpage.cpp" line="205"/>
        <source>Club</source>
        <translation>Klub</translation>
    </message>
    <message>
        <location filename="gstartlistpage.cpp" line="287"/>
        <source>Number of participants</source>
        <translation>Počet účastníků</translation>
    </message>
    <message>
        <location filename="gstartlistpage.cpp" line="287"/>
        <source>Enter the number of participants:</source>
        <translation>Nastav počet účastníků:</translation>
    </message>
    <message>
        <location filename="gstartlistpage.cpp" line="292"/>
        <source>Team </source>
        <translation>Účastník </translation>
    </message>
    <message>
        <location filename="gstartlistpage.cpp" line="293"/>
        <source>Club </source>
        <translation>Klub </translation>
    </message>
    <message>
        <location filename="gstartlistpage.cpp" line="330"/>
        <source>Select file</source>
        <translation>Vyber soubor</translation>
    </message>
    <message>
        <location filename="gstartlistpage.cpp" line="330"/>
        <source>Lists (*.txt);Tournaments (*.turn)</source>
        <translation>Seznamy (*.txt);Turnaje (*.turn)</translation>
    </message>
    <message>
        <location filename="gstartlistpage.cpp" line="334"/>
        <source>The file could not be opened: %1
ERROR:%22</source>
        <translation>Tento soubor nemohl být otevřen: %1
ERROR:%22</translation>
    </message>
</context>
<context>
    <name>GTabStagesEditor</name>
    <message>
        <location filename="gtabstageseditor.cpp" line="134"/>
        <source>Are you sure?</source>
        <translation>Jste si jistý?</translation>
    </message>
    <message>
        <location filename="gtabstageseditor.cpp" line="135"/>
        <source>Do you really want to clear the tabs?All entererd data will by lost.</source>
        <translation>Opravdu chcete smazat všechny fáze. Vložená data budou stracena.</translation>
    </message>
    <message>
        <location filename="gtabstageseditor.cpp" line="201"/>
        <source>[Drop stages here.]</source>
        <translation>[Pusťte fáze zde.]</translation>
    </message>
</context>
<context>
    <name>NewTournament</name>
    <message>
        <location filename="gnewdialog.cpp" line="26"/>
        <source> | New Tournament Dialog</source>
        <translation> | Dialog pro vytváření nového turnaje</translation>
    </message>
    <message>
        <location filename="gnewdialog.cpp" line="100"/>
        <source>Sorry, the help is not available yet.</source>
        <translation>Omlouváme se, pomoc zde není zatím dostupná.</translation>
    </message>
    <message>
        <source>Do you really want to quit? 
All entered data will be lost.</source>
        <translation type="vanished">Opravdu chcete skončit?
V3echna data budou stracena.</translation>
    </message>
    <message>
        <location filename="gnewdialog.cpp" line="71"/>
        <source>Do you really want to quit?
 All entered data will be lost.</source>
        <translation>Opravdu chcete skončit?
Všechna zadaná data budou stracena.</translation>
    </message>
    <message>
        <source>Sorry, help is not available yet.</source>
        <translation type="vanished">Omlouváme se, pomoc zde zatím není dostupná.</translation>
    </message>
    <message>
        <location filename="gnewdialog.cpp" line="100"/>
        <source>New Tournament Help</source>
        <translation>Pomoc se zakládáním turnaje</translation>
    </message>
</context>
</TS>
