#include "gstagespage.h"
#include "global_creation.h"
#include "gtabstageseditor.h"
#include "gdragabletoolbars.h"

#include <QBoxLayout>
#include <QLabel>

GStagesCreationPage::GStagesCreationPage(QWidget *parent)
    : QWizardPage(parent)
{
    setTitle(tr("Tournament Stages"));
    setSubTitle(tr("You can create your own tournament"
                   " format or edit prepared one here."));
    registerField("stages.data*", this, "_stagesData", SIGNAL(stagesDataChanged()));

    QVBoxLayout * vLayout = new QVBoxLayout();
    vLayout->addWidget(createTitle());
    QHBoxLayout * hLayout = new QHBoxLayout();
    hLayout->addWidget(createStagesTabWidget());
    hLayout->addWidget(createStagesToolBar());
    vLayout->addLayout(hLayout);
    setLayout(vLayout);
}

GStagesCreationPage::~GStagesCreationPage()
{
    _stagesData.clear();
}

int GStagesCreationPage::nextId() const
{
    return PageIndex::Page_Fields;
}

void GStagesCreationPage::initializePage()
{
    stagesSettingTabWidget->setNumTeams(field("start_list.num_teams").toInt());
    if(field("format.prepared").toBool()){
        setField("format.prepared", QVariant(false));
        stagesSettingTabWidget->initialize(field("format.prepared_type").toString());
    }
}

QLabel *GStagesCreationPage::createTitle()
{
    QLabel* label = new QLabel(tr("Please enter at"
                                  " least one stage of your choice from the menu below"
                                  " to the frame where you can set its properties."), this);
    label->setWordWrap(true);
    return label;
}

QToolBar *GStagesCreationPage::createStagesToolBar()
{
    QMap<QString, QString> formats = CreationConstants::getInstance().getFormatsMap();
    tbarStages = new GDragableToolBar(tr("Stages"),this);
    tbarStages->addToolButton(QIcon(":/icons/robin_round.png"), formats[keyRobinRound],
                              tr("Robin-Round\nAll participants will play matches with each other."));
    tbarStages->addToolButton(QIcon(":/icons/groups.png"), formats[keyGroups],
                              tr("Groups\nDivide participants into several groups.\n"
                                 "Each group will play independent robin-round tournament."));
    tbarStages->addToolButton(QIcon(":/icons/playoff.png"), formats[keyPlayOff],
                              tr("Play Off\nAll participants will play one match,\n"
                                 "the losers are eliminated and the winners play\n"
                                 "another game with each other until only on remains\n"
                                 "undefeated."));
    tbarStages->setOrientation(Qt::Vertical);
    tbarStages->setIconSize(tbarStages->iconSize()*5/3);
    return tbarStages;
}

QTabWidget *GStagesCreationPage::createStagesTabWidget()
{
    stagesSettingTabWidget = new GTabStagesEditor(this);
    stagesSettingTabWidget->setAcceptDrops(true);
    connect(stagesSettingTabWidget,SIGNAL(dataChanged()),
            this, SLOT(stagesDataChange()));
    connect(stagesSettingTabWidget, SIGNAL(currentChanged(int)),
            this, SLOT(currentStageChange(int)));
    return stagesSettingTabWidget;
}

void GStagesCreationPage::stagesDataChange()
{
    QVariantList data = stagesSettingTabWidget->getStagesData();
    if(stagesData() == data){
        return;
    }
    setStagesData(data);
    emit stagesDataChanged();
}

void GStagesCreationPage::currentStageChange(int index)
{
    if(index == -1){
        tbarStages->setEnabled(true);
        return;
    }
    tbarStages->setEnabled(4 <= stagesSettingTabWidget->getStagesNumTeams(index));
}

QList<QVariant> GStagesCreationPage::stagesData() const
{
    return _stagesData;
}

bool GStagesCreationPage::isComplete() const
{
    return !stagesData().isEmpty();
}

void GStagesCreationPage::setStagesData(const QList<QVariant> &stagesData)
{
    _stagesData = stagesData;
}
