#pragma once

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "lplayoffdrawerfactory.h"
#include "tst_robinroundmatchesorder.h"
#include "tst_groupscreation.h"

#include <QList>
#include <QtMath>

using namespace testing;


QList<QList<int>> allRoundsToOne(const QList<QList<QList<int>>>& matches)
{
    QList<QList<int>>list;
    for (const auto& round : matches) {
        if(!round.isEmpty()){
            list.append(round);
        }
    }
    return list;
}

void rightNumberOfMatches(const QList<QList<QList<int>>>& matches, int numTeams)
{
    auto allRounds = allRoundsToOne(matches);
    int matchesCount = 0;
    for(const auto& match : allRounds){
        if(!match.isEmpty()){
            matchesCount++;
        }
    }
    ASSERT_EQ(matchesCount, numTeams-1);
}

void rightNumberOfRounds(const QList<QList<QList<int>>>& matches, int numTeams)
{
    ASSERT_TRUE(qPow(2,matches.size()) >= numTeams);
    ASSERT_TRUE(qPow(2,matches.size()-1) < numTeams);
    ASSERT_TRUE(qPow(2,matches.size()+1) > numTeams);
}

void everybodyOnlyOnceInEachRound(const QList<QList<QList<int>>>& matches, int numTeams)
{
    for (const auto& round : matches) {
        if(2*round.size()<=numTeams){
            for(int i = 0;i<round.size()*2;i++){
                ASSERT_EQ(toOneList(round).count(i), 1);
            }
        }
        else{
            for(int i = 2*round.size() - numTeams;i<numTeams;i++){
                ASSERT_EQ(toOneList(round).count(i), 1);
            }
        }
    }
}

void testMatches(const QList<QList<QList<int>>>& matches, int numTeams)
{
    rightNumberOfRounds(matches, numTeams);
    rightNumberOfMatches(matches, numTeams);
    nobodyPlaysItself(allRoundsToOne(matches));
}

void testSimplePlayOffDrawer(APlayOffDrawer* drawer)
{
    for (int numTeams = 2; numTeams < 50; numTeams++){
        auto matches = drawer->drawPlayOff(numTeams);
        testMatches(matches, numTeams);
        everybodyOnlyOnceInEachRound(matches, numTeams);
    }
}

int sumList(const QList<int>& list)
{
    int sum = 0;
    for(int i : list){
        sum += i;
    }
    return sum;
}

QList<int> createNumTeamsList(int numSources, int numTeams)
{
    QList<int> numTeamsBySources;
    for(int i = 0; i<numSources; i++){
        numTeamsBySources.push_back(numTeams/numSources);
    }
    int index = 0;
    while(sumList(numTeamsBySources) < numTeams){
        numTeamsBySources[index]++;
        index++;
    }
    return numTeamsBySources;
}

void testSourcesPlayOffDrawer(APlayOffDrawer* drawer)
{
    QList<int> numTeamsBySources;
    for (int numSources = 1; numSources<15; numSources++){
        for (int numTeams = numSources; numTeams < 65; numTeams++){
            if(numTeams<2){
                numTeams++;
            }
            numTeamsBySources = createNumTeamsList(numSources, numTeams);
            drawer->drawPlayOffWithDifferentSources(numTeamsBySources);
            testMatches(drawer->getActualPlayOff(), sumList(numTeamsBySources));
        }
    }
}
QList<QList<QList<QList<int >>>> entryListToIntList(QList<QList<QList<Entry >>> matches)
{
    QList<QList<QList<QList<int >>>> lists;
    for(const auto& round : matches){
        lists.push_back(QList<QList<QList<int >>>());
        for(const auto& match : round){
            lists.last().push_back(QList<QList<int >>());
            for(const auto& entry : match){
                lists.last().last().push_back({entry._sourceIndex, entry._rankInSource});
            }
        }
    }
    return lists;
}


TEST(playoffcreation, advantageous_simple)
{
    APlayOffDrawer* advantageous = LPlayOffDrawerFactory::Instance()->createDrawer("advantageous");
    testSimplePlayOffDrawer(advantageous);
    QList<QList<QList<int>>> rightForEight = {{{0,7},{4,3},{2,5},{6,1}},
                                             {{0,3},{2,1}},
                                             {{0,1}}};

    ASSERT_EQ(rightForEight, advantageous->drawPlayOff(8));
    QList<QList<QList<int>>> rightForNine = {{{},{8,7},{},{},{},{},{},{}},
                                            {{0,7},{4,3},{2,5},{6,1}},
                                            {{0,3},{2,1}},
                                            {{0,1}}};
    ASSERT_EQ(rightForNine, advantageous->drawPlayOff(9));
    QList<QList<int>> rightMatchesOrder = {{8,7},{0,7},{4,3},{6,1},{2,5},{0,3},{2,1},{0,1}};
    ASSERT_EQ(rightMatchesOrder, advantageous->getMatchesInOrder());
    delete advantageous;
}

TEST(playoffcreation, advantageous_sources)
{
    APlayOffDrawer* advantageous = LPlayOffDrawerFactory::Instance()->createDrawer("advantageous");
    testSourcesPlayOffDrawer(advantageous);

    QList<QList<QList<QList<int >>>> rightForThreeFromThree = {{{},{{2,2},{1,2}},{},{},{},{},{},{}},
                                                              {{{0,0},{1,2}},{{2,1},{1,1}},{{2,0},{0,1}},{{0,2},{1,0}}},
                                                              {{{0,0},{1,1}},{{2,0},{1,0}}},
                                                              {{{0,0},{1,0}}}};
    ASSERT_EQ(rightForThreeFromThree, entryListToIntList(advantageous->drawPlayOffWithDifferentSources({3,3,3})));
    delete advantageous;
}


TEST(playoffcreation, equal_simple)
{
    APlayOffDrawer* equal = LPlayOffDrawerFactory::Instance()->createDrawer("equal");
    testSimplePlayOffDrawer(equal);
    QList<QList<QList<int>>> rightForEight = {{{0,5},{4,3},{2,7},{6,1}},
                                              {{0,3},{2,1}},
                                              {{0,1}}};

    ASSERT_EQ(rightForEight, equal->drawPlayOff(8));
    QList<QList<QList<int>>> rightForNine = {{{},{},{},{},{},{8,7},{},{}},
                                             {{0,5},{4,3},{2,7},{6,1}},
                                             {{0,3},{2,1}},
                                             {{0,1}}};
    ASSERT_EQ(rightForNine, equal->drawPlayOff(9));

    QList<QList<int>> rightMatchesOrder = {{8,7},{0,5},{4,3},{6,1},{2,7},{0,3},{2,1},{0,1}};
    ASSERT_EQ(rightMatchesOrder, equal->getMatchesInOrder());
    delete equal;
}

TEST(playoffcreation, equal_sources)
{
    APlayOffDrawer* equal = LPlayOffDrawerFactory::Instance()->createDrawer("equal");
    testSourcesPlayOffDrawer(equal);

    QList<QList<QList<QList<int >>>> rightForThreeFromThree = {{{},{{2,2},{1,2}},{},{},{},{},{},{}},
                                                              {{{0,0},{1,2}},{{2,1},{1,1}},{{2,0},{0,2}},{{0,1},{1,0}}},
                                                              {{{0,0},{1,1}},{{2,0},{1,0}}},
                                                              {{{0,0},{1,0}}}};
    ASSERT_EQ(rightForThreeFromThree, entryListToIntList(equal->drawPlayOffWithDifferentSources({3,3,3})));
    delete equal;
}
