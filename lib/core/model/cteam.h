#ifndef CTEAM_H
#define CTEAM_H

#include <QString>
#include <QObject>

class CTeam: public QObject
{
    Q_OBJECT

public:
    CTeam(int id, QObject* parent = nullptr);
    CTeam(int id, const QString& name, QObject* parent = nullptr);
    ~CTeam();

    void setClub(const QString &value);
    void setRank(int value);
    void rename(const QString& newName);
    void setActive(bool active);

    int getID() const;
    QString getClub() const;    
    int getRank() const;
    bool isActive() const;

    QString toString() const;
    QStringList toStringList() const;
    QVariant toVariant() const;

    static void swapData(CTeam* first, CTeam *second);

signals:
    void updated();
    void activeChanged();

private:
    int _id;
    int _rank;

    QString _name;
    QString _club;

    bool _active;
};

#endif // CTEAM_H
