#include "gstageviewfactory.h"
#include "ggroupstageview.h"
#include "grobinroundview.h"
#include "gplayoffview.h"
#include "astagemodel.h"

GStageViewFactory::GStageViewFactory()
{
    registerType("robin_round", GRobinRoundView::create);
    registerType("groups_stage", GGroupStageView::create);
    registerType("play_off", GPlayOffView::create);
}

GStageViewFactory *GStageViewFactory::getInstance()
{
    static GStageViewFactory instance;
    return &instance;
}

GStageViewFactory::~GStageViewFactory()
{
    registeredTypes.clear();
}

QStringList GStageViewFactory::keys()
{
    return registeredTypes.keys();
}

void GStageViewFactory::registerType(const QString &key, FcnCreateStageView create)
{
    registeredTypes[key]=create;
}

AStageView *GStageViewFactory::create(AStageModel* model)
{
    return registeredTypes[model->getType()](model);
}
