#ifndef GEDITATIONTABLE_H
#define GEDITATIONTABLE_H

#include "gtournamenttable.h"

class GEditationTable : public GTournamentTable
{
    Q_OBJECT

public:
    GEditationTable(const QVariant& data, QWidget *parent = nullptr);

    QVariant getNewData() const;
    QVariant getOriginalData() const;

private:
    void setColumnHeaders(const QStringList& originalHeaders);
    void addRow(int row, const QVariant& data);
    void addDoubleCell(int row, int col, const QString& text);
    void addLabel(int row, int col, const QString& text);
    void addLineEdit(int row, int col, const QString& text);

    QStringList _originalHeaders;
    QVariant _originalData;
};

#endif // GEDITATIONTABLE_H
