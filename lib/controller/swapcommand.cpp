#include "swapcommand.h"
#include "amodel.h"

SwapCommand::SwapCommand(AModel *model, const QVariant &identificator, const QVariant& data, QUndoCommand *parent)
    :QUndoCommand (parent)
{
    _model = model;
    _identificator = identificator;
    _data = data;
}

SwapCommand::~SwapCommand()
{

}

void SwapCommand::undo()
{
    _model->execEdit(ExecType::Swap, _identificator, _data);
}

void SwapCommand::redo()
{
    _model->execEdit(ExecType::Swap, _identificator, _data);
}
