#ifndef AGROUPMATCHESDRAWER_H
#define AGROUPMATCHESDRAWER_H

#include <QList>
#include <QPair>

class AMatchesOrderer
{

public:
    AMatchesOrderer();
    virtual ~AMatchesOrderer();
    virtual QList<QList<int>> planMatches(int numTeams) = 0;
    virtual AMatchesOrderer* copy() = 0;

protected:
    QList<QList<int>> prepareStandardFirstRound(int numTeams);
    QPair<QList<int>, QList<int>> getHostAndGuestsLists(const QList<QList<int>>& matches);
    QList<QList<int>> getMatches(const QPair<QList<int>, QList<int>>& hostAndGuests);
    QList<QList<int>> rotateMatrix(const QList<QList<int>>& matches, const QPair<int,int>& fixedPosition);
    QList<QList<int>> removeMatchesWithTeam(const QList<QList<int>>& matches, int teamIndex);
    QList<int> switchHostAndGuest(QList<int> match);
};


class LBergerTables: public AMatchesOrderer
{

public:
    LBergerTables();
    ~LBergerTables() override;
    QList<QList<int>> planMatches(int numTeams) override;
    AMatchesOrderer* copy() override;
    static AMatchesOrderer * __stdcall  create();

private:
    QList<QList<int>> optimalizeOddTeamsPlan(const QList<QList<int>>& matches, int numTeams);
};


class LRotation: public AMatchesOrderer
{

public:
    LRotation();
    ~LRotation() override;
    QList<QList<int>> planMatches(int numTeams) override;
    AMatchesOrderer* copy() override;
    static AMatchesOrderer * __stdcall  create();

};

class LKirkman: public AMatchesOrderer
{

public:
    LKirkman();
    ~LKirkman() override;
    QList<QList<int>> planMatches(int numTeams) override;
    AMatchesOrderer* copy() override;
    static AMatchesOrderer * __stdcall  create();

private:
    QList<QList<int>> switchHostAndGuestInEvenMatches(const QList<QList<int>>& matches);
    QList<QList<int>> moveMatchWithFirstUp(const QList<QList<int>>& matches);
};
#endif // AGROUPMATCHESDRAWER_H
