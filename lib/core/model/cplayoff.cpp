#include "cplayoff.h"
#include "aplayoffdrawer.h"
#include <QtMath>
#include "cteamstable.h"
#include "global_model.h"
#include "cteam.h"

CPlayOff::CPlayOff(int id, const QString &name, APlayOffDrawer * drawer, QObject *parent)
    : AStageModel(id, StageType::PlayOff, parent)
{
    setName(name);
    _drawer = drawer;
}

CPlayOff::~CPlayOff()
{
    clear();
    delete _drawer;
}

void CPlayOff::setTeamsCount(int numTeams)
{
    AStageModel::setTeamsCount(numTeams);
    createMatches(numTeams);
}

void CPlayOff::setTeams(const QList<CTeam*>& teams)
{
    setTeams(new CTeamsList(teams));
}

void CPlayOff::setTeams(CTeamsList *teams)
{
    blockSignals(true);
    if(_matchesByRounds.empty()){
        auto intMatches = _drawer->drawPlayOff(teamsCount());
        createMatchesMatrix(intMatches);
    }
    _teams->changeTeams(teams);
    placeTeamsToMatches(_teams);
    blockSignals(false);
    emit dataChanged();
}

void CPlayOff::setTeamsFromSources(const QList<CTeamsList *> &teams)
{
    blockSignals(true);
    QList<int> sourcesNumbers;
    for(const auto& source : teams){
        sourcesNumbers.push_back(source->getTeams().size());
    }
    if(_matchesByRounds.empty()){
        _drawer->drawPlayOffWithDifferentSources(sourcesNumbers);
        createMatchesMatrix(_drawer->getActualPlayOff());
    }
    _teams->changeTeams(teams);
    placeTeamsToMatches(_teams);
    blockSignals(false);
    emit dataChanged();
}

void CPlayOff::setNextStage(AStageModel *nextStage)
{
    _nextStage = nextStage;
    if(_nextStage){
        nextStageCare();
    }
}

CTeam *CPlayOff::getTeam(const QVariant &identificator) const
{
    return findTeam(identificator.toMap());
}

void CPlayOff::setTeamsFromVariant(const QVariant &variant, const QMap<int, CTeam *>& teamsById)
{
    QList<CTeam*> teams;
    for(const auto& teamVariant : variant.toMap()["teams"].toList()){
        if(!teamVariant.toMap().isEmpty()){
            teams.push_back(teamsById[teamVariant.toMap()["id"].toInt()]);
        }
        else{
            teams.push_back(nullptr);
        }
    }
    setTeams(new CTeamsList(teams));
}

void CPlayOff::resultsFromVariant(const QVariant &variant)
{
    int roundIndex = 0;
    for(const auto& list: variant.toMap()["rounds"].toList()){
        int matchIndex = 0;
        for(const auto& match: list.toList()){
            if(match.toMap().contains("result")){
                setResult(roundIndex, matchIndex, match.toMap()["result"].toString());
            }
            matchIndex++;
        }
        roundIndex++;
    }
}

void CPlayOff::setResult(int roundIndex, int matchIndex, const QString& result)
{
    CPlayOffMatch * match = findMatch(roundIndex, matchIndex);
    if(match){
        match->setResult(result);
    }
    if(finished()){
        nextStageCare();
    }
}

void CPlayOff::nextStageCare()
{
    if (_nextStage){
        int proceedingTeamsNumber = _nextStage->teamsCount();
        CTeamsList* results = countStandings();
        CTeamsList* groupProceedingTable = new CTeamsList();
        groupProceedingTable->setColumnHeaders(results->getColumnHeaders());
        QList<QString> placeholders;
        for (int i = 0; i<=results->getTeams().size() && i< proceedingTeamsNumber;i++){
            groupProceedingTable->addRow(results->getRow(i));
            placeholders.push_back(getName() + " " + QString::number(i+1));
            if(!finished()){
                groupProceedingTable->changeTeam(i, nullptr);
            }
        }
        groupProceedingTable->addColumn("place_holder", placeholders);
        _nextStage->setTeams(groupProceedingTable);
        delete results;
    }
}

QVariant CPlayOff::getData() const
{
    return toVariant();
}

QVariant CPlayOff::toVariant() const
{
    QVariantMap map;
    map["name"] = getName();
    map["type"] = key_PLAY_OFF;

    QVariantMap mapParameters;
    mapParameters["num_teams"] = teamsCount();
    map["parameters"] = mapParameters;

    if(getWinner()){
        map["winner"] = QVariant(getWinner()->toString());
    }
    QVariantList list;
    for(const auto &matchList : _matchesByRounds){
        QVariantList roundList;
        for(const auto match: matchList){
            if(match){
                roundList.push_back(match->toVariant());
            }
            else{
                roundList.push_back(QVariant());
            }
        }
        list.push_back(roundList);
    }
    map["rounds"] = list;
    map["teams"] = _teams->teamsToVariant();
    return map;
}

void CPlayOff::changeTeam(const QVariant &identificator, CTeam *team)
{
    CMatch* match = findMatch(identificator.toMap());
    if(match){
        match->setTeam(team, identificator.toMap().contains("host"));
    }
}

CTeamsList* CPlayOff::countStandings()
{
    int counter = _matchesByRounds.size() - 1;
    QList<CTeam*> teamsInOrder;
    QList<int> orderInOrder;
    QList<int> orderIsFinal;
    for(const auto& round: _matchesByRounds){
        int order = static_cast<int>(pow(2, counter)) + 1;
        for(const auto match : round){
            if(match){
                if(match->getLoser()){
                    teamsInOrder.push_front(match->getLoser());
                    orderInOrder.push_front(order);
                    orderIsFinal.push_front(true);
                }
                else{
                    teamsInOrder.push_front(nullptr);
                    orderInOrder.push_front(order);
                    orderIsFinal.push_front(false);
                }
            }
        }
        counter--;
    }
    if(_matchesByRounds.last().first()){
        teamsInOrder.push_front(_matchesByRounds.last().first()->getWinner());
        orderInOrder.push_front(1);
        orderIsFinal.push_front(_matchesByRounds.last().first()->getWinner() != nullptr);
    }
    else{
        teamsInOrder.push_front(nullptr);
        orderInOrder.push_front(1);
        orderIsFinal.push_front(false);
    }
    CTeamsList * teamsTable = new CTeamsList(teamsInOrder);
    teamsTable->addColumn("order", orderInOrder);
    teamsTable->addColumn("order_is_final", orderIsFinal);
    return teamsTable;
}

bool CPlayOff::setResult(const QVariant& identificator, const QString& result)
{
    if(isItForMe(identificator)){
        CMatch* match = findMatch(identificator.toMap());
        if(match){
            blockSignals(true);
            match->setResult(result);
            blockSignals(false);
            emit dataChanged();
            if(finished()){
                nextStageCare();
            }
            return true;
        }
    }
    return false;
}

bool CPlayOff::execEdit(ExecType type, const QVariant &identificator, const QVariant &data)
{
    if(isItForMe(identificator)){
        switch (type) {
        case ExecType::Swap:
            return swapTeams(data.toMap());
        case ExecType::Rename:
            return renameTeam(identificator, data);
        default:
            return false;
        }
    }
    return false;
}

void CPlayOff::matchChanged()
{
    emit dataChanged();
    if(_nextStage){
        nextStageCare();
    }
}

bool CPlayOff::swapTeams(const QVariantMap &data)
{
    if(!data.contains("old")|| !data.contains("new")){
        return false;
    }
    QVariantMap firstData = data["old"].toMap();
    QVariantMap secondData = data["new"].toMap();
    bool firstHost = firstData.contains("host");
    bool secondHost = secondData.contains("host");
    CPlayOffMatch* firstMatch = findMatch(firstData);
    CPlayOffMatch* secondMatch = findMatch(secondData);
    if(firstMatch && secondMatch && sameRound(firstMatch, secondMatch)){
        blockSignals(true);
        CTeam * firstTeam = firstMatch->getTeam(firstHost);
        CTeam * secondTeam = secondMatch->getTeam(secondHost);
        if(firstTeam && secondTeam && firstTeam != secondTeam){
            firstMatch->setTeam(secondTeam, firstHost);
            secondMatch->setTeam(firstTeam, secondHost);
        }
        blockSignals(false);
        emit dataChanged();
        return true;
    }
    return false;
}

bool CPlayOff::renameTeam(const QVariant &identificator, const QVariant &data)
{
    CMatch* match = findMatch(identificator.toMap());
    if(match){
        if(identificator.toMap().contains("guest")){
            match->getGuest()->rename(data.toMap()["name"].toString());
        }
        if(identificator.toMap().contains("host")){
            match->getHost()->rename(data.toMap()["name"].toString());
        }
        return true;
    }
    return false;
}

bool CPlayOff::sameRound(CPlayOffMatch *first, CPlayOffMatch *second)
{
    for(const auto& round: _matchesByRounds){
        if(round.contains(first) && round.contains(second)){
            return true;
        }
    }
    return false;
}

void CPlayOff::teamChange()
{
    emit dataChanged();
}

void CPlayOff::clear()
{
    qDeleteAll(_matches);
    _matches.clear();
    _matchesByRounds.clear();
}

void CPlayOff::createMatchesMatrix(const QList<QList<QList<int> > > &intMatches)
{
    _matchesByRounds.clear();
    int matchPlaceIndex = 0;
    for (int round = 0; round < intMatches.size(); round++) {
        _matchesByRounds.push_back({});
        for(const auto& intMatch : intMatches[round]){
            CPlayOffMatch * match;
            if(intMatch.isEmpty()){
                match = nullptr;
            }
            else{
                match = static_cast<CPlayOffMatch *>(_matches[matchPlaceIndex]);
                match->setIndexes(intMatch[0], intMatch[1]);
                match->setIsTopPredecessor((1 +_matchesByRounds.last().size()) % 2);
                matchPlaceIndex++;
            }
            _matchesByRounds.last().push_back(match);
        }
    }
    setNextMatches();
    sortMatches();
}

void CPlayOff::createMatches(int numTeams)
{
    clear();
    _drawer->drawPlayOff(numTeams);
    auto intMatches = _drawer->getMatchesInOrder();
    for(const auto& intMatch : intMatches){
        if(!intMatch.isEmpty()){
            CPlayOffMatch * match = new CPlayOffMatch(getId(), this);
            connect(match, SIGNAL(resultUpdated()),
                    this, SIGNAL(dataChanged()));
            connect(match, SIGNAL(teamsUpdated()),
                    this, SIGNAL(dataChanged()));
            match->setIndexes(intMatch[0],intMatch[1]);
            match->setSourceName(getName());
            _matches.push_back(match);
        }
    }
}

void CPlayOff::placeTeamsToMatches(CTeamsList *teams)
{
    blockSignals(true);
    for (auto match : _matches) {
        auto rowHost = teams->getRow(match->getFalseHostIndex());
        match->setHost(rowHost.first);
        match->setPlaceHolderHost(rowHost.second["place_holder"].toString());

        auto rowGuest = teams->getRow(match->getFalseGuestIndex());
        match->setGuest(rowGuest.first);
        match->setPlaceHolderGuest(rowGuest.second["place_holder"].toString());
    }
    for(int round = 0; round<_matchesByRounds.size()-1;round++){
        for(int match = 0; match<_matchesByRounds[round].size(); match++){
            if(findMatch(round, match)){
                CPlayOffMatch* playOffMatch = _matchesByRounds[round][match];
                auto next = playOffMatch->getMatchForWinner();
                if(next){
                    playOffMatch->setNextMatchTeam();
                    if(playOffMatch->isTopPredecessor()){
                        next->setPlaceHolderHost(createMatchPlaceHolder(round+1));
                    }
                    else{
                        next->setPlaceHolderGuest(createMatchPlaceHolder(round+1));
                    }
                }
            }

        }
    }
    blockSignals(false);
    emit dataChanged();
}

void CPlayOff::setNextMatches()
{
    for(int round = 0; round<_matchesByRounds.size()-1;round++){
        int nextMatch = 0;
        for(int match = 0; match<_matchesByRounds[round].size(); match+=2){
            if(findMatch(round, match)){
                _matchesByRounds[round][match]->setMatchForWinner(_matchesByRounds[round+1][nextMatch]);
            }
            if(findMatch(round, match+1)){
                _matchesByRounds[round][match+1]->setMatchForWinner(_matchesByRounds[round+1][nextMatch]);
            }
            nextMatch++;
        }
    }
}

void CPlayOff::sortMatches()
{
    QList<CMatch*> newOrderMatches;
    QList<QList<int>> orderedIntMatches = _drawer->getMatchesInOrder();
    for(const auto& intMatch : orderedIntMatches){
        for (const auto match : _matches) {
            if(!intMatch.isEmpty() && match->isThisMatch(intMatch[0],intMatch[1])){
                newOrderMatches.push_back(match);
                _matches.removeOne(match);
                break;
            }
        }
    }
    _matches = newOrderMatches;
}

CTeam *CPlayOff::getWinner() const
{
    if(_matchesByRounds.isEmpty() || _matchesByRounds.last().isEmpty() || !_matchesByRounds.last().first()){
        return nullptr;
    }
    return _matchesByRounds.last().first()->getWinner();
}

CPlayOffMatch *CPlayOff::findMatch(const QVariantMap &identificator) const
{
    if(!identificator.contains("row")|| !identificator.contains("col")){
        return nullptr;
    }
    int roundIndex = identificator["row"].toInt();
    int matchIndex = identificator["col"].toInt();
    return findMatch(roundIndex, matchIndex);
}

CTeam *CPlayOff::findTeam(const QVariantMap &identificator) const
{
    CMatch* match = findMatch(identificator);
    if(match){
        return match->getTeam(identificator.contains("host"));
    }
    return nullptr;
}

CPlayOffMatch *CPlayOff::findMatch(int roundIndex, int matchIndex) const
{
    if(roundIndex<_matchesByRounds.size()){
        if(matchIndex<_matchesByRounds[roundIndex].size()){
            return _matchesByRounds[roundIndex][matchIndex];
        }
    }
    return nullptr;
}

QString CPlayOff::createMatchPlaceHolder(int round) const
{
    switch (_matchesByRounds.size() - round) {
    case 1:
        return tr("Final");
    case 2:
        return tr("Semifinal");
    case 3:
        return tr("Quaterfinal");
    default:
        return QString::number(round+1) + tr(". round");
    }
}

QString CPlayOff::createResultPlaceHolder(int place) const
{
    switch (place) {
    case 1:
        return tr("Winner");
    case 2:
        return tr("Beaten Finalist");
    case 3:
        return tr("Beaten Semi-Finalist");
    case 4:
        return tr("Beaten Semi-Finalist");
    default:
        return tr("Beaten in ") + QString::number(_matchesByRounds.size() - qCeil(log2(place)) - 1) + tr(" round");
    }
}

CPlayOffMatch::CPlayOffMatch(int sourceId, QObject *parent)
    : CMatch(sourceId, parent)
{
    _isTopPredecessor = true;
    _matchForWinner = nullptr;
    _matchForLoser = nullptr;
}

void CPlayOffMatch::setResult(const QString& result)
{
    CMatch::setResult(result);
    setNextMatchTeam();
}

bool CPlayOffMatch::isTopPredecessor() const
{
    return _isTopPredecessor;
}

void CPlayOffMatch::setMatchForWinner(CPlayOffMatch *matchForWinner)
{
    _matchForWinner = matchForWinner;
}

CPlayOffMatch *CPlayOffMatch::getMatchForWinner() const
{
    return _matchForWinner;
}

void CPlayOffMatch::setMatchForLosser(CPlayOffMatch *matchForLosser)
{
    _matchForLoser = matchForLosser;
}

void CPlayOffMatch::setIsTopPredecessor(bool isTopPredecessor)
{
    _isTopPredecessor = isTopPredecessor;
}

void CPlayOffMatch::setNextMatchTeam()
{
    if (_matchForWinner){
        if (_isTopPredecessor) {
            _matchForWinner->setHost(getWinner());
        }
        else{
            _matchForWinner->setGuest(getWinner());
        }
    }
}

