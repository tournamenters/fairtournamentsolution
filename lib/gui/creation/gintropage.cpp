#include "gintropage.h"
#include "global_creation.h"
#include <QLabel>
#include <QBoxLayout>
#include <QLineEdit>

GIntroPage::GIntroPage(QWidget *parent)
    : QWizardPage(parent)
{
    setTitle(tr("Introduction"));
    QVBoxLayout *layout = new QVBoxLayout();
    layout->addWidget(createTitle());
    layout->addStretch(1);
    layout->addLayout(createNameLayout());
    layout->addStretch(2);
    setLayout(layout);
}

int GIntroPage::nextId() const
{
    return PageIndex::Page_Teams;
}

QLabel *GIntroPage::createTitle()
{
    QLabel * label = new QLabel(tr("You have decided to create a new tournament. "
                                  "This dialog will guide you through setting "
                                  "its data and parameters."),this);
    label->setWordWrap(true);
    return label;
}

QLayout *GIntroPage::createNameLayout()
{
    QHBoxLayout * hLayout = new QHBoxLayout();
    QLabel * lblName = new QLabel(tr("Tournament name:"),this);
    hLayout->addWidget(lblName);
    hLayout->addWidget(createNameEdit());
    return hLayout;
}

QLineEdit *GIntroPage::createNameEdit()
{
    _tournamentName = new QLineEdit(this);
    _tournamentName->setPlaceholderText(tr("Please, insert a name for your new"
                                          " tournament."));
    _tournamentName->setClearButtonEnabled(true);
    registerField("introduction.name*", _tournamentName);
    return _tournamentName;
}

