#ifndef CSTARTLIST_H
#define CSTARTLIST_H
#include "cteamstable.h"
#include "serialization.h"

class CStartList : public CTeamsList, public ASerializable
{
    Q_OBJECT

public:
    explicit CStartList(QObject *parent = nullptr);
    CStartList(const QStringList& columnNames, QObject *parent = nullptr);
    ~CStartList() override;

    void createTeam(const QVariant& teamData);
    void createTeams(const QVariant& teamsData);

    void setRanked(bool ranked);
    void setHaveClubs(bool clubs);
    void setRankedType(bool descendinly);

    void fromVariant(const QVariant& variant) override;

    QList<CTeam*> getTeamsInValidOrder() const;
    QVariant getData() const override;
    QMap<int, CTeam*> getTeamsById() const;

    QVariant toVariant() const override;

    void clear() override;

public slots:
    bool execEdit(ExecType type, const QVariant& identificator, const QVariant& data) override;

protected:
    bool swapTeams(const QVariant& data);
    bool editStartList(const QVariant& data);

private:
    QList<CTeam*> orderTeamsByRank(const QList<CTeam*>& teams)  const;
    QList<CTeam*> shuffleTeams(const QList<CTeam*>& teams) const;

    bool _isRanked;
    bool _rankedDescendinly;
    bool _hasClubs;
    int _nextTeamID;

};

#endif // CSTARTLIST_H
