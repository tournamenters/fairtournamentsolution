#ifndef GSTATSVIEW_H
#define GSTATSVIEW_H

#include "aview.h"

#include <QVariant>

class QLineEdit;
class QLabel;
class QLayout;
class QProgressBar;

class AModel;

class GStatsView: public AView
{
    Q_OBJECT

public:
    GStatsView(AModel * model, QWidget* parent = nullptr);
    ~GStatsView() override;

    QWidget * getViewWidget() override;
    QString getTournamentName();

public slots:
    void update() override;

signals:
    void tournamentNameChanged(const QString& newName);

private slots:
    void tournamentNameEditChange();

private:
    void showRatioInLabel(QLabel* label, int divisor, int denominator);
    void initializeWidgets();
    QLayout* putWidgetsToLayout();
    QProgressBar * createProgressBar();

    QWidget* viewWidget;
    AModel* _model;

    QLabel* laTeamsTitle;
    QLabel* laNameTitle;
    QLineEdit* leTournamentName;
    QLabel* laMatchesTitle;
    QLabel* laMatchesRatio;
    QLabel* laTeamsCount;
    QProgressBar* pbTournamentProgress;

};

#endif // GSTATSVIEW_H
