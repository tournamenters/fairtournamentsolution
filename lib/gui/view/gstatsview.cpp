#include "gstatsview.h"
#include <QGridLayout>
#include <QProgressBar>
#include <QLabel>
#include <QLineEdit>
#include <QVariant>
#include <QFormLayout>
#include "amodel.h"

GStatsView::GStatsView(AModel * model, QWidget *parent)
    : AView(parent)
{
    _title = tr("Information");
    _model = model;
    _identificator = model->getIdentificator();
    viewWidget = new QWidget();
    initializeWidgets();
    viewWidget->setLayout(putWidgetsToLayout());
    viewWidget->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
    update();
    connect(model, SIGNAL(dataChanged()),
            this, SLOT(update()));
}

GStatsView::~GStatsView()
{

}

void GStatsView::initializeWidgets()
{
    leTournamentName = new QLineEdit(viewWidget);
    connect(leTournamentName, SIGNAL(editingFinished()),
            this, SLOT(tournamentNameEditChange()));
    laTeamsTitle = new QLabel(tr("Number of participants:"), viewWidget);
    laNameTitle = new QLabel(tr("Tournament: "), viewWidget);
    laMatchesTitle = new QLabel(tr("Played Matches / Total Matches:"), viewWidget);
    laMatchesRatio = new QLabel(viewWidget);
    laTeamsCount = new QLabel(viewWidget);
    pbTournamentProgress = createProgressBar();
}

QProgressBar *GStatsView::createProgressBar()
{
    QProgressBar * pbProgress = new QProgressBar(viewWidget);
    pbProgress->setFormat(tr("Tournament progress: %p%"));
    pbProgress->setAlignment(Qt::AlignCenter);
    pbProgress->setTextVisible(true);
    return pbProgress;
}

QLayout *GStatsView::putWidgetsToLayout()
{
    QGridLayout* infoLayout = new QGridLayout(viewWidget);
    QFormLayout*nameLayout = new QFormLayout();
    nameLayout->addRow(laNameTitle, leTournamentName);
    infoLayout->addLayout(nameLayout,0,0,1,2);
    infoLayout->addWidget(laTeamsTitle,1,0,1,1);
    infoLayout->addWidget(laTeamsCount,1,1,1,1, Qt::AlignRight);
    infoLayout->addWidget(laMatchesTitle,2,0,1,1);
    infoLayout->addWidget(laMatchesRatio,2,1,1,1, Qt::AlignRight);
    infoLayout->addWidget(pbTournamentProgress,3,0,1,2);
    infoLayout->setVerticalSpacing(infoLayout->verticalSpacing()*2);
    infoLayout->setAlignment(Qt::AlignTop);
    return infoLayout;
}

void GStatsView::update()
{
    QVariantMap map = _model->getData().toMap();
    leTournamentName->blockSignals(true);
    leTournamentName->setText(map["tournament_name"].toString());
    leTournamentName->blockSignals(false);
    laTeamsCount->setText("<b>" + map["num_teams"].toString() + "</b>");
    showRatioInLabel(laMatchesRatio,map["num_matches_played"].toInt(), map["num_matches"].toInt());
    pbTournamentProgress->setValue(map["tournament_progress"].toInt());
    emit tournamentNameChanged(leTournamentName->text());
}

void GStatsView::tournamentNameEditChange()
{
    emit stateEdited(ExecType::Rewrite, _identificator,leTournamentName->text());
}

QWidget *GStatsView::getViewWidget()
{
    return viewWidget;
}

QString GStatsView::getTournamentName()
{
    return leTournamentName->text();
}

void GStatsView::showRatioInLabel(QLabel *label, int divisor, int denominator)
{
    label->setText("<b>" + QString::number(divisor) + "</b> / <b>" + QString::number(denominator) + "</b>");
}

