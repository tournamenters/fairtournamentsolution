#include "cfieldmatch.h"
#include "cresult.h"

int CFieldMatch::getSourceIndex() const
{
    return _sourceIndex;
}

CFieldMatch::CFieldMatch()
{

}

CFieldMatch::CFieldMatch(QList<int> indexes)
{
    _sourceIndex=indexes[0];
    _groupIndex=indexes[1];
    _roundIndex=indexes[2];
    if(indexes.size()>3){
        _hostIndex=indexes[3];
        _guestIndex=indexes[4];
    }
}

void CFieldMatch::setData(MatchPackage package)
{
    _host = package.host;
    _guest = package.guest;
    _result = package.result;
    _sourceName = package.sourceString;
}

int CFieldMatch::getGroupIndex() const
{
    return _groupIndex;
}

void CFieldMatch::setGroupIndex(int value)
{
    _groupIndex = value;
}

int CFieldMatch::getRoundIndex() const
{
    return _roundIndex;
}

void CFieldMatch::setRoundIndex(int value)
{
    _roundIndex = value;
}

MatchPackage CFieldMatch::toPackage()
{
    MatchPackage package;
    package.guest = _guest;
    package.host = _host;
    package.result = _result;
    package.source = _groupIndex;
    package.sourceString = _sourceName;
    return package;
}

QVariant CFieldMatch::toVariant()
{
    QMap<QString, QVariant> map;
    map["guest"] = QVariant(_guest);
    map["host"] = QVariant(_host);
    map["result"] = QVariant(_result);
    map["source"] = QVariant(_groupIndex);
    map["source_string"] = QVariant(_sourceName);

    return QVariant(map);
}


