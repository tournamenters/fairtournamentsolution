#-------------------------------------------------
#
# Project created by QtCreator 2019-04-30T20:49:10
#
#-------------------------------------------------

TARGET = controller
TEMPLATE = lib

QT += widgets
QT += core

CONFIG += c++11
CONFIG -= app_bundle
CONFIG += staticlib

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    controller.cpp \
    editationcommand.cpp \
    ordercommand.cpp \
    renamecommand.cpp \
    serializationcontroller.cpp \
    swapcommand.cpp \
    resultcommand.cpp

HEADERS += \
    controller.h \
    editationcommand.h \
    ordercommand.h \
    renamecommand.h \
    serializationcontroller.h \
    swapcommand.h \
    resultcommand.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

include($$PWD/../core/core.pri)

DISTFILES += \
    controller.pri
