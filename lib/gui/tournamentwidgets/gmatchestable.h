#ifndef GMATCHESTABLE_H
#define GMATCHESTABLE_H

#include "gteameditstable.h"

class GResultEdit;

class GMatchesTable : public GTeamEditsTable
{
    Q_OBJECT

public:
    GMatchesTable(QWidget *parent = nullptr, int teamEditColumnsCount = 2, int resultColumns = 1);
    GMatchesTable(const QVariant& identificator, QWidget *parent = nullptr, int teamEditColumnsCount = 2, int resultColumns = 1);
    ~GMatchesTable() override;

    void setData(const QVariant& data) override;
    void setResultEditColumnsCount(int resultColumns);

signals:
    void resultChanged(int row, int col, const QString& result, const QString& oldResult);

protected:
    void addResultEdit(int row, int col);
    void removeResultEdit(int row, int col);
    void prepareResultCells(int rows, int columns);
    void setGreyRows(const QVariantList& greyData);
    QList<GResultEdit*> _resultEditCells;
    int _resultEditColumnsCount;

private:    
    void createVerticalHeader();
};

#endif // GMATCHESTABLE_H
