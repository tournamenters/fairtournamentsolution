#include "gfieldsview.h"

#include <QSplitter>
#include <QScrollArea>
#include <QVBoxLayout>
#include <QScrollBar>
#include <QHeaderView>
#include <QLabel>

#include "amodel.h"
#include "gtournamenttable.h"
#include "gmatchestableview.h"

GFieldsView::GFieldsView(const QList<AMatchesModel *>& models, QWidget *parent)
    :AMatchesView (parent)
{
    _title = tr("Fields");
    _models = models;
    _exportable = nullptr;
    int nameInteger = 1;
    for(const auto model : models){
        GMatchesTableView * view = new GMatchesTableView(model, this);
        _fieldViews.push_back(view);
        nameInteger++;
    }
}

GFieldsView::~GFieldsView()
{
    qDeleteAll(_fieldViews);
    _fieldViews.clear();
    _models.clear();
    if(_exportable){
        delete _exportable;
    }
}

void GFieldsView::update()
{
    for(const auto& field : _fieldViews){
        field->update();
    }
}

void GFieldsView::findText(const QString &text)
{
    for(const auto& field : _fieldViews){
        field->findText(text);
    }
}

QWidget *GFieldsView::getViewWidget()
{
    if(!_viewWidget){
        return createViewWidget();
    }
    return _viewWidget;
}

QList<AMatchesView* > GFieldsView::getFieldViews() const
{
    QList<AMatchesView* > list;
    for(const auto field : _fieldViews){
        list.push_back(field);
    }
    return list;
}

AExportable *GFieldsView::getExportableWidget()
{
    if(!_exportable){
        return createExportable();
    }
    return _exportable;
}

QLabel* GFieldsView::createTitleLabel(const QString &title)
{
    QLabel * laTitle = new QLabel(title);
    laTitle->setBackgroundRole(QPalette::Base);
    laTitle->setAutoFillBackground(true);
    laTitle->setAlignment(Qt::AlignCenter);
    return laTitle;
}

QWidget* GFieldsView::createViewWidget()
{
    _viewWidget = new QSplitter();
    for(const auto field : _fieldViews){
        _viewWidget->addWidget(createFieldScrollArea(field));
        _viewWidget->setStretchFactor(_fieldViews.indexOf(field),1);
    }
    _viewWidget->addWidget(new QWidget());
    _viewWidget->setStretchFactor(_fieldViews.size(), 10);
    return _viewWidget;
}

AExportable *GFieldsView::createExportable()
{
    QList<AExportable*> parts;
    for(const auto field : _fieldViews){
        parts.push_back(field->getExportableWidget());
    }
    _exportable = new GComplexExportable(parts);
    _exportable->setTitle(_title);
    return _exportable;
}

QLayout *GFieldsView::createFieldLayout(GMatchesTableView *field)
{
    QVBoxLayout * fieldLayout = new QVBoxLayout;
    fieldLayout->addWidget(createTitleLabel(field->getTitle()));
    fieldLayout->addWidget(field->getViewWidget());
    return fieldLayout;
}

QScrollArea *GFieldsView::createFieldScrollArea(GMatchesTableView *field)
{
    QScrollArea * scField = new QScrollArea();
    scField->setLayout(createFieldLayout(field));
    scField->setWidgetResizable(true);
    GTournamentTable* table = qobject_cast<GTournamentTable*>(field->getViewWidget());
    if(table){
        scField->setMinimumWidth(table->getRealWidth());
    }
    return  scField;
}
